#!/usr/bin/env python3

from pathlib import Path
from setuptools import setup, find_packages
from setuptools.command.develop import develop
from setuptools.command.install import install
try:
    from setuptools.command.editable_wheel import editable_wheel
except ImportError:
    editable_wheel = None
import pkg_resources
import subprocess
import importlib.util
import sys


def parse_requirements():
    with Path('requirements.txt').open() as requirements_txt:
        install_requires = [
            str(requirement) for requirement in
            pkg_resources.parse_requirements(requirements_txt)
        ]
        return install_requires
    return []


def check_instructions_xed():
    """Checks that `instructions_xed.py` exists. Raises an Exception on failure."""
    root_dir = Path(__file__).parent
    instr_xed_path = root_dir / "src/pipedream/asm/x86/instructions_xed.py"
    if not instr_xed_path.is_file():
        raise Exception(
            ("Cannot find `instructions_xed.py` at {path}. Please check the "
             "README for more details.").format(
                 path=instr_xed_path.as_posix()))


def check_instructions_binutils():
    """Checks that `instructions_xed.py` exists. Raises an Exception on failure."""
    root_dir = Path(__file__).parent
    instr_binutils_path = root_dir / "src/pipedream/asm/armv8a/instructions_binutils.py"
    if not instr_binutils_path.is_file():
        raise Exception((
            "Cannot find `instructions_binutils.py` at {path}. Please check the "
            "README for more details.").format(
                path=instr_binutils_path.as_posix()))


def generate_capstone_arm64_map():
    this_dir = Path(__file__).parent
    generate_map = this_dir / "tools" / "capstone" / "generate_capstone_map.py"
    output = (this_dir / "src" / "pipedream" / "disasm" / "capstone" /
              "capstone_map_armv8a.py")
    cmd = f"{generate_map} --arch ARMv8a --format=python --output {output}"
    p = subprocess.run(cmd, text=True, shell=True)
    if p.returncode != 0:
        raise Exception(
            f"unable to generate capstone map (error {p.returncode}): {cmd}")


def pre_build():
    check_instructions_xed()
    check_instructions_binutils()
    generate_capstone_arm64_map()


class develop_pipedream(develop):
    def run(self):
        pre_build()
        super().run()


class install_pipedream(install):
    def run(self):
        pre_build()
        super().run()


if editable_wheel is not None:

    class editable_wheel_pipedream(editable_wheel):
        def run(self):
            pre_build()
            super().run()


packages = find_packages(where="src/")

setup(
    name="pipedream",
    version="0.0.1",
    description="Pipedream",
    author="CORSE",
    url="https://gitlab.inria.fr/fgruber/pipedream",
    packages=packages,
    package_dir={"": "src"},
    package_data={
        "pipedream": ["py.typed"],
    },
    include_package_data=True,
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    setup_requires=parse_requirements(),
    cmdclass={
        "install": install_pipedream,
        "develop": develop_pipedream,
        **(
            {"editable_wheel": editable_wheel_pipedream}
            if editable_wheel is not None else
            {}
        ),
    },
)
