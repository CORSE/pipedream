"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from pipedream.asm import ir
from pipedream.utils import abc

import typing as ty


__all__ = [
    "ASM_Writer",
]


class ASM_Writer(abc.ABC):
    def __init__(self, file: ty.IO[str]):
        self._file = file

    @abc.abstractmethod
    def begin_file(self, name) -> None:
        ...

    @abc.abstractmethod
    def end_file(self, name) -> None:
        ...

    @abc.abstractmethod
    def emit_label(self, label) -> None:
        ...

    @abc.abstractmethod
    def align(self):
        ...

    @abc.abstractmethod
    def newline(self):
        ...

    @abc.abstractmethod
    def insts(self, insts: ty.Sequence["ir.Instruction"]):
        """
        Print the given instructions
        """

    @abc.abstractmethod
    def emit_operand(self, arg: "ir.Operand") -> str:
        """
        Render an operand of an assembly instruction to a string.
        """

    @abc.abstractmethod
    def comment(self, *args):
        """Copy args into output as comment."""

    @abc.abstractmethod
    def begin_function(self, function_name: str):
        """Emit code to start a new function"""

    @abc.abstractmethod
    def end_function(self, function_name: str):
        """Emit code to end a function"""

    @abc.abstractmethod
    def global_byte_array(self, name: str, size: int, alignment: int):
        """Emit directives to declare a global array with given name and size."""

    @abc.abstractmethod
    def global_int_values(
        self, name: str, values: ty.List[int], elt_size: int = 8, alignment: int = 8
    ):
        """Emit directives to declare a global array with given name and values."""

    def print(self, indent, *args):
        if args:
            if indent:
                print(" " * (indent * 2), end="", file=self._file)

            print(*args, file=self._file)
        else:
            print(file=self._file)
