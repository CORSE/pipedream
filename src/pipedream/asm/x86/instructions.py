"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import collections
import copy
import types
import typing as ty

import pipedream.utils as utils
import pipedream.utils.abc as abc

from pipedream.asm import ir

from pipedream.asm.x86.operands import *
from pipedream.asm.x86.registers import *
from pipedream.asm.x86.flags import *
from pipedream.asm.x86 import instructions_xed  # type: ignore

__all__ = [
    "X86_Instruction",
    "X86_Instruction_Set",
    "ATT_MNEMONICS",
    "INTEL_MNEMONICS",
    "INSTRUCTIONS",
    "Harness",
]

Instruction_Name = str
Operand_Name = str

ALL_TAGS: ty.FrozenSet[str] = frozenset()
INSTRUCTIONS: ty.Dict[Instruction_Name, ir.Machine_Instruction] = {}
ATT_MNEMONICS: ty.Dict[Instruction_Name, str] = {}
INTEL_MNEMONICS: ty.Dict[Instruction_Name, str] = {}

RC_RSP: ir.Register_Class
RC_RIP: ir.Register_Class
RAX: X86_Register
RSP: X86_Register
RIP: X86_Register


class X86_Instruction(ir.Machine_Instruction):
    def __init__(
        self,
        name: str,
        att_mnemonic: str,
        intel_mnemonic: str,
        isa_set: instructions_xed.ISA,
        operands: ty.Sequence[ir.Operand],
        tags: ty.Sequence[str],
        can_benchmark: bool,
    ):
        self._name = name
        self._att_mnemonic = att_mnemonic
        self._intel_mnemonic = intel_mnemonic
        self._isa_set = isa_set
        self._operands = tuple(operands)
        self._tags = tuple(tags)
        self._can_benchmark = can_benchmark

        assert len(operands) == len(
            set(o.name for o in operands)
        ), "duplicate operand name in " + str(self)

    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return self._name

    @property  # type: ignore
    def att_mnemonic(self) -> str:
        return self._att_mnemonic

    @property  # type: ignore
    def intel_mnemonic(self) -> str:
        return self._intel_mnemonic

    @property  # type: ignore
    @abc.override
    def isa_set(self) -> instructions_xed.ISA:
        return self._isa_set

    @property  # type: ignore
    @abc.override
    def tags(self) -> ty.Sequence[str]:
        return self._tags

    @property  # type: ignore
    @abc.override
    def operands(self) -> ty.Sequence[ir.Operand]:
        return self._operands

    @abc.override
    def update_operand(
        self, idx_or_name: ty.Union[int, str], fn
    ) -> ir.Machine_Instruction:
        if isinstance(idx_or_name, str):
            idx = self.get_operand_idx(idx_or_name)
        else:
            idx = idx_or_name

        ops = list(self.operands)
        ops[idx] = fn(ops[idx])
        new = copy.copy(self)
        new._operands = tuple(ops)
        return new

    @abc.override
    def encodings(self) -> ty.List[ir.Instruction_Encoding]:
        raise NotImplementedError()

    @property  # type: ignore
    @abc.override
    def can_benchmark(self) -> bool:
        return self._can_benchmark


class X86_Instruction_Set(ir.Instruction_Set):
    @abc.override
    def instruction_groups(self) -> ty.List[ir.Instruction_Group]:
        return []

    @abc.override
    def instructions(self) -> ty.List[ir.Machine_Instruction]:
        return list(INSTRUCTIONS.values())

    @abc.override
    def benchmark_instructions(self) -> ty.List[ir.Machine_Instruction]:
        INS = ty.cast(ty.Dict[Instruction_Name, X86_Instruction], INSTRUCTIONS)
        return [I for I in INS.values() if I._can_benchmark]

    @property  # type: ignore
    @abc.override
    def all_tags(self):
        tags = collections.OrderedDict()

        for inst in INSTRUCTIONS.values():
            for tag in inst.tags:
                tags[tag] = True

        yield from tags

    @abc.override
    def instruction_for_name(self, name: str) -> ir.Machine_Instruction:
        return INSTRUCTIONS[name]

    @abc.override
    def instructions_for_tags(self, *tags) -> ty.Iterable[ir.Machine_Instruction]:
        set_tags = set(tags)

        for inst in INSTRUCTIONS.values():
            if set_tags <= set(inst.tags):
                yield inst

    def __getitem__(self, name):
        return INSTRUCTIONS[name]

    @abc.override
    def __iter__(self):
        return iter(INSTRUCTIONS.values())


### FIXME: many thousands of missing instructions

i64 = "i64"
i32 = "i32"
i8 = "i8"
USE = ir.Use_Def.USE
DEF = ir.Use_Def.DEF
USE_DEF = ir.Use_Def.USE_DEF
R, W, RW = USE, DEF, USE_DEF

EXPLICIT = ir.Operand_Visibility.EXPLICIT
IMPLICIT = ir.Operand_Visibility.IMPLICIT
SUPPRESSED = ir.Operand_Visibility.SUPPRESSED

TAGS_CANNOT_BENCHMARK = frozenset(
    [
        ## TODO: add support (really big stack space for benchmark??)
        "stack",
        ## :/ branches
        "branch",
        ## write kernel benchmark harness :P
        "ring0",
        ## add support for old school i386 :P
        "segmentation",
        ## get an AMD machine? (check CPU flags?)
        "amdonly",
        ## get a machine that supports it?
        "waitpkg",
        ## add protected-mode benchmark harness
        "protected-mode",
    ]
)
INTEL_MNEMONIC_CANNOT_BENCHMARK = frozenset(
    [
        ## always raises SIGILL (that's the whole point)
        "ud0",
        "ud1",
        "ud2",
        ## interrupt handling
        "int",
        "int1",
        "int3",
        "cli",
        ## priviledge: control register access
        "xgetbv",
        ## fix harness: stack access
        "leave",
        ## fix harness: weird fixed memory operand
        "insb",
        "insd",
        "insw",
        "outsb",
        "outsd",
        "outsw",
        "lods",
        "lodsb",
        "lodsd",
        "lodsw",
        "lodsq",
        "stos",
        "stosb",
        "stosd",
        "stosw",
        "stosq",
        "xlat",
        "xlatb"
        ## fix harness: string copy/move instructions
        "movs",
        "movsb",
        "movsd",
        "movsw",
        "movsq",
        "scas",
        "scasb",
        "scasd",
        "scasw",
        "scasq",
        ## direct I/O port access
        "in",
        "out",
        ## MXCSR access (updated harness)
        "ldmxcsr",
        "stmxcsr",
        "vldmxcsr",
        "vstmxcsr",
        ## read/write protection keys (privileged)
        "rdpkru",
        "wrpkru",
        ## read PMU registers (privileged)
        "rdpmc",
        ## VM instructions (privileged)
        "vmfunc",
        ## prefetch nop (get newer CPU for testing)
        "prefetch_exclusive",
        ## fix harness: direct write to flags register: causes segfault
        "std",
        "sti",
    ]
)
INST_NAME_CANNOT_BENCHMARK = frozenset(
    [
        # legacy string instructions with weird address operand
        # we can benchmark the vector versions same mnemonic and normal memory operand
        "CMPSB",
        "CMPSD",
        "CMPSW",
        "CMPSQ",
        # fix harness: divide by zero?
        "DIV_GPR8NOREXi8",
        "DIV_MEM64i16",
        "DIV_MEM64i32",
        "DIV_MEM64i64",
        "DIV_MEM64u8",
        "IDIV_GPR8NOREXi8",
        "IDIV_MEM64i16",
        "IDIV_MEM64i32",
        "IDIV_MEM64i64",
        "IDIV_MEM64u8",
        # fix harness: FP error?
        "FLDCW_MEM64OTHER",
        "FLDCW_MEM32OTHER",
        # This results in undefined behaviour (https://www.felixcloutier.com/x86/bswap)
        "BSWAP_GPR16i16",
    ]
)


def mk_inst(
    *,
    name: str,
    att_mnemonic: str,
    intel_mnemonic: str,
    operands: ty.List[ty.Callable],
    tags: ty.List[str],
    isa_set: instructions_xed.ISA,
    isa_extension: str = None,
    can_benchmark=None,
):
    # print('mk_inst', name, att_mnemonic, intel_mnemonic,
    #       list(op() for op in operands),
    #       tags, isa_set, isa_extension,
    #       can_benchmark)

    global INSTRUCTIONS, ALL_TAGS

    tags_set = frozenset(tags) | frozenset([name, att_mnemonic, intel_mnemonic])

    operands_list = tuple(mk_op() for mk_op in utils.flatten(operands))

    ## filter out instructions we currently do not support
    if can_benchmark is None:
        can_benchmark = pipedream_asm_backend_can_handle(
            name, isa_set, intel_mnemonic, att_mnemonic, tags_set, operands_list
        )

    inst = X86_Instruction(
        name,
        att_mnemonic,
        intel_mnemonic,
        isa_set,
        operands_list,
        list(tags_set),
        can_benchmark,
    )
    ATT_MNEMONICS[name] = att_mnemonic
    INTEL_MNEMONICS[name] = intel_mnemonic

    if name in INSTRUCTIONS:
        raise ValueError(
            "\n".join(
                [
                    f"Duplicate instruction:",
                    f" ({INSTRUCTIONS[name]}",
                    f"  vs",
                    f"  {inst})",
                ]
            )
        )

    INSTRUCTIONS[name] = inst
    ALL_TAGS = ALL_TAGS | tags_set

    return inst


def pipedream_asm_backend_can_handle(
    name: str,
    isa_set,
    intel_mnemonic: str,
    att_mnemonic: str,
    tags: ty.Union[ty.Set[str], ty.FrozenSet[str]],
    operands: ty.Sequence[ir.Operand],
):
    if tags & TAGS_CANNOT_BENCHMARK:
        return False

    if intel_mnemonic in INTEL_MNEMONIC_CANNOT_BENCHMARK:
        return False

    ##### need to update benchmark harness (currently crash)

    if name in INST_NAME_CANNOT_BENCHMARK:
        return False

    assert name != "DIV_MEM64i16", [name, INST_NAME_CANNOT_BENCHMARK]

    ##### modern extensions not available on my machine

    ISA = instructions_xed.ISA

    ## VIA CPU instructions
    if isa_set in (ISA.VIA_PADLOCK_RNG,):
        return False

    ## Total Memory Encryption (TME)
    if isa_set in (ISA.PCONFIG,):
        return False

    ## AVX Galois Field instructions
    if isa_set in (
        ISA.GFNI,
        ISA.AVX_GFNI,
    ):
        return False

    ## Software Guard Extensions (SGX)
    if isa_set in (ISA.SGX, ISA.SGX_ENCLV):
        return False

    ## Cache Write Back
    if isa_set is ISA.CLWB:
        return False

    ## Supervisor Mode Access Prevention
    if isa_set is ISA.SMAP:
        return False

    ## Control-Flow Enforcement Technology (CET)
    if isa_set is ISA.CET:
        return False

    ## Intel Processor Trace (PT)
    if isa_set is ISA.PT:
        return False

    ## Read Processor ID
    if isa_set is ISA.RDPID:
        return False

    ## Intel SHA extensions
    if isa_set is ISA.SHA:
        return False

    ## AVX & AVX-512 AES instructions
    if isa_set in (
        ISA.VAES,
        ISA.AVXAES,
        ISA.AVX512_VAES_128,
        ISA.AVX512_VAES_256,
        ISA.AVX512_VAES_512,
    ):
        return False

    ## AVX 512 in general
    if isa_set.name.startswith("AVX512"):
        return False

    ## Intel Virtualization Extensions (VT-x)
    if isa_set is ISA.VTX:
        return False

    ## Direct store (MOVDIRI extension)
    if isa_set is ISA.MOVDIR:
        return False

    ## TSX Load-tracking (Spectre/Meltdown mitigation instructions)
    if isa_set in (ISA.TSX_LDTRK, ISA.SERIALIZE):
        return False

    ## ???
    if isa_set is ISA.VPCLMULQDQ:
        return False

    ##### some funky instruction operand-size problems

    def match(name, noperands):
        if att_mnemonic != name:
            return False
        if len(operands) != noperands:
            return False
        return True

    def is_reg(idx: int, reg_class):
        op = operands[idx]
        if not isinstance(operands[idx], ir.Register_Operand):
            return False
        op_reg = ty.cast(ir.Register_Operand, op)
        if op_reg.register_class is not reg_class:
            return False
        return True

    def is_imm(idx: int):
        op = operands[idx]
        if not isinstance(operands[idx], ir.Immediate_Operand):
            return False
        return True

    def is_mem(idx: int):
        op = operands[idx]
        if not isinstance(operands[idx], ir.Memory_Operand):
            return False
        return True

    for op in operands:
        if isinstance(op, ir.Register_Operand):
            if op.register_class is VRX512:
                return False

        if isinstance(op, ir.Memory_Operand):
            if op.address_width != 64:
                return False

    ## produces: "MOVSXD_GPR32i32_GPR32i32"
    ## which is not a valid instruction according to GAS.
    if match("movsll", 2) and is_reg(0, GPR32) and is_reg(1, GPR32):
        return False
    if match("movsll", 2) and is_reg(0, GPR32) and is_mem(1):
        return False

    ## produces: "MOVSXD_GPR16i16_GPR32i32"
    ## which is not a valid instruction according to GAS.
    if match("movslw", 2) and is_reg(0, GPR16) and is_reg(1, GPR32):
        return False
    if match("movslw", 2) and is_reg(0, GPR16) and is_mem(1):
        return False
    if match("movslw", 2) and is_reg(0, GPR16) and is_reg(1, GPR16):
        return False

    ## produces: "MOVSX_GPR16i16_GPR16i16"
    ## which is not a valid instruction according to GAS.
    if match("movsww", 2) and is_reg(0, GPR16) and is_reg(1, GPR16):
        return False
    if match("movsww", 2) and is_reg(0, GPR16) and is_mem(1):
        return False

    ## produces: "MOVZX_GPR16i16_GPR16i16" (with a 66 prefix)
    ## which is a valid instruction (and objdump can disassemble it), but gas says it is invalid.
    ##
    ## >> echo "movzww %ax, %ax # 66 0f b7 c0" | as
    ## Error: invalid instruction suffix for `movzw'
    if match("movzww", 2) and is_reg(0, GPR16) and is_reg(1, GPR16):
        return False
    if match("movzww", 2) and is_reg(0, GPR16) and is_mem(1):
        return False

    ## FIXME: there are some XMM variants GAS does not accept
    if match("vcvtpd2dq", 2) or match("vcvtpd2ps", 2) or match("vcvttpd2dq", 2):
        return False

    if match("maskmovq", 3) or match("maskmovdqu", 3) or match("vmaskmovdqu", 3):
        if all(o.visibility is ir.Operand_Visibility.EXPLICIT for o in operands):
            return True
        else:
            ## FIXME: fixed memory base register DS:DI/EDI/RDI (currently can't handle it)
            return False

    ## FIXME: register/imm operand is actually second displacement/index operand
    ##        need to tell this to the register allocator
    if (match("btw", 3) or match("btl", 3) or match("btq", 3)) and is_mem(0):
        return False
    if (match("btcw", 3) or match("btcl", 3) or match("btcq", 3)) and is_mem(0):
        return False
    if (match("btsw", 3) or match("btsl", 3) or match("btsq", 3)) and is_mem(0):
        return False
    if (match("btrw", 3) or match("btrl", 3) or match("btrq", 3)) and is_mem(0):
        return False

    return True


def make_reg_op(
    *,
    name: str,
    reg_class: ir.Register_Class,
    reg: X86_Register = None,
    action: ir.Use_Def,
    type,
    elems: int,
    visibility: ir.Operand_Visibility,
):
    return lambda: X86_Register_Operand(name, visibility, action, reg_class, reg)


def make_imm_op(
    *,
    name: str,
    imm_bits: int,
    type,
    elems: int,
    value: int = None,
    visibility: ir.Operand_Visibility,
):
    if type[0] == "i":
        clss = {
            8: Imm8,
            16: Imm16,
            32: Imm32,
            64: Imm64,
        }[imm_bits]
    elif type[0] == "u":
        clss = {
            8: ImmU8,
            16: ImmU16,
            32: ImmU32,
            64: ImmU64,
        }[imm_bits]
    else:
        raise ValueError("invalid type for immediate operand: " + repr(type))

    return lambda: clss(name, visibility, value)


def make_brdisp_op(
    *, name: str, disp_bits: int, type, elems: int, visibility: ir.Operand_Visibility
):
    ## TODO: add a proper branch displacement op type

    return make_imm_op(
        name=name,
        imm_bits=disp_bits,
        type=type,
        elems=elems,
        visibility=visibility,
        value=0,
    )


def make_flags_op(
    *,
    name: str,
    reg: X86_Register,
    read: X86_Flags,
    write: X86_Flags,
    visibility: ir.Operand_Visibility,
):
    if not read:
        read = X86_Flags(0)
    if not write:
        write = X86_Flags(0)

    return lambda: X86_Flags_Operand(name, visibility, reg, read, write)


def make_addr_op(
    name: str,
    addr_bits: int,
    type,
    elems: int,
    visibility: ir.Operand_Visibility,
    base: ty.Optional[X86_Register] = None,
):
    # TODO: more addressing modes

    if addr_bits == 64:
        base_reg_class = BASE_REGISTER_64
        index_reg_class = INDEX_REGISTER_64
        disp_imm_class = Imm32
        scale_imm_class = Scale_Imm
    elif addr_bits == 32:
        base_reg_class = BASE_REGISTER_32
        index_reg_class = INDEX_REGISTER_32
        disp_imm_class = Imm32
        scale_imm_class = Scale_Imm
    elif addr_bits == 16:
        base_reg_class = BASE_REGISTER_16
        index_reg_class = INDEX_REGISTER_16
        disp_imm_class = Imm32
        scale_imm_class = lambda name: Scale_Imm(1)  # type: ignore
    else:
        raise ValueError("invalid width for LEA src operand", addr_bits)

    EXPLICIT = ir.Operand_Visibility.EXPLICIT
    USE = ir.Use_Def.USE

    assert base is None or (isinstance(base, X86_Register) and base in base_reg_class)

    base_op = X86_Register_Operand("base", EXPLICIT, USE, base_reg_class, base)
    displacement_op = disp_imm_class("displacement", EXPLICIT)

    # base         = X86_Register_Operand('base', EXPLICIT, USE, base_reg_class, None) if 'B' in mode else None
    # index        = X86_Register_Operand('index', EXPLICIT, USE, index_reg_class, None) if 'I' in mode else None
    # scale        = scale_imm_class('scale', EXPLICIT,) if 'S' in mode else None
    # displacement = disp_imm_class('displacement', EXPLICIT,) if 'D' in mode else None

    return lambda: X86_Base_Displacement_Address_Operand(
        name, visibility, addr_bits, base_op, displacement_op
    )


def make_mem_op(
    name: str,
    addr_bits: int,
    mem_bits: int,
    action: ir.Use_Def,
    type,
    elems: int,
    visibility: ir.Operand_Visibility,
    base: ty.Optional[X86_Register] = None,
):
    # TODO: more addressing modes

    if addr_bits == 64:
        base_reg_class = BASE_REGISTER_64
        index_reg_class = INDEX_REGISTER_64
        disp_imm_class = Imm32
        scale_imm_class = Scale_Imm
    elif addr_bits == 32:
        base_reg_class = BASE_REGISTER_32
        index_reg_class = INDEX_REGISTER_32
        disp_imm_class = Imm32
        scale_imm_class = Scale_Imm
    elif addr_bits == 16:
        base_reg_class = BASE_REGISTER_16
        index_reg_class = INDEX_REGISTER_16
        disp_imm_class = Imm32
        scale_imm_class = lambda name: Scale_Imm(1)  # type: ignore
    else:
        raise ValueError("invalid width for LEA src operand", addr_bits)

    if base is not None:
        base = base.as_width(addr_bits)

    EXPLICIT = ir.Operand_Visibility.EXPLICIT
    USE = ir.Use_Def.USE

    assert base is None or (
        isinstance(base, X86_Register) and base in base_reg_class
    ), [
        base,
        *base_reg_class,  # type:ignore
    ]

    displacement = None

    if base is not None and base.widest is RSP:
        displacement = 0

    if base and base.widest is RAX:
        # FIXME: extract-xed-database puts AX/EAX/RAX as default base register everywhere
        base = None

    base_op = X86_Register_Operand("base", EXPLICIT, USE, base_reg_class, base)
    displacement_op = disp_imm_class("displacement", EXPLICIT, displacement)

    # base         = X86_Register_Operand('base', EXPLICIT, USE, base_reg_class, None) if 'B' in mode else None
    # index        = X86_Register_Operand('index', EXPLICIT, USE, index_reg_class, None) if 'I' in mode else None
    # scale        = scale_imm_class('scale', EXPLICIT,) if 'S' in mode else None
    # displacement = disp_imm_class('displacement', EXPLICIT,) if 'D' in mode else None

    return lambda: X86_Base_Displacement_Memory_Operand(
        name, visibility, action, addr_bits, mem_bits, base_op, displacement_op
    )


instructions_xed.make_instruction_database(
    make_instruction=mk_inst,
    reg_op=make_reg_op,
    mem_op=make_mem_op,
    addr_op=make_addr_op,
    imm_op=make_imm_op,
    brdisp_op=make_brdisp_op,
    flags_op=make_flags_op,
)


class Harness:
    """
    Well known instructions used in benchmark harness
    """

    ADD_GPR64 = INSTRUCTIONS["ADD_GPR64i64_GPR64i64"]
    IMUL_IMM_GPR64 = INSTRUCTIONS["IMUL_GPR64i64_GPR64i64_IMMi32"]
    MOV_GPR64 = INSTRUCTIONS["MOV_GPR64i64_GPR64i64"]
    MOV_IMM32_GPR32 = INSTRUCTIONS["MOV_GPR32i32_IMMi32"]
    MOV_IMM64_GPR64 = INSTRUCTIONS["MOV_GPR64i64_IMMi64"]
    MOV_IMM64_GPR64 = INSTRUCTIONS["MOV_GPR64i64_IMMi64"]
    SUB_IMM8_GPR64 = INSTRUCTIONS["SUB_GPR64i64_IMMi8"]
    ADD_IMM8_GPR64 = INSTRUCTIONS["ADD_GPR64i64_IMMi8"]
    TEST_GPR64 = INSTRUCTIONS["TEST_GPR64i64_GPR64i64"]
    TEST_GPR32 = INSTRUCTIONS["TEST_GPR32i32_GPR32i32"]
    XOR_GPR32 = INSTRUCTIONS["XOR_GPR32i32_GPR32i32"]

    LOAD_MEM64_TO_REG64 = INSTRUCTIONS["MOV_GPR64i64_MEM64i64"]
    STORE_REG64_TO_MEM64 = INSTRUCTIONS["MOV_MEM64i64_GPR64i64"]

    POP_GPR64 = INSTRUCTIONS["POP_GPR64i64"]
    PUSH_GPR64 = INSTRUCTIONS["PUSH_GPR64i64"]

    XOR_GPR8i8_GPR8i8 = INSTRUCTIONS["XOR_GPR8i8_GPR8i8"]
    XOR_GPR16i16_GPR16i16 = INSTRUCTIONS["XOR_GPR16i16_GPR16i16"]
    XOR_GPR16i16_GPR16i16 = INSTRUCTIONS["XOR_GPR16i16_GPR16i16"]
    XOR_GPR32i32_GPR32i32 = INSTRUCTIONS["XOR_GPR32i32_GPR32i32"]
    XOR_GPR64i64_GPR64i64 = INSTRUCTIONS["XOR_GPR64i64_GPR64i64"]

    VMOVDQA_VR256_MEM64 = INSTRUCTIONS["VMOVDQA_VR256i32x8_MEM64i32x8"]

    LEAVE = INSTRUCTIONS["LEAVE_MEM64i64"]
    CPUID = INSTRUCTIONS["CPUID"]
    VZEROALL = INSTRUCTIONS[
        "VZEROALL"
    ]  # TODO: supressed write to all XMM/YMM/ZMM vector registers
    JNE_32 = INSTRUCTIONS["JNZ_BRDISP32"]
    JEQ_32 = INSTRUCTIONS["JZ_BRDISP32"]

    RET = mk_inst(
        name="RET",
        att_mnemonic="ret",
        intel_mnemonic="ret",
        isa_set=instructions_xed.ISA.I86,
        operands=[
            ## TODO: add memory operand support
            ## TODO: add stackrel memory operand support
            # make_mem_op(name='stack', addr_bits=64, mem_bits=64, type=i64, elems=1, action=R, visibility=SUPPRESSED),
            make_reg_op(
                name="sp",
                reg=RSP,
                reg_class=RC_RSP,
                type=i64,
                elems=1,
                action=RW,
                visibility=SUPPRESSED,
            ),
            make_reg_op(
                name="ip",
                reg=RIP,
                reg_class=RC_RIP,
                type=i64,
                elems=1,
                action=W,
                visibility=SUPPRESSED,
            ),
        ],
        tags=["stack", "scalar"],
        can_benchmark=False,
    )
    CALL = mk_inst(
        name="CALL",
        att_mnemonic="call",
        intel_mnemonic="call",
        isa_set=instructions_xed.ISA.I86,
        operands=[
            make_imm_op(
                name="dst", imm_bits=32, type=i32, elems=1, visibility=EXPLICIT
            ),
            make_reg_op(
                name="ip",
                reg=RIP,
                reg_class=RC_RIP,
                action=RW,
                type=i64,
                elems=1,
                visibility=SUPPRESSED,
            ),
            make_mem_op(
                name="stack",
                addr_bits=64,
                mem_bits=64,
                base=RSP,
                action=W,
                type=i64,
                elems=1,
                visibility=SUPPRESSED,
            ),
        ],
        tags=["branch", "conditional-branch", "relative-branch"],
        can_benchmark=False,
    )
    # zero byte jump
    JMP_E9_0 = mk_inst(
        name="JMP_0",
        # att_mnemonic   = '.byte 0xeb, 0',
        att_mnemonic=".byte 0xe9, 0, 0, 0, 0",
        intel_mnemonic=".byte 0xe9, 0, 0, 0, 0",
        isa_set=instructions_xed.ISA.I86,
        operands=[
            # TODO: relbr operand type
            # make_imm_op(name='dst', imm_bits=8, type=i8, elems=1, value=ir.Label('0'), visibility=EXPLICIT),
            make_reg_op(
                name="ip",
                reg=RIP,
                reg_class=RC_RIP,
                action=RW,
                type=i64,
                elems=1,
                visibility=SUPPRESSED,
            ),
        ],
        tags=["branch", "conditional-branch", "relative-branch"],
        can_benchmark=True,
    )

    ## leave function (essentially `RSP = RBP`)
    # LEAVE = mk_inst(
    #   name           = 'LEAVE',
    #   att_mnemonic   = 'leave',
    #   intel_mnemonic = 'leave',
    #   isa_set        = instructions_xed.ISA.I86,
    #   operands       = [
    #     make_reg_op(name='src', reg=RBP, reg_class=RC_RBP, action=R, type=i64, elems=1, visibility=SUPPRESSED),
    #     make_reg_op(name='dst', reg=RSP, reg_class=RC_RSP, action=W, type=i64, elems=1, visibility=SUPPRESSED),
    #   ],
    #   tags           = ['branch', 'conditional-branch', 'relative-branch'],
    #   can_benchmark  = False,
    # )

    # special nop instruction used for IACA start/stop markers
    IACA_START_STOP_NOP = mk_inst(
        name="IACA_START_STOP_NOP",
        att_mnemonic="fs addr32 nop",
        intel_mnemonic="fs addr32 nop",
        isa_set=instructions_xed.ISA.I86,
        operands=[],
        tags=["nop"],
        can_benchmark=False,
    )


tmp = collections.OrderedDict()
for inst in sorted(INSTRUCTIONS.values(), key=lambda i: i.name):  # type: ignore
    tmp[inst.name] = inst
INSTRUCTIONS = types.MappingProxyType(tmp)  # type: ignore
