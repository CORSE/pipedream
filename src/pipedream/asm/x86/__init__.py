"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


"""
  I tried to keep this file clean, I swear.
  x86 is just an extremely complex instruction set.
"""

import typing as ty
import random
import functools

import pipedream.utils.abc as abc

from pipedream.utils import *
from pipedream.utils import pyperfs
from pipedream.asm.ir import *
from pipedream.asm.allocator import *
from pipedream.asm.asmwriter import *

from pipedream.benchmark.types import Loop_Overhead

from . import registers
from . import operands
from . import instructions

from .asmwriter import *
from .operands import *
from .registers import *
from .instructions import *

__all__ = [
    *registers.__all__,
    *operands.__all__,
    *instructions.__all__,
]

RAX: X86_Register
EAX: X86_Register
EBX: X86_Register
RCX: X86_Register
ECX: X86_Register
RDX: X86_Register
EDX: X86_Register
RSP: X86_Register
ESP: X86_Register
SP: X86_Register
RIP: X86_Register
R12: X86_Register
R13: X86_Register
R14: X86_Register
R15: X86_Register
RBP: X86_Register
RBX: X86_Register
EBP: X86_Register
RDI: X86_Register
EDI: X86_Register
RSI: X86_Register
ESI: X86_Register
SPL: X86_Register
ARG_1: X86_Register
ARG_2: X86_Register
ARG_3: X86_Register
ARG_4: X86_Register
ARG_5: X86_Register
ARG_6: X86_Register
ARG_7: X86_Register


X86_Tools_Config = Tools_Config(
    as_cmd="as", ld_cmd="ld", ldflags_perf=["-l" + pyperfs.get_perf_lib().lib_name]
)
X86_L1_Cache_Config = Cache_Config(size=32768, cacheline_size=64, associativity=8)


class X86_Architecture(Architecture):
    def __init__(self):
        self._instruction_set = X86_Instruction_Set()
        self._register_set = X86_Register_Set()
        self._asm_dialects = [X86_ATT_Asm_Dialect(), X86_INTEL_Asm_Dialect()]

    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return "x86"

    @property  # type: ignore
    @abc.override
    def nb_vector_reg(self) -> int:
        return len(VR256)

    @property  # type: ignore
    @abc.override
    def max_vector_size(self) -> int:
        return 256 // 8

    @abc.override
    def instruction_set(self) -> Instruction_Set:
        return self._instruction_set

    @abc.override
    def register_set(self) -> Register_Set:
        return self._register_set

    @abc.override
    def asm_dialects(self) -> ty.List[Asm_Dialect]:
        return self._asm_dialects

    @abc.override
    def make_asm_writer(self, dialect: Asm_Dialect, file: ty.IO[str]) -> ASM_Writer:
        return X86_ASM_Writer(dialect, file)

    @abc.override
    def make_ir_builder(self) -> "X86_IR_Builder":
        return X86_IR_Builder(self)

    @abc.override
    def make_register_allocator(self) -> Register_Liveness_Tracker:
        regs = self.register_set()

        return Register_Liveness_Tracker(
            all_registers=regs.all_registers(),
            callee_save_registers=regs.callee_save_registers(),
            read_only_registers=iter(
                self.register_set().stack_pointer_register_class()
            ),
        )

    @property  # type: ignore
    @abc.override
    def tools_config(self) -> Tools_Config:
        return X86_Tools_Config

    @property  # type: ignore
    @abc.override
    def l1_cache_config(self) -> Cache_Config:
        return X86_L1_Cache_Config

    @abc.override
    def loop_overhead(
        self, num_iterations: int, num_prologue_inst: int
    ) -> Loop_Overhead:
        # there are two overhead instructions per iterations in a our X86 counting
        # loops:
        # ```asm
        #     xorq loop_counter, loop_counter
        #   .align 16
        #   loop:
        #     ... kernel ...
        #     subq loop_counter, $1
        #     jne loop.
        # ```
        # TODO: the detection of such instructions should be automatised

        ## instructions
        insts_per_iteration = 2 * num_iterations
        insts_total = num_prologue_inst + insts_per_iteration

        ## muops
        muops_init = num_prologue_inst  # TODO: this is an approximation only!
        # the subq+jne at the end of the loop are macro-fused
        muops_per_iteration = num_iterations
        muops_total = muops_init + muops_per_iteration

        return Loop_Overhead(insts_total, muops_total)


class X86_Asm_Dialect(Asm_Dialect):
    pass


class X86_ATT_Asm_Dialect(X86_Asm_Dialect):
    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return "att"

    @abc.override
    def operand_to_str(self, op: "Operand") -> str:
        return render_att_arg(op)

    @abc.override
    def instruction_mnemonic(self, inst: "Instruction") -> str:
        return ATT_MNEMONICS[inst.name]  # type:ignore

    @abc.override
    def instruction_operands(self, inst: "Instruction") -> ty.List[str]:
        ops = [
            self.operand_to_str(op)
            for op in inst.operands
            if op.visibility is not Operand_Visibility.SUPPRESSED
        ][::-1]
        return ops

    @abc.override
    def begin_file(self, file_name: str) -> ty.Sequence[str]:
        return [".att_syntax"]


class X86_INTEL_Asm_Dialect(X86_Asm_Dialect):
    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return "intel"

    @abc.override
    def instruction_mnemonic(self, inst: "Instruction") -> str:
        return INTEL_MNEMONICS[inst.name]  # type:ignore

    @abc.override
    def operand_to_str(self, op: "Operand") -> str:
        return render_intel_arg(op)

    @abc.override
    def instruction_operands(self, inst: "Instruction") -> ty.List[str]:
        ops = [
            self.operand_to_str(op)
            for op in inst.operands
            if op.visibility is not Operand_Visibility.SUPPRESSED
        ]
        return ops

    @abc.override
    def begin_file(self, file_name: str) -> ty.Sequence[str]:
        return [".intel_syntax noprefix"]


class X86_IR_Builder(IR_Builder):
    def __init__(self, arch):
        self._arch = arch
        self._insts = arch.instruction_set()

    @abc.override
    def get_return_register(self) -> Register:
        return RAX

    @abc.override
    def get_argument_register(self, idx: int) -> Register:
        return {0: ARG_1, 1: ARG_2, 2: ARG_3, 3: ARG_4, 4: ARG_5, 5: ARG_6}[idx]

    @abc.override
    def get_scratch_register(self, idx: int) -> Register:
        return {0: R12, 1: R13, 2: R14, 3: R15, 4: RBP, 5: RBX}[idx]

    @abc.override
    def select_memory_base_register(
        self,
        insts: ty.List[Instruction],
        free_regs: ty.Set[Register],
        address_width: int,
    ) -> Register:
        """
        Select a register that can be used as a memory base register for all memory accesses
        in the given list of instructions :insts:.
        """

        candidates: ty.Set[Register]
        candidates = set([i.as_width(address_width) for i in iter(GPR64)])
        candidates = set(candidates) & free_regs

        # # used in sequentialize CPU
        # candidates -= set([AL, AH, AX, EAX, RAX,
        #                    BL, BH, BX, EBX, RBX,
        #                    CL, CH, CX, ECX, RCX,
        #                    DL, DH, DX, EDX, RDX,
        #                    ])
        # do not clobber stack
        candidates -= set([SPL, SP, ESP, RSP, ECX, RAX, RCX, RDX])

        for i in insts:
            for op in i.operands:
                if not op.is_def:
                    continue
                if not isinstance(op, Register_Operand):
                    continue
                if len(op.register_class) != 1:
                    continue

                reg = op.register_class[0].widest

                candidates -= set([reg])

        assert candidates, [candidates, [r.widest for r in free_regs]]

        list_reg = list(candidates)
        list_reg.sort(key=lambda r: r.name, reverse=True)
        reg = list_reg[0]

        return reg.as_width(address_width)

    @abc.override
    def preallocate_benchmark(
        self, alloc: Register_Liveness_Tracker, instructions: ty.Sequence[Instruction]
    ) -> ty.Tuple[object, ty.List[Instruction]]:
        stolen_regs = []
        preallocated_insts = []

        if self._benchmark_contains_scalar_int_div(instructions):
            alloc.take(RDX)
            stolen_regs.append(RDX)

        for i in instructions:
            if i.name.startswith("DIV_") or i.name.startswith("IDIV_"):  # type:ignore
                pass

            preallocated_insts.append(i)

        return stolen_regs, preallocated_insts

    @abc.override
    def free_stolen_benchmark_registers(
        self, alloc: Register_Liveness_Tracker, stolen_regs: ty.List[Register]
    ):
        for reg in stolen_regs:
            alloc.free(reg)

    def _benchmark_contains_scalar_int_div(self, kernel: ty.Sequence[Instruction]):
        for i in kernel:
            if i.name.startswith("DIV_") or i.name.startswith("IDIV_"):  # type:ignore
                return True

    @abc.override
    def emit_loop_prologue(
        self,
        kernel: ty.Sequence[Instruction],
        free_regs: ty.Sequence[X86_Register],
        reg_values: X86_Register,
    ) -> ty.List[Instruction]:
        """`reg_values` is a register containing a pointer to a memory region from
        where vector registers can be initialized"""

        out: ty.List[Instruction] = []

        # Initialize YMMi
        for ymmid, ymm in enumerate(iter(VR256)):
            out += self.emit_put_basedisplacement_in_register(
                reg_values, 32 * ymmid, ymm
            )

        out += self.emit_push_to_stack(reg_values)

        for gpr in filter(lambda r: r in GPR64, free_regs):
            if gpr == RSP:
                continue

            # Somewhere between int32_max+43 and int64_max-42 with non-null first byte
            rand_value_upper = random.randint(2**24 + 42, 2**55 - 42) << 8
            rand_lower_byte = random.randint(1, 0xFF)
            rand_value = rand_value_upper | rand_lower_byte

            out += self.emit_put_const_in_register(rand_value, gpr)

        return out

    @abc.override
    def emit_benchmark_prologue(
        self,
        kernel: ty.Sequence[Instruction],
        free_regs: ty.Sequence[X86_Register],
    ) -> ty.List[Instruction]:
        scalar_int_div = False
        leave = False

        for i in kernel:
            if i.name.startswith("DIV_GPR") or i.name.startswith(  # type:ignore
                "IDIV_GPR"
            ):
                scalar_int_div = True

                op = i.get_operand("src")
                assert isinstance(op, Register_Operand)
                if op.register.widest == RDX:  # type:ignore
                    raise ValueError(
                        "can't run benchmark where RDX is source operand of division"
                    )

            if i.name == "LEAVE":
                leave = True

        out = []

        for gpr in filter(lambda r: r in CALLER_SAVED, free_regs):
            if gpr == RSP:
                continue

            # Somewhere between int32_max+43 and int64_max-42 with non-null first byte
            rand_value_upper = random.randint(2**24 + 42, 2**55 - 42) << 8
            rand_lower_byte = random.randint(1, 0xFF)
            rand_value = rand_value_upper | rand_lower_byte

            out += self.emit_put_const_in_register(rand_value, gpr)

        if leave:
            out += self.emit_put_const_in_register(
                Label("_memory_arena_@GOTPCREL(%rip)"), RBP
            )
            i = self._set_registers(Harness.STORE_REG64_TO_MEM64, "src", RSP)

            def update(op: Operand):
                assert isinstance(op, Base_Displacement_Memory_Operand)
                return op.with_base(RBP).with_displacement(0)

            i = i.update_operand("dst", update)
            out += [i]

        return out

    @abc.override
    def emit_benchmark_epilogue(
        self,
        kernel: ty.Sequence[Instruction],
        free_regs: ty.Sequence[X86_Register],
    ) -> ty.List[Instruction]:
        leave = False

        for i in kernel:
            if i.name == "LEAVE":
                leave = True

        out = []

        if leave:
            # out += self.emit_put_const_in_register(0, RAX)
            i = self._set_registers(Harness.LOAD_MEM64_TO_REG64, "dst", RSP)

            def update(op: Operand):
                assert isinstance(op, Base_Displacement_Memory_Operand)
                return op.with_base(RIP).with_displacement(
                    Label("_memory_arena_@GOTPCREL")
                )

            i = i.update_operand("src", update)
            out += [i]

        return out

    @abc.override
    def emit_loop_epilogue(
        self,
        kernel: ty.Sequence[Instruction],
        free_regs: ty.Sequence[X86_Register],
        reg_values: X86_Register,
    ) -> ty.List[Instruction]:
        out = []
        out += self.emit_pop_from_stack(reg_values)
        return out

    def emit_dependency_breaker(self, reg: Register) -> ty.List[Instruction]:
        if reg in GPR8:
            inst = Harness.XOR_GPR8i8_GPR8i8
        if reg in GPR16:
            inst = Harness.XOR_GPR16i16_GPR16i16
        elif reg in GPR32:
            inst = Harness.XOR_GPR32i32_GPR32i32
        elif reg in GPR64:
            inst = Harness.XOR_GPR64i64_GPR64i64
        else:
            return []

        return [self._set_registers(inst, "src", reg, "src-dst", reg)]

    @abc.override
    def emit_sequentialize_cpu(
        self, alloc: Register_Liveness_Tracker
    ) -> ty.List[Instruction]:
        regs = (EAX, EBX, ECX, EDX)
        for reg in regs:
            alloc.take(reg)

        out = self.emit_put_const_in_register(0, RAX) + [Harness.CPUID]

        for reg in regs:
            alloc.free(reg)

        return out

    @abc.override
    def emit_copy(self, src: X86_Register, dst: X86_Register) -> ty.List[Instruction]:
        assert (src in GPR64) and (
            dst in GPR64
        ), f"can only do 64-bit GPR for now ({src}, {dst}, {list(iter(GPR64))})"

        return [self._set_registers(Harness.MOV_GPR64, "src", src, "dst", dst)]

    @abc.override
    def emit_push_to_stack(self, src: X86_Register) -> ty.List[Instruction]:
        assert src in GPR64, f"can only do 64-bit GPR for now ({src})"

        return [self._set_register(Harness.PUSH_GPR64, "src", src)]

    @abc.override
    def emit_pop_from_stack(self, dst: X86_Register) -> ty.List[Instruction]:
        assert dst in GPR64, f"can only do 64-bit GPR for now {dst}"

        return [self._set_register(Harness.POP_GPR64, "dst", dst)]

    @abc.override
    def emit_branch_if_not_zero(
        self, reg: X86_Register, dst: Label
    ) -> ty.List[Instruction]:
        if reg in GPR64:
            return [
                self._set_registers(Harness.TEST_GPR64, "src1", reg, "src2", reg),
                self._set_value(Harness.JNE_32, "target", dst),
            ]
        elif reg in GPR32:
            return [
                self._set_registers(Harness.TEST_GPR32, "src1", reg, "src2", reg),
                self._set_value(Harness.JNE_32, "target", dst),
            ]

        assert False, f"can only do 64-bit GPR for now ({reg})"

    @abc.override
    def emit_branch_if_zero(
        self, reg: X86_Register, dst: Label
    ) -> ty.List[Instruction]:
        assert reg in GPR64, f"can only do 64-bit GPR for now ({reg})"

        return [
            self._set_registers(Harness.TEST_GPR64, "a", reg, "b", reg),
            self._set_value(Harness.JEQ_32, "target", dst),
        ]

    @abc.override
    def emit_call(self, dst: Label) -> ty.List[Instruction]:
        return [
            # FIXME: the generated operand names are a bit wonky
            self._set_value(Harness.CALL, "dst", dst),
        ]

    @abc.override
    def emit_return(self, reg: X86_Register) -> ty.List[Instruction]:
        assert reg in GPR, f"can only do GPR for now ({reg})"

        out = []
        R64 = reg.as_width(64)
        if R64 != RAX:
            out.append(self.emit_copy(R64, RAX))
        out.append(Harness.RET)
        return out

    @abc.override
    def emit_put_const_in_register(
        self, const: int, reg: X86_Register
    ) -> ty.List[Instruction]:
        assert reg in GPR, f"can only do GPR for now ({reg})"

        assert type(const) in (int, Label)
        assert type(const) != int or 0 <= const <= (2**64) - 1

        reg32 = reg.as_width(32)
        reg64 = reg.as_width(64)

        if const == 0:
            i = self._set_registers(Harness.XOR_GPR32, "src", reg32, "src-dst", reg32)
        elif type(const) is int and const <= 2**32 - 1:
            ins = Harness.MOV_IMM32_GPR32
            i = self._set_value(ins, "src", const)
            i = self._set_register(i, "dst", reg32)
        else:
            ins = Harness.MOV_IMM64_GPR64
            i = self._set_value(ins, "src", const)
            i = self._set_register(i, "dst", reg64)
        return [i]

    @abc.override
    def emit_put_basedisplacement_in_register(
        self, base: "X86_Register", displacement: int, reg: "X86_Register"
    ) -> ty.List["Instruction"]:
        assert reg in VR256, f"can only do VR256 (aka ymmX) for now ({reg})"
        ins = Harness.VMOVDQA_VR256_MEM64
        i = self._set_registers(ins, "dst", reg)
        i = self._set_base_displacement(i, "src", base, displacement)
        return [i]

    @abc.override
    def emit_mul_reg_reg(self, reg: "X86_Register", reg_const: "X86_Register"):
        raise NotImplementedError("emit_mul_reg_reg is not implemented on x86 target")

    @abc.override
    def emit_mul_reg_const(self, reg: X86_Register, const: int) -> ty.List[Instruction]:
        assert reg in GPR64, f"can only do 64-bit GPR for now ({reg})"

        ins = Harness.IMUL_IMM_GPR64
        i = self._set_register(ins, "src1", reg)
        i = self._set_value(i, "src2", const)
        i = self._set_register(i, "dst", reg)
        return [i]

    @abc.override
    def emit_add_registers(
        self, src_reg: X86_Register, src_dst_reg: X86_Register
    ) -> ty.List[Instruction]:
        return [
            self._set_registers(
                Harness.ADD_GPR64, "src", src_reg, "src-dst", src_dst_reg
            )
        ]

    @abc.override
    def emit_substract_one_from_reg_and_branch_if_not_zero(
        self, loop_counter: X86_Register, dst: "Label"
    ) -> ty.List[Instruction]:
        sub_ins = Harness.SUB_IMM8_GPR64
        sub = self._set_value(sub_ins, "src", 1)
        sub = self._set_register(sub, "src-dst", loop_counter)

        jmp = self._set_value(Harness.JNE_32, "target", dst)

        return [sub, jmp]

    @abc.override
    def emit_add_imm_to_reg(
        self, dst: X86_Register, value: int
    ) -> ty.List[Instruction]:
        sub_ins = Harness.ADD_IMM8_GPR64
        sub = self._set_value(sub_ins, "src", value)
        sub = self._set_register(sub, "src-dst", dst)
        return [sub]

    @abc.override
    def emit_sub_imm_to_reg(
        self, dst: X86_Register, value: int
    ) -> ty.List[Instruction]:
        sub_ins = Harness.SUB_IMM8_GPR64
        sub = self._set_value(sub_ins, "src", value)
        sub = self._set_register(sub, "src-dst", dst)
        return [sub]

    @abc.override
    def dump_stack_state(self) -> ty.Dict:
        return dict()

    @abc.override
    def load_stack_state(self, state):
        pass

    def _set_registers(self, instr: Instruction, *args) -> Instruction:
        assert len(args) % 2 == 0

        for i in range(0, len(args), 2):
            instr = self._set_register(instr, args[i], args[i + 1])

        return instr

    def _set_register(
        self, instr: Instruction, op_idx_or_name, reg: X86_Register
    ) -> Instruction:
        def update(op: Operand):
            assert isinstance(op, Register_Operand)
            return op.with_register(reg)

        return instr.update_operand(op_idx_or_name, update)

    def _set_value(
        self, instr: Instruction, op_idx_or_name, val: ty.Union[int, Label]
    ) -> Instruction:
        def update(op: Operand):
            assert isinstance(op, Immediate_Operand)
            return op.with_value(val)

        return instr.update_operand(op_idx_or_name, update)

    def _set_base_displacement(
        self,
        instr: Instruction,
        op_idx_or_name,
        base: X86_Register,
        displacement: int,
    ) -> Instruction:
        def update(op: Operand):
            assert isinstance(op, Base_Displacement_Operand)
            return op.with_base(base).with_displacement(displacement)

        return instr.update_operand(op_idx_or_name, update)


@functools.singledispatch
def render_att_arg(arg):
    raise ValueError(
        "Cannot render argument " + repr(arg) + " of type " + type(arg).__name__
    )


@render_att_arg.register(Label)
def _render_label(arg):
    return arg.name


@render_att_arg.register(X86_Register_Operand)
def _render_register(arg):
    return arg.register.att_asm_name


@render_att_arg.register(X86_Immediate_Operand)
def _render_immediate(arg):
    if type(arg.value) is Label:
        return arg.value.name
    else:
        return "$" + str(arg.value)


@render_att_arg.register(str)
def _render_str(arg):
    # TODO: introduce symbol type
    return arg


@render_att_arg.register(X86_Base_Displacement_Operand)
def _render_base_displacement(arg):
    return (
        render_att_mem_arg(arg.displacement) + "(" + render_att_mem_arg(arg.base) + ")"
    )


# @render_att_arg.register(X86_Memory_Operand)
# def _(arg):
#   mode = arg.address_mode()

#   assert mode in X86_Memory_Operand._VALID_ADDRESS_MODES

#   if mode == 'B':
#     return '(' + render_att_mem_arg(arg.base) + ')'
#   if mode == 'D':
#     return '(' + render_att_mem_arg(arg.displacement) + ')'
#   if mode == 'BD':
#     return render_att_mem_arg(arg.displacement) + '(' + render_att_mem_arg(arg.base) + ')'
#   if mode == 'BIS':
#     return ''.join((
#       '(',
#       render_att_mem_arg(arg.base),
#       ',',
#       render_att_mem_arg(arg.index),
#       ',',
#       render_att_mem_arg(arg.scale),
#       ')',
#     ))
#   if mode == 'ISD':
#     return ''.join((
#       render_att_mem_arg(arg.displacement),
#       '(',
#       ',',
#       render_att_mem_arg(arg.index),
#       ',',
#       render_att_mem_arg(arg.scale),
#       ')',
#     ))
#   if mode == 'BISD':
#     return ''.join((
#       render_att_mem_arg(arg.displacement),
#       '(',
#       render_att_mem_arg(arg.base),
#       ',',
#       render_att_mem_arg(arg.index),
#       ',',
#       render_att_mem_arg(arg.scale),
#       ')',
#     ))

#   raise ValueError('invalid memory operand', arg)


@functools.singledispatch
def render_att_mem_arg(arg):
    raise ValueError(
        "Cannot render memory argument " + repr(arg) + " of type " + type(arg).__name__
    )


@render_att_mem_arg.register(X86_Immediate_Operand)
def _render_mem_immediate(arg) -> str:
    assert arg.value is not None

    if arg.value == 0:
        return ""

    if type(arg.value) is Label:
        return arg.value.name

    assert type(arg.value) is int
    return str(arg.value)


@render_att_mem_arg.register(X86_Register_Operand)
def _render_mem_register(arg) -> str:
    assert arg.register is not None
    return arg.register.att_asm_name


@functools.singledispatch
def render_intel_arg(arg):
    raise ValueError(
        "Cannot render argument " + repr(arg) + " of type " + type(arg).__name__
    )


@render_intel_arg.register(Label)
def _render_intel_label(arg):
    return arg.name


@render_intel_arg.register(X86_Register_Operand)
def _render_intel_register(arg):
    assert arg.register is not None
    assert arg.register.att_asm_name[0] == "%"
    intel_name = arg.register.att_asm_name[1:]
    return intel_name


@render_intel_arg.register(X86_Immediate_Operand)
def _render_intel_immediate(arg):
    if type(arg.value) is Label:
        return arg.value.name
    else:
        return str(arg.value)


@render_intel_arg.register(str)
def _render_intel_str(arg):
    # TODO: introduce symbol type
    return arg


@render_intel_arg.register(X86_Base_Displacement_Operand)
def _render_intel_base_displacement(arg):
    if isinstance(arg, X86_Memory_Operand):
        mem_spec = {8: "byte", 16: "word", 32: "dword", 64: "qword"}.get(
            arg._memory_width, ""
        )
    else:
        mem_spec = ""
    if mem_spec:
        mem_spec = f"{mem_spec} ptr "
    if type(arg.displacement.value) is Label or arg.displacement.value >= 0:
        return f"{mem_spec}[{render_intel_arg(arg.base)} + {render_intel_arg(arg.displacement)}]"
    else:
        return f"{mem_spec}[{render_intel_arg(arg.base)} - {-arg.displacement.value}]"


@functools.singledispatch
def render_intel_mem_arg(arg):
    raise ValueError(
        "Cannot render memory argument " + repr(arg) + " of type " + type(arg).__name__
    )


@render_intel_mem_arg.register(X86_Immediate_Operand)
def _render_intel_mem_immediate(arg) -> str:
    return render_att_mem_arg(arg)


@render_intel_mem_arg.register(X86_Register_Operand)
def _render_intel_mem_register(arg) -> str:
    return render_intel_arg(arg)
