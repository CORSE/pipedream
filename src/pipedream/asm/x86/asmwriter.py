"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from pipedream.utils import abc
from pipedream.asm.ir import *
from pipedream.asm.asmwriter import *
from pipedream.asm.x86.registers import *
from pipedream.asm.x86.operands import *

import functools
import math
import typing as ty


__all__ = [
    "X86_ASM_Writer",
]


class X86_ASM_Writer(ASM_Writer):
    def __init__(self, dialect: "Asm_Dialect", file: ty.IO[str]):
        super().__init__(file)
        self.dialect = dialect

    @abc.override
    def begin_file(self, name):
        for l in self.dialect.begin_file(name):
            self.print(1, l)
        self.print(1, '.file 1 "' + name + '"')

    @abc.override
    def end_file(self, name):
        self.print(1, '.ident "pipe-dream"')
        for l in self.dialect.end_file(name):
            self.print(1, l)

    @abc.override
    def begin_function(self, function_name: str):
        for l in self.dialect.begin_function(function_name):
            self.print(1, l)
        self.print(1, ".text")
        self.print(1, ".globl " + function_name)
        self.print(1, ".type  " + function_name + ", @function")
        self.align()
        self.print(0, function_name + ":")
        self.print(1, ".cfi_startproc")

    @abc.override
    def end_function(self, function_name: str):
        self.print(1, ".cfi_endproc")
        self.print(1, ".size " + function_name + ", .-" + function_name)
        self.print(1, ".type " + function_name + ", @function")
        for l in self.dialect.end_function(function_name):
            self.print(1, l)

    @abc.override
    def emit_label(self, label):
        self.print(0, label.name + ":")

    @abc.override
    def align(self):
        self.print(1, ".align 16, 0x90")

    @abc.override
    def insts(self, insts: ty.Sequence[Instruction]):
        for inst in insts:
            assert isinstance(inst, Instruction)
            assert not any(o.is_virtual for o in inst.operands), [
                inst,
                [o for o in inst.operands if o.is_virtual],
            ]

            try:
                self.print(1, self.dialect.instruction_to_str(inst))
            except AssertionError:
                print("#", inst)
                raise

    @abc.override
    def global_byte_array(self, name: str, size: int, alignment: int):
        ## this is just copy/adapted from ASM emitted by GCC

        assert type(name) is str
        assert type(size) is int
        assert type(alignment) is int

        assert name and name.isprintable()
        assert size >= 0
        assert alignment >= 0
        assert math.log2(alignment).is_integer()

        ## assume we may be in text section
        ## switch to BSS & emit array
        self.print(1, ".bss")
        self.print(1, ".global", name)
        self.print(1, ".balign", alignment)
        self.print(1, ".type", name + ",", "@object")
        self.print(1, ".size", name + ",", size)
        self.print(0, name + ":")
        self.print(1, ".zero", size)
        ## switch back to text section
        self.print(1, ".text")
        self.print(0, "")

    @abc.override
    def global_int_values(
        self, name: str, values: ty.List[int], elt_size: int = 8, alignment: int = 8
    ):
        ## assume we may be in text section
        ## switch to DATA & emit array
        directive_map = {1: ".byte", 2: ".short", 4: ".long", 8: ".quad"}
        directive = directive_map.get(elt_size)
        assert directive is not None

        self.print(1, ".data")
        self.print(1, f".global {name}")
        self.print(1, f".balign {alignment}")
        self.print(0, f"{name}:")
        for val in values:
            self.print(1, f"{directive} {val}")
        self.print(1, f".type {name}, @object")
        self.print(1, f".size {name}, . - {name}")
        self.print(1, ".text")
        self.print(0, "")

    @abc.override
    def comment(self, *args):
        self.print(1, "#", *args)

    @abc.override
    def newline(self):
        self.print(0)

    @abc.override
    def emit_operand(self, op) -> str:
        raise NotImplementedError()
