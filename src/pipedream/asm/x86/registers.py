"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import List, Set, Dict, cast

from pipedream.utils import abc
from pipedream.asm.ir import *

__all__ = [
    "X86_Register_Set",
    "X86_Register",
    "ANY_REGISTER",
    "ARGUMENT_REGISTER",
    "CALLER_SAVED",
    "CALLEE_SAVED",
    "GPR",
    "GPR64",
    "GPR32",
    "GPR16",
    "GPR8",
    "GPR8NOREX",
    "VR",
    "VR64",
    "VR128",
    "VRX128",
    "VR256",
    "VRX256",
    "VRX512",
    "CL_REGISTER",
    "FLAGS_REGISTER",
    "PC_REGISTER",
    "STACK_POINTER_REGISTER",
    "BASE_REGISTER_64",
    "BASE_REGISTER_32",
    "BASE_REGISTER_16",
    "INDEX_REGISTER_64",
    "INDEX_REGISTER_32",
    "INDEX_REGISTER_16",
]


GPR: Register_Class
GPR64: Register_Class
GPR32: Register_Class
GPR16: Register_Class
GPR8: Register_Class
GPR8NOREX: Register_Class
VR: Register_Class
VR64: Register_Class
VR128: Register_Class
VR256: Register_Class
VRX128: Register_Class
VRX256: Register_Class
VRX512: Register_Class
ANY_REGISTER: Register_Class
ARGUMENT_REGISTER: Register_Class
CALLEE_SAVED: Register_Class
CALLER_SAVED: Register_Class
BASE_REGISTER_64: Register_Class
BASE_REGISTER_32: Register_Class
BASE_REGISTER_16: Register_Class
INDEX_REGISTER_64: Register_Class
INDEX_REGISTER_32: Register_Class
INDEX_REGISTER_16: Register_Class
RC_RIP: Register_Class
RC_RSP: Register_Class
FPST: Register_Class
RC_FLAGS: Register_Class
RC_EFLAGS: Register_Class
RC_RFLAGS: Register_Class
RC_IP: Register_Class
RC_EIP: Register_Class
RC_SP: Register_Class
RC_ESP: Register_Class
RC_CS: Register_Class
RC_DS: Register_Class
RC_ES: Register_Class
RC_FS: Register_Class
RC_GS: Register_Class
RC_SS: Register_Class
RC_FSBASE: Register_Class
RC_GSBASE: Register_Class
RC_TR: Register_Class
RC_LDTR: Register_Class
RC_GDTR: Register_Class
RC_SSP: Register_Class
RC_TSCAUX: Register_Class
RC_CR0: Register_Class
RC_CR1: Register_Class
RC_CR2: Register_Class
RC_CR3: Register_Class
RC_CR4: Register_Class
RC_CR5: Register_Class
RC_CR6: Register_Class
RC_CR7: Register_Class
RC_CR8: Register_Class
RC_CR9: Register_Class
RC_CR10: Register_Class
RC_CR11: Register_Class
RC_CR12: Register_Class
RC_CR13: Register_Class
RC_CR14: Register_Class
RC_CR15: Register_Class
RC_AL: Register_Class
RC_CL: Register_Class
RC_AH: Register_Class
RC_AX: Register_Class
RC_BX: Register_Class
RC_CX: Register_Class
RC_DX: Register_Class
RC_DI: Register_Class
RC_SI: Register_Class
RC_BP: Register_Class
RC_EAX: Register_Class
RC_EBX: Register_Class
RC_ECX: Register_Class
RC_EDX: Register_Class
RC_EDI: Register_Class
RC_ESI: Register_Class
RC_EBP: Register_Class
RC_RAX: Register_Class
RC_RBX: Register_Class
RC_RCX: Register_Class
RC_RDX: Register_Class
RC_RDI: Register_Class
RC_RSI: Register_Class
RC_RBP: Register_Class
RC_R8: Register_Class
RC_R9: Register_Class
RC_R10: Register_Class
RC_R11: Register_Class
RC_R12: Register_Class
RC_R13: Register_Class
RC_R14: Register_Class
RC_R15: Register_Class
RC_XMM0: Register_Class
RC_ST0: Register_Class
RC_ST1: Register_Class
RC_ST2: Register_Class
RC_ST3: Register_Class
RC_ST4: Register_Class
RC_ST5: Register_Class
RC_ST6: Register_Class
RC_ST7: Register_Class
RC_X87STATUS: Register_Class
RC_X87CONTROL: Register_Class
RC_X87TAG: Register_Class
RC_MXCSR: Register_Class
RC_TSC: Register_Class
RC_MSRS: Register_Class
RC_XCR0: Register_Class


AH: "X86_Register"
BH: "X86_Register"
CH: "X86_Register"
DH: "X86_Register"
RAX: "X86_Register"
RDI: "X86_Register"
RSI: "X86_Register"
RDX: "X86_Register"
RCX: "X86_Register"
R8: "X86_Register"
R9: "X86_Register"
R10: "X86_Register"
R11: "X86_Register"
RBX: "X86_Register"
RBP: "X86_Register"
R12: "X86_Register"
R13: "X86_Register"
R14: "X86_Register"
R15: "X86_Register"
EAX: "X86_Register"
AX: "X86_Register"
AL: "X86_Register"
EDI: "X86_Register"
DI: "X86_Register"
DIL: "X86_Register"
ESI: "X86_Register"
SI: "X86_Register"
SIL: "X86_Register"
EDX: "X86_Register"
DX: "X86_Register"
DL: "X86_Register"
ECX: "X86_Register"
CX: "X86_Register"
CL: "X86_Register"
R8D: "X86_Register"
R8W: "X86_Register"
R8B: "X86_Register"
R9D: "X86_Register"
R9W: "X86_Register"
R9B: "X86_Register"
R10D: "X86_Register"
R10W: "X86_Register"
R10B: "X86_Register"
R11D: "X86_Register"
R11W: "X86_Register"
R11B: "X86_Register"
RSP: "X86_Register"
ESP: "X86_Register"
SP: "X86_Register"
SPL: "X86_Register"
EBX: "X86_Register"
BX: "X86_Register"
BL: "X86_Register"
EBP: "X86_Register"
BP: "X86_Register"
BPL: "X86_Register"
R12D: "X86_Register"
R12W: "X86_Register"
R12B: "X86_Register"
R13D: "X86_Register"
R13W: "X86_Register"
R13B: "X86_Register"
R14D: "X86_Register"
R14W: "X86_Register"
R14B: "X86_Register"
R15D: "X86_Register"
R15W: "X86_Register"
R15B: "X86_Register"
ST0: "X86_Register"
ST1: "X86_Register"
ST2: "X86_Register"
ST3: "X86_Register"
ST4: "X86_Register"
ST5: "X86_Register"
ST6: "X86_Register"
ST7: "X86_Register"
MM0: "X86_Register"
MM1: "X86_Register"
MM2: "X86_Register"
MM3: "X86_Register"
MM4: "X86_Register"
MM5: "X86_Register"
MM6: "X86_Register"
MM7: "X86_Register"
XMM0: "X86_Register"
XMM1: "X86_Register"
XMM2: "X86_Register"
XMM3: "X86_Register"
XMM4: "X86_Register"
XMM5: "X86_Register"
XMM6: "X86_Register"
XMM7: "X86_Register"
XMM8: "X86_Register"
XMM9: "X86_Register"
XMM10: "X86_Register"
XMM11: "X86_Register"
XMM12: "X86_Register"
XMM13: "X86_Register"
XMM14: "X86_Register"
XMM15: "X86_Register"
XMM16: "X86_Register"
XMM17: "X86_Register"
XMM18: "X86_Register"
XMM19: "X86_Register"
XMM20: "X86_Register"
XMM21: "X86_Register"
XMM22: "X86_Register"
XMM23: "X86_Register"
XMM24: "X86_Register"
XMM25: "X86_Register"
XMM26: "X86_Register"
XMM27: "X86_Register"
XMM28: "X86_Register"
XMM29: "X86_Register"
XMM30: "X86_Register"
XMM31: "X86_Register"
YMM0: "X86_Register"
YMM1: "X86_Register"
YMM2: "X86_Register"
YMM3: "X86_Register"
YMM4: "X86_Register"
YMM5: "X86_Register"
YMM6: "X86_Register"
YMM7: "X86_Register"
YMM8: "X86_Register"
YMM9: "X86_Register"
YMM10: "X86_Register"
YMM11: "X86_Register"
YMM12: "X86_Register"
YMM13: "X86_Register"
YMM14: "X86_Register"
YMM15: "X86_Register"
YMM16: "X86_Register"
YMM17: "X86_Register"
YMM18: "X86_Register"
YMM19: "X86_Register"
YMM20: "X86_Register"
YMM21: "X86_Register"
YMM22: "X86_Register"
YMM23: "X86_Register"
YMM24: "X86_Register"
YMM25: "X86_Register"
YMM26: "X86_Register"
YMM27: "X86_Register"
YMM28: "X86_Register"
YMM29: "X86_Register"
YMM30: "X86_Register"
YMM31: "X86_Register"
ZMM0: "X86_Register"
ZMM1: "X86_Register"
ZMM2: "X86_Register"
ZMM3: "X86_Register"
ZMM4: "X86_Register"
ZMM5: "X86_Register"
ZMM6: "X86_Register"
ZMM7: "X86_Register"
ZMM8: "X86_Register"
ZMM9: "X86_Register"
ZMM10: "X86_Register"
ZMM11: "X86_Register"
ZMM12: "X86_Register"
ZMM13: "X86_Register"
ZMM14: "X86_Register"
ZMM15: "X86_Register"
ZMM16: "X86_Register"
ZMM17: "X86_Register"
ZMM18: "X86_Register"
ZMM19: "X86_Register"
ZMM20: "X86_Register"
ZMM21: "X86_Register"
ZMM22: "X86_Register"
ZMM23: "X86_Register"
ZMM24: "X86_Register"
ZMM25: "X86_Register"
ZMM26: "X86_Register"
ZMM27: "X86_Register"
ZMM28: "X86_Register"
ZMM29: "X86_Register"
ZMM30: "X86_Register"
ZMM31: "X86_Register"
FLAGS: "X86_Register"
EFLAGS: "X86_Register"
RFLAGS: "X86_Register"
IP: "X86_Register"
EIP: "X86_Register"
RIP: "X86_Register"
CS: "X86_Register"
DS: "X86_Register"
ES: "X86_Register"
FS: "X86_Register"
GS: "X86_Register"
SS: "X86_Register"
FSBASE: "X86_Register"
GSBASE: "X86_Register"
TR: "X86_Register"
LDTR: "X86_Register"
GDTR: "X86_Register"
SSP: "X86_Register"
TSCAUX: "X86_Register"
CR0: "X86_Register"
CR1: "X86_Register"
CR2: "X86_Register"
CR3: "X86_Register"
CR4: "X86_Register"
CR5: "X86_Register"
CR6: "X86_Register"
CR7: "X86_Register"
CR8: "X86_Register"
CR9: "X86_Register"
CR10: "X86_Register"
CR11: "X86_Register"
CR12: "X86_Register"
CR13: "X86_Register"
CR14: "X86_Register"
CR15: "X86_Register"
XCR0: "X86_Register"
X87STATUS: "X86_Register"
X87CONTROL: "X86_Register"
X87TAG: "X86_Register"
MXCSR: "X86_Register"
TSC: "X86_Register"
MSRS: "X86_Register"
ARG_1: "X86_Register"
ARG_2: "X86_Register"
ARG_3: "X86_Register"
ARG_4: "X86_Register"
ARG_5: "X86_Register"
ARG_6: "X86_Register"
ARG_7: "X86_Register"


class X86_Register(Register):
    def __init__(self, name: str, att_asm_name: str, idx: int, width: int, subs: set):
        self._idx = idx
        self._name = name
        self._att_asm_name = att_asm_name
        self._width = width

        self._subs: Set["X86_Register"] = set(subs)
        self._supers: Set["X86_Register"] = set()
        self._aliases: Set[Register] = set()
        self._widest: "X86_Register" = self
        self._as_width: Dict[int, "X86_Register"] = {width: self}

    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return self._name

    @property  # type: ignore
    @abc.override
    def att_asm_name(self) -> str:
        return self._att_asm_name

    @property  # type: ignore
    @abc.override
    def width(self):
        """Bit width of register."""
        return self._width

    @abc.override
    def as_width(self, bits: int) -> "X86_Register":
        return self._as_width[bits]

    @property  # type: ignore
    @abc.override
    def widest(self) -> "X86_Register":
        return self._widest

    @property  # type: ignore
    @abc.override
    def sub_registers(self):
        return iter(self._subs)

    @property  # type: ignore
    @abc.override
    def super_registers(self):
        return iter(self._supers)

    @property  # type: ignore
    @abc.override
    def aliases(self):
        return iter(self._aliases)

    def __lt__(self, other):
        if type(self) is not type(other):
            return NotImplemented

        return self._idx < other._idx

    def __str__(self):
        return type(self).__name__ + "(" + self.name + ")"

    def __repr__(self):
        return type(self).__name__ + "(" + self.name + ")"

    def __deepcopy__(self, memo):
        return self


_ALL_REGISTERS_: List[X86_Register] = []

## hack to allow putting register objects in module scope
self = vars()


def _def_x86_reg(name: str, width: int, *immediate_sub_regs, att_asm_name: str = None):
    assert name not in self, "duplicate register def"

    if not att_asm_name:
        att_asm_name = "%" + name.lower()

    idx = len(_ALL_REGISTERS_)
    reg = X86_Register(name, att_asm_name, idx, width, set(immediate_sub_regs))

    self[name] = reg
    _ALL_REGISTERS_.append(reg)
    __all__.append(reg.name)


## in order of encoding
## http://wiki.osdev.org/X86-64_Instruction_Encoding#Registers

# TODO: AVX write mask registers k0-k7

# Return value, caller-saved
_def_x86_reg("RAX", 64, "EAX")
_def_x86_reg("EAX", 32, "AX")
_def_x86_reg("AX", 16, "AL", "AH")
_def_x86_reg("AL", 8)
_def_x86_reg("AH", 8)
# 4th argument, caller-saved
_def_x86_reg("RCX", 64, "ECX")
_def_x86_reg("ECX", 32, "CX")
_def_x86_reg("CX", 16, "CL", "CH")
_def_x86_reg("CL", 8)
_def_x86_reg("CH", 8)
# 3rd argument, caller-saved
_def_x86_reg("RDX", 64, "EDX")
_def_x86_reg("EDX", 32, "DX")
_def_x86_reg("DX", 16, "DL", "DH")
_def_x86_reg("DL", 8)
_def_x86_reg("DH", 8)
# Local variable, callee-saved
_def_x86_reg("RBX", 64, "EBX")
_def_x86_reg("EBX", 32, "BX")
_def_x86_reg("BX", 16, "BL", "BH")
_def_x86_reg("BL", 8)
_def_x86_reg("BH", 8)
# Stack pointer, callee-saved
_def_x86_reg("RSP", 64, "ESP")
_def_x86_reg("ESP", 32, "SP")
_def_x86_reg("SP", 16, "SPL")
_def_x86_reg("SPL", 8)
# Local variable, callee-saved
_def_x86_reg("RBP", 64, "EBP")
_def_x86_reg("EBP", 32, "BP")
_def_x86_reg("BP", 16, "BPL")
_def_x86_reg("BPL", 8)
# 2nd argument, caller-saved
_def_x86_reg("RSI", 64, "ESI")
_def_x86_reg("ESI", 32, "SI")
_def_x86_reg("SI", 16, "SIL")
_def_x86_reg("SIL", 8)
# 1st argument, caller-saved
_def_x86_reg("RDI", 64, "EDI")
_def_x86_reg("EDI", 32, "DI")
_def_x86_reg("DI", 16, "DIL")
_def_x86_reg("DIL", 8)
# 5th argument, caller-saved
_def_x86_reg("R8", 64, "R8D")
_def_x86_reg("R8D", 32, "R8W")
_def_x86_reg("R8W", 16, "R8B")
_def_x86_reg("R8B", 8)
# 6th argument, caller-saved
_def_x86_reg("R9", 64, "R9D")
_def_x86_reg("R9D", 32, "R9W")
_def_x86_reg("R9W", 16, "R9B")
_def_x86_reg("R9B", 8)
# Scratch/temporary, caller-saved
_def_x86_reg("R10", 64, "R10D")
_def_x86_reg("R10D", 32, "R10W")
_def_x86_reg("R10W", 16, "R10B")
_def_x86_reg("R10B", 8)
# Scratch/temporary, caller-saved
_def_x86_reg("R11", 64, "R11D")
_def_x86_reg("R11D", 32, "R11W")
_def_x86_reg("R11W", 16, "R11B")
_def_x86_reg("R11B", 8)
# Local variable, callee-saved
_def_x86_reg("R12", 64, "R12D")
_def_x86_reg("R12D", 32, "R12W")
_def_x86_reg("R12W", 16, "R12B")
_def_x86_reg("R12B", 8)
# Local variable, callee-saved
_def_x86_reg("R13", 64, "R13D")
_def_x86_reg("R13D", 32, "R13W")
_def_x86_reg("R13W", 16, "R13B")
_def_x86_reg("R13B", 8)
# Local variable, callee-saved
_def_x86_reg("R14", 64, "R14D")
_def_x86_reg("R14D", 32, "R14W")
_def_x86_reg("R14W", 16, "R14B")
_def_x86_reg("R14B", 8)
# Local variable, callee-saved
_def_x86_reg("R15", 64, "R15D")
_def_x86_reg("R15D", 32, "R15W")
_def_x86_reg("R15W", 16, "R15B")
_def_x86_reg("R15B", 8)

# Instruction pointer
_def_x86_reg("RIP", 64, "EIP")
_def_x86_reg("EIP", 32, "IP")
_def_x86_reg("IP", 16)

# Status/condition code bits
_def_x86_reg("RFLAGS", 64, "EFLAGS")
_def_x86_reg("EFLAGS", 32, "FLAGS")
_def_x86_reg("FLAGS", 16)

# weird system registers
# TODO: fix bit widths
_def_x86_reg("CS", 16)
_def_x86_reg("DS", 16)
_def_x86_reg("ES", 16)
_def_x86_reg("FS", 16)
_def_x86_reg("GS", 16)
_def_x86_reg("SS", 16)
_def_x86_reg("FSBASE", 16)
_def_x86_reg("GSBASE", 16)
_def_x86_reg("TR", 16)
_def_x86_reg("LDTR", 16)
_def_x86_reg("GDTR", 16)
_def_x86_reg("SSP", 16)
_def_x86_reg("TSC", 32)
_def_x86_reg("TSCAUX", 16)
_def_x86_reg("X87STATUS", 16)
_def_x86_reg("X87TAG", 16)
_def_x86_reg("X87CONTROL", 16)
_def_x86_reg("MXCSR", 32)
_def_x86_reg("MSRS", 64)


# CPU control registers
# TODO: fix bit widths
for i in range(16):
    _def_x86_reg(f"CR{i}", 16)
_def_x86_reg("XCR0", 32)

for i in range(8):
    ## FIXME: this is probably wrong
    _def_x86_reg(f"MM{i}", 64)
    _def_x86_reg(f"ST{i}", 80, f"MM{i}", att_asm_name=f"%st({i})")

for i in range(32):
    _def_x86_reg(f"XMM{i}", 128)

for i in range(32):
    _def_x86_reg(f"YMM{i}", 256, f"XMM{i}")

for i in range(32):
    _def_x86_reg(f"ZMM{i}", 512, f"YMM{i}")


## fill in sub/super registers
for reg in _ALL_REGISTERS_:
    subs = set()

    for sub in reg._subs:
        if isinstance(sub, str):
            sub = self[sub]

        assert isinstance(sub, X86_Register)
        subs.add(sub)
        sub._supers.add(reg)

    reg._subs = subs

for reg in _ALL_REGISTERS_:
    reg._aliases = set(reg.all_sub_registers) | set(reg.all_super_registers)
    as_width = {reg.width: reg}
    widest = reg

    for alias in reg._aliases:
        if alias in [AH, BH, CH, DH]:
            continue

        assert alias.width not in as_width, (reg, alias, as_width)

        assert isinstance(alias, X86_Register)
        as_width[alias.width] = alias

        if alias.width > widest.width:
            widest = alias

    reg._as_width = as_width
    reg._widest = widest

del self


################################################################################
### register classes

_ALL_REGISTER_CLASSES_ = []


def _def_x86_register_class(name, *registers, **aliases):
    ## put register aliases in module scope
    for k, v in aliases.items():
        assert k not in globals() or globals()[k] is v

        globals()[k] = v
        __all__.append(k)

    clss = Register_Class(name, registers, aliases)
    _ALL_REGISTER_CLASSES_.append(clss)
    globals()[name] = clss
    __all__.append(name)


_def_x86_register_class("ANY_REGISTER", *_ALL_REGISTERS_)

_def_x86_register_class(
    "ARGUMENT_REGISTER",
    RDI,
    RSI,
    RDX,
    RCX,
    R8,
    R9,
    ARG_1=RDI,
    ARG_2=RSI,
    ARG_3=RDX,
    ARG_4=RCX,
    ARG_5=R8,
    ARG_6=R9,
)

_def_x86_register_class("CALLER_SAVED", RAX, RDI, RSI, RDX, RCX, R8, R9, R10, R11)

_def_x86_register_class(
    "CALLEE_SAVED",
    RBX,
    RBP,
    R12,
    R13,
    R14,
    R15,
)

_def_x86_register_class(
    "GPR",
    RAX,
    EAX,
    AX,
    AL,
    # 1st argument, caller-saved
    RDI,
    EDI,
    DI,
    DIL,
    # 2nd argument, caller-saved
    RSI,
    ESI,
    SI,
    SIL,
    # 3rd argument, caller-saved
    RDX,
    EDX,
    DX,
    DL,
    # 4th argument, caller-saved
    RCX,
    ECX,
    CX,
    CL,
    # 5th argument, caller-saved
    R8,
    R8D,
    R8W,
    R8B,
    # 6th argument, caller-saved
    R9,
    R9D,
    R9W,
    R9B,
    # Scratch/temporary, caller-saved
    R10,
    R10D,
    R10W,
    R10B,
    # Scratch/temporary, caller-saved
    R11,
    R11D,
    R11W,
    R11B,
    # Stack pointer, callee-saved
    RSP,
    ESP,
    SP,
    SPL,
    # Local variable, callee-saved
    RBX,
    EBX,
    BX,
    BL,
    # Local variable, callee-saved
    RBP,
    EBP,
    BP,
    BPL,
    # Local variable, callee-saved
    R12,
    R12D,
    R12W,
    R12B,
    # Local variable, callee-saved
    R13,
    R13D,
    R13W,
    R13B,
    # Local variable, callee-saved
    R14,
    R14D,
    R14W,
    R14B,
    # Local variable, callee-saved
    R15,
    R15D,
    R15W,
    R15B,
)

_def_x86_register_class("GPR64", *[r for r in iter(GPR) if r.width == 64])

_def_x86_register_class("GPR32", *[r for r in iter(GPR) if r.width == 32])

_def_x86_register_class("GPR16", *[r for r in iter(GPR) if r.width == 16])

_def_x86_register_class(
    "GPR8", *[r for r in iter(GPR) if r.width == 8 if r not in (AH, BH, CH, DH)]
)

_def_x86_register_class(
    "GPR8NOREX",
    AL,
    BL,
    CL,
    DL,
    # AH,
    # BH,
    # CH,
    # DH,
)

_def_x86_register_class("FPST", ST0, ST1, ST2, ST3, ST4, ST5, ST6, ST7)

_def_x86_register_class(
    "VR",
    MM0,
    MM1,
    MM2,
    MM3,
    MM4,
    MM5,
    MM6,
    MM7,
    XMM0,
    XMM1,
    XMM2,
    XMM3,
    XMM4,
    XMM5,
    XMM6,
    XMM7,
    XMM8,
    XMM9,
    XMM10,
    XMM11,
    XMM12,
    XMM13,
    XMM14,
    XMM15,
    XMM16,
    XMM17,
    XMM18,
    XMM19,
    XMM20,
    XMM21,
    XMM22,
    XMM23,
    XMM24,
    XMM25,
    XMM26,
    XMM27,
    XMM28,
    XMM29,
    XMM30,
    XMM31,
    YMM0,
    YMM1,
    YMM2,
    YMM3,
    YMM4,
    YMM5,
    YMM6,
    YMM7,
    YMM8,
    YMM9,
    YMM10,
    YMM11,
    YMM12,
    YMM13,
    YMM14,
    YMM15,
    YMM16,
    YMM17,
    YMM18,
    YMM19,
    YMM20,
    YMM21,
    YMM22,
    YMM23,
    YMM24,
    YMM25,
    YMM26,
    YMM27,
    YMM28,
    YMM29,
    YMM30,
    YMM31,
    ZMM0,
    ZMM1,
    ZMM2,
    ZMM3,
    ZMM4,
    ZMM5,
    ZMM6,
    ZMM7,
    ZMM8,
    ZMM9,
    ZMM10,
    ZMM11,
    ZMM12,
    ZMM13,
    ZMM14,
    ZMM15,
    ZMM16,
    ZMM17,
    ZMM18,
    ZMM19,
    ZMM20,
    ZMM21,
    ZMM22,
    ZMM23,
    ZMM24,
    ZMM25,
    ZMM26,
    ZMM27,
    ZMM28,
    ZMM29,
    ZMM30,
    ZMM31,
)

_def_x86_register_class("VR64", *[r for r in iter(VR) if r.width == 64])

_def_x86_register_class("VRX128", *[r for r in iter(VR) if r.width == 128])

_def_x86_register_class("VRX256", *[r for r in iter(VR) if r.width == 256])

_def_x86_register_class("VRX512", *[r for r in iter(VR) if r.width == 512])

_def_x86_register_class("VR128", *VRX128[0:16])  # type: ignore
_def_x86_register_class("VR256", *VRX256[0:16])  # type: ignore

_def_x86_register_class("CL_REGISTER", CL)

STACK_POINTER_REGISTER: Register_Class
_def_x86_register_class(
    "STACK_POINTER_REGISTER",
    SP,
    ESP,
    RSP,
)

_def_x86_register_class(
    "FLAGS_REGISTER",
    FLAGS,
    EFLAGS,
    RFLAGS,
)

_def_x86_register_class(
    "PC_REGISTER",
    IP,
    EIP,
    RIP,
)

## singleton register classes
for reg in [
    FLAGS,
    EFLAGS,
    RFLAGS,
    IP,
    EIP,
    RIP,
    SP,
    ESP,
    RSP,
    CS,
    DS,
    ES,
    FS,
    GS,
    SS,
    FSBASE,
    GSBASE,
    TR,
    LDTR,
    GDTR,
    SSP,
    TSCAUX,
    CR0,
    CR1,
    CR2,
    CR3,
    CR4,
    CR5,
    CR6,
    CR7,
    CR8,
    CR9,
    CR10,
    CR11,
    CR12,
    CR13,
    CR14,
    CR15,
    XCR0,
    AL,
    CL,
    AH,
    AX,
    BX,
    CX,
    DX,
    DI,
    SI,
    BP,
    SP,
    EAX,
    EBX,
    ECX,
    EDX,
    EDI,
    ESI,
    EBP,
    ESP,
    RAX,
    RBX,
    RCX,
    RDX,
    RDI,
    RSI,
    RBP,
    RSP,
    R8,
    R9,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
    XMM0,
    ST0,
    ST1,
    ST2,
    ST3,
    ST4,
    ST5,
    ST6,
    ST7,
    X87STATUS,
    X87CONTROL,
    X87TAG,
    MXCSR,
    TSC,
    MSRS,
]:
    _def_x86_register_class("RC_" + reg.name, reg)


### memory addressing
## https://en.wikipedia.org/wiki/X86#Addressing_modes

_def_x86_register_class(
    "BASE_REGISTER_16",
    BX,
    BP,
)
_def_x86_register_class(
    "BASE_REGISTER_32",
    EAX,
    EBX,
    ECX,
    EDX,
    ESP,
    EBP,
    ESI,
    EDI,
)
_def_x86_register_class(
    "BASE_REGISTER_64",
    *GPR64,  # type: ignore
    RSP,
    RIP,
)

_def_x86_register_class(
    "INDEX_REGISTER_16",
    SI,
    DI,
)
_def_x86_register_class(
    "INDEX_REGISTER_32",
    EAX,
    EBX,
    ECX,
    EDX,
    EBP,
    ESI,
    EDI,
)
_def_x86_register_class(
    "INDEX_REGISTER_64",
    *GPR64,  # type: ignore
)


################################################################################
### legacy

# 'GPRS',
# [Reg64, Reg32, Reg16, Reg8],
# ['rax', 'eax',  'ax',   'al'],    # Return value, caller-saved
# ['rdi', 'edi',  'di',   'dil'],   # 1st argument, caller-saved
# ['rsi', 'esi',  'si',   'sil'],   # 2nd argument, caller-saved
# ['rdx', 'edx',  'dx',   'dl'],    # 3rd argument, caller-saved
# ['rcx', 'ecx',  'cx',   'cl'],    # 4th argument, caller-saved
# ['r8',  'r8d',  'r8w',  'r8b'],   # 5th argument, caller-saved
# ['r9',  'r9d',  'r9w',  'r9b'],   # 6th argument, caller-saved
# ['r10', 'r10d', 'r10w', 'r10b'],  # Scratch/temporary, caller-saved
# ['r11', 'r11d', 'r11w', 'r11b'],  # Scratch/temporary, caller-saved
# ['rsp', 'esp',  'sp',   'spl'],   # Stack pointer, callee-saved
# ['rbx', 'ebx',  'bx',   'bl'],    # Local variable, callee-saved
# ['rbp', 'ebp',  'bp',   'bpl'],   # Local variable, callee-saved
# ['r12', 'r12w', 'r12w', 'r12b'],  # Local variable, callee-saved
# ['r13', 'r13d', 'r13w', 'r13b'],  # Local variable, callee-saved
# ['r14', 'r14d', 'r14w', 'r14b'],  # Local variable, callee-saved
# ['r15', 'r15d', 'r15w', 'r15b'],  # Local variable, callee-saved
# ['rip', 'eip', 'ip'],             # Instruction pointer
# ['rflags', 'eflags'],             # Status/condition code bits


class X86_Register_Set(Register_Set):
    @abc.override
    def stack_pointer_register(self) -> "Register":
        return RSP

    @abc.override
    def stack_pointer_register_class(self) -> "Register_Class":
        return STACK_POINTER_REGISTER

    @abc.override
    def register_classes(self) -> List["Register_Class"]:
        return _ALL_REGISTER_CLASSES_

    @abc.override
    def all_registers(self) -> "Register_Class":
        return ANY_REGISTER

    @abc.override
    def argument_registers(self) -> "Register_Class":
        return ARGUMENT_REGISTER

    @abc.override
    def callee_save_registers(self) -> "Register_Class":
        return CALLEE_SAVED
