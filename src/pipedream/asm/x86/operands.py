"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from pipedream.asm.x86.flags import X86_Flags
from pipedream.asm import ir
from pipedream.utils import abc
from .registers import *

from random import Random

import copy
import typing as ty


__all__ = [
    "X86_Operand",
    "X86_Register_Operand",
    "X86_Immediate_Operand",
    "X86_Flags_Operand",
    "X86_Memory_Operand",
    "X86_Address_Operand",
    "X86_Base_Displacement_Operand",
    "X86_Base_Displacement_Memory_Operand",
    "X86_Base_Displacement_Address_Operand",
    "Imm8",
    "Imm16",
    "Imm32",
    "Imm64",
    "ImmU8",
    "ImmU16",
    "ImmU32",
    "ImmU64",
    "Scale_Imm",
    "Shift_Imm",
]


class X86_Operand(ir.Operand):
    def __init__(self, name: str, visibility: ir.Operand_Visibility):
        self._name = name
        self._visibility = visibility

    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return self._name

    @property  # type: ignore
    @abc.override
    def visibility(self) -> ir.Operand_Visibility:
        return self._visibility


################################################################################
##### REGISTERS


class X86_Register_Operand(X86_Operand, ir.Register_Operand):
    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        use_def: ir.Use_Def,
        reg_class: ir.Register_Class,
        reg: ty.Optional[X86_Register],
    ):
        super().__init__(name, visibility)
        self._use_def = use_def
        self._reg_class = reg_class
        self._reg = reg

    @property  # type: ignore
    @abc.override
    def short_name(self) -> str:
        if self._reg:
            return self._reg.name
        else:
            return self._reg_class.name

    @property  # type: ignore
    @abc.override
    def register_class(self) -> ir.Register_Class:
        return self._reg_class

    @property  # type: ignore
    @abc.override
    def register(self) -> ty.Optional[X86_Register]:
        return self._reg

    @abc.override
    def with_register(self, reg: ir.Register) -> "X86_Register_Operand":
        assert isinstance(reg, X86_Register)

        if reg not in self.register_class:
            raise TypeError(
                f"Register {reg.name} is not a member of {self.register_class.name}"
            )

        return X86_Register_Operand(
            self.name, self.visibility, self.use_def, self.register_class, reg
        )

    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        return self._use_def

    @abc.override
    def _arbitrary(self, random) -> "ir.Register":
        """
        Generate a random register.
        """
        return self._reg_class[0]


class X86_Flags_Operand(X86_Operand, ir.Flags_Operand):
    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        reg: X86_Register,
        flags_read: X86_Flags,
        flags_written: X86_Flags,
    ):
        assert isinstance(flags_read, X86_Flags), flags_read
        assert isinstance(flags_written, X86_Flags), flags_written

        super().__init__(name, visibility)
        self._reg = reg
        self._flags_read = flags_read
        self._flags_written = flags_written

    @property  # type: ignore
    @abc.override
    def is_virtual(self):
        return False

    @property  # type: ignore
    @abc.override
    def flags_read(self) -> X86_Flags:
        return self._flags_read

    @property  # type: ignore
    @abc.override
    def flags_written(self) -> X86_Flags:
        return self._flags_written

    @property  # type: ignore
    @abc.override
    def register(self) -> X86_Register:
        return self._reg

    @property  # type: ignore
    @abc.override
    def short_name(self) -> str:
        return self._reg.name

    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        if self._flags_read and self._flags_written:
            return ir.Use_Def.USE_DEF
        if self._flags_read:
            return ir.Use_Def.USE
        if self._flags_written:
            return ir.Use_Def.DEF

        ## FIXME: should never happen
        # assert False, [self.name, self._reg, self._flags_read, self._flags_written]
        return ir.Use_Def.USE

    def __repr__(self):
        txt = self.name + ":"

        rw = self.flags_read | self.flags_written
        flags = [f for f in X86_Flags if f & rw]

        if flags:
            for f in flags:
                txt += f.name

                if f & self.flags_read:
                    txt += "?"
                if f & self.flags_written:
                    txt += "!"
        else:
            txt += self._reg.name

        return txt

    @abc.override
    def _arbitrary(self, random: Random):
        return None  # immutable


################################################################################
##### IMMEDIATES


class X86_Immediate_Operand(X86_Operand, ir.Immediate_Operand):
    def __init__(self, name: str, visibility: ir.Operand_Visibility, value: int = None):
        super().__init__(name, visibility)

        assert value is None or isinstance(
            value, (int, ir.Label)
        ), f"want int or Label, have {value!r}"

        self._value = value

    @property  # type: ignore
    @abc.override
    def short_name(self) -> str:
        return type(self).__name__.upper()

    @property  # type: ignore
    @abc.override
    def value(self) -> ty.Union[int, float, None]:
        return self._value

    @abc.override
    def with_value(self, value):
        clss = type(self)

        if not self.is_valid_value(value):
            raise TypeError(
                f"{value} is not a valid value for immediates of type {clss}"
            )

        return clss(self.name, self.visibility, value)

    @abc.override
    def arbitrary_value(self, random: Random) -> ty.Any:
        """
        Return an arbitrary concrete value or list of values for this operand.
        Can be passed for instance to with_operand_value.
        """
        return self._arbitrary(random)

    @abc.abstractmethod
    def _arbitrary(self, random) -> ty.Union[int, float]:
        """
        Generate a random int/float that can fit in an immediate of this type.
        """


def _signed_min_max(num_bits: int):
    min = -(2 ** (num_bits - 1))
    max = +(2 ** (num_bits - 1)) - 1
    return min, max


def _unsigned_min_max(num_bits: int):
    min = 0
    max = +(2 ** (num_bits - 1))
    return min, max


class Imm8(X86_Immediate_Operand):
    """
    class of 8 bit signed immediates
    """

    _NUM_BITS = 8
    _min, _max = _signed_min_max(_NUM_BITS)

    @property  # type: ignore
    @abc.override
    def num_bits(self):
        return self._NUM_BITS

    @abc.override
    def is_valid_value(self, value):
        return type(value) is int and self._min <= value <= self._max

    @abc.override
    def _arbitrary(self, random):
        return self._min
        # return random.randint(self._min, self._max)


class ImmU8(X86_Immediate_Operand):
    """
    class of 8 bit unsigned immediates
    """

    _NUM_BITS = 8
    _min, _max = _unsigned_min_max(_NUM_BITS)

    @property  # type: ignore
    @abc.override
    def num_bits(self):
        return self._NUM_BITS

    @abc.override
    def is_valid_value(self, value):
        return type(value) is int and self._min <= value <= self._max

    @abc.override
    def _arbitrary(self, random):
        return self._max
        # return random.randint(self._min, self._max)


class Imm16(X86_Immediate_Operand):
    """
    class of 16 bit signed immediates
    """

    _NUM_BITS = 16
    _min, _max = _signed_min_max(_NUM_BITS)

    @property  # type: ignore
    @abc.override
    def num_bits(self):
        return self._NUM_BITS

    @abc.override
    def is_valid_value(self, value):
        if type(value) is ir.Label:
            return True

        if type(value) is int:
            return self._min <= value <= self._max

        return False

    @abc.override
    def _arbitrary(self, random):
        return self._min
        # return random.randint(self._min, self._max)


class ImmU16(X86_Immediate_Operand):
    """
    class of 16 bit unsigned immediates
    """

    _NUM_BITS = 16
    _min, _max = _unsigned_min_max(_NUM_BITS)

    @property  # type: ignore
    @abc.override
    def num_bits(self):
        return self._NUM_BITS

    @abc.override
    def is_valid_value(self, value):
        if type(value) is ir.Label:
            return True

        if type(value) is int:
            return self._min <= value <= self._max

        return False

    @abc.override
    def _arbitrary(self, random):
        return self._max
        # return random.randint(self._min, self._max)


class Imm32(X86_Immediate_Operand):
    """
    class of 32 bit signed immediates
    """

    _NUM_BITS = 32
    _min, _max = _signed_min_max(_NUM_BITS)

    @property  # type: ignore
    @abc.override
    def num_bits(self):
        return self._NUM_BITS

    @abc.override
    def is_valid_value(self, value):
        if type(value) is ir.Label:
            return True

        if type(value) is int:
            return self._min <= value <= self._max

        return False

    @abc.override
    def _arbitrary(self, random):
        return self._min
        # return random.randint(self._min, self._max)


class ImmU32(X86_Immediate_Operand):
    """
    class of 32 bit unsigned immediates
    """

    _NUM_BITS = 32
    _min, _max = _unsigned_min_max(_NUM_BITS)

    @property  # type: ignore
    @abc.override
    def num_bits(self):
        return self._NUM_BITS

    @abc.override
    def is_valid_value(self, value):
        if type(value) is ir.Label:
            return True

        if type(value) is int:
            return self._min <= value <= self._max

        return False

    @abc.override
    def _arbitrary(self, random):
        return self._max
        # return random.randint(self._min, self._max)


class Imm64(X86_Immediate_Operand):
    """
    class of 64 bit signed immediates
    """

    _NUM_BITS = 64
    _min, _max = _signed_min_max(_NUM_BITS)

    @property  # type: ignore
    @abc.override
    def num_bits(self):
        return self._NUM_BITS

    @abc.override
    def is_valid_value(self, value):
        if type(value) is ir.Label:
            return True

        if type(value) is int:
            return self._min <= value <= self._max

        return False

    @abc.override
    def _arbitrary(self, random):
        return self._min
        # return random.randint(self._min, self._max)


class ImmU64(X86_Immediate_Operand):
    """
    class of 64 bit unsigned immediates
    """

    _NUM_BITS = 64
    _min, _max = _unsigned_min_max(_NUM_BITS)

    @property  # type: ignore
    @abc.override
    def num_bits(self):
        return self._NUM_BITS

    @abc.override
    def is_valid_value(self, value):
        if type(value) is ir.Label:
            return True

        if type(value) is int:
            return self._min <= value <= self._max

        return False

    @abc.override
    def _arbitrary(self, random):
        return self._max
        # return random.randint(self._min, self._max)


class Scale_Imm(X86_Immediate_Operand):
    """
    Immediate for `scale` operand of a memory access in X86.
    """

    @property  # type: ignore
    @abc.override
    def short_name(self) -> str:
        return "SCALE"

    @abc.override
    def is_valid_value(self, value):
        return type(value) is int and value in (1, 2, 4, 8)

    @abc.override
    def _arbitrary(self, random):
        return 1
        # return 2 ** random.randint(0, 3)


class Shift_Imm(X86_Immediate_Operand):
    """
    Immediate operand for a shit instruction.
    """

    _min = 0
    _max = 31

    @property  # type: ignore
    @abc.override
    def short_name(self) -> str:
        return "SHIFT"

    @abc.override
    def is_valid_value(self, value):
        return type(value) is int and self._min <= value <= self._max

    @abc.override
    def _arbitrary(self, random):
        return 3
        # return random.randint(self._min, self._max)


################################################################################
##### MEMORY


class X86_Memory_Or_Address_Operand(X86_Operand, ir.Composite_Operand):
    """
    Mixin for defining memory & address operands
    """

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        address_width: int,
    ):
        super().__init__(name, visibility)

        assert type(address_width) is int
        assert address_width in (16, 32, 64)

        # how many bits are in address (16/32/64)
        self._address_width = address_width

    @property
    def address_width(self) -> int:
        return self._address_width

    @property  # type: ignore
    @abc.override
    def is_virtual(self) -> bool:
        return any(op.is_virtual for op in self.sub_operands)

    @abc.override
    def update_sub_operand(
        self, idx_or_name: ty.Union[int, str], fn: ty.Callable[[ir.Operand], ir.Operand]
    ):
        name: str
        if isinstance(idx_or_name, int):
            name = self.get_operand_name(idx_or_name)  # type:ignore
        else:
            name = idx_or_name
        new = copy.copy(self)
        sub = fn(getattr(new, f"_{name}"))
        assert isinstance(sub, ir.Operand)
        setattr(new, f"_{name}", sub)
        return new


class X86_Memory_Operand(X86_Memory_Or_Address_Operand, ir.Memory_Operand):
    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        use_def: ir.Use_Def,
        address_width: int,
        memory_width: int,
    ):
        super().__init__(name, visibility, address_width)

        assert type(use_def) is ir.Use_Def
        assert type(memory_width) is int

        # how many bits are loaded/stored
        self._memory_width = memory_width

        self._use_def = use_def

    @property  # type: ignore
    @abc.override
    def memory_width(self) -> int:
        return self._memory_width

    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        return self._use_def


class X86_Address_Operand(X86_Memory_Or_Address_Operand, ir.Address_Operand):
    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        return ir.Use_Def.USE


class X86_Base_Displacement_Operand(ir.Base_Displacement_Operand):
    """
    Mixin for defining base/displacement operands
    """

    def __init__(
        self,
        base: X86_Register_Operand,
        displacement: X86_Immediate_Operand,
    ):
        self._base = base
        self._displacement = displacement

    @property  # type: ignore
    def base(self) -> ir.Register_Operand:
        return self._base

    @property  # type: ignore
    def displacement(self) -> ir.Immediate_Operand:
        return self._displacement

    def with_base(self, base_reg: ir.Register) -> "X86_Base_Displacement_Operand":
        new = copy.copy(self)
        new._base = new._base.with_register(base_reg)
        return new

    def with_displacement(
        self, disp_reg: ty.Union[int, ir.Label, ir.Register]
    ) -> ir.Base_Displacement_Operand:
        new = copy.copy(self)
        new._displacement = new._displacement.with_value(disp_reg)
        ret = ty.cast(ir.Base_Displacement_Operand, new)
        return ret

    @abc.abstractproperty  # type: ignore
    def _short_short_name(self) -> str:
        pass

    @property  # type: ignore
    @abc.override
    def short_name(self) -> str:
        return (
            f"{self._short_short_name}BD{self.address_width}_"  # type: ignore
            f"{self.memory_width}"  # type: ignore
        )

    @property  # type: ignore
    @abc.override
    def sub_operands(self):
        yield self._base
        yield self._displacement

    def get_operand_name(self, idx: int) -> str:
        return {
            0: "base",
            1: "displacement",
        }[idx]

    def __repr__(self):
        tmp = [
            self.name,
            ":",
            self._short_short_name,
            "BD",
            str(self.address_width),
            "/",
        ]
        if hasattr(self, "memory_width"):
            tmp += [
                str(self.memory_width),
                "/",
            ]
        tmp += ["(", repr(self.base), ", ", repr(self.displacement), ")"]

        return "".join(tmp)

    @abc.override
    def _arbitrary(self, random: Random):
        raise NotImplementedError()


class X86_Base_Displacement_Memory_Operand(
    X86_Base_Displacement_Operand,
    X86_Memory_Operand,
    ir.Base_Displacement_Memory_Operand,
):
    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        use_def: ir.Use_Def,
        address_width: int,
        memory_width: int,
        base: X86_Register_Operand,
        displacement: X86_Immediate_Operand,
    ):
        X86_Memory_Operand.__init__(
            self, name, visibility, use_def, address_width, memory_width
        )
        X86_Base_Displacement_Operand.__init__(self, base, displacement)

    @property  # type: ignore
    @abc.override
    def _short_short_name(self) -> str:
        return "mem"


class X86_Base_Displacement_Address_Operand(
    X86_Base_Displacement_Operand,
    X86_Address_Operand,
    ir.Base_Displacement_Address_Operand,
):
    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        address_width: int,
        base: X86_Register_Operand,
        displacement: X86_Immediate_Operand,
    ):
        X86_Address_Operand.__init__(self, name, visibility, address_width)
        X86_Base_Displacement_Operand.__init__(
            ty.cast(X86_Base_Displacement_Operand, self), base, displacement
        )

    @property  # type: ignore
    @abc.override
    def _short_short_name(self) -> str:
        return "addr"
