"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from pipedream.utils import *
from pipedream.utils import abc

import math
import random
import typing as ty

import pipedream.asm.ir as ir

__all__ = [
    "Mem_Access_Regs",
    "Register_Allocator",
    "Memory_Arena_Allocator",
    "Memory_Arena_Punctual_Allocator",
    "Memory_Arena_Round_Robin_Allocator",
    "Minimize_Deps_Register_Allocator",
    "Minimize_Deps_Pooled_Register_Allocator",
    "Maximize_Deps_Register_Allocator",
]

LValue = RValue = None

# Structure containing information about registers used for memory access,
# with the following semantic:
# `attribute` -> `No_Need` => `attibute` will not be used in the benchmark
# `attribute` -> `None` => `attribute` is mandatory for the program to run, but
# `attribute` is not currently in a register / has a value
# `attribute` -> `val` => `attribute` is `val` (immediate) or is stored in reg `val`
# (register)
#
# With `attribute` in:
# - `pure_ld`: register used for pure loads (no disp/offset)
# - `pure_st`: register used for pure stores (no disp/offset)
# - `base_area`: register used for imm disp and reg offset loads and stores
# - `cst_disp_ld`: constant value when using `pure_ld` for reg + imm disp loads
# - `cst_disp_st`: constant value when using `pure_st` for reg + imm disp stores
# - `cst_disp_reg_off_ld`: constant value when using `base_area` for reg + imm disp loads
# - `cst_disp_reg_off_st`: constant value when using `base_area` for reg + imm disp stores
# - `reg_off_ld`: register added to `base_area` for reg + reg offset loads
# - `reg_off_st`: register added to `base_area` for reg + reg offset stores
# Note: when memory is both read and written (RW), this must be accessed/allocated as a store (W)
class Mem_Access_Regs:
    class No_Need:
        def __bool__(self):
            return False

    def __init__(self):
        self.pure_ld: ty.Union[
            "Mem_Access_Regs.No_Need", ty.Optional[Register]
        ] = self.No_Need()
        self.pure_st: tr.Union[
            "Mem_Access_Regs.No_Need", ty.Optional[Register]
        ] = self.No_Need()
        self.base_area: tr.Union[
            "Mem_Access_Regs.No_Need", ty.Optional[Register]
        ] = self.No_Need()
        self.cst_disp_ld: tr.Union["Mem_Access_Regs.No_Need", ty.Optional[int]] = None
        self.cst_disp_st: tr.Union["Mem_Access_Regs.No_Need", ty.Optional[int]] = None
        self.cst_disp_reg_off_ld: tr.Union[
            "Mem_Access_Regs.No_Need", ty.Optional[int]
        ] = None
        self.cst_disp_reg_off_st: tr.Union[
            "Mem_Access_Regs.No_Need", ty.Optional[int]
        ] = None
        self.reg_off_ld: tr.Union[
            "Mem_Access_Regs.No_Need", ty.Optional[Register]
        ] = self.No_Need()
        self.reg_off_st: tr.Union[
            "Mem_Access_Regs.No_Need", ty.Optional[Register]
        ] = self.No_Need()

    def used_regs(self):
        if not isinstance(self.pure_ld, self.No_Need):
            yield self.pure_ld
        if not isinstance(self.pure_st, self.No_Need):
            yield self.pure_st
        if not isinstance(self.base_area, self.No_Need):
            yield self.base_area
        if not isinstance(self.reg_off_ld, self.No_Need):
            yield self.reg_off_ld
        if not isinstance(self.reg_off_st, self.No_Need):
            yield self.reg_off_st


class CacheAllocator:
    """
    Simple view of a cache. Following a round-robin policy.
    Only the first entries in the cache sets are used, such that
    there can be no cache conflict unless when wrapping around.
    """

    def __init__(self, size, cache_config, global_offset=0):
        assert size <= cache_config.size
        size = max(size, cache_config.cacheline_size)
        overhead = size % cache_config.cacheline_size
        if overhead:
            size = size - overhead
        self.base = 0
        self.offset = 0
        self.global_offset = int(global_offset)
        self.size = size
        self.cache_config = cache_config
        self.nb_sets = max(
            1,
            int(
                size
                / (self.cache_config.cacheline_size * self.cache_config.associativity)
            ),
        )

    def allocate(self, size):
        ret = self.current_offset()
        assert self.cache_config.cacheline_size % size == 0
        self.offset += self.cache_config.cacheline_size
        self.offset %= int(self.cache_config.cacheline_size * self.nb_sets)
        return ret

    def current_offset(self):
        return self.base + self.offset + self.global_offset


class Memory_Arena_Allocator(abc.ABC):
    """
    Abstract base class for Memory Arena Allocators.
    The concret classes implement strategies for allocating memory element
    on load/store memory operands for a list of instructions.
    Basically the size of the arenas and the offsets
    of new allocated elements into these arenas.
    """

    # An architecture independant maximum for the arenas
    # will be the value allocated for each of the load and store arena
    MAX_ARENA_SIZE = 64 * (1 << 10)
    OFFSET_MEMORY = 16 * (1 << 10)
    LD_TO_STORE_OFFSET = MAX_ARENA_SIZE // 2

    @abc.abstractproperty
    def load_alloc_size(self) -> int:
        """
        Return the necessary allocation size for load memory operands.
        """

    @abc.abstractproperty
    def store_alloc_size(self) -> int:
        """
        Return the necessary allocation size for store memory operands.
        """

    @abc.abstractmethod
    def load_allocate(self, elt_size: int, max_val: int, valid: bool = True) -> int:
        """
        Return the current load arena offset and allocate the elt_size.
        If valid is false the arena offset is reset to 0.
        """

    @abc.abstractmethod
    def store_allocate(self, elt_size: int, max_val: int, valid: bool = True) -> int:
        """
        Return the current store arena offset and allocate the elt_size.
        If valid is false the arena offset is reset to 0.
        """

    @abc.abstractproperty
    def load_disp(self) -> int:
        """
        Return the current load arena offset.
        """

    @abc.abstractproperty
    def store_disp(self) -> int:
        """
        Return the current store arena offset.
        """


class Memory_Arena_Punctual_Allocator(Memory_Arena_Allocator):
    """
    Punctual Arena, all load to the same address, all stores to the same address.
    """

    _MAX_MEMWIDTH = 4096  # Just make sure the biggest machine load/store is supported.

    def __init__(self) -> None:
        pass

    @property  # type: ignore
    @abc.override
    def load_alloc_size(self) -> int:
        return self._MAX_MEMWIDTH

    @property  # type: ignore
    @abc.override
    def store_alloc_size(self) -> int:
        return self._MAX_MEMWIDTH

    @abc.override
    def load_allocate(self, elt_size: int, max_val: int, valid: bool = True) -> int:
        assert elt_size <= self._MAX_MEMWIDTH
        return 0

    @abc.override
    def store_allocate(self, elt_size: int, max_val: int, valid: bool = True) -> int:
        assert elt_size <= self._MAX_MEMWIDTH
        return 0

    @property  # type: ignore
    @abc.override
    def load_disp(self) -> int:
        return 0

    @property  # type: ignore
    @abc.override
    def store_disp(self) -> int:
        return 0


class Memory_Arena_Round_Robin_Allocator(Memory_Arena_Allocator):
    """
    Round robin allocator, within an optional maximal size.
    Ensure in addition that memory elements are correctly spread in L1
    cache.
    """

    def __init__(self, l1_cache_config: ir.Cache_Config, max_size: int = None) -> None:
        size = min(l1_cache_config.size // 2, self.MAX_ARENA_SIZE // 2)
        if max_size is not None:
            size = min(size, max_size)
        self.mem_load = CacheAllocator(size, l1_cache_config)
        self.mem_store = CacheAllocator(size, l1_cache_config)

    @property  # type: ignore
    @abc.override
    def load_alloc_size(self) -> int:
        return self.mem_load.size

    @property  # type: ignore
    @abc.override
    def store_alloc_size(self) -> int:
        return self.mem_store.size

    @abc.override
    def load_allocate(
        self, elt_size: int, max_val: int = None, valid: bool = True
    ) -> int:
        if not valid:
            self.mem_load.offset = 0
        offset = (
            self.mem_load.allocate(elt_size) % max_val
            if max_val is not None
            else self.mem_load.allocate(elt_size)
        )
        assert offset + elt_size <= self.load_alloc_size
        return offset

    @abc.override
    def store_allocate(
        self, elt_size: int, max_val: int = None, valid: bool = True
    ) -> int:
        if not valid:
            self.mem_store.offset = 0
        offset = (
            self.mem_store.allocate(elt_size) % max_val
            if max_val is not None
            else self.mem_store.allocate(elt_size)
        )
        assert offset + elt_size <= self.store_alloc_size
        return offset

    @property  # type: ignore
    @abc.override
    def load_disp(self) -> int:
        return self.mem_load.current_offset()

    @property  # type: ignore
    @abc.override
    def store_disp(self) -> int:
        return self.mem_store.current_offset()


class Register_Allocator(abc.ABC):
    """
    Base class for all register allocators.
    These are not really register allocators in the classical sense,
    they just randomly assign registers to instructions in a way that either
    creates or avoid data dependencies (depending on the sub-class).
    """

    USE = ir.Use_Def.USE
    DEF = ir.Use_Def.DEF
    USE_DEF = ir.Use_Def.USE_DEF

    def allocate(
        self, instructions: ty.List[ir.Instruction], irb: ir.IR_Builder
    ) -> ty.List[ir.Instruction]:
        """
        Allocate all unallocated operands (i.e. register operands with no register assigned)
        for all instructions in *instructions*.
        Returns a list with the updated instructions, the input list is not modified.
        """
        return [self._allocate_inst(i) for i in instructions]

    def _allocate_inst(self, inst: ir.Instruction) -> ir.Instruction:
        for idx, op in enumerate(inst.operands):
            # Update of an operand may allocate several operands at once
            if not inst.operands[idx].is_virtual:
                continue
            inst = inst.update_operand(idx, self._make_op)
        return inst

    def _make_op(self, op):
        if isinstance(op, ir.Immediate_Operand):
            return self._make_immediate_operand(op)

        if isinstance(op, ir.Register_Operand):
            return self._make_register_operand(op)

        if isinstance(op, ir.Base_Displacement_Address_Operand):
            return self._make_base_displacement_address_operand(op)

        if isinstance(op, ir.Base_Memory_Operand):
            return self._make_base_memory_operand(op)

        if isinstance(op, ir.Base_Displacement_Memory_Operand):
            return self._make_base_displacement_memory_operand(op)

        if isinstance(op, ir.Base_Offset_Memory_Operand):
            return self._make_base_offset_memory_operand(op)

        raise NotImplementedError(op)


class Punctual_Register_Allocator(Register_Allocator):
    """
    Punctual allocator which simply select arbitray values for
    operands. No specific dependency assumption or correctness is
    given, each instruction is arbitrary allocated independently.
    The resulting instructions are not supposed to be executable
    as allocation of memory pointers for instance may be arbitrary.
    The principal usage of this allocator is to generate
    arbitray instructions for testing or generating
    instruction for a disassembler.
    """

    def __init__(
        self,
        regs: ty.Sequence[ir.Register],
        regs_ro: ty.Sequence[ir.Register] = (),
        random: random.Random = None,
    ) -> None:
        self.random = random
        all_regs, ro_regs = list(regs), list(regs_ro)
        self.alloc = ir.Register_Liveness_Tracker(
            all_registers=all_regs, read_only_registers=ro_regs
        )

    def _allocate_inst(self, inst: ir.Instruction) -> ir.Instruction:
        self.alloc.free_all()
        for idx, op in enumerate(inst.operands):
            # Update of an operand may allocate several operands at once
            if not inst.operands[idx].is_virtual:
                continue
            if not (isinstance(op, ir.Register_Operand) and op.is_def):
                continue
            inst = inst.update_operand(idx, self._make_op)

        for idx, op in enumerate(inst.operands):
            # Update of an operand may allocate several operands at once
            if not inst.operands[idx].is_virtual:
                continue
            inst = inst.update_operand(idx, self._make_op)
        return inst

    def _allocate_reg(self, optype: ir.Register_Class, is_use: bool):
        try:
            reg = self.alloc.take_any(optype, can_take_ro=is_use)
        except ir.Allocation_Error as e:
            if not is_use:
                raise e
            reg = self.alloc.free_all()
            reg = self.alloc.take_any(optype, can_take_ro=is_use)
        return reg

    def _make_immediate_operand(self, op):
        assert isinstance(op, ir.Immediate_Operand)
        val = op.arbitrary_value(self.random)
        return op.with_value(val)

    def _make_register_operand(self, op):
        assert isinstance(op, ir.Register_Operand)
        reg = self._allocate_reg(op.register_class, op.use_def is self.USE)
        return op.with_register(reg)

    def _make_base_memory_operand(self, op):
        assert isinstance(op, ir.Base_Memory_Operand)
        base = self._allocate_reg(op.base.register_class, op.use_def is self.USE)
        return op.with_base(base)

    def _make_base_offset_memory_operand(self, op):
        assert isinstance(op, ir.Base_Offset_Memory_Operand)
        base = self._allocate_reg(op.base.register_class, op.use_def is self.USE)
        disp = self._allocate_reg(op.offset.register_class, op.use_def is self.USE)
        return op.with_base(base).with_offset(disp)

    def _make_base_displacement_memory_operand(self, op):
        assert isinstance(op, ir.Base_Displacement_Memory_Operand)
        base = self._allocate_reg(op.base.register_class, op.use_def is self.USE)
        disp = op.displacement.arbitrary_value(self.random)
        return op.with_base(base).with_displacement(disp)

    def _make_base_displacement_address_operand(self, op):
        assert isinstance(op, ir.Base_Displacement_Address_Operand)
        base = self._allocate_reg(op.base.register_class, op.use_def is self.USE)
        disp = op.displacement.arbitrary_value(self.random)
        return op.with_base(base).with_displacement(disp)


class Register_Memory_Allocator(Register_Allocator):
    """
    Abstract base class for registers allocator managment of memory arenas.
    """

    def __init__(self, memory_regs: Mem_Access_Regs):
        self.memory_regs = memory_regs
        self.random = random.Random("look, I'm sooo random")

    def _make_immediate_operand(self, op):
        assert isinstance(op, ir.Immediate_Operand)
        val = op._arbitrary(self.random)
        return op.with_value(val)

    def _make_base_memory_operand(self, op):
        assert isinstance(op, ir.Base_Memory_Operand)
        assert self.memory_regs is not None
        if op.is_def:
            assert self.memory_regs.pure_st
            return ty.cast(ir.Operand, op.with_base(self.memory_regs.pure_st))
        if op.is_use:
            assert self.memory_regs.pure_ld
            return ty.cast(ir.Operand, op.with_base(self.memory_regs.pure_ld))

    def _make_base_offset_memory_operand(self, op):
        assert isinstance(op, ir.Base_Offset_Memory_Operand)
        assert self.memory_regs.base_area
        if op.is_def:
            assert self.memory_regs.reg_off_st
            return ty.cast(
                ir.Operand,
                op.with_base(self.memory_regs.base_area).with_offset(
                    self.memory_regs.reg_off_st
                ),
            )
        else:
            assert self.memory_regs.reg_off_ld
            return ty.cast(
                ir.Operand,
                op.with_base(self.memory_regs.base_area).with_offset(
                    self.memory_regs.reg_off_ld
                ),
            )

    def _make_base_displacement_memory_operand(self, op):
        assert isinstance(op, ir.Base_Displacement_Memory_Operand)
        assert self.memory_regs is not None
        if self.memory_regs.pure_st and op.is_def:
            base_reg = self.memory_regs.pure_st
            disp = self.memory_regs.cst_disp_st
        elif self.memory_regs.pure_ld and op.is_use:
            base_reg = self.memory_regs.pure_ld
            disp = self.memory_regs.cst_disp_ld
        elif self.memory_regs.base_area:
            base_reg = self.memory_regs.base_area
            if op.is_def:
                disp = self.memory_regs.cst_disp_reg_off_st
            else:
                disp = self.memory_regs.cst_disp_reg_off_ld
        else:
            raise Exception("No memory reg provided for memory op!")
        assert disp <= op.displacement._max
        base_reg = base_reg.as_width(op.address_width)
        if op.is_def:
            current = self.memory_allocator.store_disp
        else:
            current = self.memory_allocator.load_disp
        valid = op.displacement.is_valid_value(disp + current)
        if op.is_def:
            current = self.memory_allocator.store_allocate(
                op.memory_width // 8, op.displacement._max - disp, valid=valid
            )
        else:
            current = self.memory_allocator.load_allocate(
                op.memory_width // 8, op.displacement._max - disp, valid=valid
            )
        return op.with_base(base_reg).with_displacement(disp + current)

    def _make_base_displacement_address_operand(self, op):
        assert isinstance(op, ir.Base_Displacement_Address_Operand)
        base = self._allocate_reg(op.base.register_class, op.use_def is self.USE)
        disp = op.displacement._arbitrary(self.random) + self.memory_regs.cst_disp_ld

        return op.with_base(base).with_displacement(disp)


class Maximize_Deps_Register_Allocator(Register_Memory_Allocator):
    """
    Allocator that tries to maximize flow/true deps in output code.
    I.e. create code that executes completely sequential.
    It still creates anti-dependences.
    """

    def __init__(
        self,
        regs: ty.List[ir.Register],
        memory_regs: Mem_Access_Regs,
        memory_allocator: Memory_Arena_Allocator = None,
        regs_ro: ty.Sequence[ir.Register] = (),
    ):
        self.regs = tuple(list(regs))
        self.regs_ro = tuple(list(regs_ro))

        assert self.regs, "allocator initialized with zero available registers?"
        assert all(r not in regs for r in memory_regs.used_regs())

        self.alloc = ir.Register_Liveness_Tracker(self.regs, self.regs_ro)
        self.cache: ty.Dict[ir.Register_Class, ir.Register] = {}
        self.memory_allocator = memory_allocator
        self.defs: ty.List[ir.Register] = []
        super().__init__(memory_regs=memory_regs)

    @abc.override
    def _allocate_inst(self, inst: ir.Instruction) -> ir.Instruction:
        self.defs = []
        for idx, op in enumerate(inst.operands):
            # Update of an operand may allocate several operands at once
            if not inst.operands[idx].is_virtual:
                continue
            if not (isinstance(op, ir.Register_Operand) and op.is_def):
                continue
            inst = inst.update_operand(idx, self._make_op)
        for reg in self.defs[1:]:
            self.alloc.free(reg)
        for idx, op in enumerate(inst.operands):
            # Update of an operand may allocate several operands at once
            if not inst.operands[idx].is_virtual:
                continue
            inst = inst.update_operand(idx, self._make_op)
        return inst

    @abc.override
    def _make_op(self, op):
        if isinstance(op, ir.Immediate_Operand):
            return self._make_immediate_operand(op)

        if isinstance(op, ir.Register_Operand):
            usedef = op.use_def
            regclass = op.register_class

            if usedef is self.USE:
                return op.with_register(self._allocate_reg(regclass, True))

            assert usedef in (self.DEF, self.USE_DEF)

            return op.with_register(self._allocate_reg(regclass, False))

        if isinstance(op, ir.Base_Displacement_Address_Operand):
            return self._make_base_displacement_address_operand(op)

        if isinstance(op, ir.Base_Memory_Operand):
            return self._make_base_memory_operand(op)

        if isinstance(op, ir.Base_Displacement_Memory_Operand):
            return self._make_base_displacement_memory_operand(op)

        if isinstance(op, ir.Base_Offset_Memory_Operand):
            return self._make_base_offset_memory_operand(op)

        raise NotImplementedError(op)

    def _allocate_reg(self, optype: ir.Register_Class, is_use: bool):
        if not is_use:
            return self._allocate_def(optype)
        return self._allocate_one(optype, is_use)

    def _allocate_def(self, optype: ir.Register_Class):
        if len(self.defs) == 0:
            reg = self._allocate_one(optype, False)
        else:
            reg = self.alloc.take_any(optype, False)
        self.defs.append(reg)
        return reg

    def _allocate_one(self, optype: ir.Register_Class, is_use: bool):
        try:
            reg = self.cache[optype]
        except KeyError:
            # check if we can reuse an aliasing register
            for allocated in self.cache.values():
                for reg in iter(optype):
                    if allocated in reg.aliases:
                        self.cache[optype] = reg
                        return reg
            # allocate a new register
            reg = self.cache[optype] = self.alloc.take_any(optype, is_use)
        return reg


class Minimize_Deps_Register_Allocator_Abstract(Register_Memory_Allocator):
    ALIGNMENT = 64  # TODO take cache size instead
    """
    Allocator that tries to minimize flow/true deps in output code.
    I.e. create code that is as parallel as possible.
    It still creates anti-dependences.
    """

    def __init__(
        self,
        memory_regs: Mem_Access_Regs,
        memory_allocator: Memory_Arena_Allocator,
        insert_dependency_breakers: bool = False,
        regs_ro: ty.Sequence[ir.Register] = (),
    ):
        self.insert_dependency_breakers = insert_dependency_breakers
        self.memory_allocator = memory_allocator
        self.regs_ro = tuple(iter(regs_ro))

        super().__init__(memory_regs=memory_regs)

    @abc.override
    def allocate(
        self, instructions: ty.List[ir.Instruction], irb: ir.IR_Builder
    ) -> ty.List[ir.Instruction]:
        out = []

        insert_dependency_breakers = self.insert_dependency_breakers

        for i in instructions:
            if insert_dependency_breakers:
                for op in i.operands:
                    needs_breaker = False
                    if (
                        isinstance(op, ir.Register_Operand)
                        and op.is_def
                        and len(op.register_class) == 1
                    ):
                        needs_breaker = True
                    if needs_breaker:
                        assert isinstance(op, ir.Register_Operand)
                        out += irb.emit_dependency_breaker(op.register_class[0])

            i = self._allocate_inst(i)
            out.append(i)

        return out

    @abc.override
    def _allocate_inst(self, inst: ir.Instruction) -> ir.Instruction:
        for idx, op in enumerate(inst.operands):
            # Update of an operand may allocate several operands at once
            if not inst.operands[idx].is_virtual:
                continue
            if not (isinstance(op, ir.Register_Operand) and op.is_def):
                continue
            inst = inst.update_operand(idx, self._make_op)
        for idx, op in enumerate(inst.operands):
            # Update of an operand may allocate several operands at once
            if not inst.operands[idx].is_virtual:
                continue
            inst = inst.update_operand(idx, self._make_op)
        return inst

    @abc.abstractmethod
    def make_reg_op(self, op) -> ir.Operand:
        ...

    @abc.override
    def _make_op(self, op) -> ir.Operand:
        if isinstance(op, ir.Immediate_Operand):
            return self._make_immediate_operand(op)

        if isinstance(op, ir.Base_Memory_Operand):
            return self._make_base_memory_operand(op)

        if isinstance(op, ir.Base_Displacement_Memory_Operand):
            return self._make_base_displacement_memory_operand(op)

        if isinstance(op, ir.Base_Offset_Memory_Operand):
            return self._make_base_offset_memory_operand(op)

        return self.make_reg_op(op)


class Minimize_Deps_Register_Allocator(Minimize_Deps_Register_Allocator_Abstract):
    def __init__(
        self,
        regs: ty.List[ir.Register],
        memory_regs: Mem_Access_Regs,
        memory_allocator: Memory_Arena_Allocator,
        insert_dependency_breakers: bool = False,
        regs_ro: ty.Sequence[ir.Register] = (),
    ):
        self.cache: ty.Dict[ir.Register_Class, ir.Register] = {}
        self.regs = tuple(iter(regs))
        assert self.regs, "allocator initialized with zero available registers?"
        assert all(r not in regs for r in memory_regs.used_regs())
        self.alloc = ir.Register_Liveness_Tracker(self.regs)

        super().__init__(
            memory_regs=memory_regs,
            memory_allocator=memory_allocator,
            insert_dependency_breakers=insert_dependency_breakers,
            regs_ro=regs_ro,
        )

    def _allocate_src(self, optype: ir.Register_Class):
        try:
            return self.cache[optype]
        except KeyError:
            reg = self.cache[optype] = self._allocate_reg(optype, True)
            return reg

    def _allocate_dst(self, optype: ir.Register_Class):
        return self._allocate_reg(optype, False)

    def _allocate_reg(self, optype: ir.Register_Class, can_take_ro):
        try:
            return self.alloc.take_any(optype, can_take_ro)
        except ir.Allocation_Error:
            ## free all registers and carry on
            ## TODO: add dependency breaker on newly added register
            self.alloc = ir.Register_Liveness_Tracker(self.regs, self.regs_ro)
            self.cache.clear()
            return self.alloc.take_any(optype, can_take_ro)

    @abc.override
    def make_reg_op(self, op) -> ir.Operand:
        if isinstance(op, ir.Register_Operand):
            usedef = op.use_def
            regclass = op.register_class

            if usedef is self.USE:
                return op.with_register(self._allocate_src(regclass))

            assert usedef in (self.DEF, self.USE_DEF)

            return op.with_register(self._allocate_dst(regclass))

        if isinstance(op, ir.Base_Displacement_Address_Operand):
            base = self._allocate_src(op.base.register_class)
            disp = op.displacement._arbitrary(self.random)

            return ty.cast(ir.Operand, op.with_base(base).with_displacement(disp))

        raise NotImplementedError(op)


class Minimize_Deps_Pooled_Register_Allocator(
    Minimize_Deps_Register_Allocator_Abstract
):
    """
    Enriched version of the Minimize_Deps_Register_Allocator that allocates from
    operands from a precomputed pool of registers.
    """

    def __init__(
        self,
        reg_pools: ty.Dict[ir.Register_Operand, ty.FrozenSet[ir.Register]],
        memory_regs: Mem_Access_Regs,
        memory_allocator: Memory_Arena_Allocator,
        insert_dependency_breakers: bool = False,
        regs_ro: ty.Sequence[ir.Register] = (),
    ):
        self.reg_pools = reg_pools
        self.cache: ty.Dict[
            ty.FrozenSet[ir.Register], ty.Dict[ir.Register_Class, ir.Register]
        ] = {}
        self.alloc: ty.Dict[ir.Register_Operand, ir.Register_Liveness_Tracker] = {}
        allocs: ty.Dict[ty.FrozenSet[ir.Register], ir.Register_Liveness_Tracker] = {}
        for op, pool in self.reg_pools.items():
            assert all(r not in pool for r in memory_regs.used_regs())
            assert len(pool) > 0, (
                "allocator initialized with zero available" f"registers? (op {op})"
            )
            if allocs.get(pool) is None:
                allocs[pool] = ir.Register_Liveness_Tracker(list(pool))
                self.cache[pool] = {}
            self.alloc[op] = allocs[pool]
        super().__init__(
            memory_regs=memory_regs,
            memory_allocator=memory_allocator,
            insert_dependency_breakers=insert_dependency_breakers,
            regs_ro=regs_ro,
        )

    def _allocate_srcop(self, op: ir.Register_Operand):
        try:
            return self.cache[self.reg_pools[op]][op.register_class]
        except KeyError:
            reg = self.cache[self.reg_pools[op]][
                op.register_class
            ] = self._allocate_regop(op, True)
            return reg

    def _allocate_dstop(self, op: ir.Register_Operand):
        return self._allocate_regop(op, False)

    def _allocate_regop(
        self, op: ir.Register_Operand, can_take_ro: bool
    ) -> ir.Register:
        try:
            return self.alloc[op].take_any(op.register_class, can_take_ro)
        except ir.Allocation_Error:
            ## free all registers and carry on
            ## TODO: add dependency breaker on newly added register
            self.alloc[op].free_all()
            self.cache[self.reg_pools[op]].clear()
            return self.alloc[op].take_any(op.register_class, can_take_ro)

    @abc.override
    def make_reg_op(self, op) -> ir.Operand:
        if isinstance(op, ir.Register_Operand):
            usedef = op.use_def

            if usedef is self.USE:
                return op.with_register(self._allocate_srcop(op))

            assert usedef in (self.DEF, self.USE_DEF)

            return op.with_register(self._allocate_dstop(op))

        if isinstance(op, ir.Base_Displacement_Address_Operand):
            base = self._allocate_srcop(op.base)
            disp = op.displacement._arbitrary(self.random)

            return ty.cast(ir.Operand, op.with_base(base).with_displacement(disp))

        raise NotImplementedError(op)
