"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import collections
import ctypes
import copy
import enum
import functools
import types
import typing as ty
import math
from random import Random
from dataclasses import dataclass

from pipedream.utils import abc, Sentinel
from pipedream.asm.asmwriter import ASM_Writer


__all__ = [
    "Architecture",
    "Tools_Config",
    "Cache_Config",
    "Instruction_Set",
    "Register_Set",
    "IR_Builder",
    "Allocation_Error",
    "Register_Liveness_Tracker",
    "Asm_Dialect",
    "Instruction",
    "Machine_Instruction",
    "Operand",
    "Use_Def",
    "Operand_Visibility",
    "Register_Operand",
    "Immediate_Operand",
    "ImmMinMax_Operand",
    "Shifted_Immediate_Operand",
    "Memory_Operand",
    "Address_Operand",
    "Base_Operand",
    "Base_Displacement_Operand",
    "Base_Displacement_Address_Operand",
    "Base_Displacement_Memory_Operand",
    "Label_Operand",
    "Register",
    "Register_Class",
    "Label",
    "Loop",
]


class Architecture(abc.ABC):
    _REGISTRY_: ty.Dict[str, "Architecture"] = {}

    class NotEnoughRegisters(Exception):
        pass

    @classmethod
    def for_name(cls, name: str) -> "Architecture":
        arch = cls._REGISTRY_.get(name)
        if arch is None:
            if name == "ARMv8a":
                import pipedream.asm.armv8a

                cls._REGISTRY_[name] = pipedream.asm.armv8a.ARMv8_Architecture()
            elif name == "x86":
                import pipedream.asm.x86

                cls._REGISTRY_[name] = pipedream.asm.x86.X86_Architecture()
            else:
                raise ValueError("unknown arch " + repr(name))

        return cls._REGISTRY_[name]

    @abc.abstractproperty  # type: ignore
    def name(self) -> str:
        ...

    @abc.abstractproperty  # type: ignore
    def nb_vector_reg(self) -> int:
        ...

    @abc.abstractproperty  # type: ignore
    def max_vector_size(self) -> int:
        ...

    @abc.abstractmethod
    def instruction_set(self) -> "Instruction_Set":
        ...

    @abc.abstractmethod
    def register_set(self) -> "Register_Set":
        ...

    @abc.abstractmethod
    def asm_dialects(self) -> ty.List["Asm_Dialect"]:
        ...

    @abc.abstractmethod
    def make_asm_writer(self, dialect: "Asm_Dialect", file: ty.IO[str]) -> ASM_Writer:
        return NotImplemented

    @abc.abstractmethod
    def make_ir_builder(self) -> "IR_Builder":
        ...

    @abc.abstractmethod
    def make_register_allocator(self) -> "Register_Liveness_Tracker":
        ...

    @abc.abstractproperty  # type: ignore
    def tools_config(self) -> "Tools_Config":
        ...

    @abc.abstractproperty
    def l1_cache_config(self) -> "Cache_Config":
        ...

    @abc.abstractmethod
    def loop_overhead(self, num_iterations: int, num_prologue_inst: int):
        """
        Returns the number of instructions executed by *num_iterations* iterations of
        a `loop`.
        """

    def make_register_pools(
        self,
        regs: ty.Iterable["Register"],
        regs_ro: ty.Iterable["Register"],
        coefs: ty.Optional[ty.Dict["Instruction", int]] = None,
    ) -> ty.Union[
        ty.FrozenSet["Register"], ty.Dict["Operand", ty.FrozenSet["Register"]]
    ]:
        if coefs is None:
            return frozenset(iter(regs))

        all_avail_regs = list(regs)
        # "Correct" register class in order to get meaningfull aliases
        # TODO: there could be implemented a quick fix for AVX-512
        def correct_reg_class(reg_class: "Register_Class") -> "Register_Class":
            if reg_class.name == "BASE_REGISTER_64":
                import pipedream.asm.x86.registers as x86regs

                return x86regs.GPR64  # type: ignore
            else:
                return reg_class

        def get_uppermost_class(reg_class: "Register_Class") -> "Register_Class":
            reg_class = correct_reg_class(reg_class)
            maxi = reg_class
            for reg_class_test in self.register_set().register_classes():
                # FIXME: currently we want to keep the same number of elements (== in len condition), as this fixed *some* issues
                # for AVX-512 registers. Indeed, some instructions take as argument a VR128 which does
                # not include XMM16-31 registers. Thus get_uppermost_class must send back VRX512,
                # but it must not allocate XMM16-31 to the VR128 instruction.
                # This dirty fix limits the parent to YMM registers: no compatibility with AVX-512
                if (
                    len(maxi) == len(reg_class_test)
                    and all(
                        [
                            len(
                                set(iter(reg.aliases)).union([reg])
                                & set(iter(reg_class_test))
                            )
                            > 0
                            for reg in iter(reg_class)
                        ]
                    )
                    and all(
                        [
                            reg_class_test[0].width == reg.width
                            for reg in iter(reg_class_test)
                        ]
                    )
                    and reg_class_test[0].width > maxi[0].width
                    and set(reg_class_test) < set(all_avail_regs)
                ):
                    maxi = reg_class_test
            return maxi

        tot: ty.Dict["Register_Class", int] = {}
        for instr, value in coefs.items():
            seen_classes: ty.Set["Register_Class"] = set()
            for op in instr.operands:
                uppermost_class: ty.Optional["Register_Class"] = None
                if isinstance(op, Register_Operand):
                    uppermost_class = get_uppermost_class(op.register_class)
                elif isinstance(op, Base_Displacement_Address_Operand):
                    uppermost_class = get_uppermost_class(op.base.register_class)

                if uppermost_class is not None and uppermost_class not in seen_classes:
                    if tot.get(uppermost_class) == None:
                        tot[uppermost_class] = 0
                    tot[uppermost_class] += value
                    seen_classes.add(uppermost_class)
                    assert (
                        not set(uppermost_class) <= set(regs_ro)
                    ) or op.use_def is Use_Def.USE

        first: ty.Dict["Register_Class", float] = {}
        last: ty.Dict["Register_Class", float] = {}
        reg_class_to_avail_regs: ty.Dict["Register_Class", ty.List["Register"]] = {}
        reg_class: ty.Optional["Register_Class"] = None
        for reg_class in tot.keys():
            first[reg_class] = 0.0
            last[reg_class] = 0.0
            # Reading operand that can only read from RO regs
            # are allowed to do so
            if set(reg_class) <= set(regs_ro):
                reg_class_to_avail_regs[reg_class] = list(reg_class)
            else:
                reg_class_to_avail_regs[reg_class] = [
                    reg for reg in iter(reg_class) if reg in all_avail_regs
                ]
            assert (
                len(reg_class_to_avail_regs[reg_class]) > 0
            ), f"Class {reg_class}, avail: {all_avail_regs}"

        ret: ty.Dict["Operand", ty.FrozenSet["Register"]] = {}
        for instr, value in coefs.items():
            seen: ty.Dict["Register_Class", ty.List["Register"]] = {}
            for op in instr.operands:
                real_op = op
                reg_class = None
                if isinstance(op, Register_Operand):
                    reg_class = correct_reg_class(op.register_class)
                elif isinstance(op, Base_Displacement_Address_Operand):
                    real_op = op.base
                    reg_class = correct_reg_class(op.base.register_class)

                # Pick in the same pool for several operands of the same class
                if reg_class is not None:
                    uppermost_class = get_uppermost_class(reg_class)
                    big_slice_reg: ty.List["Register"] = []
                    # No choice for reg class with 1 reg supported (e.g. Arm's XPC)
                    if len(reg_class) == 1:
                        big_slice_reg = list(reg_class)
                    elif seen.get(uppermost_class) == None:
                        avail_regs = reg_class_to_avail_regs[uppermost_class]
                        assert (
                            len(uppermost_class._reg_set.intersection(avail_regs)) > 0
                        ), f"Class: {uppermost_class}, regs: {avail_regs}"
                        len_reg = len(avail_regs)
                        last[uppermost_class] += value / tot[uppermost_class]
                        # Slice affected to real_op, reprensented as the biggest classes
                        big_slice_reg = avail_regs[
                            round(first[uppermost_class] * len_reg) : round(
                                last[uppermost_class] * len_reg
                            )
                        ]
                        seen[uppermost_class] = big_slice_reg
                        if len(big_slice_reg) < 2 and len(reg_class) > 2:
                            raise Architecture.NotEnoughRegisters()
                        first[uppermost_class] = last[uppermost_class]
                    else:
                        big_slice_reg = seen[uppermost_class]
                    tot_reg = [
                        reg for big_reg in big_slice_reg for reg in big_reg.aliases
                    ]
                    tot_reg += [big_reg for big_reg in big_slice_reg]
                    ret[real_op] = frozenset(tot_reg)
                    assert len(ret[real_op]) > 0, (
                        f"Instr: {instr}, op:{op} has no assigned regs, benchmark is "
                        f"{coefs}"
                    )

        return ret


@dataclass(frozen=True)
class Tools_Config:
    as_cmd: str
    ld_cmd: str
    ldflags_perf: ty.List[str]


@dataclass(frozen=True)
class Cache_Config:
    size: int
    cacheline_size: int
    associativity: int


class IR_Builder(abc.ABC):
    """
    Helper class for building IR.
    """

    #### generate operands

    @abc.abstractmethod
    def get_return_register(self) -> "Register":
        """
        Get the register for returning integers from functions on this architecture.
        """

    @abc.abstractmethod
    def get_argument_register(self, idx: int) -> "Register":
        """
        Get the Nth (zero based indexing) argument register for function calls.
        Raises an exception of there is no Nth arg register on this architecture.
        """

    @abc.abstractmethod
    def get_scratch_register(self, idx: int) -> "Register":
        """
        Get the Nth scratch register code can use in benchmark kernels.
        Every architecure must provide at least three such registers.
        """

    @abc.abstractmethod
    def select_memory_base_register(
        self,
        insts: ty.List["Instruction"],
        free_regs: ty.Set["Register"],
        address_width: int,
    ) -> "Register":
        """
        Select a register that can be used as a memory base register for all memory accesses
        in the given list of instructions :insts:.
        """

    @abc.abstractmethod
    def preallocate_benchmark(
        self,
        alloc: "Register_Liveness_Tracker",
        instructions: ty.Sequence["Instruction"],
    ) -> ty.Tuple[object, ty.List["Instruction"]]:
        """
        Fill in some operands of benchmark kernel if required by architecture constraints.
        Returns a state object that must be passed to :free_stolen_benchmark_registers():
        to free them again.
        """

    @abc.abstractmethod
    def free_stolen_benchmark_registers(
        self, alloc: "Register_Liveness_Tracker", stolen_regs: object
    ):
        """
        Free registers taken by :steal_benchmark_registers():
        """

    #### low-level IR generation
    @abc.abstractmethod
    def emit_loop_prologue(
        aelf,
        kernel: ty.Sequence["Instruction"],
        free_regs: ty.Sequence["Register"],
        reg_values: "Register",
    ) -> ty.List["Instruction"]:
        """
        Emit code to initialize registers for benchmark kernel.
        """

    @abc.abstractmethod
    def emit_benchmark_prologue(
        self,
        kernel: ty.Sequence["Instruction"],
        free_regs: ty.Sequence["Register"],
    ) -> ty.List["Instruction"]:
        """
        Emit code to initialize registers for benchmark kernel, after potential PAPI
        calls
        """

    @abc.abstractmethod
    def emit_benchmark_epilogue(
        self,
        kernel: ty.Sequence["Instruction"],
        free_regs: ty.Sequence["Register"],
    ) -> ty.List["Instruction"]:
        """
        Emit code to do dirty backend stuff.
        """

    @abc.abstractmethod
    def emit_loop_epilogue(
        self,
        kernel: ty.Sequence["Instruction"],
        free_regs: ty.Sequence["Register"],
        reg_values: "Register",
    ) -> ty.List["Instruction"]:
        """
        Put the memory location of the register values back int reg_values
        """

    @abc.abstractmethod
    def emit_dependency_breaker(self, reg: "Register") -> ty.List["Instruction"]:
        """
        Emit code to break data dependencies along register :reg:.
        The contents of :reg: are undefined after the depdency breaker code executes.

        If the architecture does not support dependency breakers an empty list is returned.
        """

    @abc.abstractmethod
    def emit_sequentialize_cpu(
        self, alloc: "Register_Liveness_Tracker"
    ) -> ty.List["Instruction"]:
        """Emit instructions that sequentialize the CPU."""

    @abc.abstractmethod
    def emit_branch_if_not_zero(
        self, reg: "Register", dst: "Label"
    ) -> ty.List["Instruction"]:
        """
        emit code to branch to label :dst: if register :reg: does not contains zero.
        """

    @abc.abstractmethod
    def emit_branch_if_zero(
        self, reg: "Register", dst: "Label"
    ) -> ty.List["Instruction"]:
        """
        emit code to branch to label :dst: if register :reg: contains zero.
        """

    @abc.abstractmethod
    def emit_copy(self, src: "Register", dst: "Register") -> ty.List["Instruction"]:
        """
        emit register to register copy.
        """

    @abc.abstractmethod
    def emit_push_to_stack(self, src: "Register") -> ty.List["Instruction"]:
        """
        emit code to push :src: to stack.
        """

    @abc.abstractmethod
    def emit_call(self, dst: "Label") -> ty.List["Instruction"]:
        """
        emit code to call the procedure given by :dst:.
        """

    @abc.abstractmethod
    def emit_return(self, reg: "Register") -> ty.List["Instruction"]:
        """
        emit code to return the value in :reg: from the current procedure.

        TODO: add helper to gen proc-enter to push link register for ARM.
        """

    @abc.abstractmethod
    def emit_put_const_in_register(
        self, const: int, reg: "Register"
    ) -> ty.List["Instruction"]:
        """
        emit code to put value :const: in register :reg:.
        """

    @abc.abstractmethod
    def emit_put_basedisplacement_in_register(
        self, base: "Register", displacement: int, reg: "Register"
    ) -> ty.List["Instruction"]:
        """
        emit code to put the value from memory at :displacement:(:base:) in register
        :reg:
        """

    @abc.abstractmethod
    def emit_mul_reg_reg(
        self, reg: "Register", reg_const: "Register"
    ) -> ty.List["Instruction"]:
        """
        emit code to multiply the contents of a register by the content of another
        register.
        The result is stored in :reg:.
        """

    @abc.abstractmethod
    def emit_mul_reg_const(self, reg: "Register", const: int) -> ty.List["Instruction"]:
        """
        emit code to multiply the contents of a register by a constant.
        The result is stored in :reg:.
        """

    @abc.abstractmethod
    def emit_add_registers(
        self, src_reg: "Register", src_dst_reg: "Register"
    ) -> ty.List["Instruction"]:
        """
        emit code to add contents of two registers.
        The result is stored in the second register :src_dst_reg:.
        """

    @abc.abstractmethod
    def emit_substract_one_from_reg_and_branch_if_not_zero(
        self, loop_counter: "Register", dst: "Label"
    ) -> ty.List["Instruction"]:
        """
        emit code to substract one from the contents of register :loop_counter:
        and store the result in :loop_counter:.
        Then branches to :dst: if :loop_counter: now contains zero.
        """

    @abc.abstractmethod
    def emit_pop_from_stack(self, dst: "Register") -> ty.List["Instruction"]:
        """
        Pop one register from the stack and put its value in :dst:
        """

    @abc.abstractmethod
    def emit_add_imm_to_reg(
        self, dst: "Register", value: int
    ) -> ty.List["Instruction"]:
        """
        Add an immediate value :val: to a register :dst:  and store it back
        """

    @abc.abstractmethod
    def emit_sub_imm_to_reg(
        self, dst: "Register", value: int
    ) -> ty.List["Instruction"]:
        """
        Substract an immediate value :val: to a register :dst:  and store it back
        """

    @abc.abstractmethod
    def dump_stack_state(self) -> ty.Dict:
        """
        Save the current stack state (used to conserve consistency in stack alignement
        accross labels
        """

    @abc.abstractmethod
    def load_stack_state(self, state: ty.Dict):
        """
        Load a previously saved stack state :state:
        """


class Allocation_Error(Exception):
    """
    Signals that an error occured while trying to reserve/free a register.
    """


class Register_Liveness_Tracker:
    """
    Helper class for implementing register allocators.

    For a set of registers it tracks which register is in use or available at a point in time.

    You don't have to provide all aliases for every register when creating a Register_Liveness_Tracker object.
    If you do not provide an alias it cannot be allocated.
    But aliases are still taken into account for liveness!
    (i.e. you can not allocate two subregisters of a register at the same time, even if they do not overlap)
    """

    _FREE = 0
    _TAKEN: ty.ClassVar[Sentinel] = Sentinel("REGISTER_IS_TAKEN")

    def __init__(
        self,
        all_registers: ty.Sequence["Register"],
        read_only_registers: ty.Sequence["Register"] = (),
        callee_save_registers: ty.Sequence["Register"] = (),
    ):
        self._regs: ty.List[Register] = list(all_registers)
        self._regs.sort(key=lambda reg: (len(reg.name), reg.name), reverse=True)

        self._regs_ro: ty.List[Register] = list(read_only_registers)
        self._regs_ro.sort(key=lambda reg: (len(reg.name), reg.name), reverse=True)

        self._regs_per_class: ty.Dict[str, ty.List[Register]] = collections.defaultdict(
            list
        )

        ## Every register R is either marked as:
        ##   free/available to be allocated:         R maps to '0'
        ##   taken because an alias of it is taken:  R maps to a positive integer.
        ##   taken explicitly via take/take_any:     R maps to '_TAKEN'.
        self._status: ty.Dict[
            Register, ty.Union[int, Sentinel]
        ] = collections.OrderedDict()

        ## initially all registers are available

        for r in list(all_registers) + self._regs_ro:
            assert isinstance(r, Register), r

            self._status[r] = self._FREE
            for alias in r.aliases:
                self._status[alias] = self._FREE

        ## except for callee save registers

        for r in callee_save_registers:
            assert r in all_registers

            self.take(r)

    ### QUERY REGISTER STATUS

    def is_free(self, reg: "Register") -> bool:
        """
        Check if a register is currently not in use, i.e. can be taken.
        """
        return self._status[reg] == self._FREE

    def is_taken(self, reg: "Register") -> bool:
        """
        Check if a register is currently in use, i.e. can not be taken.
        """
        return self._status[reg] != self._FREE

    @abc.override
    def is_any_free(self, reg_class: "Register_Class") -> bool:
        """
        Query if any register of the given register class is currently available.
        """

        for reg in self.iter_free_registers(reg_class):
            return True

        return False

    ### CONTAINER OF REGISTERS

    def iter_registers(self, reg_class: "Register_Class") -> ty.Iterable["Register"]:
        """
        Iterator over all registers in register class *reg_class* managed by this
        allocator.
        """
        if reg_class.name not in self._regs_per_class:
            for reg in self._regs:
                if reg in reg_class:
                    self._regs_per_class[reg_class.name].append(reg)
        return iter(self._regs_per_class[reg_class.name])

    def iter_free_registers(
        self, reg_class: "Register_Class" = None, with_ro=False
    ) -> ty.Iterable["Register"]:
        """
        Iterator over all currently free registers of class *reg_class*.
        If *reg_class* is None, iterator over all free registers.
        """
        # print("iter_free_registers", self._regs, reg_class)
        if reg_class is None:
            for reg in self._regs:
                if self.is_free(reg):
                    yield reg
        else:
            for reg in self.iter_registers(reg_class):
                if self.is_free(reg):
                    yield reg

        if with_ro:
            for reg in self._regs_ro:
                if self.is_free(reg) and (reg_class is None or reg in reg_class):
                    yield reg

    def __contains__(self, reg: "Register") -> bool:
        """
        Check if *reg* is managed by this liveness tracker.
        """

        return reg in self._regs

    ### ALLOCATE

    def take(self, reg: "Register"):
        """
        Mark a register as used.
        The register and all of its aliases must not currently be in use.
        """

        if self.is_taken(reg):
            raise Allocation_Error(repr(reg) + " is not free")

        for alias in reg.aliases:
            if self._status[alias] is self._TAKEN:
                raise Allocation_Error(
                    repr(reg) + " is already taken (via " + repr(alias) + ")"
                )

        self._status[reg] = self._TAKEN
        ## also mark all aliases as taken
        ## (to avoid two different sub-registers of a register being taken at the same time.)
        for alias in reg.aliases:
            oldval = self._status[alias]
            assert isinstance(oldval, int)
            self._status[alias] = oldval + 1

    def take_any(self, reg_class: "Register_Class", can_take_ro=False) -> "Register":
        """
        Mark an arbitrary free register from *reg_class* as taken.
        Returns the register that is marked as taken.
        """

        assert type(reg_class) is Register_Class

        for reg in self.iter_free_registers(reg_class, can_take_ro):
            self.take(reg)
            return reg

        raise Allocation_Error("No location of type " + reg_class.name + " available")

    def free(self, reg: "Register"):
        """
        Mark a register as unused.
        The register must currently be in use.
        """

        if self.is_free(reg):
            raise Allocation_Error(repr(reg) + " is already free")

        self._status[reg] = self._FREE

        for alias in reg.aliases:
            oldstatus = self._status[alias]
            assert isinstance(oldstatus, int)
            if oldstatus != self._FREE:
                self._status[alias] = oldstatus - 1

    def free_all(self):
        """
        Mark all registers managed by this liveness tracker as unused.
        """

        for reg in self._status:
            self._status[reg] = self._FREE


class Instruction_Set(abc.ABC):
    @abc.abstractmethod
    def instruction_groups(self) -> ty.List["Instruction_Group"]:
        ...

    @abc.abstractmethod
    def instructions(self) -> ty.List["Instruction"]:
        ...

    @abc.abstractmethod
    def benchmark_instructions(self) -> ty.List["Instruction"]:
        """
        List of instructions intended for benchmarking.
        Subset of self.instructions.
        """

    @abc.abstractmethod
    def instruction_for_name(self, name: str) -> "Instruction":
        ...

    @abc.abstractmethod
    def instructions_for_tags(self, tags: str):
        ...

    def __iter__(self) -> ty.Iterator["Instruction"]:
        return iter(self.instructions())


class Register_Set(abc.ABC):
    @abc.abstractmethod
    def stack_pointer_register(self) -> "Register":
        ...

    @abc.abstractmethod
    def stack_pointer_register_class(self) -> "Register_Class":
        ...

    @abc.abstractmethod
    def register_classes(self) -> ty.List["Register_Class"]:
        ...

    @abc.abstractmethod
    def all_registers(self) -> "Register_Class":
        ...

    @abc.abstractmethod
    def argument_registers(self) -> "Register_Class":
        ...

    @abc.abstractmethod
    def callee_save_registers(self) -> "Register_Class":
        ...


class Asm_Dialect:
    """
    Defines how instructions and operands are printed.
    """

    @abc.abstractproperty  # type: ignore
    def name(self) -> str:
        ...

    @abc.abstractmethod
    def instruction_mnemonic(self, inst: "Instruction") -> str:
        ...

    @abc.abstractmethod
    def operand_to_str(self, op: "Operand") -> str:
        ...

    @abc.abstractmethod
    def instruction_operands(self, inst: "Instruction") -> ty.List[str]:
        ...

    def instruction_to_str(self, inst: "Instruction") -> str:
        mnem = self.instruction_mnemonic(inst)
        ops = self.instruction_operands(inst)
        if len(ops) > 0:
            return f'{mnem:12} {", ".join(ops)}'
        else:
            return mnem

    def begin_function(self, function_name: str) -> ty.Sequence[str]:
        return []

    def end_function(self, function_name: str) -> ty.Sequence[str]:
        return []

    def begin_file(self, file_name: str) -> ty.Sequence[str]:
        return []

    def end_file(self, file_name: str) -> ty.Sequence[str]:
        return []


class Instruction_Group:
    """
    Groups instructions that do `the same thing`.
    Can be more or less abstract.
    One instruction ca be in many groups.
    """

    @abc.abstractproperty  # type: ignore
    def name(self) -> str:
        ...

    @abc.abstractmethod
    def instructions(self) -> ty.List["Instruction"]:
        ...


@functools.total_ordering
class Register_Class(abc.ABC):
    """
    A finite non-empty frozen set of machine registers.
    Two registers classes can overlap.
    If a register class is compleletely contained in another one it is a
    sub register class.
    The register class containment relation forms a DAG.

    Note that a register can be in many register classes.
    """

    def __init__(self, name, registers, register_aliases={}):
        self._name = name
        self._reg_iter = tuple(registers)
        self._reg_set = frozenset(self._reg_iter)
        self._reg_aliases = dict(register_aliases)

        assert all(isinstance(r, Register) for r in self._reg_iter)

    @property  # type: ignore
    def name(self) -> str:
        return self._name

    def __getattr__(self, reg):
        """allow accessing registers as attributes of a register class."""

        try:
            return self._reg_aliases[reg]
        except KeyError:
            raise AttributeError(reg)

    ### treat register classes like sets

    def __contains__(self, value: "Register") -> bool:
        return value in self._reg_set

    def __or__(self, that: "Register_Class") -> "Register_Class":
        if type(self) != type(that):
            return NotImplemented

        set = self._reg_set | that._reg_set
        iter = self._reg_iter + that._reg_iter

        return Register_Class(
            name=self.name + "|" + that.name,
            registers=tuple(t for t in iter if t in set),
            register_aliases=self._alias_merge(
                set, self._reg_aliases, that._reg_aliases
            ),
        )

    def __and__(self, that: "Register_Class") -> "Register_Class":
        if type(self) != type(that):
            return NotImplemented

        set = self._reg_set & that._reg_set
        iter = self._reg_iter + that._reg_iter

        return Register_Class(
            name=self.name + "&" + that.name,
            registers=tuple(t for t in iter if t in set),
            register_aliases=self._alias_merge(
                set, self._reg_aliases, that._reg_aliases
            ),
        )

    def __eq__(self, that):
        if type(self) != type(that):
            return NotImplemented

        return self._reg_set == that._reg_set

    def __lt__(self, that):
        if type(self) != type(that):
            return NotImplemented

        return self._reg_set < that._reg_set

    def __le__(self, that):
        if type(self) != type(that):
            return NotImplemented

        return self._reg_set <= that._reg_set

    def __hash__(self):
        return hash(self._reg_set)

    ### iteration

    def __iter__(self) -> ty.Iterator["Register"]:
        return iter(self._reg_iter)

    def __getitem__(self, idx) -> "Register":
        return self._reg_iter[idx]

    def __len__(self) -> int:
        return len(self._reg_iter)

    ### MISC

    def __str__(self):
        return type(self).__name__ + "(" + repr(self.name) + ")"

    ### private

    @staticmethod
    def _alias_merge(
        reg_set: ty.Set["Register"], alias1, alias2
    ) -> ty.Dict[str, "Register"]:
        out = {}
        out.update({alias: reg for alias, reg in alias1.items() if reg in reg_set})
        out.update({alias: reg for alias, reg in alias2.items() if reg in reg_set})
        return out


class Register(abc.ABC):
    """A machine register."""

    @abc.abstractproperty  # type: ignore
    def name(self) -> str:
        ...

    @abc.abstractproperty  # type: ignore
    def width(self) -> int:
        """
        Width of register in bits
        """

    @abc.abstractmethod
    def as_width(self, bits: int) -> "Register":
        """
        Get alias with a given width.
        """
        return NotImplemented

    @abc.abstractproperty  # type: ignore
    def widest(self) -> "Register":
        """
        Get widest alias.
        """
        return NotImplemented

    @property  # type: ignore
    @abc.abstractmethod
    def sub_registers(self) -> ty.Iterator["Register"]:
        """An iterator over the registers immediate sub-registers."""

    @property  # type: ignore
    @abc.abstractmethod
    def super_registers(self) -> ty.Iterator["Register"]:
        """An iterator over the registers immediate super-registers."""

    @property  # type: ignore
    @abc.abstractmethod
    def aliases(self) -> ty.Iterator["Register"]:
        """
        An iterator over the registers that alias this register.
        I.e. an iterator of all registers that share storage/are affected by
        stores to this register (and vice versa).
        """

    @property  # type: ignore
    def all_sub_registers(self) -> ty.Iterator["Register"]:
        """An iterator over all sub-registers."""

        for reg in self.sub_registers:
            yield reg
            yield from reg.all_sub_registers

    @property  # type: ignore
    def all_super_registers(self) -> ty.Iterator["Register"]:
        """An iterator over all super-registers."""

        for reg in self.super_registers:
            yield reg
            yield from reg.all_super_registers


class Instruction(abc.ABC):
    """
    Descriptor for one instruction.
    """

    @abc.abstractmethod
    def name(self) -> str:
        ...

    @abc.abstractproperty  # type: ignore
    def tags(self) -> ty.Sequence[str]:
        ...

    @abc.abstractproperty  # type: ignore
    def operands(self) -> ty.Sequence["Operand"]:
        ...

    @property  # type: ignore
    def defs(self) -> ty.Iterator["Operand"]:
        """Operands written to"""
        for op in self.operands:
            if op.is_def:
                yield op

    @property  # type: ignore
    def uses(self) -> ty.Iterator["Operand"]:
        """Operands read from"""
        for op in self.operands:
            if op.is_use:
                yield op

    def get_operand(self, name: str) -> "Operand":
        for op in self.operands:
            if op.name == name:
                return op

        raise KeyError(name)

    def get_operand_idx(self, name: str) -> int:
        for idx, op in enumerate(self.operands):
            if op.name == name:
                return idx

        raise KeyError(name)

    @abc.abstractmethod
    def update_operand(
        self, idx_or_name: ty.Union[int, str], fn: ty.Callable[["Operand"], "Operand"]
    ) -> "Instruction":
        """
        Create new instruction where the operand indicated by :idx_or_name:
        has been replaced with the result of calling :fn: with the original operand.
        """

    def set_operand(
        self, idx_or_name: ty.Union[int, str], op: "Operand"
    ) -> "Instruction":
        def inject(org: "Operand") -> "Operand":
            return op

        return self.update_operand(idx_or_name, inject)

    def has_memory_operand(self) -> bool:
        for op in self.operands:
            if isinstance(op, Memory_Operand):
                return True
        return False

    @abc.abstractmethod
    def encodings(self) -> ty.List["Instruction_Encoding"]:
        """
        List of ways that an instruction can be encoded.
        """

    @abc.abstractproperty  # type: ignore
    def can_benchmark(self) -> bool:
        """
        Can this instruction occur in a benchmark kernel?
        """

    def __repr__(self):
        txt = self.name

        if self.operands:
            txt += " " + ", ".join(map(str, self.operands))

        return txt


class Pseudo_Instruction(Instruction):
    """
    Helper pseudo instruction that is not directly encodable.
    Must be lowered to zero or more real machine instructions.
    """

    def encodings(self):
        return []

    @property  # type: ignore
    @abc.override
    def can_benchmark(self) -> bool:
        return False


class Machine_Instruction(Instruction):
    """
    Real machine instruction that can be encoded to binary.
    """


class Instruction_Encoding:
    """
    A concrete encoding for an instruction, a pattern of bytes.
    All encodings for an instruction behave semantically the same, but
    have a different byte pattern.
    """

    def operands(self) -> ty.List["Operand"]:
        pass

    def encode(self) -> bytes:
        ...


class Use_Def(enum.IntFlag):
    USE = 1
    DEF = 2
    USE_DEF = USE | DEF

    def repr_name(self):
        return {self.USE: "?", self.DEF: "!", self.USE_DEF: "?!"}[self]


class Operand_Visibility(enum.IntFlag):
    ## operand shown in asm & instruction bits
    EXPLICIT = 0
    ## operand shown in asm, implicit in instruction bits
    IMPLICIT = 1
    ## operand not show in asm, implicit in instruction bits
    SUPPRESSED = 2


class Operand(abc.ABC):
    @abc.abstractproperty  # type: ignore
    def name(self) -> str:
        ...

    @abc.abstractproperty  # type: ignore
    def use_def(self) -> Use_Def:
        ...

    @abc.abstractproperty  # type: ignore
    def visibility(self) -> Operand_Visibility:
        ...

    @property  # type: ignore
    def is_use(self) -> bool:
        return bool(self.use_def & Use_Def.USE)

    @property  # type: ignore
    def is_def(self) -> bool:
        return bool(self.use_def & Use_Def.DEF)

    @abc.abstractproperty  # type: ignore
    def is_virtual(self) -> bool:
        """
        An operand is 'virtual' if it is not fully specified yet.
        I.e. a Register_Operand where 'register' returns None or a
        Immediate_Operand where 'value' returns None.
        """

    @abc.abstractmethod
    def _arbitrary(self, random: Random) -> ty.Any:
        ...

    def arbitrary_value(self, random: Random) -> ty.Any:
        """
        Return an arbitrary concrete value or list of values for this operand.
        Can be passed for instance to with_operand_value.
        """
        return self._arbitrary(random)

    @abc.abstractmethod
    def with_operand_value(self, operand_value: ty.Any) -> "Operand":
        """
        Set a concret value for the operand, i.e. an immediate value or a register.
        """

    def __repr__(self):
        txt = f"{self.name}{self.use_def.repr_name()}"
        return txt


class Register_Operand(Operand):
    @abc.abstractproperty  # type: ignore
    def register_class(self) -> Register_Class:
        ...

    @abc.abstractproperty  # type: ignore
    def register(self) -> ty.Optional[Register]:
        """
        Return register read/written by this operand.
        If the returned value is none no register has been assigned yet.
        I.e. this is a virtual register that still needs to be allocated.
        """

    @abc.abstractmethod
    def with_register(self, reg: Register) -> "Register_Operand":
        """
        Return a copy of :self: where the register read/written by this operand
        has been changed to :reg:.
        If :reg: is not a member of self.register_class a TypeError is raised.
        """

    @abc.override
    def with_operand_value(self, operand_value: ty.Any) -> "Operand":
        return self.with_register(operand_value)

    @property  # type: ignore
    @abc.override
    def is_virtual(self) -> bool:
        return self.register is None

    def __repr__(self):
        txt = self.name + ":"

        if self.register:
            txt += self.register.name
        else:
            txt += self.register_class.name

        txt += self.use_def.repr_name()
        return txt


Immediate_Value = ty.Union[int, float, "Label"]


class Immediate_Operand(Operand):
    """
    Constant operand encoded directly into the instruction.
    """

    _max: ty.Any = None
    _min: ty.Any = None

    def __init__(self, *args, **kwargs):
        self._value: ty.Any
        super().__init__(*args, **kwargs)

    @property  # type: ignore
    @abc.override
    @abc.final
    def use_def(self) -> Use_Def:
        return Use_Def.USE

    @abc.abstractproperty
    def value(self) -> ty.Optional["Immediate_Value"]:
        ...

    @abc.abstractmethod
    def with_value(self, val: Immediate_Value) -> "Immediate_Operand":
        """
        Return a copy of :self: where value has been changed to :val:.
        If :val: is not a valid constant for self a TypeError is raised.
        """

    @abc.abstractmethod
    def is_valid_value(self, val: Immediate_Value) -> bool:
        """
        Return whether the given immediate is valid for the operand.
        """

    @abc.override
    def with_operand_value(self, operand_value: ty.Any) -> "Operand":
        return self.with_value(operand_value)

    @property  # type: ignore
    @abc.override
    def is_virtual(self) -> bool:
        return self.value is None

    def __repr__(self):
        if self.value is not None:
            txt = str(self.value)
        else:
            txt = type(self).__name__
        return self.name + ":" + txt + "?"


class ImmMinMax_Operand(Immediate_Operand):
    """
    Every operands with specific min/max values and optional
    multiplier.
    """

    _min = None
    _max = None
    _mult = 1

    def __init__(
        self,
        name: str,
        visibility: Operand_Visibility,
        min_val: int = None,
        max_val: int = None,
        value: int = None,
        mult: int = None,
    ):
        if min_val is not None:
            self._min = min_val
        if max_val is not None:
            self._max = max_val
        if mult is not None:
            self._mult = mult
        super().__init__(name, visibility, value)

    @abc.override
    def with_value(self, value):
        clss = type(self)

        if not self.is_valid_value(value):
            raise TypeError(
                f"{value} is not a valid value for immediates of type {clss}"
            )

        new = copy.copy(self)
        new._value = value
        return new

    @property  # type: ignore
    @abc.override
    def num_bits(self) -> int:
        assert self._max is not None
        assert self._min is not None
        return int(math.log(self._max - self._min + 1, 2))

    @abc.override
    def is_valid_value(self, value):  # type: ignore
        return (
            type(value) is int
            and self._min <= value <= self._max
            and value % self._mult == 0
        )

    @abc.override
    def _arbitrary(self, random: Random):  # type: ignore
        # For now take max of min and 1 as there are undescribed constraints
        # Take 0 if 1 is invalid
        mini = self._min
        assert mini is not None
        mult = self._mult
        assert mult is not None
        base = max(mini, 1)
        if not self.is_valid_value(base):
            base = 0
        if base < 0:
            val = (base + self._mult - 1) // mult
        else:
            val = base // mult
        val *= mult
        return val

    @abc.abstractclassmethod
    def _init_min_max(clss) -> ty.Tuple[int, int]:
        ...

    @classmethod
    def max(clss) -> int:
        if clss._max is not None:
            return clss._max
        else:
            clss._min, clss._max = clss._init_min_max()
            return clss._max

    @classmethod
    def min(clss) -> int:
        if clss._min is not None:
            return clss._min
        else:
            clss._min, clss._max = clss._init_min_max()
            return clss._min


class Shifted_Immediate_Operand(Immediate_Operand):
    """
    Constant operand encoded directly into the instruction, shifted by a fixed value.
    """

    def __init__(self, *args, **kwargs):
        self._shift: optional[int] = None
        super().__init__(*args, **kwargs)

    @abc.abstractmethod
    def with_shifted_value(self, value, shift):
        ...


class Flags_Operand(Operand):
    """
    Read/Write to a flags register.
    Since reads/writes to flags are usually partial & highly optimized
    we don't count it as a normal register operand.
    I.e. if instruction A writes flag FA and instruction B reads flag FB
    there is usually no data dependence between the two.
    If we treated this like a normal read/write to the FLAGS register we'd get
    wrong dependencies.

    TODO: put the flags_read/flags_written properties on the instruction?
    """

    @abc.abstractproperty  # type: ignore
    def register(self) -> "Register":
        return NotImplemented

    @abc.abstractproperty  # type: ignore
    def flags_read(self) -> ty.Set:
        return NotImplemented

    @abc.abstractproperty  # type: ignore
    def flags_written(self) -> ty.Set:
        return NotImplemented

    @abc.override
    def with_operand_value(self, operand_value: ty.Any) -> "Operand":
        return self  # immutable


class Composite_Operand(Operand):
    """
    An operand composed of multiple sub-operands.
    """

    @abc.abstractproperty  # type: ignore
    def sub_operands(self) -> ty.List[Operand]:
        ...

    @abc.abstractmethod
    def update_sub_operand(
        self, idx_or_name: ty.Union[int, str], fn: ty.Callable[["Operand"], "Operand"]
    ) -> "Composite_Operand":
        ...

    @abc.override
    def arbitrary_value(self, random: Random) -> ty.Any:
        return [x.arbitrary_value(random) for x in self.sub_operands]

    @abc.override
    def with_operand_value(self, operand_value: ty.Any) -> "Composite_Operand":
        def inject(op_value, op):
            op = op.with_operand_value(op_value)
            return op

        new = self
        for i, (op, op_value) in enumerate(zip(self.sub_operands, operand_value)):
            new = new.update_sub_operand(i, functools.partial(inject, op_value))
        return new

    def __repr__(self):
        return (
            self.name
            + ":"
            + "("
            + ", ".join(map(str, self.sub_operands))
            + ")"
            + self.use_def.repr_name()
        )


class Memory_Operand(Composite_Operand):
    """
    Operand reads/writes to a memory address.
    """

    @abc.abstractproperty  # type: ignore
    def address_width(self) -> int:
        return NotImplemented

    @abc.abstractproperty  # type: ignore
    def memory_width(self) -> int:
        return NotImplemented


class Address_Operand(Composite_Operand):
    """
    Operand calculates a memory address, but does not access it. (LEA, BND, ...)
    """

    @abc.abstractproperty  # type: ignore
    def address_width(self) -> int:
        return NotImplemented


class Base_Operand(Operand):
    @abc.abstractproperty  # type: ignore
    def base(self) -> Register_Operand:
        return NotImplemented

    @abc.abstractmethod
    def with_base(self, base_reg: Register) -> "Base_Operand":
        return NotImplemented


class Base_Displacement_Operand(Operand):
    @abc.abstractproperty  # type: ignore
    def base(self) -> Register_Operand:
        return NotImplemented

    @abc.abstractproperty  # type: ignore
    def displacement(self) -> Immediate_Operand:
        return NotImplemented

    @abc.abstractmethod
    def with_base(self, base_reg: Register) -> "Base_Displacement_Operand":
        return NotImplemented

    @abc.abstractmethod
    def with_displacement(
        self, disp_reg: ty.Union[int, "Label", Register]
    ) -> "Base_Displacement_Operand":
        return NotImplemented


class Base_Register_Operand:
    @abc.abstractproperty
    def base(self) -> Register_Operand:
        return NotImplemented

    @abc.abstractproperty
    def displacement(self) -> Register_Operand:
        return NotImplemented

    @abc.abstractmethod
    def with_base(self, base_reg: Register) -> "Base_Register_Operand":
        return NotImplemented

    @abc.abstractmethod
    def with_displacement(
        self, disp: ty.Union[int, "Label", Register]
    ) -> "Base_Register_Operand":
        return NotImplemented


class Base_Displacement_Address_Operand(Address_Operand, Base_Displacement_Operand):
    """
    Operand that computes a memory address specified by a base register + a constant displacement.
    """


class Base_Displacement_Memory_Operand(Memory_Operand, Base_Displacement_Operand):
    """
    Operand reads/writes to a memory address specified by a base register + a constant displacement.
    """


class Label_Operand:
    """
    Operand specified by a label.
    """

    @abc.abstractmethod
    def with_label(self, value):
        ...


class Base_Offset_Memory_Operand(Memory_Operand, Base_Register_Operand):
    """
    Operand reads/writes to a memory address specified by a base register + a register
    offset.
    """

    @abc.abstractmethod
    def with_base(self, base_reg: Register) -> "Base_Offset_Memory_Operand":
        ...

    @abc.abstractmethod
    def with_offset(self, offset_reg: Register) -> "Base_Offset_Memory_Operand":
        ...


class Base_Address_Operand(Address_Operand, Base_Operand):
    """
    Operand that computes a memory address specified by a base register
    """


class Base_Memory_Operand(Memory_Operand, Base_Operand):
    """
    Operand reads/writes to a memory address specified by a base register
    """


################################################################################
##### IR HELPERS


class Label(ty.NamedTuple):
    """
    A reference to a symbol in the program.
    Can be used as value for Immediate_Operands
    This is only a helper, in real machine code labels are encoded as Constants.
    """

    name: str


class Loop(ty.NamedTuple):
    """A helper for writing loops."""

    name: str
    head: Label
    exit: Label
