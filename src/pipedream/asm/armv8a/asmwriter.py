"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from pipedream.utils import abc
from pipedream.asm.ir import *
from pipedream.asm.asmwriter import *
from pipedream.asm.armv8a.registers import *
from pipedream.asm.armv8a.operands import *

import functools
import math

from typing import IO, Sequence, List

__all__ = [
    "ARMv8_ASM_Writer",
]


class ARMv8_ASM_Writer(ASM_Writer):
    def __init__(self, dialect: "Asm_Dialect", file: IO[str]):
        super().__init__(file)
        self.dialect = dialect

    @abc.override
    def begin_file(self, name):
        # For extensions/versions, refer for instance to: https://gcc.gnu.org/onlinedocs/gcc/ARM-Options.html
        self.print(1, ".arch armv8.6-a+bf16+crypto+fp16+simd+crc+i8mm+sve+f32mm+f64mm")
        self.print(1, '.file 1 "' + name + '"')

    @abc.override
    def end_file(self, name):
        self.print(1, '.ident "pipe-dream"')

    @abc.override
    def begin_function(self, function_name: str):
        self.print(1, ".text")
        self.print(1, ".globl " + function_name)
        self.print(1, ".type  " + function_name + ", @function")
        self.align()
        self.print(0, function_name + ":")
        self.print(1, ".cfi_startproc")

    @abc.override
    def end_function(self, function_name: str):
        self.print(1, ".cfi_endproc")
        self.print(1, ".size " + function_name + ", .-" + function_name)
        self.print(1, ".type " + function_name + ", @function")

    @abc.override
    def emit_label(self, label):
        self.print(0, label.name + ":")

    @abc.override
    def align(self):
        self.print(1, ".p2align 4")

    @abc.override
    def insts(self, insts: Sequence[Instruction]):
        for inst in insts:
            assert isinstance(inst, Instruction)
            assert not any(o.is_virtual for o in inst.operands), [
                inst,
                [o for o in inst.operands if o.is_virtual],
            ]

            try:
                mnem = self.dialect.instruction_mnemonic(inst)
                ops = [
                    op
                    for op in inst.operands
                    if op.visibility is not Operand_Visibility.SUPPRESSED
                ]

                if ops:
                    self.print(
                        1, mnem.ljust(12), ", ".join(self.emit_operand(o) for o in ops)
                    )
                else:
                    self.print(1, mnem)

            except AssertionError:
                print("#", inst)
                raise

    @abc.override
    def global_byte_array(self, name: str, size: int, alignment: int):
        ## this is just copy/adapted from ASM emitted by GCC

        assert type(name) is str
        assert type(size) is int
        assert type(alignment) is int

        assert name and name.isprintable()
        assert size >= 0
        assert alignment >= 0
        assert math.log2(alignment).is_integer()

        ## assume we may be in text section
        ## switch to BSS & emit array
        self.print(1, ".bss")
        self.print(1, ".global", name)
        self.print(1, ".balign", alignment)
        self.print(1, ".type", name + ",", "@object")
        self.print(1, ".size", name + ",", size)
        self.print(0, name + ":")
        self.print(1, ".zero", size)
        ## switch back to text section
        self.print(1, ".text")
        self.print(0, "")

    @abc.override
    def global_int_values(
        self, name: str, values: List[int], elt_size: int = 8, alignment: int = 8
    ):
        ## assume we may be in text section
        ## switch to DATA & emit array
        directive_map = {1: ".byte", 2: ".short", 4: ".long", 8: ".quad"}
        directive = directive_map.get(elt_size)
        assert directive is not None

        self.print(1, ".data")
        self.print(1, f".global {name}")
        self.print(1, f".balign {alignment}")
        self.print(0, f"{name}:")
        for val in values:
            self.print(1, f"{directive} {val}")
        self.print(1, f".type {name}, @object")
        self.print(1, f".size {name}, . - {name}")
        self.print(1, ".text")
        self.print(0, "")

    @abc.override
    def comment(self, *args):
        self.print(1, "#", *args)

    @abc.override
    def newline(self):
        self.print(0)

    @abc.override
    def emit_operand(self, op) -> str:
        return renderARMv8_arg(op)


@functools.singledispatch
def renderARMv8_arg(arg):
    raise ValueError(
        "Cannot render argument " + repr(arg) + " of type " + type(arg).__name__
    )


@renderARMv8_arg.register(ARMv8_Modifier)
def _render_modifier(arg):
    return arg.asm_str


@renderARMv8_arg.register(Label)
def _render_label(arg):
    return arg.name


@renderARMv8_arg.register(ARMv8_Register_Operand)
def _render_register(arg):
    reg_op = arg.register.asm_name
    pre, pst = "", ""
    if arg._list_idx is not None:
        idx, num = arg._list_idx
        pre = "{" if idx == 0 else ""
        if idx == num - 1:
            pst = "}"
            if arg.register._elt_idx is not None:
                pst += f"[{arg.register._elt_idx}]"
        if arg.register._elt_idx is not None:
            reg_op = f"{arg.register._reg_name}.{arg.register._elt_size}"
    reg_op = f"{reg_op}/{arg._modifier}" if arg._modifier else reg_op
    reg_op = f"{reg_op}, {arg._shift}" if arg._has_shift and arg._shift else reg_op
    reg_op = f"{reg_op}, {arg._extend}" if arg._has_extend and arg._extend else reg_op
    return f"{pre}{reg_op}{pst}"


@renderARMv8_arg.register(ARMv8_Immediate_Operand)
def _render_immediate(arg):
    if type(arg.value) is Label:
        return arg.value.name
    else:
        return arg.asm_str


@renderARMv8_arg.register(str)
def _render_str(arg):
    # TODO: introduce symbol type
    return arg


@renderARMv8_arg.register(ARMv8_Memory_Address_Operand)
def _render_memory_address(arg):
    ops = [renderARMv8_arg(x) for x in arg.sub_operands]
    return f"[{', '.join(ops)}]"


@renderARMv8_arg.register(ARMv8_PCRel_Operand)
def _render_pcrel(arg):
    if arg._value is None:
        return f". + {arg._displacement._value}"
    else:
        assert type(arg._value) is Label
        return arg._value.name
