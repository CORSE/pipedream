"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import collections
import types
import pipedream.utils.abc as abc

import pipedream.utils as utils
from pipedream.asm import ir

from pipedream.asm.armv8a.operands import *
from pipedream.asm.armv8a.registers import *
from pipedream.asm.armv8a.flags import *
from pipedream.asm.armv8a import instructions_binutils  # type: ignore
from pipedream.asm.armv8a.instructions_binutils import ISA, ISA_Extension, ISA_Feature

import copy
from typing import (
    Dict,
    Sequence,
    Iterable,
    Union,
    List,
    Tuple,
    FrozenSet,
    Optional,
    Any,
    Set,
)


__all__ = [
    "ARMv8_Instruction",
    "ARMv8_Instruction_Set",
    "MNEMONICS",
    "INSTRUCTIONS",
    "Harness",
]

Instruction_Name = str
Operand_Name = str

INSTRUCTIONS: Dict[Instruction_Name, "ARMv8_Instruction"] = {}
MNEMONICS: Dict[Instruction_Name, str] = {}

SP: ir.Register_Class
WSP: ir.Register_Class


class ARMv8_Instruction(ir.Machine_Instruction):
    def __init__(
        self,
        name: str,
        mnemonic: str,
        isa_set: ISA,
        operands: Sequence[ir.Operand],
        tags: Sequence[str],
        can_benchmark: bool,
    ):
        self._name = name
        self._mnemonic = mnemonic
        self._isa_set = isa_set
        self._operands = tuple(operands)
        self._tags = tuple(tags)
        self._can_benchmark = can_benchmark

        assert len(operands) == len(
            set(o.name for o in operands)
        ), "duplicate operand name in " + str(self)

    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return self._name

    @property  # type: ignore
    @abc.override
    def tags(self) -> Sequence[str]:
        return self._tags

    @property  # type: ignore
    def mnemonic(self) -> str:
        return self._mnemonic

    @property  # type: ignore
    @abc.override
    def isa_set(self) -> ISA:
        return self._isa_set

    @property  # type: ignore
    @abc.override
    def operands(self) -> Sequence[ir.Operand]:
        return self._operands

    @abc.override
    def update_operand(self, idx_or_name: Union[int, str], fn) -> ir.Instruction:
        if isinstance(idx_or_name, str):
            idx = self.get_operand_idx(idx_or_name)
        else:
            idx = idx_or_name

        ops = list(self.operands)
        ops[idx] = fn(ops[idx])
        new = copy.copy(self)
        new._operands = tuple(ops)
        return new._update_operand_list(ops[idx])

    def _update_operands(self, operands: List[ir.Operand]) -> ir.Instruction:
        ops = list(self.operands)
        for op in operands:
            idx = self.get_operand_idx(op.name)
            ops[idx] = op
        new = copy.copy(self)
        new._operands = tuple(ops)
        return new

    def _update_operand_list(self, op: ARMv8_Operand) -> ir.Instruction:
        if (
            not isinstance(op, ARMv8_Register_Operand)
            or op.is_virtual
            or op._list_idx is None
        ):
            return self
        assert op._reg is not None and op._reg._reg_idx is not None
        idx, num = op._list_idx
        reg_list = [
            op
            for op in self.operands
            if isinstance(op, ARMv8_Register_Operand) and op._list_idx is not None
        ]
        assert len(reg_list) == num
        n_virtual = len([op for op in reg_list if op.is_virtual])
        if n_virtual == 0:
            return self
        assert n_virtual == num - 1, f"unexpected pre-allocation of registers list"
        reg_idx_map = {
            reg._reg_idx: reg
            for reg in op._reg_class
            if isinstance(reg, ARMv8_Register) and reg._reg_idx is not None
        }
        reg_idx_list = sorted(set(reg_idx_map.keys()))
        reg_idx_idx = reg_idx_list.index(op._reg._reg_idx)
        operands = []
        for op in reg_list:
            assert op._list_idx is not None
            if op._list_idx[0] == idx:
                continue
            op_reg_idx_idx = (reg_idx_idx + op._list_idx[0] - idx) % len(reg_idx_list)
            op_reg_idx = reg_idx_list[op_reg_idx_idx]
            op_reg = reg_idx_map[op_reg_idx]
            operands.append(op.with_register(op_reg))
        new = self._update_operands(operands)
        return new

    @property  # type: ignore
    @abc.override
    def can_benchmark(self) -> bool:
        return self._can_benchmark

    @abc.override
    def encodings(self) -> List[ir.Instruction_Encoding]:
        raise NotImplementedError()


class ARMv8_Instruction_Set(ir.Instruction_Set):
    @abc.override
    def instruction_groups(self) -> List[ir.Instruction_Group]:
        return []

    @abc.override
    def instructions(self) -> List[ir.Machine_Instruction]:
        return list(INSTRUCTIONS.values())

    @abc.override
    def benchmark_instructions(self) -> List[ir.Machine_Instruction]:
        return [I for I in INSTRUCTIONS.values() if I.can_benchmark]

    @abc.override
    def instruction_for_name(self, name: str) -> ir.Machine_Instruction:
        return INSTRUCTIONS[name]

    @abc.override
    def instructions_for_tags(self, *tags) -> Iterable[ir.Machine_Instruction]:
        tags_set = set(tags)

        for inst in INSTRUCTIONS.values():
            if tags_set <= inst.tags:
                yield inst

    def __getitem__(self, name):
        return INSTRUCTIONS[name]

    @abc.override
    def __iter__(self):
        return iter(INSTRUCTIONS.values())


FEATURE_CANNOT_BENCHMARK: FrozenSet[str] = frozenset(
    [
        "SVE",  # armv8 FEAT_SVE # TODO
        "MEMTAG",  # armv9 FEAT_MTE/FEAT_MTE2
        "SVE2",  # armv9 FEAT_SVE2
        "TME",  # FEAT_TME
        "SHA3",  # FEAT_SHA3
        "SM4",  # Requires FEAT_SM3/FEAT_SM4
        "V8_7",  # unsupported old aarch64 assemblers
        "DOTPROD",  # TODO: disable until managed
        "RDMA",  # TODO: disable until managed
        "RCPC",  # TODO: disable until managed
        "F16",  # TODO: disable until managed
        "BFLOAT16",  # TODO: disable until managed
        "COMPNUM",  # TODO: disable until managed
        "FRINTTS",  # FP trapping ops not supported on all ARMv8 archs
    ]
)

MNEMONIC_CANNOT_BENCHMARK: FrozenSet[str] = frozenset(
    [
        # TODO
        "shll",  # Constraint immediate shift depending on another operand
        "shll2",  # idem
        "movprfx",  # special instruction for predication which has constraint on the following instruction
        "ldraa",  # Requires pointer authentication
        "ldrab",  # idem
        "xpacd",  # idem
        "xpaci",  # idem
        "xpaclri",  # idem
        "sdiv",  # division
        "udiv",  # idem
    ]
)

MNEMONIC_CANNOT_BENCHMARK_PFX: FrozenSet[str] = frozenset([])

ISA_SET_CANNOT_BENCHMARK: FrozenSet[str] = frozenset(
    [
        "ic_system",  # Exclude system level instructions
        "ldstexcl",  # Exclude special exclusive ld/st
        "ldst_unpriv",  # Exclude special unprivileged ldt/stt
        # TODO: disabled until verified/managed
        "compbranch",  # branches
        "condbranch",  # branches
        "branch_imm",  # branches
        "branch_reg",  # branches
        "testbranch",  # branches
        "asisdlsep",  # post adressing modes not supported
        "asisdlsop",  # idem
        "asimdtbl",  # tbl/tbx can't handle multiple regs
    ]
)

INST_NAME_CANNOT_BENCHMARK: FrozenSet[str] = frozenset([])


def pipedream_asm_backend_can_handle(
    name: str,
    isa_set: ISA,
    isa_features: str,
    mnemonic: str,
    operands: Sequence[ir.Operand],
):
    if isa_set.value in ISA_SET_CANNOT_BENCHMARK:
        return False

    if mnemonic in MNEMONIC_CANNOT_BENCHMARK:
        return False
    for pfx in MNEMONIC_CANNOT_BENCHMARK_PFX:
        if mnemonic.startswith(pfx):
            return False

    if name in INST_NAME_CANNOT_BENCHMARK:
        return False

    for feature in isa_features.split("|"):
        if feature in FEATURE_CANNOT_BENCHMARK:
            return False

    for op in operands:
        if (
            isinstance(op, ARMv8_Register_Operand)
            and op.is_def
            and op.register_class in [SP, WSP]
        ):
            return False

    return True


def mk_inst(
    *,
    mnemonic: str,
    isa_set: ISA,
    operands: Union[Tuple, List],
    isa_extension: ISA_Extension,
    isa_features: str,
    can_benchmark: Optional[bool] = None,
):
    global INSTRUCTIONS

    operands = tuple(mk_op() for mk_op in utils.flatten(operands))
    name = mnemonic
    if operands:
        name += "_" + "_".join(op.id_name for op in operands)
    name = name.upper()

    ## filter out instructions we currently do not support
    if can_benchmark is None:
        can_benchmark = pipedream_asm_backend_can_handle(
            name, isa_set, isa_features, mnemonic, list(operands)
        )

    inst = ARMv8_Instruction(
        name, mnemonic, isa_set, operands, isa_features.split("|"), can_benchmark
    )
    MNEMONICS[name] = mnemonic

    if name in INSTRUCTIONS:
        raise ValueError(
            "\n".join(
                [
                    f"Duplicate instruction for name {name}:",
                    f" ({INSTRUCTIONS[name]}",
                    f"  vs",
                    f"  {inst})",
                ]
            )
        )

    INSTRUCTIONS[name] = inst

    return inst


def make_reg_op(
    *,
    name: str,
    reg_class: ir.Register_Class,
    action: ir.Use_Def,
    visibility: ir.Operand_Visibility,
    tied: int = 0,
    modifier: str = None,
    n_regs: int = 0,
    has_extend: bool = False,
    has_shift: bool = False,
):
    # TODO: tied argument ignored for now
    if not n_regs:
        return lambda: ARMv8_Register_Operand(
            name,
            visibility,
            action,
            reg_class,
            modifier=modifier,
            has_extend=has_extend,
            has_shift=has_shift,
        )
    return [
        lambda name=name, idx=idx, n_regs=n_regs: ARMv8_Register_Operand(
            f"{name}_{idx}",
            visibility,
            action,
            reg_class,
            modifier=modifier,
            list_idx=(idx, n_regs),
            has_extend=has_extend,
            has_shift=has_shift,
        )
        for idx in range(n_regs)
    ]


def make_addr_op(
    name: str,
    visibility: ir.Operand_Visibility,
    addr_class: Any,
    action: Optional[ir.Use_Def] = None,
    memory_width: Optional[int] = None,
    reg_class: Optional[Any] = None,
):
    assert reg_class is None
    if memory_width is not None:
        return lambda: addr_class(
            name, visibility=visibility, memory_width=memory_width, use_def=action
        )
    else:
        return lambda: addr_class(name, visibility=visibility, use_def=action)


def make_imm_op(
    *,
    name: str,
    visibility: ir.Operand_Visibility,
    imm_class: Any,
    reg_class: Optional[Any] = None,
    min_val: Optional[int] = None,
    max_val: Optional[int] = None,
):
    if min_val is not None:
        return lambda: imm_class(
            name, visibility=visibility, min_val=min_val, max_val=max_val
        )
    elif reg_class is not None:
        return lambda: imm_class(name, visibility=visibility, reg_class=reg_class)
    else:
        return lambda: imm_class(name, visibility)


# FIXME: this is currently never used in `instruction-binutils`
def make_flags_op(
    *,
    name: str,
    read: ARMv8_NZCV,
    write: ARMv8_NZCV,
    visibility: ir.Operand_Visibility,
):
    if not read:
        read = ARMv8_NZCV(0)
    if not write:
        write = ARMv8_NZCV(0)

    return lambda: ARMv8_Flags_Operand(
        name, visibility, FLAGS, read, write  # type:ignore
    )


instructions_binutils.make_instruction_database(
    make_instruction=mk_inst,
    reg_op=make_reg_op,
    addr_op=make_addr_op,
    imm_op=make_imm_op,
    flags_op=make_flags_op,
)


tmp = collections.OrderedDict()
for inst in sorted(INSTRUCTIONS.values(), key=lambda inst: inst.name):  # type: ignore
    tmp[inst.name] = inst
INSTRUCTIONS = types.MappingProxyType(tmp)  # type: ignore


class Harness:
    """
    Well known instructions used in benchmark harness
    """

    STR_RT_X_ADDR_SIMM9_ADDR_X_SP_IMM_ADDR_SIMM9 = INSTRUCTIONS["STR_RT_X_ADDR_SIMM9"]
    LDR_RT_X_ADDR_SIMM9_ADDR_X_SP_IMM_ADDR_SIMM9 = INSTRUCTIONS["LDR_RT_X_ADDR_SIMM9"]
    SUB_RD_SP_X_SP_RN_SP_X_SP_AIMM = INSTRUCTIONS["SUB_RD_SP_X_SP_RN_SP_X_SP_AIMM"]
    ADD_RD_SP_X_SP_RN_SP_X_SP_AIMM = INSTRUCTIONS["ADD_RD_SP_X_SP_RN_SP_X_SP_AIMM"]
    MOV_RD_X_RM_SFT_X = INSTRUCTIONS["MOV_RD_X_RM_SFT_X"]
    RET_RN_X = INSTRUCTIONS["RET_RN_X"]
    MUL_RD_X_RN_X_RM_X = INSTRUCTIONS["MUL_RD_X_RN_X_RM_X"]
    MOVZ_RD_X_HALF = INSTRUCTIONS["MOVZ_RD_X_HALF"]
    MOVK_RD_X_HALF = INSTRUCTIONS["MOVK_RD_X_HALF"]
    BNE_ADDR_PCREL26_ADR_XPC_PCREL26 = INSTRUCTIONS["BNE_ADDR_PCREL26"]
    BL_ADDR_PCREL26_ADR_XPC_PCREL26 = INSTRUCTIONS["BL_ADDR_PCREL26"]
    ADD_RD_SP_X_SP_RN_SP_X_SP_RM_EXT_X = INSTRUCTIONS[
        "ADD_RD_SP_X_SP_RN_SP_X_SP_RM_EXT_X"
    ]
    LDR_FT_Q_ADDR_UIMM12_ADDR_X_SP_IMM_ADDR_UIMM12 = INSTRUCTIONS[
        "LDR_FT_Q_ADDR_UIMM12"
    ]
    ISB = INSTRUCTIONS["ISB"]
    CMP_RN_SP_X_SP_AIMM = INSTRUCTIONS["CMP_RN_SP_X_SP_AIMM"]
