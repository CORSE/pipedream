"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from enum import Enum, auto
from typing import Optional, Set, Union, Tuple, List, FrozenSet, Dict, cast
from pipedream.utils import abc
from pipedream.asm.ir import *

__all__ = [
    "ARMv8_Register_Set",
    "ARMv8_Register",
    "ANY_REGISTER",
    "ARGUMENT_REGISTER",
    "CALLER_SAVED",
    "CALLEE_SAVED",
]


# Wrapper to allow multiple values in `Enum` (i.e. `Enum.A == Enum.B` but
# `Enum.A is not Enum.B`)
class Unique:
    def __init__(self, value: int) -> None:
        self._value = value

    @property
    def value(self):
        return self._value


_ARMv8_REGBITS_Z = 512  # we set SVE size to 512 bits (arbitrary)
_ARMv8_REGBITS_P = _ARMv8_REGBITS_Z // 8  # predicate register holds 1 bit per Z byte


class _ARMv8_REGISTER_TYPE(Enum):
    # Int
    W = Unique(32)
    X = Unique(64)
    # FP
    B = Unique(8)
    H = Unique(16)
    S = Unique(32)
    D = Unique(64)
    Q = Unique(128)
    # Vect elements
    VB = Unique(8)
    VH = Unique(16)
    VS = Unique(32)
    VD = Unique(64)
    # 4 bytes vector element and subtype for int8mm dotprod feature
    V4B = Unique(32)
    _4B = Unique(32)
    V_2H = Unique(32)
    # Vects
    V_8B = Unique(64)
    V_4H = Unique(64)
    V_2S = Unique(64)
    V_1D = Unique(64)
    V_16B = Unique(128)
    V_8H = Unique(128)
    V_4S = Unique(128)
    V_2D = Unique(128)
    V_1Q = Unique(128)
    # SVE Z vector, elements and vectors
    Z = Unique(_ARMv8_REGBITS_Z)
    ZB = Unique(8)
    ZH = Unique(16)
    ZS = Unique(32)
    ZD = Unique(64)
    ZQ = Unique(128)
    Z_B = Unique(_ARMv8_REGBITS_Z)
    Z_H = Unique(_ARMv8_REGBITS_Z)
    Z_S = Unique(_ARMv8_REGBITS_Z)
    Z_D = Unique(_ARMv8_REGBITS_Z)
    Z_Q = Unique(_ARMv8_REGBITS_Z)
    # SVE Predicate register
    P = Unique(_ARMv8_REGBITS_Z)
    P_B = Unique(_ARMv8_REGBITS_Z)
    P_H = Unique(_ARMv8_REGBITS_Z)
    P_S = Unique(_ARMv8_REGBITS_Z)
    P_D = Unique(_ARMv8_REGBITS_Z)

    # Other
    WSP = Unique(32)
    WZR = Unique(32)
    FLAGS = Unique(32)
    SP = Unique(64)
    ZXR = Unique(64)
    CR = Unique(64)
    SYSREG = Unique(64)
    WPC = Unique(32)
    XPC = Unique(64)

    @property  # type: ignore
    @classmethod  # type: ignore
    def LR(clss):  # type: ignore
        return clss.X

    @property
    def value(self):
        return super().value.value

    @property
    def name(self):
        n = super().name
        return "4B" if n == "_4B" else n


for type_ in _ARMv8_REGISTER_TYPE:
    __all__.append(type_.name)

_ARMv8_REGISTER_TYPE_ALIASES = {"V2H": "V_2H"}

_ARMv8_BASE_SIZES = [
    _ARMv8_REGISTER_TYPE.B,
    _ARMv8_REGISTER_TYPE.H,
    _ARMv8_REGISTER_TYPE.S,
    _ARMv8_REGISTER_TYPE.D,
    _ARMv8_REGISTER_TYPE.Q,
    _ARMv8_REGISTER_TYPE.Z,
]

_ARMv8_WIDTH_TO_V_ELMT_TYPE = {
    _ARMv8_REGISTER_TYPE.B: [_ARMv8_REGISTER_TYPE.VB, _ARMv8_REGISTER_TYPE.ZB],
    _ARMv8_REGISTER_TYPE.H: [_ARMv8_REGISTER_TYPE.VH, _ARMv8_REGISTER_TYPE.ZH],
    _ARMv8_REGISTER_TYPE.S: [_ARMv8_REGISTER_TYPE.VS, _ARMv8_REGISTER_TYPE.ZS],
    _ARMv8_REGISTER_TYPE.D: [_ARMv8_REGISTER_TYPE.VD, _ARMv8_REGISTER_TYPE.ZD],
    _ARMv8_REGISTER_TYPE.Q: [_ARMv8_REGISTER_TYPE.ZQ],
    _ARMv8_REGISTER_TYPE.Z: [],
}

_ARMv8_V_ELMT_TYPE_TO_MAX_REGBITS = {
    _ARMv8_REGISTER_TYPE.V4B: 128,
    _ARMv8_REGISTER_TYPE.VB: 128,
    _ARMv8_REGISTER_TYPE.VH: 128,
    _ARMv8_REGISTER_TYPE.VS: 128,
    _ARMv8_REGISTER_TYPE.VD: 128,
    _ARMv8_REGISTER_TYPE.ZB: _ARMv8_REGBITS_Z,
    _ARMv8_REGISTER_TYPE.ZH: _ARMv8_REGBITS_Z,
    _ARMv8_REGISTER_TYPE.ZS: _ARMv8_REGBITS_Z,
    _ARMv8_REGISTER_TYPE.ZD: _ARMv8_REGBITS_Z,
    _ARMv8_REGISTER_TYPE.ZQ: _ARMv8_REGBITS_Z,
}

_ARMv8_V_ELMT_TYPE_TO_WIDTH = {
    _ARMv8_REGISTER_TYPE.V4B: _ARMv8_REGISTER_TYPE._4B,
    _ARMv8_REGISTER_TYPE.VB: _ARMv8_REGISTER_TYPE.B,
    _ARMv8_REGISTER_TYPE.VH: _ARMv8_REGISTER_TYPE.H,
    _ARMv8_REGISTER_TYPE.VS: _ARMv8_REGISTER_TYPE.S,
    _ARMv8_REGISTER_TYPE.VD: _ARMv8_REGISTER_TYPE.D,
    _ARMv8_REGISTER_TYPE.ZB: _ARMv8_REGISTER_TYPE.B,
    _ARMv8_REGISTER_TYPE.ZH: _ARMv8_REGISTER_TYPE.H,
    _ARMv8_REGISTER_TYPE.ZS: _ARMv8_REGISTER_TYPE.S,
    _ARMv8_REGISTER_TYPE.ZD: _ARMv8_REGISTER_TYPE.D,
    _ARMv8_REGISTER_TYPE.ZQ: _ARMv8_REGISTER_TYPE.Q,
}

_ARMv8_WIDTH_TO_VECT = {
    (_ARMv8_REGISTER_TYPE.S, _ARMv8_REGISTER_TYPE.H): _ARMv8_REGISTER_TYPE.V_2H,
    (_ARMv8_REGISTER_TYPE.D, _ARMv8_REGISTER_TYPE.B): _ARMv8_REGISTER_TYPE.V_8B,
    (_ARMv8_REGISTER_TYPE.D, _ARMv8_REGISTER_TYPE.H): _ARMv8_REGISTER_TYPE.V_4H,
    (_ARMv8_REGISTER_TYPE.D, _ARMv8_REGISTER_TYPE.S): _ARMv8_REGISTER_TYPE.V_2S,
    (_ARMv8_REGISTER_TYPE.D, _ARMv8_REGISTER_TYPE.D): _ARMv8_REGISTER_TYPE.V_1D,
    (_ARMv8_REGISTER_TYPE.Q, _ARMv8_REGISTER_TYPE.B): _ARMv8_REGISTER_TYPE.V_16B,
    (_ARMv8_REGISTER_TYPE.Q, _ARMv8_REGISTER_TYPE.H): _ARMv8_REGISTER_TYPE.V_8H,
    (_ARMv8_REGISTER_TYPE.Q, _ARMv8_REGISTER_TYPE.S): _ARMv8_REGISTER_TYPE.V_4S,
    (_ARMv8_REGISTER_TYPE.Q, _ARMv8_REGISTER_TYPE.D): _ARMv8_REGISTER_TYPE.V_2D,
    (_ARMv8_REGISTER_TYPE.Q, _ARMv8_REGISTER_TYPE.Q): _ARMv8_REGISTER_TYPE.V_1Q,
    (_ARMv8_REGISTER_TYPE.Z, _ARMv8_REGISTER_TYPE.B): _ARMv8_REGISTER_TYPE.Z_B,
    (_ARMv8_REGISTER_TYPE.Z, _ARMv8_REGISTER_TYPE.H): _ARMv8_REGISTER_TYPE.Z_H,
    (_ARMv8_REGISTER_TYPE.Z, _ARMv8_REGISTER_TYPE.S): _ARMv8_REGISTER_TYPE.Z_S,
    (_ARMv8_REGISTER_TYPE.Z, _ARMv8_REGISTER_TYPE.D): _ARMv8_REGISTER_TYPE.Z_D,
    (_ARMv8_REGISTER_TYPE.Z, _ARMv8_REGISTER_TYPE.Q): _ARMv8_REGISTER_TYPE.Z_Q,
}

_ARMv8_VECT_TO_SUBTYPE = {
    _ARMv8_REGISTER_TYPE.V_2H: _ARMv8_REGISTER_TYPE.H,
    _ARMv8_REGISTER_TYPE.V_8B: _ARMv8_REGISTER_TYPE.B,
    _ARMv8_REGISTER_TYPE.V_4H: _ARMv8_REGISTER_TYPE.H,
    _ARMv8_REGISTER_TYPE.V_2S: _ARMv8_REGISTER_TYPE.S,
    _ARMv8_REGISTER_TYPE.V_1D: _ARMv8_REGISTER_TYPE.D,
    _ARMv8_REGISTER_TYPE.V_16B: _ARMv8_REGISTER_TYPE.B,
    _ARMv8_REGISTER_TYPE.V_8H: _ARMv8_REGISTER_TYPE.H,
    _ARMv8_REGISTER_TYPE.V_4S: _ARMv8_REGISTER_TYPE.S,
    _ARMv8_REGISTER_TYPE.V_2D: _ARMv8_REGISTER_TYPE.D,
    _ARMv8_REGISTER_TYPE.V_1Q: _ARMv8_REGISTER_TYPE.Q,
    _ARMv8_REGISTER_TYPE.Z_B: _ARMv8_REGISTER_TYPE.B,
    _ARMv8_REGISTER_TYPE.Z_H: _ARMv8_REGISTER_TYPE.H,
    _ARMv8_REGISTER_TYPE.Z_S: _ARMv8_REGISTER_TYPE.S,
    _ARMv8_REGISTER_TYPE.Z_D: _ARMv8_REGISTER_TYPE.D,
    _ARMv8_REGISTER_TYPE.Z_Q: _ARMv8_REGISTER_TYPE.Q,
    _ARMv8_REGISTER_TYPE.P_B: _ARMv8_REGISTER_TYPE.B,
    _ARMv8_REGISTER_TYPE.P_H: _ARMv8_REGISTER_TYPE.H,
    _ARMv8_REGISTER_TYPE.P_S: _ARMv8_REGISTER_TYPE.S,
    _ARMv8_REGISTER_TYPE.P_D: _ARMv8_REGISTER_TYPE.D,
}


class ARMv8_Register(Register):
    _CURRENT_NUM_REG = 0

    def __init__(
        self,
        idx: Optional[int],
        sub_idx: Optional[int],
        name: Optional[str],
        type_: _ARMv8_REGISTER_TYPE,
        subs: Set["ARMv8_Register"],
    ):
        assert idx is not None or name is not None
        self._idx = self._CURRENT_NUM_REG
        self._CURRENT_NUM_REG += 1
        self._type = type_
        self._widest: "ARMv8_Register" = self
        self._as_width: Dict[int, "ARMv8_Register"] = {self.width: self}
        self._subs: Union[Set["ARMv8_Register"], Tuple] = subs.copy()
        self._aliases: Union[Set["ARMv8_Register"], Tuple] = set()
        self._supers: Union[Set["ARMv8_Register"], Tuple] = set()
        self._elt_size: Optional[str] = None
        self._elt_idx: Optional[int] = None
        self._reg_idx = idx
        if name:
            self._name = name
            self._reg_name = name
        else:
            v_or_z = self._type.name[0]
            if self._type in _ARMv8_V_ELMT_TYPE_TO_WIDTH.keys():
                assert sub_idx is not None
                subtype = _ARMv8_V_ELMT_TYPE_TO_WIDTH[self._type]
                self._reg_name = f"{v_or_z}{idx}"
                self._elt_idx = sub_idx
                self._elt_size = subtype.name
                self._name = f"{self._reg_name}.{self._elt_size}[{self._elt_idx}]"
            elif self._type in _ARMv8_VECT_TO_SUBTYPE.keys():
                assert sub_idx is None
                subtype = _ARMv8_VECT_TO_SUBTYPE[self._type]
                self._reg_name = f"{v_or_z}{idx}"
                self._name = f"{self._reg_name}.{self._type.name[2:]}"
            else:
                assert sub_idx is None
                self._name = f"{self._type.name}{idx}"
                self._reg_name = self._name

    @property  # type:ignore
    @abc.override
    def name(self) -> str:
        return self._name

    @property  # type:ignore
    def asm_name(self) -> str:
        return self._name

    @property  # type:ignore
    @abc.override
    def width(self):
        """Bit width of register."""
        return self._type.value

    @abc.override
    def as_width(self, bits: int):
        return self._as_width[bits]

    @property  # type:ignore
    @abc.override
    def widest(self) -> "ARMv8_Register":
        return self._widest

    @property  # type:ignore
    @abc.override
    def sub_registers(self):
        return iter(self._subs)

    @property  # type:ignore
    @abc.override
    def super_registers(self):
        return iter(self._supers)

    @property  # type:ignore
    @abc.override
    def aliases(self):
        return iter(self._aliases)

    def __lt__(self, other):
        if type(self) is not type(other):
            return NotImplemented

        return self._idx < other._idx

    def __str__(self):
        return type(self).__name__ + "(" + self.name + ")"

    def __repr__(self):
        return type(self).__name__ + "(" + self.name + ")"

    def __deepcopy__(self, memo):
        return self

    def freeze(self):
        self._aliases = tuple((self._aliases | self._subs | self._supers) - set([self]))
        self._subs = tuple(self._subs)
        self._supers = tuple(self._supers)


_ALL_REGISTERS_: List[ARMv8_Register] = []
_ALL_REGISTER_CLASSES_: List[Register_Class] = []

## hack to allow putting register objects in module scope
self = vars()


def _def_ARMv8_reg(
    idx_or_name: Union[int, str, Tuple[int, int]],
    type_: _ARMv8_REGISTER_TYPE,
    sub_regs: List[ARMv8_Register] = [],
    widest=False,
) -> ARMv8_Register:
    sub_idx: Optional[int] = None
    if isinstance(idx_or_name, int):
        idx: Optional[int] = idx_or_name
        name: Optional[str] = None
    elif isinstance(idx_or_name, str):
        idx = None
        name = idx_or_name
    elif isinstance(idx_or_name, tuple):
        idx, sub_idx = idx_or_name
        name = None
    else:
        assert False, "Wrong `idx_or_name` type"
    reg = ARMv8_Register(idx, sub_idx, name, type_, set(sub_regs))
    assert (
        reg.name not in self
    ), f"duplicate register def for name {reg.name}, type {type_}, idx_or_name {idx_or_name}: {reg}"

    self[reg.name] = reg
    _ALL_REGISTERS_.append(reg)
    __all__.append(reg.name)
    if widest:
        for sreg in sub_regs:
            sreg._widest = reg
            sreg._as_width[reg.width] = reg
    return reg


def _def_ARMv8_register_class(name, *registers, **aliases):
    ## put register aliases in module scope
    for k, v in aliases.items():
        assert k not in globals() or globals()[k] is v

        globals()[k] = v
        __all__.append(k)

    clss = Register_Class(name, registers, aliases)
    _ALL_REGISTER_CLASSES_.append(clss)
    globals()[name] = clss
    __all__.append(name)


# All taken from https://developer.arm.com/-/media/Arm%20Developer%20Community/PDF/Learn%20the%20Architecture/Armv8-A%20Instruction%20Set%20Architecture.pdf?revision=ebf53406-04fd-4c67-a485-1b329febfb3e

# 31 64-bit registers X0..X30 and their 32-bit part W0..W30
for idx in range(31):
    w_part = _def_ARMv8_reg(idx, _ARMv8_REGISTER_TYPE.W)
    x_reg = _def_ARMv8_reg(idx, _ARMv8_REGISTER_TYPE.X, [w_part], widest=True)
    x_reg._as_width[w_part.width] = w_part

# Link Register, which is a pure alias of X30
LR: ARMv8_Register
LR = X30  # type:ignore
r_LR = LR  # hack to avoid elision with LR register class
__all__.append("r_LR")


def recurse_vect_fp_reg_add(
    n: int, size_idx: int, vect_idx: int
) -> Tuple[List[ARMv8_Register], List[ARMv8_Register], List[ARMv8_Register]]:
    # Returns (fp_regs, vect_regs, combined_vect)
    if size_idx < 0:
        return [], [], []

    fp_type = _ARMv8_BASE_SIZES[size_idx]
    fp_width = fp_type.value
    ret_fp: List[ARMv8_Register]
    ret_vect: List[ARMv8_Register]
    ret_combined: List[ARMv8_Register]
    as_width_vect: Dict[int, ARMv8_Register]
    if size_idx < 1:
        ret_fp, ret_vect, ret_combined = [], [], []
        as_width_fp = {}
        as_width_vect = {}
    else:
        lower_type = _ARMv8_BASE_SIZES[size_idx - 1]
        lower_width = lower_type.value
        factor = fp_width // lower_width
        ret_fp, ret_vect, ret_combined = recurse_vect_fp_reg_add(
            n,
            size_idx - 1,
            factor * vect_idx,
        )
        as_width_fp = ret_fp[-1]._as_width if len(ret_fp) > 0 else {}
        as_width_vect = ret_vect[-1]._as_width if len(ret_vect) > 0 else {}
        for k in range(1, factor):
            # no `ret_combined` on odd `vect_idx`
            ret_fp_2, ret_vect_2, _ = recurse_vect_fp_reg_add(
                n,
                size_idx - 1,
                factor * vect_idx + k,
            )
            ret_fp += ret_fp_2
            ret_vect += ret_vect_2

    # Used for aliases
    same_size_fp: List[ARMv8_Register] = []
    same_size_vect: List[ARMv8_Register] = []
    same_size_combined: List[ARMv8_Register] = []

    # Adding Vn.Y[x] with Y = `vect_type` and x = `vect_idx`
    # On the first iteration, there is no Rx.Q[0] (Vx.Q[0])
    vect_type_l: List[_ARMv8_REGISTER_TYPE] = []
    if fp_type in _ARMv8_WIDTH_TO_V_ELMT_TYPE:
        vect_type_l.extend(_ARMv8_WIDTH_TO_V_ELMT_TYPE[fp_type])  # type:ignore
    for vect_type in vect_type_l:
        elt_bits = vect_type.value
        vector_bits = _ARMv8_V_ELMT_TYPE_TO_MAX_REGBITS[vect_type]
        if (vect_idx + 1) * elt_bits > vector_bits:
            continue
        new_vect = _def_ARMv8_reg(
            (n, vect_idx),
            vect_type,
            ret_vect,
        )
        # As the dict is a pointer, it is shared by the others subregisters, no need to
        # propagate
        # We use only the first V_ELMT_TYPE for as_width
        if vect_type.value not in as_width_vect:
            as_width_vect[vect_type.value] = new_vect
            new_vect._as_width = as_width_vect
            for reg in as_width_vect.values():
                reg._widest = new_vect
        same_size_vect.append(new_vect)

    # Adding Vn.xY: combined vector composed of vect of size Y = `vect_type` (x
    # integer automatically computed)
    if vect_idx == 0:
        if True:
            for idx, subvect_type_idx in enumerate(range(size_idx + 1)):
                fp_subtype = _ARMv8_BASE_SIZES[subvect_type_idx]
                if (fp_type, fp_subtype) not in _ARMv8_WIDTH_TO_VECT:
                    continue
                vect_type = _ARMv8_WIDTH_TO_VECT[fp_type, fp_subtype]
                new_combined = _def_ARMv8_reg(n, vect_type, ret_combined)
                # Adding `as_width`, only for 128-bit combined vector regs (it includes
                # only 64-bit combined vector regs)
                # Still the same hack of shared dicts
                if fp_type == _ARMv8_REGISTER_TYPE.Q and idx < size_idx:
                    ret_combined[idx]._widest = new_combined
                    as_width = ret_combined[idx]._as_width
                    as_width[new_combined.width] = new_combined
                    new_combined._as_width = as_width
                ret_combined.append(new_combined)
                same_size_combined.append(new_combined)

        # Adding Yn: FP register of size Y = `fp_type`
        new_fp = _def_ARMv8_reg(n, fp_type, ret_fp)
        as_width_fp[fp_type.value] = new_fp
        new_fp._as_width = as_width_fp
        for reg in as_width_fp.values():
            reg._widest = new_fp
        same_size_fp.append(new_fp)
        # Set aliases: current reg aliases with former reg...
        for reg in same_size_fp + same_size_vect + same_size_combined:
            # Adding all aliases to `reg` except itself
            assert isinstance(reg._aliases, set)
            reg._aliases |= set(
                same_size_fp
                + same_size_vect
                + same_size_combined
                + ret_fp
                + ret_vect
                + ret_combined
            ) - set([reg])
        # ... and reverse
        for reg in ret_fp + ret_vect + ret_combined:
            assert isinstance(reg._aliases, set)
            reg._aliases |= set(same_size_fp + same_size_vect + same_size_combined)

    return (
        ret_fp + same_size_fp,
        ret_vect + same_size_vect,
        ret_combined + same_size_combined,
    )


# CR, unused in generation
_def_ARMv8_reg("CR", _ARMv8_REGISTER_TYPE.CR)
_def_ARMv8_reg("SYSREG", _ARMv8_REGISTER_TYPE.SYSREG)

# flags handeling
_def_ARMv8_reg("_NZCV", _ARMv8_REGISTER_TYPE.FLAGS)

# stack pointer
WSP = _def_ARMv8_reg("WSP", _ARMv8_REGISTER_TYPE.WSP)
SP = _def_ARMv8_reg("SP", _ARMv8_REGISTER_TYPE.SP, [WSP], widest=True)

r_SP = SP  # hack to avoid elision with SP register class
__all__.append("r_SP")


r_WSP = WSP  # hack to avoid elision with SP register class
__all__.append("r_WSP")

# zero registers
WZR = _def_ARMv8_reg("WZR", _ARMv8_REGISTER_TYPE.WZR)
ZXR = _def_ARMv8_reg(
    "ZXR", _ARMv8_REGISTER_TYPE.ZXR, [WZR], widest=True  # technically...
)

# PC register, used for PAC relative addressing modes, never explicit on ARM
WPC = _def_ARMv8_reg("WPC", _ARMv8_REGISTER_TYPE.WPC)
PC = _def_ARMv8_reg("PC", _ARMv8_REGISTER_TYPE.XPC, [WPC], widest=True)

# 32 FP / vector regs and their derivatives
for idx in range(32):
    fp_regs, vect_regs, combined_vect_regs = recurse_vect_fp_reg_add(
        idx, len(_ARMv8_BASE_SIZES) - 1, 0
    )

# SVE predicate register, 16 registers
for idx in range(16):
    regs = [
        _def_ARMv8_reg(idx, _ARMv8_REGISTER_TYPE.P),
        _def_ARMv8_reg(idx, _ARMv8_REGISTER_TYPE.P_B),
        _def_ARMv8_reg(idx, _ARMv8_REGISTER_TYPE.P_H),
        _def_ARMv8_reg(idx, _ARMv8_REGISTER_TYPE.P_S),
        _def_ARMv8_reg(idx, _ARMv8_REGISTER_TYPE.P_D),
    ]
    for reg in regs:
        reg._aliases = set(regs) - set([reg])

# Stack pointers
STACK_POINTER_REGISTER: Register_Class
_def_ARMv8_register_class("STACK_POINTER_REGISTER", SP, WSP)

# SIMD top 64-bit register_class Vx.D[1]
_def_ARMv8_register_class("VxD1", *[self[f"V{i}.D[1]"] for i in range(32)])

# Fill in super registers
_ALL_REGISTERS_.sort(key=lambda reg: reg.width)
for reg in _ALL_REGISTERS_:
    for sub in reg._subs:
        sub._supers.add(reg)  # type: ignore

# ARMv8.2 int8mm dotproduct 4-bytes vector elements
# For instance V0.4B[3] and V0.S[3] share the same bits on V0
# Here we do this after updating supers registers,
# and we have to redo the update of subs afterward.
# TODO: may integrate in above recursive function if simpler
for idx in range(32):
    for elt in range(4):
        reg_4b = _def_ARMv8_reg((idx, elt), _ARMv8_REGISTER_TYPE.V4B)
        reg_1s = self[f"V{idx}.S[{elt}]"]
        aliases = reg_1s._aliases | reg_1s._subs | reg_1s._supers | set([reg_1s])
        for alias in aliases:
            alias._aliases |= set([reg_4b])
        reg_4b._aliases = aliases
        reg_4b._subs = reg_1s._subs
        reg_4b._supers = reg_1s._supers
        reg_4b._as_width = reg_1s._as_width
        reg_4b._widest = reg_1s._widest

# Fill in sub registers (after adding Vx.4B[k] registers)
_ALL_REGISTERS_.sort(key=lambda reg: reg.width)
for reg in _ALL_REGISTERS_:
    for sup in reg._supers:
        assert isinstance(sub._subs, set)
        sup._subs.add(reg)  # type: ignore

# Set `as_width` and (which will set correctly `_alias`)
for reg in _ALL_REGISTERS_:
    reg.freeze()

_def_ARMv8_register_class("ANY_REGISTER", *_ALL_REGISTERS_)

# See "Procedure Call Standard for the Arm 64-bit Architecture"
_def_ARMv8_register_class(
    "ARGUMENT_REGISTER",
    *[self[f"X{i}"] for i in range(0, 8)],
    **{f"ARG_{i}": self[f"X{i-1}"] for i in range(1, 8)},
)


_def_ARMv8_register_class(
    "CALLER_SAVED",
    *[self[f"X{i}"] for i in range(0, 19)],
)

_def_ARMv8_register_class("CALLEE_SAVED", LR, *[self[f"X{i}"] for i in range(19, 30)])

for type_ in _ARMv8_REGISTER_TYPE:
    _def_ARMv8_register_class(
        type_.name, *filter(lambda reg: reg._type == type_, _ALL_REGISTERS_)
    )

_def_ARMv8_register_class(
    "X_SP", *([self[f"X{i}"] for i in range(0, 31)] + [self["r_SP"]])
)

_def_ARMv8_register_class(
    "W_SP", *([self[f"W{i}"] for i in range(0, 31)] + [self["r_WSP"]])
)

_def_ARMv8_register_class(
    "VH_16", *([self[f"V{i}.H[{elt}]"] for i in range(16) for elt in range(8)])
)

_def_ARMv8_register_class(
    "VS_16", *([self[f"V{i}.S[{elt}]"] for i in range(0, 16) for elt in range(4)])
)

for alias, clss in _ARMv8_REGISTER_TYPE_ALIASES.items():
    globals()[alias] = globals()[clss]
    __all__.append(alias)

# End of hack
del self


class ARMv8_Register_Set(Register_Set):
    @abc.override
    def stack_pointer_register(self) -> Register:
        return r_SP

    @abc.override
    def stack_pointer_register_class(self) -> Register_Class:
        return STACK_POINTER_REGISTER

    @abc.override
    def register_classes(self) -> List[Register_Class]:
        return _ALL_REGISTER_CLASSES_

    @abc.override
    def all_registers(self) -> Register_Class:
        return ANY_REGISTER  # type: ignore

    @abc.override
    def argument_registers(self) -> Register_Class:
        return ARGUMENT_REGISTER  # type: ignore

    @abc.override
    def callee_save_registers(self) -> Register_Class:
        return CALLEE_SAVED  # type:ignore
