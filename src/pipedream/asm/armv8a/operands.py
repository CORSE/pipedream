"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import functools
import math
import copy
from random import Random
from pipedream.asm import ir
from pipedream.utils import abc

from .flags import ARMv8_NZCV, ARMv8_CPSR
from .registers import *
from typing import Optional, Union, Any, Callable, List, Tuple, cast, Type, Sequence


__all__ = [
    "ARMv8_Operand",
    "ARMv8_Modifier",
    "ARMv8_Memory_Address_Operand",
    "ARMv8_Base_Displacement_Address_Operand",
    "ARMv8_PCRel_Operand",
    "ARMv8_Base_Memory_Operand",
    "ARMv8_Base_Displacement_Memory_Operand",
    "ARMv8_Base_Offset_Memory_Operand",
    "ARMv8_Register_Operand",
    "ARMv8_Immediate_Operand",
    "ARMv8_Shifted_Immediate_Operand",
    "ARMv8_Flags_Operand",
]

X: ir.Register_Class
X_SP: ir.Register_Class
Z_D: ir.Register_Class
Z_S: ir.Register_Class
XPC: ir.Register_Class
PX: ARMv8_Register
PC: ARMv8_Register


class ARMv8_Asm_Str_Mixin:
    @abc.abstractproperty
    def asm_str(self) -> str:
        """
        ASM representation of the operand when.
        """


class ARMv8_Operand(ir.Operand):
    def __init__(self, name: str, classname: str, visibility: ir.Operand_Visibility):
        self._name = name
        self._classname = classname
        self._visibility = visibility

    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return self._name

    @property  # type: ignore
    @abc.override
    def classname(self) -> str:
        return self._classname

    @property  # type: ignore
    def id_name(self) -> str:
        """Used to construct unique instruction ID from operands"""
        return (
            f"{self.name}_{self.classname}"
            if self.name != self.classname
            else self.name
        )

    def __repr__(self) -> str:
        return ir.Operand.__repr__(self)

    @property  # type: ignore
    @abc.override
    def visibility(self) -> ir.Operand_Visibility:
        return self._visibility


class ARMv8_Register_Operand(ARMv8_Operand, ir.Register_Operand):
    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        use_def: ir.Use_Def,
        reg_class: ir.Register_Class,
        reg: Optional[ARMv8_Register] = None,
        modifier: Optional[str] = None,
        list_idx: Tuple[int, int] = None,
        has_extend: bool = False,
        extend: Optional[str] = None,
        has_shift: bool = False,
        shift: Optional[str] = None,
    ):
        classname = reg_class.name
        super().__init__(name, classname, visibility)
        self._use_def = use_def
        self._reg_class: ir.Register_Class = reg_class
        self._reg = reg
        self._modifier = modifier
        self._list_idx = list_idx
        self._has_extend = has_extend
        self._extend = extend
        self._has_shift = has_shift
        self._shift = shift
        # TODO: there is no mechanism for a register operand to assign a shift or
        # an extension.
        # Set a sensible default such that we generate a valid instruction.
        if self._has_extend and self._extend is None and self._reg_class.name == "W":
            self._extend = "SXTW"
        if self._has_shift and self._shift is None and self._reg_class.name == "W":
            self._shift = "LSR #1"

    @property  # type: ignore
    @abc.override
    def register_class(self) -> ir.Register_Class:
        return self._reg_class

    @property  # type: ignore
    @abc.override
    def register(self) -> Optional[ARMv8_Register]:
        return self._reg

    @abc.override
    def with_register(self, reg: ir.Register) -> "ARMv8_Register_Operand":
        assert isinstance(reg, ARMv8_Register)

        if reg not in self.register_class:
            raise TypeError(
                f"Register {reg.name} is not a member of {self.register_class.name}"
            )

        return ARMv8_Register_Operand(
            self.name,
            self.visibility,
            self.use_def,
            self.register_class,
            reg,
            self._modifier,
            self._list_idx,
            self._has_extend,
            self._extend,
            self._has_shift,
            self._shift,
        )

    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        return self._use_def

    @abc.override
    def _arbitrary(self, random: Random) -> "ARMv8_Register":
        return list(self.register_class)[0]

    def __repr__(self) -> str:
        return ir.Register_Operand.__repr__(self)


class ARMv8_Flags_Operand(ARMv8_Operand, ir.Flags_Operand):
    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        reg: ARMv8_Register,
        flags_read: ARMv8_NZCV,
        flags_written: ARMv8_NZCV,
    ):
        assert isinstance(flags_read, ARMv8_NZCV), flags_read
        assert isinstance(flags_written, ARMv8_NZCV), flags_written

        super().__init__(name, "NZCV", visibility)
        self._reg = reg
        self._flags_read = flags_read
        self._flags_written = flags_written

    @property  # type: ignore
    @abc.override
    def is_virtual(self):
        return False

    @property  # type: ignore
    @abc.override
    def flags_read(self) -> ARMv8_NZCV:
        return self._flags_read

    @property  # type: ignore
    @abc.override
    def flags_written(self) -> ARMv8_NZCV:
        return self._flags_written

    @property  # type: ignore
    @abc.override
    def register(self) -> ARMv8_Register:
        return self._reg

    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        if self._flags_read and self._flags_written:
            return ir.Use_Def.USE_DEF
        if self._flags_read:
            return ir.Use_Def.USE
        if self._flags_written:
            return ir.Use_Def.DEF

        # The register in an operand is either used or defined
        assert False, [self.name, self._reg, self._flags_read, self._flags_written]

    def __repr__(self):
        txt = self.name + ":"

        rw = self.flags_read | self.flags_written
        # Taking the biggest PSTATE register: APSR
        flags = [f for f in ARMv8_CPSR if f & rw]

        if flags:
            for f in flags:
                txt += f.name

                if f & self.flags_read:
                    txt += "?"
                if f & self.flags_written:
                    txt += "!"
        else:
            txt += self._reg.name

        return txt


class ARMv8_Memory_Address_Operand(ARMv8_Operand, ir.Composite_Operand):
    @property  # type: ignore
    @abc.override
    def is_virtual(self) -> bool:
        return any(
            (not isinstance(op, ir.Label)) and op.is_virtual for op in self.sub_operands
        )

    def get_operand_name(self, idx: int) -> str:
        return {
            0: "base",
            1: "displacement",
        }[idx]

    @abc.override
    def update_sub_operand(
        self, idx_or_name: Union[int, str], fn: Callable[[ir.Operand], ir.Operand]
    ):
        if isinstance(idx_or_name, int):
            name = self.get_operand_name(idx_or_name)
        else:
            name = idx_or_name

        new = copy.copy(self)
        sub = fn(getattr(new, f"_{name}"))
        assert isinstance(sub, ir.Operand)
        setattr(new, f"_{name}", sub)
        return new

    @property  # type: ignore
    @abc.override
    def address_width(self) -> int:
        return 64

    @abc.override
    def _arbitrary(self, random):
        raise NotImplementedError()

    def __repr__(self):
        return ir.Composite_Operand.__repr__(self)


class ARMv8_Address_Operand(ARMv8_Memory_Address_Operand):
    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        return ir.Use_Def.USE


class ARMv8_Memory_Operand(ARMv8_Memory_Address_Operand):
    default_memory_width = 64

    def __init__(
        self,
        name: str,
        classname: str,
        visibility: ir.Operand_Visibility,
        memory_width: int = None,
        use_def: ir.Use_Def = None,
    ):
        super().__init__(name, classname, visibility)
        self._use_def = use_def if use_def is not None else ir.Use_Def.USE
        self._memory_width = (
            self.default_memory_width if memory_width is None else memory_width
        )

    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        return self._use_def

    @property  # type: ignore
    @abc.override
    def memory_width(self) -> int:
        return self._memory_width


class ARMv8_Immediate_Operand(ARMv8_Operand, ir.Immediate_Operand, ARMv8_Asm_Str_Mixin):
    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        value: Any = None,
    ):
        super().__init__(name, self.__class__.__name__, visibility)
        if not (value is None or self.is_valid_value(value)):
            raise TypeError(
                f"{value} is not a valid value for immediates of type "
                f"{type(self).__name__}"
            )
        self._value: Any = value
        self._shift: Optional[int] = None

    @property  # type: ignore
    @abc.override
    def value(self) -> Any:
        return self._value

    @property  # type: ignore
    def shift(self) -> Optional[int]:
        return self._shift

    @abc.override
    def with_value(self, value):
        clss = type(self)

        if not self.is_valid_value(value):
            raise TypeError(
                f"{value} is not a valid value for immediates of type {self.__name__}"
            )

        return clss(self.name, self.visibility, value)

    @property  # type: ignore
    @abc.override
    def is_virtual(self):
        return self._value is None

    @abc.abstractmethod
    def _arbitrary(self, random: Random):
        """
        Generate a random int/float that can fit in an immediate of this type.
        """

    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        """
        Default representation for an immediate.
        """
        if isinstance(self, (float, int)):
            return f"#{self._value}"
        else:
            return f"{self._value}"

    def __repr__(self) -> str:
        return ir.Immediate_Operand.__repr__(self)


class ARMv8_Base_Displacement_Address_Operand(
    ARMv8_Address_Operand, ir.Base_Displacement_Address_Operand
):
    """
    base/displacement addressing arithmetic
    """

    _base_cls: ir.Register_Class = X
    _disp_cls: Optional[Type[ARMv8_Immediate_Operand]] = None

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        use_def: ir.Use_Def = None,
    ):
        assert use_def == ir.Use_Def.USE or use_def is None
        assert self._base_cls is not None
        base = ARMv8_Register_Operand(
            self._base_cls.name, visibility, ir.Use_Def.USE, self._base_cls
        )
        assert self._disp_cls is not None
        displacement = self._disp_cls(self._disp_cls.__name__, visibility)
        super().__init__(name, name, visibility)
        self._displacement = displacement
        self._base = base

    def get_operand_name(self, idx: int) -> str:
        return {
            0: "base",
            1: "displacement",
        }[idx]

    @property  # type: ignore
    def base(self) -> ir.Register_Operand:
        return self._base

    @property  # type: ignore
    def displacement(self) -> ARMv8_Immediate_Operand:
        return self._displacement

    def with_base(
        self, base_reg: ir.Register
    ) -> "ARMv8_Base_Displacement_Address_Operand":
        new = copy.copy(self)
        new._base = new._base.with_register(base_reg)
        return new

    def with_displacement(
        self, disp: Union[int, ir.Label, ir.Register]
    ) -> "ARMv8_Base_Displacement_Address_Operand":
        new = copy.copy(self)
        new._displacement = new._displacement.with_value(disp)
        return new

    @property  # type: ignore
    @abc.override
    def sub_operands(self):
        yield self._base
        yield self._displacement

    @property  # type: ignore
    @abc.override
    def is_virtual(self) -> bool:
        return any(
            (not isinstance(op, ir.Label)) and op.is_virtual for op in self.sub_operands
        )


class ARMv8_PCRel_Operand(ARMv8_Base_Displacement_Address_Operand, ir.Label_Operand):
    # PCRel op may carry a label, so it inherits also from `ir.Label_Operand`

    _base_cls: ir.Register_Class = XPC

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        use_def: ir.Use_Def = None,
        value: Optional[ir.Label] = None,
    ):
        assert use_def == ir.Use_Def.USE or use_def is None
        super().__init__(name, visibility)
        self._value = value

    @abc.override
    def is_valid_value(self, value):
        if type(value) is ir.Label:
            return True

    # Jump target has no arbitrary value!
    @abc.override
    def _arbitrary(self, random: Random):
        assert False

    @property  # type: ignore
    @abc.override
    def sub_operands(self):
        if self._value is not None:
            yield self._value
        else:
            yield self._base
            yield self._displacement

    @abc.override
    def with_label(self, value):
        clss = type(self)

        if not self.is_valid_value(value):
            raise TypeError(
                f"{value} is not a valid value for immediates of type {self.__name__}"
            )

        return clss(self.name, self.visibility, value=value)


class ARMv8_Base_Memory_Operand(ARMv8_Memory_Operand, ir.Base_Memory_Operand):
    """
    Base class for simple base register memory addressing mode.
    """

    _base_cls: Optional[ir.Register_Class] = None

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        memory_width: int = None,
        use_def: ir.Use_Def = None,
    ):
        assert self._base_cls is not None
        base = ARMv8_Register_Operand(
            self._base_cls.name, visibility, ir.Use_Def.USE, self._base_cls
        )
        super().__init__(name, name, visibility, memory_width, use_def)
        self._base = base

    def get_operand_name(self, idx: int) -> str:
        return {
            0: "base",
        }[idx]

    @property  # type: ignore
    def base(self) -> ir.Register_Operand:
        return self._base

    def with_base(self, base_reg: ir.Register) -> "ARMv8_Base_Memory_Operand":
        new = copy.copy(self)
        new._base = new._base.with_register(base_reg)
        return new

    @property  # type: ignore
    @abc.override
    def sub_operands(self):
        yield self._base

    @abc.override
    def _arbitrary(self, random):
        raise NotImplementedError()


class ARMv8_Modifier(ir.Operand):
    def __init__(self, values: Sequence[str]):
        self._name = "Mod"
        self._values = values
        self._value: Optional[str] = None

    def copy(self) -> "ARMv8_Modifier":
        ret = ARMv8_Modifier(self._values)
        ret._value = self._value
        return ret

    @abc.override
    def _arbitrary(self, random) -> str:
        return self._values[0]  # forced

    @property  # type: ignore
    @abc.override
    def is_virtual(self):
        return False

    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return self._name

    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        assert self._value is not None
        return self._value

    @property  # type: ignore
    @abc.override
    def visibility(self) -> ir.Operand_Visibility:
        return ir.Operand_Visibility.EXPLICIT

    @abc.override
    def with_operand_value(self, value: str):
        self._value = value
        return self

    @property  # type: ignore
    @abc.override
    def use_def(self) -> ir.Use_Def:
        return ir.Use_Def.USE


class ARMv8_Base_Displacement_Memory_Operand(
    ARMv8_Memory_Operand, ir.Base_Displacement_Memory_Operand
):
    """
    Base class register base + immediate disp. memory addressing mode.
    """

    _base_cls: ir.Register_Class = X
    _disp_cls: Optional[Type[ARMv8_Immediate_Operand]] = None
    _mod_set: Optional[ARMv8_Modifier] = None

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        memory_width: int = None,
        use_def: ir.Use_Def = None,
    ):
        assert self._base_cls is not None
        base = ARMv8_Register_Operand(
            self._base_cls.name, visibility, ir.Use_Def.USE, self._base_cls
        )
        assert self._disp_cls is not None
        self._base = base
        self._displacement = self._disp_cls(self._disp_cls.__name__, visibility)
        self._mod = self._mod_set.copy() if self._mod_set else None  # forced
        super().__init__(name, name, visibility, memory_width, use_def)

    def get_operand_name(self, idx: int) -> str:
        return {
            0: "base",
            1: "displacement",
            2: "mod",
        }[idx]

    @property  # type: ignore
    def base(self) -> ir.Register_Operand:
        return self._base

    @property  # type: ignore
    def displacement(self) -> ARMv8_Immediate_Operand:
        return self._displacement

    def with_base(
        self, base_reg: ir.Register
    ) -> "ARMv8_Base_Displacement_Memory_Operand":
        new = copy.copy(self)
        new._base = new._base.with_register(base_reg)
        return new

    def with_displacement(
        self, disp: Union[int, ir.Label, ir.Register]
    ) -> "ARMv8_Base_Displacement_Memory_Operand":
        assert isinstance(disp, int)
        new = copy.copy(self)
        new._displacement = new._displacement.with_value(disp)
        return new

    @property  # type: ignore
    @abc.override
    def sub_operands(self):
        yield self._base
        yield self._displacement
        if self._mod:
            yield self._mod


class ARMv8_Base_Offset_Memory_Operand(
    ARMv8_Memory_Operand, ir.Base_Offset_Memory_Operand
):
    """
    Base class for defining base/register operands
    """

    _base_cls: ir.Register_Class = X
    _off_cls: ir.Register_Class
    _mod_set: Optional[ARMv8_Modifier] = None

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        memory_width: int = None,
        use_def: ir.Use_Def = None,
    ):
        assert self._base_cls is not None
        assert self._off_cls is not None
        self._base = ARMv8_Register_Operand(
            self._base_cls.name, visibility, ir.Use_Def.USE, self._base_cls
        )
        self._offset = ARMv8_Register_Operand(
            self._off_cls.name, visibility, ir.Use_Def.USE, self._off_cls
        )
        self._mod = self._mod_set.copy() if self._mod_set else None
        super().__init__(name, name, visibility, memory_width, use_def)

    def get_operand_name(self, idx: int) -> str:
        return {0: "base", 1: "offset", 2: "mod"}[idx]

    @property
    def base(self) -> ir.Register_Operand:
        return cast(ir.Register_Operand, self._base)

    @property
    def offset(self) -> ir.Register_Operand:
        return cast(ir.Register_Operand, self._offset)

    def with_base(self, base_reg: ir.Register) -> "ARMv8_Base_Offset_Memory_Operand":
        new = copy.copy(self)
        new._base = new._base.with_register(base_reg)
        return new

    def with_offset(
        self, offset_reg: ir.Register
    ) -> "ARMv8_Base_Offset_Memory_Operand":
        new = copy.copy(self)
        new._offset = new._offset.with_register(offset_reg)
        return new

    @property  # type: ignore
    @abc.override
    def sub_operands(self):
        yield self._base
        yield self._offset
        if self._mod is not None:
            yield self._mod


def _signed_min_max(num_bits: int):
    assert num_bits >= 0
    assert type(num_bits) is int
    if num_bits == 0:
        return 0, 0
    min = -(2 ** (num_bits - 1))
    max = +(2 ** (num_bits - 1)) - 1
    return min, max


def _unsigned_min_max(num_bits: int):
    assert num_bits >= 0
    assert type(num_bits) is int
    if num_bits == 0:
        return 0, 0
    min = 0
    max = 2**num_bits - 1
    return min, max


class ImmBits(ir.ImmMinMax_Operand, ARMv8_Immediate_Operand):
    """
    Every operands with specific min/max values
    """

    _num_bits: int
    _signed: bool

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        num_bits: int = None,
        signed: bool = None,
        value: int = None,
        mult: int = None,
        min_val: int = None,
        max_val: int = None,
    ):
        if num_bits is not None:
            self._num_bits = num_bits
        if signed is not None:
            self._signed = signed
        _min, _max = self._init_min_max()
        if min_val is not None:
            _min = min_val
        if max_val is not None:
            _max = max_val
        super().__init__(name, visibility, _min, _max, value, mult)

    @abc.override
    @classmethod
    def _init_min_max(clss) -> Tuple[int, int]:
        _min, _max = None, None
        if clss._num_bits is not None:
            if clss._signed:
                _min, _max = _signed_min_max(clss._num_bits)
            else:
                _min, _max = _unsigned_min_max(clss._num_bits)
            if clss._mult:
                return _min * clss._mult, _max * clss._mult
            else:
                return _min, _max
        raise ValueError(
            f"Invalid num_bit or min/max override not defined (class {clss})"
        )

    @property  # type: ignore
    @abc.override
    def num_bits(self) -> int:
        assert self._num_bits is not None
        return self._num_bits

    @property
    def signed(self) -> bool:
        assert self._signed is not None
        return self._signed

    @abc.override
    def _arbitrary(self, random):
        if self._mult:
            return self._mult
        else:
            return 1


class ARMv8_Value_Set_Immediate(ARMv8_Immediate_Operand, ARMv8_Asm_Str_Mixin):
    """
    Base class for an immediate/str operand with a set of values.
    """

    _value_set: Tuple = ()

    def __init__(self, name: str, visibility: ir.Operand_Visibility, value: Any = None):
        super().__init__(name, visibility, value)

    @abc.override
    def is_valid_value(self, value):
        return value in self._value_set

    @abc.override
    def _arbitrary(self, random: Random):
        assert len(self._value_set) > 0
        return self._value_set[0]


### Operands as described by binutils ###


class AIMM(ImmBits):
    """
    12-bit unsigned immediate with optional left shift of 12 bits
    """

    # TODO: Add support for shifts
    _num_bits = 12
    _signed = False


class UIMM3_OP1(ImmBits):
    """
    a 3-bit unsigned immediate
    """

    _num_bits = 3
    _signed = False


UIMM3_OP2 = UIMM3_OP1


class SVE_FPIMM8(ImmBits):
    """
    8-bit floating-point immediate
    """

    # TODO: Actually a 8 bit value?
    _num_bits = 8
    _signed = False


class TME_UIMM16(ImmBits):
    """
    a 16-bit unsigned immediate for TME tcancel
    """

    _num_bits = 16
    _signed = False


class LIMM(ImmBits):
    """
    Logical Immediate, 10-bit
    """

    _num_bits = 10
    _signed = False


class IMM_MOV(ImmBits):
    """
    Immediate (?)
    """

    _num_bits = 16
    _signed = True


class ImmAddr(ImmBits):
    @abc.override
    def _arbitrary(self, random: Random) -> Any:
        # always align to max bytes alignment for addresses (here 16*4)
        maxi = self._max
        assert maxi is not None
        mini = self._min
        assert mini is not None
        aligned = maxi & ~(64 - 1)
        return mini if aligned < mini else aligned


class ImmPCRelAddr(ImmBits):
    @abc.override
    def _arbitrary(self, random: Random):
        return 16 * 4  # Just set an arbitray offset of 16 instructions


class IMM_ADDR_SIMM7(ImmAddr):
    """
    7-bit signed immediate
    """

    _num_bits = 7
    _signed = True


class IMM_ADDR_SIMM9(ImmAddr):
    """
    9-bit signed immediate
    """

    _num_bits = 9
    _signed = True


class IMM_ADDR_SIMM10(ImmAddr):
    """
    10-bit signed immediate
    """

    _num_bits = 10
    _signed = True


class IMM_ADDR_SIMM11(ImmAddr):
    """
    11-bit signed immediate
    """

    _num_bits = 11
    _signed = True


class IMM_ADDR_SIMM13(ImmAddr):
    """
    address with 13-bit signed immediate (multiple of 16) offset
    """

    _num_bits = 13
    _signed = True
    _mult = 16


class IMM_ADDR_UIMM12(ImmAddr):
    """
    address with 12-bit unsigned immediate
    """

    _num_bits = 12
    _signed = False


class IMM_PCREL14(ImmPCRelAddr):
    """
    Immediate for PC rel 14 bits
    """

    _num_bits = 14
    _signed = True


class IMM_PCREL19(ImmPCRelAddr):
    """
    Immediate for PC rel 21 bits
    """

    _num_bits = 21
    _signed = True


class IMM_PCREL21(ImmPCRelAddr):
    """
    Immediate for PC rel 21 bits
    """

    _num_bits = 21
    _signed = True


class IMM_PCREL26(ImmPCRelAddr):
    """
    Immediate for PC rel 26 bits
    """

    _num_bits = 26
    _signed = True


class IMM_ADRP(ImmPCRelAddr):
    """
    Immediate for PC rel 21 bits used in adrp
    """

    _num_bits = 21
    _signed = True


class ARMv8_Shifted_Immediate_Operand(ir.Shifted_Immediate_Operand):
    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        if self._shift is not None:
            return f"#{self._value}, LSL #{self._shift}"
        else:
            return f"#{self._value}"


class HALF(ImmBits, ARMv8_Shifted_Immediate_Operand):
    """
    16-bit usigned immediate with optional left shift
    """

    _num_bits = 16
    _signed = False

    @abc.override
    def with_shifted_value(self, value, shift):
        assert shift is not None
        clss = type(self)

        if not self.is_valid_value(value):
            raise TypeError(
                f"{value} is not a valid value for immediates of type {self.__name__}"
            )

        new = copy.copy(self)
        new._value = value
        new._shift = shift
        return new

    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        return ARMv8_Shifted_Immediate_Operand.asm_str.fget(self)


class UIMM10(ImmBits):
    """
    10-bit unsigned multiple of 16
    """

    _num_bits = 10
    _signed = False
    _mult = 16


class UIMM4_ADDG(ImmBits):
    """
    4-bit unsigned Logical Address Tag modifier
    """

    # TODO: define tag modifier
    _num_bits = 4
    _signed = False


class IDX(ImmBits):
    """
    an immediate as the index of the least significant byte
    """

    _num_bits = 4
    _signed = False


class WIDTH(ImmBits):
    """
    the width of the bit-field
    """

    _num_bits = 6
    _signed = False

    @abc.override
    def _arbitrary(self, random: Random) -> Union[int, float]:
        assert self._min is not None
        return self._min  # used in combination with other operands, take least value


class IMMR(ImmBits):
    """
    the right rotate amount
    """

    _num_bits = 6
    _signed = False


class IMMS(ImmBits):
    """
    the leftmost bit number to be moved from the source
    """

    _num_bits = 6
    _signed = False


class IMM(ImmBits):
    """
    An immediate
    """

    _num_bits = 6
    _signed = False


class IMM_2(ImmBits):
    """
    An immediate
    """

    _num_bits = 6
    _signed = False


class FBITS(ImmBits):
    """
    the number of bits after the binary point in the fixed-point value
    """

    _num_bits = 4
    _signed = False


class BIT_NUM(ImmBits):
    """
    the bit number to be tested
    """

    _num_bits = 4
    _signed = False


class MASK(ImmBits):
    """
    an immediate as the index of the least significant byte
    """

    _num_bits = 4
    _signed = False


class IMM_ROT1(ARMv8_Value_Set_Immediate):
    """
    2-bit rotation specifier for complex arithmetic operations
    """

    _value_set = (0, 90, 180, 270)


class IMM_ROT2(ARMv8_Value_Set_Immediate):
    """
    2-bit rotation specifier for complex arithmetic operations
    """

    _value_set = (0, 90, 180, 270)


class IMM_ROT3(ARMv8_Value_Set_Immediate):
    """
    1-bit rotation specifier for complex arithmetic operations
    """

    _value_set = (90, 270)


class IMM_LSL(ImmBits):
    """
    left shift amount for an AdvSIMD register
    """

    _num_bits = 8
    _signed = False
    _modifier_set = (0, 8, 16, 24)
    _modifier = 0  # TODO: arbitrarily set

    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        return f"#{self._value}, LSL #{self._modifier}"


class IMM_MSL(ImmBits):
    """
    left shift ones for an AdvSIMD register
    """

    _num_bits = 8
    _signed = False
    _modifier_set = (8, 16)
    _modifier = 8  # TODO: arbitrarily set

    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        return f"#{self._value}, MSL #{self._modifier}"


class SIMD_FPIMM(ARMv8_Value_Set_Immediate):
    """
    an 8-bit floating-point constant
    """

    _value_set = (0.125, 10.0, 31.0)


FPIMM = SIMD_FPIMM


class SIMD_IMM(ImmBits):
    """
    An immediate
    """

    _num_bits = 8
    _signed = False

    def _arbitrary(self, random):
        return 0


class IMM0(ARMv8_Value_Set_Immediate):
    """
    Constant 0 operand
    """

    _value_set = (0,)


class FPIMM0(ARMv8_Value_Set_Immediate):
    """
    Constant 0.0 operand
    """

    _value_set = (0.0,)


class SHLL_IMM(ImmBits):
    """
    An immediate shift amount of 8, 16 or 32
    """

    _value_set = (8, 16, 32)

    @abc.override
    def _arbitrary(self, random: Random) -> int:
        return 8

    @abc.override
    def _init_min_max(self):
        return 8, 32


class IMM_VLSR(ImmBits):
    """
    a right shift amount for an AdvSIMD register
    """

    _num_bits = 7
    _signed = False

    @abc.override
    def _arbitrary(self, random: Random) -> int:
        return 7  # Ensure that shift amount is always less than size of smallest element (byte)


class IMM_VLSL(ImmBits):
    """
    a left shift amount for an AdvSIMD register
    """

    # TODO: change operand width to match other operands?
    _num_bits = 7
    _signed = False

    @abc.override
    def _arbitrary(self, random: Random) -> int:
        return 7  # Ensure that shift amount is always less than size of smallest element (byte)


class SVE_SIMM4(ImmBits):
    """
    4-bit signed immediate
    """

    _num_bits = 4
    _signed = True


class SVE_SIMM4x2(ImmBits):
    """
    4-bit signed immediate, multiple of 2
    """

    _num_bits = 4
    _signed = True
    _mult = 2


class SVE_SIMM4x3(ImmBits):
    """
    4-bit signed immediate, multiple of 3
    """

    _num_bits = 4
    _signed = True
    _mult = 3


class SVE_SIMM4x4(ImmBits):
    """
    4-bit signed immediate, multiple of 4
    """

    _num_bits = 4
    _signed = True
    _mult = 4


class SVE_SIMM4x16(ImmBits):
    """
    4-bit signed immediate, multiple of 64
    """

    _num_bits = 4
    _signed = True
    _mult = 16


class SVE_SIMM4x32(ImmBits):
    """
    4-bit signed immediate, multiple of 32
    """

    _num_bits = 4
    _signed = True
    _mult = 32


class SIMM5(ImmBits):
    """
    5-bit signed immediate
    """

    _num_bits = 5
    _signed = True


class SVE_SIMM5(ImmBits):
    """
    5-bit signed immediate
    """

    _num_bits = 5
    _signed = True


class SVE_SIMM5B(ImmBits):
    """
    5-bit signed immediate
    """

    _num_bits = 5
    _signed = True


class SVE_UIMM5(ImmBits):
    """
    5-bit unsigned immediate
    """

    _num_bits = 5
    _signed = False


class SVE_UIMM5x2(ImmBits):
    """
    5-bit unsigned immediate, mult of 2
    """

    _num_bits = 5
    _signed = False
    _mult = 2


class SVE_UIMM5x4(ImmBits):
    """
    5-bit unsigned immediate, mult of 4
    """

    _num_bits = 5
    _signed = False
    _mult = 4


class SVE_UIMM5x8(ImmBits):
    """
    5-bit unsigned immediate, mult of 8
    """

    _num_bits = 5
    _signed = False
    _mult = 8


class SVE_UIMM6(ImmBits):
    """
    6-bit unsigned immediate
    """

    _num_bits = 6
    _signed = False


class SVE_UIMM6x2(ImmBits):
    """
    6-bit unsigned immediate, mult of 2
    """

    _num_bits = 6
    _signed = False
    _mult = 2


class SVE_UIMM6x4(ImmBits):
    """
    6-bit unsigned immediate, mult of 4
    """

    _num_bits = 6
    _signed = False
    _mult = 4


class SVE_UIMM6x6(ImmBits):
    """
    6-bit unsigned immediate
    """

    _num_bits = 6
    _signed = False
    _mult = 6


class SVE_UIMM7(ImmBits):
    """
    7-bit unsigned immediate
    """

    _num_bits = 7
    _signed = False


class SVE_UIMM8_53(ImmBits):
    """
    8-bit unsigned immediate
    """

    _num_bits = 8
    _signed = False


class SVE_LIMM_MOV(ImmBits):
    """
    8-bit signed immediate
    """

    _num_bits = 8
    _signed = True


class SVE_LIMM(ImmBits):
    """
    13-bit unsigned immediate
    Used in AND <Zdn>.<T>, <Zdn>.<T>, #<const> for instance
    """

    _num_bits = 13
    _signed = False


class SVE_SHRIMM_PRED(ImmBits):
    """
    5-bit unsigned immediate
    Used in ASR <Zdn>.<T>, <Pg>/M, <Zdn>.<T>, #<const> for instance
    """

    _num_bits = 5
    _signed = False


class SVE_SHRIMM_UNPRED(ImmBits):
    """
    5-bit unsigned immediate
    Used in ASR <Zd>.<T>, <Zn>.<T>, #<const> for instance
    """

    _num_bits = 5
    _signed = False


class SVE_SHRIMM_UNPRED_22(ImmBits):
    """
    5-bit unsigned immediate
    As in: in UQRSHRNB <Zd>.<T>, <Zn>.<Tb>, #<const>
    """

    _num_bits = 5
    _signed = False


class SVE_SHLIMM_PRED(ImmBits):
    """
    5-bit unsigned immediate
    Used in LSL <Zdn>.<T>, <Pg>/M, <Zdn>.<T>, #<const> for instance
    """

    _num_bits = 5
    _signed = False


class SVE_SHLIMM_UNPRED(ImmBits):
    """
    5-bit unsigned immediate
    Used in LSL <Zd>.<T>, <Zn>.<T>, #<const> for instance
    """

    _num_bits = 5
    _signed = False


class SVE_SHLIMM_UNPRED_22(ImmBits):
    """
    5-bit unsigned immediate
    As in: in SSHLLB <Zd>.<T>, <Zn>.<Tb>, #<const>
    """

    _num_bits = 5
    _signed = False


class SVE_ASIMM(ImmBits):
    """
    8-bit signed immediate
    """

    _num_bits = 8
    _signed = True


class SVE_AIMM(ImmBits):
    """
    8-bit unsigned immediate
    """

    _num_bits = 8
    _signed = False


class SVE_SIMM6(ImmBits):
    """
    6-bit signed immediate
    """

    _num_bits = 6
    _signed = True


class SVE_SIMM8(ImmBits):
    """
    8-bit signed immediate
    """

    _num_bits = 8
    _signed = True


class SVE_UIMM8(ImmBits):
    """
    8-bit unsigned immediate
    """

    _num_bits = 8
    _signed = False


class SVE_SIMM16(ImmBits):
    """
    16-bit signed immediate
    """

    _num_bits = 16
    _signed = True


class SVE_I1_ZERO_ONE(ARMv8_Value_Set_Immediate):
    """
    1-bit unsigned immediate
    """

    _value_set = (0.0, 1.0)


class SVE_I1_HALF_TWO(ARMv8_Value_Set_Immediate):
    """
    1-bit unsigned immediate
    """

    _value_set = (0.5, 2.0)


class SVE_I1_HALF_ONE(ARMv8_Value_Set_Immediate):
    """
    1-bit unsigned immediate
    """

    _value_set = (0.5, 1.0)


class SVE_IMM_ROT1(ARMv8_Value_Set_Immediate):
    """
    1-bit rotation specifier for complex arithmetic operations
    """

    _value_set = (90, 270)


class SVE_IMM_ROT2(ARMv8_Value_Set_Immediate):
    """
    2-bit rotation specifier for complex arithmetic operations
    """

    _value_set = (0, 90, 180, 270)


class SVE_IMM_ROT3(ARMv8_Value_Set_Immediate):
    """
    1-bit unsigned immediate
    """

    _value_set = (90, 270)


class SVE_UIMM3(ImmBits):
    """
    3-bit unsigned immediate
    """

    _num_bits = 3
    _signed = False


class CCMP_IMM(ImmBits):
    """
    5-bit unsigned immediate
    """

    _num_bits = 5
    _signed = False


class SVE_INV_LIMM(ImmBits):
    """
    Logical Immediate, an inverted 13-bit logical immediate
    """

    _num_bits = 13
    _signed = False


class SVE_PATTERN_SCALED(ARMv8_Immediate_Operand):
    """
    A scaled pattern {, <pattern>{, MUL #<imm>}}
    As in: CNTH <Xd>{, <pattern>{, MUL #<imm>}}
    We encode value as a tuple (pattern, imm)
    Default to {ALL, MUL #1}
    """

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        value: Tuple[Union[str, int], int] = None,
    ):
        super().__init__(name, visibility, value)

    @abc.override
    def is_valid_value(self, value):
        if not isinstance(value, tuple):
            return False
        pattern, scale = value
        if not (1 <= scale <= 16):
            return False
        if isinstance(pattern, int) and not (0 <= pattern <= 2**5 - 1):
            return False
        if not isinstance(pattern, str):
            return False
        valid_pattern = (
            ["POW2"]
            + [f"VL{a}" for a in list(range(1, 9)) + [8, 16, 32, 64, 128, 256]]
            + ["MUL4", "MUL3", "ALL"]
        )
        if pattern not in valid_pattern:
            return False
        return True

    @abc.override
    def _arbitrary(self, random: Random):
        return ("ALL", 2)

    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        assert self._value is not None
        pfx = "#" if isinstance(self._value[0], int) else ""
        if self.value[1] != 1:
            return f"{pfx}{self._value[0]}, MUL {self._value[1]}"
        else:
            return f"{pfx}{self._value[0]}"


class SVE_PATTERN(SVE_PATTERN_SCALED):
    """
    A non scaled pattern {, <pattern>}
    As in: PTRUE <Pd>.<T>{, <pattern>}
    We encode value as a in the SVE_PATTERN_SCALED, a tuple (pattern, imm)
    Default to {ALL}
    """

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        value: Tuple[Union[str, int], int] = None,
    ):
        super().__init__(name, visibility, value)

    @abc.override
    def is_valid_value(self, value):
        if not super().is_valid_value(value):
            return False
        if value[1] != 1:
            return False
        return True

    @abc.override
    def _arbitrary(self, random: Random):
        return ("ALL", 1)


class PRFOP(ARMv8_Immediate_Operand):
    """
    The prefetch operation specifier
    As in: PRM <prfop>, [<Xn|SP>, <Wm>|<Xm>]
    """

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        value: Union[str, int] = None,
    ):
        super().__init__(name, visibility, value)

    @abc.override
    def is_valid_value(self, value):
        if isinstance(value, int) and (0 <= value <= 2**5 - 1):
            return True
        if isinstance(value, str) and value in [
            "PLDL1KEEP",
            "PLDL1STRM",
            "PLDL2KEEP",
            "PLDL2STRM",
            "PLDL3KEEP",
            "PLDL3STRM",
            "PLIL1KEEP",
            "PLIL1STRM",
            "PLIL2KEEP",
            "PLIL2STRM",
            "PLIL3KEEP",
            "PLIL3STRM",
            "PSTL1KEEP",
            "PSTL1STRM",
            "PSTL2KEEP",
            "PSTL2STRM",
            "PSTL3KEEP",
            "PSTL3STRM",
        ]:
            return True
        return False

    @abc.override
    def _arbitrary(self, random: Random):
        return "PLDL1KEEP"

    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        if isinstance(self._value, int):
            return f"#{self._value}"
        else:
            return f"{self._value}"


class SVE_PRFOP(ARMv8_Immediate_Operand):
    """
    The prefetch operation specifier
    As in: PRFW <prfop>, <Pg>, [<Xn|SP>, <Zm>.D, LSL #2]
    """

    def __init__(
        self,
        name: str,
        visibility: ir.Operand_Visibility,
        value: Union[str, int] = None,
    ):
        super().__init__(name, visibility, value)

    @abc.override
    def is_valid_value(self, value):
        if isinstance(value, int) and (0 <= value <= 2**4 - 1):
            return True
        if isinstance(value, str) and value in [
            "PLDL1KEEP",
            "PLDL1STRM",
            "PLDL2KEEP",
            "PLDL2STRM",
            "PLDL3KEEP",
            "PLDL3STRM",
            "PSTL1KEEP",
            "PSTL1STRM",
            "PSTL2KEEP",
            "PSTL2STRM",
            "PSTL3KEEP",
            "PSTL3STRM",
        ]:
            return True
        return False

    @abc.override
    def _arbitrary(self, random: Random):
        return "PLDL1KEEP"

    @property  # type: ignore
    @abc.override
    def asm_str(self) -> str:
        if isinstance(self._value, int):
            return f"#{self._value}"
        else:
            return f"{self._value}"


class COND(ARMv8_Value_Set_Immediate):
    """
    One of the standard condition flags:
    EQ  Equal
    NE  Not equal
    CS  Carry set (identical to HS)
    HS  Unsigned higher or same (identical to CS)
    CC  Carry clear (identical to LO)
    LO  Unsigned lower (identical to CC)
    MI  Minus or negative result
    PL  Positive or zero result
    VS  Overflow
    VC  No overflow
    HI  Unsigned higher
    LS  Unsigned lower or same
    GE  Signed greater than or equal
    LT  Signed less than
    GT  Signed greater than
    LE  Signed less than or equal
    AL  Always (this is the default)
    NV  Identical to AL
    """

    _value_set = (
        "EQ",
        "NE",
        "CS",
        "HS",
        "CC",
        "LO",
        "MI",
        "PL",
        "VS",
        "VC",
        "HI",
        "LS",
        "GE",
        "LT",
        "GT",
        "LE",
        "AL",
        "NV",
    )


class COND1(ARMv8_Value_Set_Immediate):
    """
    One of the standard condition flags except AL, NV.following condition flag:
    """

    _value_set = (
        "EQ",
        "NE",
        "CS",
        "HS",
        "CC",
        "LO",
        "MI",
        "PL",
        "VS",
        "VC",
        "HI",
        "LS",
        "GE",
        "LT",
        "GT",
        "LE",
    )


class NZCV(ImmBits):
    """
    flag bit specifier giving an alternative value for each flag (0 to 15)
    """

    _num_bits = 4
    _signed = False


class SIMD_ADDR_SIMPLE(ARMv8_Base_Memory_Operand):
    """
    [Xn|SP] addr operand
    """

    _base_cls = X_SP


class SIMD_ADDR_POST(ARMv8_Base_Memory_Operand):
    """
    [Xn|SP]! addr operand
    """

    # TODO: post increment use/def on base
    _base_cls = X_SP


class ADDR_SIMPLE(ARMv8_Base_Memory_Operand):
    """
    [Xn|Sp]
    """

    _base_cls = X_SP


class ADDR_SIMM7(ARMv8_Base_Displacement_Memory_Operand):
    """
    [XN|Sp + 7-bit signed immediate]
    """

    _base_cls = X_SP
    _disp_cls = IMM_ADDR_SIMM7


class ADDR_SIMM9(ARMv8_Base_Displacement_Memory_Operand):
    """
    [XN|Sp + 9-bit signed immediate]
    """

    _base_cls = X_SP
    _disp_cls = IMM_ADDR_SIMM9


class ADDR_OFFSET(ARMv8_Base_Displacement_Memory_Operand):
    """
    [XN|Sp + 9-bit signed immediate] (unscaled)
    """

    _base_cls = X_SP
    _disp_cls = IMM_ADDR_SIMM9


class ADDR_SIMM10(ARMv8_Base_Displacement_Memory_Operand):
    """
    [XN|Sp + 10-bit signed immediate]
    """

    _base_cls = X_SP
    _disp_cls = IMM_ADDR_SIMM10


class ADDR_SIMM11(ARMv8_Base_Displacement_Memory_Operand):
    """
    [XN|Sp + 11-bit signed immediate]
    """

    _base_cls = X_SP
    _disp_cls = IMM_ADDR_SIMM11


class ADDR_SIMM13(ARMv8_Base_Displacement_Memory_Operand):
    """
    [XN|Sp + 13-bit signed immediate]
    address with 13-bit signed immediate (multiple of 16) offset
    """

    _base_cls = X_SP
    _disp_cls = IMM_ADDR_SIMM13


class ADDR_UIMM12(ARMv8_Base_Displacement_Memory_Operand):
    """
    [XN|Sp + 12-bit usigned immediate]
    address with 12-bit unsigned immediate
    """

    _base_cls = X_SP
    _disp_cls = IMM_ADDR_UIMM12


class ADDR_PCREL14(ARMv8_PCRel_Operand):
    """
    PC relative IMM14
    """

    _disp_cls = IMM_PCREL14


class ADDR_PCREL19(ARMv8_PCRel_Operand):
    """
    PC relative IMM19
    """

    _disp_cls = IMM_PCREL19


class ADDR_PCREL21(ARMv8_PCRel_Operand):
    """
    PC relative IMM21
    """

    _disp_cls = IMM_PCREL21


class ADDR_PCREL26(ARMv8_PCRel_Operand):
    """
    PC relative IMM26
    """

    _disp_cls = IMM_PCREL26


class ADDR_ADRP(ARMv8_PCRel_Operand):
    """
    PC relative IMM21 for ADRP inst
    """

    _disp_cls = IMM_ADRP


class ADDR_REGOFF(ARMv8_Base_Offset_Memory_Operand):
    """
    [Xn|SP + Wm|Xw , <extend> {<amount>}]
    """

    # TODO: handle Wm argument and extend amount
    _base_cls = X_SP
    _off_cls = X


class SVE_ADDR_ZZ_SXTW_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Zn>.D, <Zm>.D, SXTW{ <amount>}]
    """

    # TODO: handle SXTW argument
    _base_cls = Z_D
    _off_cls = Z_D


class SVE_ADDR_ZZ_UXTW_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Zn>.D, <Zm>.D, UXTW{ <amount>}]
    """

    # TODO: handle UXTW argument
    _base_cls = Z_D
    _off_cls = Z_D


class SVE_ADDR_ZZ_LSL_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Zn>.<T>, <Zm>.<T>{, <mod> <amount>}]
    """

    # TODO: handle {, <mod> <amount>} argument
    _base_cls = Z_S
    _off_cls = Z_S


class SVE_ADDR_ZZ_LSL_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Zn>.<T>, <Zm>.<T>{, <mod> <amount>}]
    """

    # TODO: handle {, <mod> <amount>} argument
    _base_cls = Z_D
    _off_cls = Z_D


class SVE_ADDR_RZ_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D]
    As in: LD1B { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Zm>.D]
    """

    # TODO: handle <mod> argument
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_ZI_U5_S(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Zn>.S{, #<imm>}]
    As in: LD1B { <Zt>.S }, <Pg>/Z, [<Zn>.S{, #<imm>}]
    """

    _base_cls = Z_S
    _disp_cls = SVE_UIMM5


class SVE_ADDR_ZI_U5_D(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Zn>.D{, #<imm>}]
    As in: LD1B { <Zt>.D }, <Pg>/Z, [<Zn>.D{, #<imm>}]
    """

    _base_cls = Z_D
    _disp_cls = SVE_UIMM5


class SVE_ADDR_RX(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Xm>]
    As in: LD1B { <Zt>.B }, <Pg>/Z, [<Xn|SP>, <Xm>]
    """

    _base_cls = X
    _off_cls = X


class SVE_ADDR_RI_S4xVL(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>, MUL VL}]
    As in: LD1B { <Zt>.B }, <Pg>/Z, [<Xn|SP>{, #<imm>, MUL VL}]
    """

    # TODO: handle mul vl
    _mod_set = ARMv8_Modifier(("MUL VL",))
    _base_cls = X
    _disp_cls = SVE_SIMM4


class SVE_ADDR_RI_S4x2xVL(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>, MUL VL}]
    As in: LD2B { <Zt1>.B, <Zt2>.B }, <Pg>/Z, [<Xn|SP>{, #<imm>, MUL VL}]
    """

    # TODO: handle mul vl
    _mod_set = ARMv8_Modifier(("MUL VL",))
    _base_cls = X
    _disp_cls = SVE_SIMM4x2


class SVE_ADDR_RI_S4x3xVL(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>, MUL VL}]
    As in: LD3B { <Zt1>.B, <Zt2>.B, <Zt3>.B }, <Pg>/Z, [<Xn|SP>{, #<imm>, MUL VL}]
    """

    # TODO: handle mul vl
    _mod_set = ARMv8_Modifier(("MUL VL",))
    _base_cls = X
    _disp_cls = SVE_SIMM4x3


class SVE_ADDR_RI_S4x4xVL(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>, MUL VL}]
    As in: LD4D { <Zt1>.D, <Zt2>.D, <Zt3>.D, <Zt4>.D }, <Pg>/Z, [<Xn|SP>{, #<imm>, MUL VL}]
    """

    # TODO: handle mul vl
    _mod_set = ARMv8_Modifier(("MUL VL",))
    _base_cls = X
    _disp_cls = SVE_SIMM4x4


class SVE_ADDR_RX_LSL3(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Xm>, LSL #3]
    As in: LD1D { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Xm>, LSL #3]
    """

    _mod_set = ARMv8_Modifier(("LSL #3",))
    _base_cls = X
    _off_cls = X


class SVE_ADDR_RZ_LSL3_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, LSL #3]
    As in: LDFF1D { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Zm>.D, LSL #3]
    """

    _mod_set = ARMv8_Modifier(("LSL #3",))
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_ZI_U5x2_S(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Zn>.S{, #<imm>}]
    As in: LD1H { <Zt>.S }, <Pg>/Z, [<Zn>.S{, #<imm>}]
    """

    _base_cls = Z_S
    _disp_cls = SVE_UIMM5x2


class SVE_ADDR_ZI_U5x2_D(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Zn>.D{, #<imm>}]
    As in: LD1H { <Zt>.S }, <Pg>/Z, [<Zn>.D{, #<imm>}]
    """

    _base_cls = Z_D
    _disp_cls = SVE_UIMM5x2


class SVE_ADDR_ZI_U5x4_S(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Zn>.S{, #<imm>}]
    As in: LD1W { <Zt>.S }, <Pg>/Z, [<Zn>.S{, #<imm>}]
    """

    _base_cls = Z_D
    _disp_cls = SVE_UIMM5x4


class SVE_ADDR_ZI_U5x4_D(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Zn>.D{, #<imm>}]
    As in: LD1SW { <Zt>.D }, <Pg>/Z, [<Zn>.D{, #<imm>}]
    """

    _base_cls = Z_D
    _disp_cls = SVE_UIMM5x4


class SVE_ADDR_ZI_U5x8_D(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Zn>.D{, #<imm>}]
    As in: LD1D { <Zt>.D }, <Pg>/Z, [<Zn>.D{, #<imm>}]
    """

    _base_cls = Z_D
    _disp_cls = SVE_UIMM5x8


class SVE_ADDR_ZI_U5x8_S(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Zn>.S{, #<imm>}]
    As in: LD1D { <Zt>.S }, <Pg>/Z, [<Zn>.S{, #<imm>}]
    """

    _base_cls = Z_S
    _disp_cls = SVE_UIMM5x8


class SVE_ADDR_RI_S4x16(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>}]
    As in: LD1RQB { <Zt>.B }, <Pg>/Z, [<Xn|SP>{, #<imm>}]
    """

    _base_cls = X
    _disp_cls = SVE_SIMM4x16


class SVE_ADDR_RI_S4x32(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>}]
    As in: LD1ROB { <Zt>.B }, <Pg>/Z, [<Xn|SP>{, #<imm>}]
    """

    _base_cls = X
    _disp_cls = SVE_SIMM4x32


class SVE_ADDR_RI_U6(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>}]
    As in: LD1RB { <Zt>.D }, <Pg>/Z, [<Xn|SP>{, #<imm>}]
    """

    _base_cls = X
    _disp_cls = SVE_UIMM6


class SVE_ADDR_RI_U6x2(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>}]
    As in: LD1RH { <Zt>.D }, <Pg>/Z, [<Xn|SP>{, #<imm>}]
    """

    _base_cls = X
    _disp_cls = SVE_UIMM6x2


class SVE_ADDR_RI_U6x4(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>}]
    As in: LD1RSW { <Zt>.D }, <Pg>/Z, [<Xn|SP>{, #<imm>}]
    """

    _base_cls = X
    _disp_cls = SVE_UIMM6x4


class SVE_ADDR_RI_U6x8(ARMv8_Base_Displacement_Memory_Operand):
    """
    [<Xn|SP>{, #<imm>}]
    As in: LD1RD { <Zt>.D }, <Pg>/Z, [<Xn|SP>{, #<imm>}]
    """

    _base_cls = X
    _disp_cls = SVE_UIMM6x6


class SVE_ADDR_RZ_XTW_14_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.S, <mod>]
    As in: ST1B { <Zt>.S }, <Pg>, [<Xn|SP>, <Zm>.S, <mod>]
    """

    # TODO: handle <mod> argument
    _base_cls = X
    _off_cls = Z_S


class SVE_ADDR_RZ_XTW_14_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, <mod>]
    As in: ST1B { <Zt>.D }, <Pg>, [<Xn|SP>, <Zm>.D, <mod>]
    """

    # TODO: handle <mod> argument
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RZ_XTW1_14_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.S, <mod> #1]
    As in: ST1H { <Zt>.S }, <Pg>, [<Xn|SP>, <Zm>.S, <mod> #1]
    """

    # TODO: handle <mod> argument
    _base_cls = X
    _off_cls = Z_S


class SVE_ADDR_RZ_XTW1_14_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, <mod> #1]
    As in: ST1H { <Zt>.D }, <Pg>, [<Xn|SP>, <Zm>.D, <mod> #1]
    """

    # TODO: handle <mod> argument
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RZ_XTW2_14_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.S, <mod> #2]
    As in: ST1W { <Zt>.S }, <Pg>, [<Xn|SP>, <Zm>.S, <mod> #2]
    """

    # TODO: handle <mod> argument
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RZ_XTW2_14_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, <mod> #2]
    As in: ST1W { <Zt>.D }, <Pg>, [<Xn|SP>, <Zm>.D, <mod> #2]
    """

    # TODO: handle <mod> argument
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RZ_XTW3_14_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, <mod> #3]
    As in: ST1D { <Zt>.D }, <Pg>, [<Xn|SP>, <Zm>.D, <mod> #3]
    """

    # TODO: handle <mod> argument
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RZ_XTW_22_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.S, <mod>]
    As in: LD1B { <Zt>.S }, <Pg>/Z, [<Xn|SP>, <Zm>.S, <mod>]
    """

    _mod_set = ARMv8_Modifier(("UXTW", "SXTW"))
    _base_cls = X
    _off_cls = Z_S


class SVE_ADDR_RZ_XTW_22_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, <mod>]
    As in: LD1B { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Zm>.D, <mod>]
    """

    _mod_set = ARMv8_Modifier(("UXTW", "SXTW"))
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RZ_XTW1_22_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.S, <mod> #1]
    As in: LD1H { <Zt>.S }, <Pg>/Z, [<Xn|SP>, <Zm>.S, <mod> #1]
    """

    _mod_set = ARMv8_Modifier(("UXTW #1", "SXTW #1"))
    _base_cls = X
    _off_cls = Z_S


class SVE_ADDR_RZ_XTW1_22_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, <mod> #1]
    As in: LD1H { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Zm>.D, <mod> #1]
    """

    _mod_set = ARMv8_Modifier(("UXTW #1", "SXTW #1"))
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RZ_XTW2_22_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.S, <mod> #2]
    As in: LD1W { <Zt>.S }, <Pg>/Z, [<Xn|SP>, <Zm>.S, <mod> #2]
    """

    _mod_set = ARMv8_Modifier(("UXTW #2", "SXTW #2"))
    _base_cls = X
    _off_cls = Z_S


class SVE_ADDR_RZ_XTW2_22_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, <mod> #2]
    As in: LD1SW { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Zm>.D, <mod> #2]
    """

    _mod_set = ARMv8_Modifier(("UXTW #2", "SXTW #2"))
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RZ_XTW3_22_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.S, <mod> #3]
    As in: LD1D { <Zt>.S }, <Pg>/Z, [<Xn|SP>, <Zm>.S, <mod> #3]
    """

    _mod_set = ARMv8_Modifier(("UXTW #3", "SXTW #3"))
    _base_cls = X
    _off_cls = Z_S


class SVE_ADDR_RZ_XTW3_22_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, <mod> #3]
    As in: LD1D { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Zm>.D, <mod> #3]
    """

    _mod_set = ARMv8_Modifier(("UXTW #3", "SXTW #3"))
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RX_LSL1(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Xm>, LSL #1]
    As in: LD1H { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Xm>, LSL #1]
    """

    _mod_set = ARMv8_Modifier(("LSL #1",))
    _base_cls = X
    _off_cls = X


class SVE_ADDR_RZ_LSL1_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, LSL #1]
    As in: LD1H { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Zm>.D, LSL #1]
    """

    _mod_set = ARMv8_Modifier(("LSL #1",))
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RX_LSL2(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Xm>, LSL #2]
    As in: LD1RQW { <Zt>.S }, <Pg>/Z, [<Xn|SP>, <Xm>, LSL #2]
    """

    _mod_set = ARMv8_Modifier(("LSL #2",))
    _base_cls = X
    _off_cls = X


class SVE_ADDR_RZ_LSL2_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Zm>.D, LSL #2]
    As in: LD1SW { <Zt>.D }, <Pg>/Z, [<Xn|SP>, <Zm>.D, LSL #2]
    """

    _mod_set = ARMv8_Modifier(("LSL #2",))
    _base_cls = X
    _off_cls = Z_D


class SVE_ADDR_RR(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>, <Xm>]
    As in: LDFF1B { <Zt>.B }, <Pg>/Z, [<Xn|SP>, <Xm>]
    """

    _base_cls = X
    _off_cls = X


class SVE_ADDR_R(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>{, <Xm>}]
    As in: LDFF1B { <Zt>.B }, <Pg>/Z, [<Xn|SP>{, <Xm>}]
    """

    _base_cls = X
    _off_cls = X


class SVE_ADDR_RR_LSL1(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>{, <Xm>}]
    As in: LDFF1H { <Zt>.B }, <Pg>/Z, [<Xn|SP>{, <Xm>, LSL #1}]
    """

    # TODO: handle lsl
    _base_cls = X
    _off_cls = X


class SVE_ADDR_RR_LSL2(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>{, <Xm>}]
    As in: LDFF1W { <Zt>.B }, <Pg>/Z, [<Xn|SP>{, <Xm>, LSL #2}]
    """

    # TODO: handle lsl
    _base_cls = X
    _off_cls = X


class SVE_ADDR_RR_LSL3(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Xn|SP>{, <Xm>}]
    As in: LDFF1D { <Zt>.B }, <Pg>/Z, [<Xn|SP>{, <Xm>, LSL #3}]
    """

    # TODO: handle lsl
    _base_cls = X
    _off_cls = X


class SVE_ADDR_ZX_S(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Zn>.S{, <Xm>}]
    As in: STNT1B { <Zt>.S }, <Pg>, [<Zn>.S{, <Xm>}]
    """

    # TODO: handle lsl
    _base_cls = Z_S
    _off_cls = X


class SVE_ADDR_ZX_D(ARMv8_Base_Offset_Memory_Operand):
    """
    [<Zn>.D{, <Xm>}]
    As in: STNT1B { <Zt>.D }, <Pg>, [<Zn>.D{, <Xm>}]
    """

    # TODO: handle lsl
    _base_cls = Z_D
    _off_cls = X
