"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import List, Sequence, IO, Set, Tuple, Optional, Union, Iterable, Dict

import random
import platform
import pipedream.utils.abc as abc

from pipedream.utils import *
from pipedream.utils import pyperfs
from pipedream.asm.ir import *
from pipedream.asm.allocator import *
from pipedream.asm.asmwriter import *

from pipedream.benchmark.types import Loop_Overhead


from . import registers
from . import operands
from . import instructions

from .asmwriter import *
from .registers import *
from .operands import *
from .instructions import *

__all__ = [
    *registers.__all__,
    *operands.__all__,
    *instructions.__all__,
]

CROSS_COMPILE = platform.uname().machine == "x86_64"

X: Register_Class
Q: Register_Class
CALLER_SAVED: Register_Class
X0: ARMv8_Register
X19: ARMv8_Register
X20: ARMv8_Register
X21: ARMv8_Register
X22: ARMv8_Register
X23: ARMv8_Register
X24: ARMv8_Register
X25: ARMv8_Register
X30: ARMv8_Register
ARG_1: ARMv8_Register
ARG_2: ARMv8_Register
ARG_3: ARMv8_Register
ARG_4: ARMv8_Register
ARG_5: ARMv8_Register
ARG_6: ARMv8_Register
ARG_7: ARMv8_Register

if CROSS_COMPILE:
    ARMv8_Tools_Config = Tools_Config(
        as_cmd="aarch64-linux-gnu-as",
        ld_cmd="aarch64-linux-gnu-ld",
        ldflags_perf=[],  # do not cross compile with -lpapi (cross compile only used for tests)
    )
else:
    ARMv8_Tools_Config = Tools_Config(
        as_cmd="as",
        ld_cmd="ld",
        ldflags_perf=["-l" + pyperfs.get_perf_lib().lib_name],
    )

ARMv8_L1_Cache_Config = Cache_Config(size=65536, cacheline_size=64, associativity=4)


class ARMv8_Architecture(Architecture):
    def __init__(self):
        self._instruction_set = ARMv8_Instruction_Set()
        self._register_set = ARMv8_Register_Set()
        self._asm_dialects = [ARMv8_Asm_Dialect()]

    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return "ARMv8a"

    @property  # type: ignore
    @abc.override
    def nb_vector_reg(self) -> int:
        return len(V_8B)  # type: ignore

    @property  # type: ignore
    @abc.override
    def max_vector_size(self) -> int:
        return 128 // 8

    @abc.override
    def instruction_set(self) -> Instruction_Set:
        return self._instruction_set

    @abc.override
    def register_set(self) -> Register_Set:
        return self._register_set

    @abc.override
    def asm_dialects(self) -> List[Asm_Dialect]:
        return self._asm_dialects

    @abc.override
    def make_asm_writer(self, dialect: Asm_Dialect, file: IO[str]) -> ASM_Writer:
        return ARMv8_ASM_Writer(dialect, file)

    @abc.override
    def make_ir_builder(self) -> "ARMv8_IR_Builder":
        return ARMv8_IR_Builder(self)

    @abc.override
    def make_register_allocator(self) -> Register_Liveness_Tracker:
        regs = self.register_set()

        return Register_Liveness_Tracker(
            all_registers=regs.all_registers(),
            callee_save_registers=regs.callee_save_registers(),
            read_only_registers=iter(
                self.register_set().stack_pointer_register_class()
            ),
        )

    @property  # type: ignore
    @abc.override
    def tools_config(self) -> Tools_Config:
        return ARMv8_Tools_Config

    @property  # type: ignore
    @abc.override
    def l1_cache_config(self) -> Cache_Config:
        return ARMv8_L1_Cache_Config

    @abc.override
    def loop_overhead(
        self, num_iterations: int, num_prologue_inst: int
    ) -> Loop_Overhead:
        # As of now, there are two overhead instructions per iterations in an
        # ARMv8a mirobenchmark
        # ```asm
        # loops:
        #  ...
        #  sub          X12, X12, 1
        #  bne          .L.kernel.loop.Loop_Overhead
        # ```
        # TODO: the detection of such instructions should be automatised

        insn_per_it = 2
        insn_total = insn_per_it * num_iterations + num_prologue_inst
        return Loop_Overhead(insn_total, insn_total)  # Considering Arm has no muops


class ARMv8_Asm_Dialect(Asm_Dialect):
    @property  # type: ignore
    @abc.override
    def name(self) -> str:
        return "arm"

    @abc.override
    def instruction_mnemonic(self, inst: Instruction) -> str:
        return MNEMONICS[inst.name]  # type: ignore


class ARMv8_IR_Builder(IR_Builder):
    def __init__(self, arch):
        self._arch = arch
        self._insts = arch.instruction_set()
        # The stack should stay aligned on 16 bytes: storing
        # the offset to the actual stack position
        self._stack_offset = 0
        self._stack_alignment = 16

    def _set_registers(self, instr: Instruction, *args) -> Instruction:
        assert len(args) % 2 == 0

        for i in range(0, len(args), 2):
            instr = self._set_register(instr, args[i], args[i + 1])

        return instr

    def _set_register(
        self, instr: Instruction, op_idx_or_name, reg: ARMv8_Register
    ) -> Instruction:
        def update(op: Operand):
            assert isinstance(op, Register_Operand)
            return op.with_register(reg)

        return instr.update_operand(op_idx_or_name, update)

    def _set_value(
        self, instr: Instruction, op_idx_or_name, val: int, shift: Optional[int] = None
    ) -> Instruction:
        def update(op: Operand):
            if shift is not None:
                assert isinstance(op, Shifted_Immediate_Operand)
                return op.with_shifted_value(val, shift)
            else:
                assert isinstance(op, Immediate_Operand)
                return op.with_value(val)

        return instr.update_operand(op_idx_or_name, update)

    def _set_label(self, instr: Instruction, op_idx_or_name, val: Label) -> Instruction:
        def update(op: Operand):
            assert isinstance(op, Label_Operand)
            return op.with_label(val)

        return instr.update_operand(op_idx_or_name, update)

    def _set_base_displacement(
        self,
        instr: Instruction,
        op_idx_or_name,
        base: ARMv8_Register,
        displacement: int,
    ) -> Instruction:
        def update(op: Operand):
            assert isinstance(op, Base_Displacement_Operand)
            return op.with_base(base).with_displacement(displacement)

        return instr.update_operand(op_idx_or_name, update)

    @abc.override
    def get_return_register(self) -> Register:
        return X0

    @abc.override
    def get_argument_register(self, idx: int) -> Register:
        return {0: ARG_1, 1: ARG_2, 2: ARG_3, 3: ARG_4, 4: ARG_5, 5: ARG_6, 6: ARG_7}[
            idx
        ]

    @abc.override
    def get_scratch_register(self, idx: int) -> Register:
        return {0: X19, 1: X20, 2: X21, 3: X22, 4: X23, 5: X24, 6: X25}[idx]

    @abc.override
    def select_memory_base_register(
        self,
        insts: List[Instruction],
        free_regs: Set[Register],
        address_width: int,
    ) -> Register:
        """
        Select a register that can be used as a memory base register for all memory accesses
        in the given list of instructions :insts:.
        """
        assert address_width == 64
        candidates: Set[Register] = set(iter(X)) & free_regs
        candidates -= set(iter(CALLER_SAVED))
        reg: Register
        for i in insts:
            for op in i.operands:
                if not op.is_def:
                    continue
                if not isinstance(op, Register_Operand):
                    continue
                if len(op.register_class) != 1:
                    continue

                reg = op.register_class[0].widest

                candidates -= set([reg])

        assert candidates, [candidates, [r.widest for r in free_regs]]

        list_reg = list(candidates)
        list_reg.sort(key=lambda r: r.name, reverse=True)
        reg = list_reg[0]

        return reg.as_width(address_width)

    @abc.override
    def preallocate_benchmark(
        self, alloc: Register_Liveness_Tracker, instructions: Sequence[Instruction]
    ) -> Tuple[object, List[Instruction]]:
        stolen_regs: List[Register] = []
        # Sweet copy here, just in case
        preallocated_insts = list(instructions)
        return stolen_regs, preallocated_insts

    @abc.override
    def free_stolen_benchmark_registers(
        self, alloc: Register_Liveness_Tracker, stolen_regs: Iterable[Register]
    ):
        for reg in stolen_regs:
            alloc.free(reg)

    @abc.override
    def emit_loop_prologue(
        self,
        kernel: Sequence[Instruction],
        free_regs: Sequence[Register],
        reg_values: Register,
    ) -> List[Instruction]:
        """`reg_values` is a register containing a pointer to a memory region from
        where vector registers can be initialized"""

        out: List[Instruction] = []

        reg: Register
        for vect_id, reg in enumerate(iter(Q)):
            out += self.emit_put_basedisplacement_in_register(
                reg_values, 16 * vect_id, reg
            )

        out += self.emit_push_to_stack(reg_values)

        for gpr in iter(set(X).intersection(free_regs)):
            if gpr == r_LR:
                continue

            # Somewhere between int32_max+43 and int64_max-42 with non-null first byte
            rand_value_upper = random.randint(2**24 + 42, 2**55 - 42) << 8
            rand_lower_byte = random.randint(1, 0xFF)
            rand_value = rand_value_upper | rand_lower_byte
            out += self.emit_put_const_in_register(rand_value, gpr)

        return out

    @abc.override
    def emit_benchmark_prologue(
        self,
        kernel: Sequence[Instruction],
        free_regs: Sequence[Register],
    ) -> List[Instruction]:
        out = []

        def filter_fun(r: Register) -> bool:
            return r in CALLER_SAVED

        for gpr in filter(filter_fun, free_regs):
            if gpr == r_LR:
                continue

            # Somewhere between int32_max+43 and int64_max-42 with non-null first byte
            rand_value_upper = random.randint(2**24 + 42, 2**55 - 42) << 8
            rand_lower_byte = random.randint(1, 0xFF)
            rand_value = rand_value_upper | rand_lower_byte

            out += self.emit_put_const_in_register(rand_value, gpr)
        return out

    @abc.override
    def emit_benchmark_epilogue(
        self,
        kernel: Sequence[Instruction],
        free_regs: Sequence[Register],
    ) -> List[Instruction]:
        return []

    @abc.override
    def emit_loop_epilogue(
        self,
        kernel: Sequence[Instruction],
        free_regs: Sequence[Register],
        reg_values: Register,
    ) -> List[Instruction]:
        out = []
        out += self.emit_pop_from_stack(reg_values)
        return out

    def emit_dependency_breaker(self, reg: Register) -> List[Instruction]:
        # TODO: instert a zero-idiom on the register
        print("DEPENDENCY BREAKER NOT SUPPORTED")
        return []

    @abc.override
    def emit_sequentialize_cpu(
        self, alloc: Register_Liveness_Tracker
    ) -> List[Instruction]:
        # See https://developer.arm.com/documentation/100941/0101/Barriers
        return [Harness.ISB]

    @abc.override
    def emit_copy(self, src: Register, dst: Register) -> List[Instruction]:
        ins: Union[Instruction, Machine_Instruction] = Harness.MOV_RD_X_RM_SFT_X
        ins = self._set_registers(ins, "Rd", dst, "Rm_SFT", src)
        return [ins]

    @abc.override
    def emit_push_to_stack(self, src: ARMv8_Register) -> List[Instruction]:
        assert src in X
        ret: List[Instruction] = []
        if self._stack_offset == 0:
            dec: Union[
                Instruction, Machine_Instruction
            ] = Harness.SUB_RD_SP_X_SP_RN_SP_X_SP_AIMM
            dec = self._set_registers(dec, "Rd_SP", r_SP, "Rn_SP", r_SP)
            dec = self._set_value(dec, "AIMM", self._stack_alignment)
            ret.append(dec)
            self._stack_offset = self._stack_alignment
        self._stack_offset -= 8

        ins: Union[
            Instruction, Machine_Instruction
        ] = Harness.STR_RT_X_ADDR_SIMM9_ADDR_X_SP_IMM_ADDR_SIMM9
        ins = self._set_register(ins, "Rt", src)
        ins = self._set_base_displacement(ins, "ADDR_SIMM9", r_SP, self._stack_offset)
        ret.append(ins)

        return ret

    @abc.override
    def emit_pop_from_stack(self, dst: ARMv8_Register) -> List[Instruction]:
        assert dst in X
        ret: List[Instruction] = []
        ins: Union[
            Instruction, Machine_Instruction
        ] = Harness.LDR_RT_X_ADDR_SIMM9_ADDR_X_SP_IMM_ADDR_SIMM9
        ins = self._set_register(ins, "Rt", dst)
        ins = self._set_base_displacement(ins, "ADDR_SIMM9", r_SP, self._stack_offset)
        ret.append(ins)
        self._stack_offset += 8

        if self._stack_offset == self._stack_alignment:
            inc: Union[
                Instruction, Machine_Instruction
            ] = Harness.ADD_RD_SP_X_SP_RN_SP_X_SP_AIMM
            inc = self._set_registers(inc, "Rd_SP", r_SP, "Rn_SP", r_SP)
            inc = self._set_value(inc, "AIMM", self._stack_alignment)
            ret.append(inc)
            self._stack_offset = 0

        return ret

    @abc.override
    def emit_branch_if_not_zero(
        self, reg: ARMv8_Register, dst: Label
    ) -> List[Instruction]:
        test: Union[Instruction, Machine_Instruction] = Harness.CMP_RN_SP_X_SP_AIMM
        test = self._set_register(test, "Rn_SP", reg)
        test = self._set_value(test, "AIMM", 0)

        jmp: Union[
            Instruction, Machine_Instruction
        ] = Harness.BNE_ADDR_PCREL26_ADR_XPC_PCREL26
        jmp = self._set_label(jmp, "ADDR_PCREL26", dst)

        return [test, jmp]

    @abc.override
    def emit_branch_if_zero(self, reg: Register, dst: Label) -> List[Instruction]:
        print("WARNING: EMIT BRANCH IF ZERO UNSUPPORTED")
        assert False
        return []

    @abc.override
    def emit_call(self, dst: Label) -> List[Instruction]:
        ins: Union[
            Instruction, Machine_Instruction
        ] = Harness.BL_ADDR_PCREL26_ADR_XPC_PCREL26
        ins = self._set_label(ins, "ADDR_PCREL26", dst)
        return [ins]

    @abc.override
    def emit_return(self, reg: Register) -> List[Instruction]:
        # X30 is the link register
        return [self._set_register(Harness.RET_RN_X, "Rn", X30)]

    @abc.override
    def emit_put_const_in_register(
        self, const: int, reg: ARMv8_Register
    ) -> List[Instruction]:
        # Only supports 32-bit const as of now
        assert reg in X
        ret: List[Instruction] = []
        idx = 0
        ins: Union[Instruction, Machine_Instruction]
        if const == 0:
            ins = Harness.MOVZ_RD_X_HALF
            ins = self._set_register(ins, "Rd", reg)
            ins = self._set_value(ins, "HALF", const)
            return [ins]
        while const != 0:
            high = (const >> idx) & ((1 << 16) - 1)
            const = const - (high << idx)
            ins = Harness.MOVZ_RD_X_HALF if idx == 0 else Harness.MOVK_RD_X_HALF
            ins = self._set_register(ins, "Rd", reg)
            ins = self._set_value(ins, "HALF", high, shift=idx)
            ret.append(ins)
            idx += 16
        return ret

    @abc.override
    def emit_mul_reg_reg(self, reg: ARMv8_Register, reg_const: ARMv8_Register):
        assert reg in X and reg_const in X
        return [
            self._set_registers(
                Harness.MUL_RD_X_RN_X_RM_X, "Rd", reg, "Rn", reg, "Rm", reg_const
            )
        ]

    @abc.override
    def emit_mul_reg_const(self, reg: Register, const: int) -> List[Instruction]:
        raise NotImplementedError("Aarch64 cannot multiply a reg by a constant")

    @abc.override
    def emit_add_registers(
        self, src_reg: Register, src_dst_reg: Register
    ) -> List[Instruction]:
        assert src_reg in X
        assert src_dst_reg in X
        add: Union[
            Instruction, Machine_Instruction
        ] = Harness.ADD_RD_SP_X_SP_RN_SP_X_SP_RM_EXT_X
        add = self._set_registers(
            add, "Rd_SP", src_dst_reg, "Rn_SP", src_dst_reg, "Rm_EXT", src_reg
        )
        return [add]

    @abc.override
    def emit_put_basedisplacement_in_register(
        self, base: ARMv8_Register, displacement: int, reg: ARMv8_Register
    ) -> List[Instruction]:
        assert reg in Q
        assert base in X
        i: Union[
            Instruction, Machine_Instruction
        ] = Harness.LDR_FT_Q_ADDR_UIMM12_ADDR_X_SP_IMM_ADDR_UIMM12
        i = self._set_register(i, "Ft", reg)
        i = self._set_base_displacement(i, "ADDR_UIMM12", base, displacement)
        return [i]

    @abc.override
    def emit_substract_one_from_reg_and_branch_if_not_zero(
        self, loop_counter: ARMv8_Register, dst: "Label"
    ) -> List[Instruction]:
        sub: Union[
            Instruction, Machine_Instruction
        ] = Harness.SUB_RD_SP_X_SP_RN_SP_X_SP_AIMM
        sub = self._set_registers(sub, "Rd_SP", loop_counter, "Rn_SP", loop_counter)
        sub = self._set_value(sub, "AIMM", 1)

        cmp: Union[Instruction, Machine_Instruction] = Harness.CMP_RN_SP_X_SP_AIMM
        cmp = self._set_register(cmp, "Rn_SP", loop_counter)
        cmp = self._set_value(cmp, "AIMM", 0)

        jmp: Union[
            Instruction, Machine_Instruction
        ] = Harness.BNE_ADDR_PCREL26_ADR_XPC_PCREL26
        jmp = self._set_label(jmp, "ADDR_PCREL26", dst)

        return [sub, cmp, jmp]

    @abc.override
    def emit_add_imm_to_reg(self, dst: Register, value: int) -> List[Instruction]:
        assert value >= 0
        if dst == r_SP:
            remainder_value = value % self._stack_alignment
            value -= remainder_value
            if self._stack_offset + remainder_value >= self._stack_alignment:
                self._stack_offset -= self._stack_alignment
                value += self._stack_alignment
            self._stack_offset += remainder_value

        ret = []
        while value != 0:
            sub: Union[
                Instruction, Machine_Instruction
            ] = Harness.ADD_RD_SP_X_SP_RN_SP_X_SP_AIMM

            sub = self._set_registers(sub, "Rd_SP", dst, "Rn_SP", dst)
            if value > operands.AIMM.max():
                value -= operands.AIMM.max()
                sub = self._set_value(sub, "AIMM", operands.AIMM.max())
            else:
                sub = self._set_value(sub, "AIMM", value)
                value = 0
            ret.append(sub)
        return ret

    @abc.override
    def emit_sub_imm_to_reg(self, dst: Register, value: int) -> List[Instruction]:
        assert value >= 0
        if dst == r_SP:
            remainder_value = value % self._stack_alignment
            value -= remainder_value
            if self._stack_offset + remainder_value >= self._stack_alignment:
                self._stack_offset -= self._stack_alignment
                value += self._stack_alignment
            self._stack_offset += remainder_value

        ret = []
        while value != 0:
            sub: Union[
                Instruction, Machine_Instruction
            ] = Harness.SUB_RD_SP_X_SP_RN_SP_X_SP_AIMM

            sub = self._set_registers(sub, "Rd_SP", dst, "Rn_SP", dst)
            if value > operands.AIMM.max():
                value -= operands.AIMM.max()
                sub = self._set_value(sub, "AIMM", operands.AIMM.max())
            else:
                sub = self._set_value(sub, "AIMM", value)
                value = 0
            ret.append(sub)
        return ret

    @abc.override
    def dump_stack_state(self) -> Dict:
        return {"offset": self._stack_offset}

    @abc.override
    def load_stack_state(self, state):
        self._stack_offset = state["offset"]  # type:ignore
