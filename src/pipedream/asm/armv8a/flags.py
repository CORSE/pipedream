"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from enum import IntFlag
from typing import Iterable

__all__ = ["ARMv8_NZCV", "ARMv8_FPSCR", "ARMv8_CPSR"]


class ARMv8_NZCV(IntFlag):
    """
    Condition Flags (64-bit, but upper 32 are reserved)
    """

    # RES0
    V = 0x1000_0000  # Overflow condition flag
    C = 0x2000_0000  # Carry condition flag
    Z = 0x4000_0000  # Zero condition flag
    N = 0x8000_0000  # Negative condition flag
    # RES0

    def __iter__(self) -> Iterable["ARMv8_NZCV"]:
        for flag in ARMv8_NZCV:
            if self & flag:
                yield flag


# Not useful as of now
# class ARMv8_FPCR(IntFlag):
#   """
#   Floating-Point Control Register
#   """
#
#   TODO


class ARMv8_FPSR(IntFlag):
    """
    Floating-Point Status Register (64-bit, but upper 32 are reserved)
    """

    IOC = 0x0000_0001  # Invalid Operation cumulative floating-point exception bit
    DZC = 0x0000_0002  # Overflow cumulative floating-point exception bit
    OFC = 0x0000_0004  # Overflow cumulative floating-point exception bit
    UFC = 0x0000_0008  # Underflow cumulative floating-point exception bit
    IXC = 0x0000_0010  # Inexact cumulative floating-point exception bit
    # RES0
    IDC = 0x0000_0080  # Input Denormal cumulative floating-point exception bit
    # RES0
    QC = 0x0800_0000  # Cumulative sturation bit

    # "Inherited" from `ARMv8_NZCV`
    # RES0
    V = 0x1000_0000  # Overflow condition flag
    C = 0x2000_0000  # Carry condition flag
    Z = 0x4000_0000  # Zero condition flag
    N = 0x8000_0000  # Negative condition flag
    # RES0

    def __iter__(self) -> Iterable["ARMv8_FPSR"]:
        for flag in ARMv8_FPSR:
            if self & flag:
                yield flag


class ARMv8_FPSCR(IntFlag):
    """
    Floating-Point Status and Control Register) (32-bit)
    """

    # Inherit from `ARMv8_FPSR`
    IOC = 0x0000_0001  # Invalid Operation cumulative floating-point exception bit
    DZC = 0x0000_0002  # Overflow cumulative floating-point exception bit
    OFC = 0x0000_0004  # Overflow cumulative floating-point exception bit
    UFC = 0x0000_0008  # Underflow cumulative floating-point exception bit
    IXC = 0x0000_0010  # Inexact cumulative floating-point exception bit
    # RES0
    IDC = 0x0000_0080  # Input Denormal cumulative floating-point exception bit

    IOE = 0x0000_0100  # Invalid Operation floating_point exception trap enable
    DZE = 0x0000_0200  # Divide by Zero floating-point exception trap enable
    OFE = 0x0000_0400  # Overflow floating-point exception trap enable
    UFE = 0x0000_0800  # Underflow floating-point exception trap enable
    IXE = 0x0000_1000  # Inexact floating-point exception trap enable
    # RES0
    IDE = 0x0000_8000  # Input Denormal floating-point exeption trap enable
    LEN = 0x0007_0000  # Implementation Defined
    FZ16 = 0x0008_0000
    # Flush-to-zero mode control bit on half-precision data-processing instructions
    Stride = 0x0030_0000  # Implementation defined
    RMode = 0x00C0_0000  # Rounding Mode control bit
    FZ = 0x0100_0000  # Flush-to-zerp mode control bit
    DN = 0x0200_0000  # Default NaN mode control bit
    AHP = 0x0400_0000  # Alternate half-precision control bit

    # Inherit from `ARMv8_FPSR`
    QC = 0x0800_0000  # Cumulative sturation bit
    # RES0

    # "Inherited" from `ARMv8_NZCV`
    V = 0x1000_0000  # Overflow condition flag
    C = 0x2000_0000  # Carry condition flag
    Z = 0x4000_0000  # Zero condition flag
    N = 0x8000_0000  # Negative condition flag
    # RES0


class ARMv8_APSR(IntFlag):
    """
    Application Program Status Register (32-bit)
    """

    # RES0: 0-3
    # RES1: 4
    # RES0
    GE = 0x000F_0000  # PSTATE greater than or equal flags

    # "Inherited" from `ARMv8_NZCV`
    V = 0x1000_0000  # Overflow condition flag
    C = 0x2000_0000  # Carry condition flag
    Z = 0x4000_0000  # Zero condition flag
    N = 0x8000_0000  # Negative condition flag
    # RES0

    def __iter__(self) -> Iterable["ARMv8_APSR"]:
        for flag in ARMv8_APSR:
            if self & flag:
                yield flag


class ARMv8_CPSR(IntFlag):
    """
    Current Program Status Register (32-bit)
    """

    M = 0x0000_001F  # PSTATE mode bits
    # RES0
    F = 0x0000_0040  # PSTATE FIQ interrupt mask bit
    I = 0x0000_0080  # PSTATE IRQ interrupt mask bit
    A = 0x0000_0100  # PSTATE SError interrupt mask bit
    E = 0x0000_0200  # PSTATE endianness bit

    # "Inherited" from `ARMv8_APSR`
    # RES0: 0-3
    # RES1: 4
    # RES0
    GE = 0x000F_0000  # PSTATE greater than or equal flags

    # "Inherited" from `ARMv8_NZCV`
    V = 0x1000_0000  # Overflow condition flag
    C = 0x2000_0000  # Carry condition flag
    Z = 0x4000_0000  # Zero condition flag
    N = 0x8000_0000  # Negative condition flag
    # RES0

    def __iter__(self) -> Iterable["ARMv8_CPSR"]:
        for flag in ARMv8_CPSR:
            if self & flag:
                yield flag
