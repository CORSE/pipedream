"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


"""
    Deport excecution of a command on a remote host
"""

import pathlib

from typing import Tuple, Any, Optional, List, Union, Dict
from pssh.clients import SSHClient
from pssh.clients.base.single import InteractiveShell
from ssh2.channel import Channel
import subprocess
import os
import re
import logging
import time

logger = logging.getLogger(__name__)

_REMOTE_LIB_NAME = "libbenchmarks.so"


class SSH_PSSH_Channel:
    def __init__(
        self,
        host: str,
        user: str,
        port: int,
        wrapper: str,
        keylocation="~/.ssh/id_ecdsa",
        pinned_core=2,
    ):
        self._init = False
        ssh_client, exec_channel, sftp_chan = self._init_session(
            host, user, port, keylocation
        )
        self._ssh_client = ssh_client
        self._exec_channel = exec_channel
        self._sftp_chan = sftp_chan
        self._execmd = pathlib.Path(wrapper).name
        self._init = True
        self._pinned_core = pinned_core
        self._run_dir = f"/tmp/pipedream_{self._pinned_core}"
        out, err = self._remote_exec(f"mkdir -p {self._run_dir}")
        assert out == "" and err == ""
        self._ssh_client.copy_file(
            wrapper, f"{self._run_dir}/{self._execmd}", self._sftp_chan
        )
        self._remote_exec(f"chmod +x {self._run_dir}/{self._execmd}")

    def _init_session(
        self, host: str, user: str, port: int, keylocation: str
    ) -> Tuple[SSHClient, Channel, Channel]:
        ssh_client = SSHClient(host=host, user=user, port=port, pkey=keylocation)
        exec_channel = ssh_client.open_session()
        sftp_channel = ssh_client.open_session()
        return (ssh_client, exec_channel, sftp_channel)

    def __del__(self):
        if self._init:
            self._ssh_client.close_channel(self._sftp_chan)
            self._ssh_client.close_channel(self._exec_channel)
            self._ssh_client.disconnect()

    def _send_library(self, library: pathlib.Path):
        self._ssh_client.copy_file(
            library.as_posix(),
            f"{self._run_dir}/{library.name}",
            self._sftp_chan,
        )

    def _del_library(self, library: str):
        out, err = self._remote_exec(f"rm {self._run_dir}/{library}")
        if out != "" or err != "":
            raise RuntimeError(
                "Could not delete file "
                f"{library}:\n\tstdout:\n{out}\n\tstderr:\n{err}"
            )

    def _remote_exec(self, cmd) -> Tuple[str, str]:
        with self._ssh_client.open_shell() as shell:
            shell.run(cmd)
        ret_out = "\n".join(shell.stdout)
        ret_err = "\n".join(shell.stderr)
        return ret_out, ret_err

    def _parse_exec_str(self, out: str) -> Tuple[List, List]:
        lines = out.split("\n")
        measures = [int(l[50:]) for l in lines]
        cycles = [measures[2 * l] for l in range(len(measures) // 2)]
        instructions = [measures[2 * l + 1] for l in range(len(measures) // 2)]
        return cycles, instructions

    def exec_bench(self, library: str, nb_reps: int):
        lib = pathlib.Path(library)
        self._send_library(lib)
        out, err = self._remote_exec(
            f"cd {self._run_dir} &&"
            f"mv {lib.name} {_REMOTE_LIB_NAME} &&"
            f"LD_LIBRARY_PATH=/usr/local/lib:. "
            f"sudo "
            f"taskset --cpu-list {self._pinned_core} "
            f"chrt -f 99 "
            f"./{self._execmd} "
            f"{nb_reps}"
        )
        if err != "":
            raise RuntimeError(f"Benchmark failed:\n\tstdout:\n{out}\n\tstderr:\n{err}")

        self._del_library(_REMOTE_LIB_NAME)
        return self._parse_exec_str(out)


from .ssh_raw import SSH_Session


class SSH_MX_Channel:

    _mx_remote_tmp_template: str = "pipedream.ssh_mx_channel.XXXXXX"
    _exec_bench_default_timeout = (
        4 * 3600
    )  # reasonably large timeout for completion of benchmark
    _min_remote_timeout = 60  # Minimal timeout for small commands

    def __init__(
        self,
        host: str,
        user: str,
        port: int,
        wrapper: str,
        keylocation: str = None,
        pinned_core: int = 2,
        multiplexing: bool = True,
        ssh_opts: Dict[str, str] = {},
    ) -> None:
        logger.debug(f"Initiate connection to {host}, pinned core {pinned_core}")
        assert wrapper.endswith(".c"), f"only support .c wrapper for now"
        self._host = host
        self._pinned_core = pinned_core
        self._wrapper = wrapper
        self._ssh_client = SSH_Session(
            host=host,
            port=port,
            user=user,
            keylocation=keylocation,
            multiplexing=multiplexing,
            ssh_opts=ssh_opts,
        )
        self._wrappers: Dict[str, str] = {}
        self._run_dir = ""

    def _init_remote(self) -> None:
        if self._run_dir:
            return

        cod, out, err = self._ssh_client.remote_exec(
            ["arch"], timeout=self._min_remote_timeout
        )
        assert cod == 0, f"unexpected error getting remote arch: {err}"
        remote_arch = out.strip()
        self._arch = {"x86_64": "x86", "aarch64": "ARMv8a"}.get(remote_arch)
        assert self._arch is not None, f"unexpected arch: {remote_arch}"
        cod, out, err = self._ssh_client.remote_exec(
            ["mktemp", "-d", "--tmpdir", self._mx_remote_tmp_template],
            timeout=self._min_remote_timeout,
        )
        assert cod == 0, f"unexpected error creating remote tmpdir: {err}"
        self._run_dir = out.strip()
        cod, err = self._ssh_client.remote_copy(
            self._wrapper,
            f"{self._run_dir}/{pathlib.Path(self._wrapper).name}",
            timeout=self._min_remote_timeout,
        )
        assert cod == 0, f"unexpected error copying wrapper: {err}"

    def _build_wrapper(self, perf_lib: str) -> str:
        if perf_lib == "papi":
            libs = "-lpapi -ldl"
            cflags = "-DPAPI"
        elif perf_lib == "perf-pipedream":
            libs = "-lperf-pipedream -ldl"
            cflags = ""
        elif perf_lib == "none":
            libs = "-DNONE -ldl"
            cflags = ""
        else:
            assert 0, f"unexpected perf lib: {perf_lib}"
        wrapper = pathlib.Path(self._wrapper)
        wrapper_exe = f"{wrapper.stem}-{perf_lib}.exe"
        cmd = (
            f"cd {self._run_dir} && "
            f"gcc -O2 {cflags} {wrapper.name} {libs} -o {wrapper_exe}"
        )
        cod, out, err = self._ssh_client.remote_exec(
            cmd, shell=True, timeout=self._min_remote_timeout
        )
        assert cod == 0, f"unexpected error compiling wrapper: {cmd}: {err}"
        return wrapper_exe

    def _send_library(self, library: pathlib.Path, timeout: float = None) -> None:
        src = library.as_posix()
        dst = f"{self._run_dir}/{library.name}"
        cod, err = self._ssh_client.remote_copy(src, dst, timeout)
        if cod != 0:
            raise RuntimeError(
                "Could not send library "
                f"{src} -> {dst}, exit code: {cod}:\n\tstderr:\n{err}"
            )

    def _parse_exec_str(self, out: str, results: List) -> int:
        for line in out.strip().split("\n"):
            if not line:
                continue
            vals = line.split(None, 1)
            if len(vals) < 2:
                return 1
            results.append(int(vals[1]))
        return 0

    def exec_bench(
        self,
        library: str,
        arch: str,
        nb_reps: int = None,
        events: List[str] = None,
        benchmark_index: int = -1,
        timeout: float = None,
        perf_lib: str = None,
        results: List[int] = None,
    ) -> int:
        self._init_remote()
        logger.debug(
            f"Exec benchmark {library}/{benchmark_index} with wrapper {self._wrapper} on {self._host}:{self._run_dir}"
        )
        assert library.endswith(".o"), f"only support .o library for now"
        assert (
            arch == self._arch
        ), f"can't exec bench for {arch} on remote arch {self._arch}"
        if perf_lib is None:
            perf_lib = "papi" if self._arch == "x86" else "perf-pipedream"
        if self._wrappers.get(perf_lib) is None:
            self._wrappers[perf_lib] = self._build_wrapper(perf_lib)
        lib = pathlib.Path(library)
        if timeout is None:
            timeout = self._exec_bench_default_timeout
        end_time = time.time() + timeout
        min_timeout = self._min_remote_timeout
        self._send_library(lib, timeout=min(min_timeout, end_time - time.time()))
        libname = "benchmarks.so"
        if events is None:
            events = ["PAPI_TOT_CYC", "PAPI_TOT_INS"]
        args_idx = f"--idx {benchmark_index}" if benchmark_index != -1 else ""
        args_nb_reps = f"--iters {nb_reps}" if nb_reps != -1 else ""
        args_events = " ".join(events)
        cmd = (
            f"cd {self._run_dir} && "
            f"gcc -shared -o {libname} {lib.name} && "
            f"taskset --cpu-list {self._pinned_core} "
            f"./{self._wrappers[perf_lib]} {args_nb_reps} {args_idx} ./{libname} {args_events}"
        )
        cod, out, err = self._ssh_client.remote_exec(
            cmd, shell=True, timeout=min(min_timeout, end_time - time.time())
        )
        if cod != 0:
            raise RuntimeError(
                f"Benchmark execution failed with exit code {cod}: {cmd}\n\tstdout:\n{out}\n\tstderr:\n{err}"
            )
        if results is None:
            return 0
        return self._parse_exec_str(out, results)

    def __del__(self) -> None:
        if not self._run_dir:
            return
        try:
            logger.debug(f"Removing remote tmp dir: {self._host}:{self._run_dir}")
            self._ssh_client.remote_exec(
                ["rm", "-rf", self._run_dir], timeout=self._min_remote_timeout
            )
        except RuntimeError:
            logger.warning(
                f"Failed removing remote tmp dir: {self._host}:{self._run_dir}"
            )
            pass


# Default implementations
SSH_Channel = SSH_MX_Channel
