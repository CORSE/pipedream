from typing import *
import logging
import subprocess
import re
import os

logger = logging.getLogger(__name__)

_ssh_quote_pos = re.compile("(?=[^-0-9a-zA-Z_./\n])")


def _ssh_quote(arg):
    """
    Returns a quoted string suitable for execution from a shell.
    Original version from:
    http://stackoverflow.com/questions/967443/
    python-module-to-shellquote-unshellquote.
    """

    def quote(arg):
        r"""
        This is the logic emacs uses.
        >>> quote('\t')
        '\\\t'
        >>> print quote('foo bar')
        foo\ bar
        >>> print quote("foo'and'bar")
        foo\'and\'bar
        """
        if arg:
            return _ssh_quote_pos.sub("\\\\", arg).replace("\n", "'\n'")
        else:
            return "''"

    return quote(arg)


class SSH_Session:
    # W.r.t. timeouts, we want to have large timeouts in order
    # to avoid premature disconnect on flaky networks, there are two cases:
    # connection: set a connect timeout with a large retry amount
    #             ConnectTimeout * ConnectionAttempts = 3 * 100 = 5 mins.
    # established: deactivate all timeouts, such that timeout is fully
    #              handled by the SSH Session methods.
    _default_ssh_opts = dict(
        StrictHostKeyChecking="False",
        LogLevel="ERROR",
        ConnectionAttempts="100",
        ConnectTimeout="3",
        TCPKeepAlive="no",
        ServerAliveInterval="0",
        ServerAliveCountMax="0",
    )
    # For control persistence, the connection is established for 300 seconds.
    # Connection is uniquely identified by (user,host,port,remote_user).
    _default_ssh_mx_opts = dict(
        ControlPath="/tmp/ssh_raw.ssh_session.%u.%h.%p.%r.sock",
        ControlPersist="300s",
        ControlMaster="auto",
    )

    def __init__(
        self,
        host: str,
        user: str = "",
        port: int = 0,
        keylocation: str = None,
        multiplexing: bool = True,
        ssh_opts: Dict[str, str] = {},
    ):
        self._host = host
        ssh_opts = {**self._default_ssh_opts}
        self._multiplexing = multiplexing
        if multiplexing:
            ssh_opts.update(self._default_ssh_mx_opts)
        if user:
            ssh_opts.update(dict(User=f"{user}"))
        if port:
            ssh_opts.update(dict(Port=f"{port}"))
        if keylocation:
            ssh_opts.update(dict(IdentityFile=f"{keylocation}"))
        ssh_opts.update(ssh_opts)
        self._ssh_opts = [f"-o{opt}={val}" for opt, val in ssh_opts.items() if val]

        self._init_session()

    def _init_session(self):
        # Execute remotely ignoring failure, ensuring establishment of the ControlMaster
        self.remote_exec(["true"], timeout=10)

    def remote_exec(
        self,
        cmd: Union[str, List[str]],
        shell: bool = False,
        timeout: float = None,
        timeout_exception: Any = None,
    ) -> Tuple[int, str, str]:
        if shell:
            assert isinstance(cmd, str)
            cmd = ["/bin/sh", "-c", _ssh_quote(cmd)]
        else:
            assert isinstance(cmd, list)
        ssh_cmd = ["ssh"] + self._ssh_opts + [self._host] + cmd
        logger.debug(f"remote_exec: {' '.join(ssh_cmd)}")
        try:
            p = subprocess.run(
                ssh_cmd,
                stdin=subprocess.DEVNULL,
                capture_output=True,
                timeout=timeout,
                encoding="utf-8",
                errors="replace",
            )
            cod, out, err = p.returncode, p.stdout, p.stderr
        except subprocess.TimeoutExpired as e:
            if timeout_exception is not None:
                if isinstance(timeout_exception, type):
                    raise timeout_exception(
                        f"timeout expired: {timeout} secs: executing {ssh_cmd}"
                    )
                else:
                    raise timeout_exception
            cod, out, err = (
                1,
                "",
                f"ERROR: timeout expired: {timeout} secs: executing {ssh_cmd}",
            )
        return cod, out, err

    def remote_copy(
        self, src: str, dst: str, timeout: float = None, timeout_exception: Any = None
    ) -> Tuple[int, str]:
        scp_cmd = ["scp"] + self._ssh_opts + [src, f"{self._host}:{dst}"]
        logger.debug(f"remote_copy: {' '.join(scp_cmd)}")
        try:
            p = subprocess.run(
                scp_cmd,
                stdin=subprocess.DEVNULL,
                capture_output=True,
                timeout=timeout,
                encoding="utf-8",
                errors="replace",
            )
            cod, err = p.returncode, p.stderr
        except subprocess.TimeoutExpired as e:
            if timeout_exception is not None:
                if isinstance(timeout_exception, type):
                    raise timeout_exception(
                        f"timeout expired: {timeout} secs: executing {scp_cmd}"
                    )
                else:
                    raise timeout_exception
            cod, err = (
                1,
                f"ERROR: timeout expired: {timeout} secs: executing {scp_cmd}",
            )
        return cod, err

    def disconnect(self) -> None:
        # Actually multiplexing is also shared between sessions, hence nothing to do
        pass
