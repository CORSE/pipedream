"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

"""
  Generate and run benchmarks
"""

__all__ = [
    "set_remote_connexion",
    "get_remote_connexion",
    "unset_remote_connexion",
    "run_benchmarks",
    "gen_benchmark_lib",
    "Benchmark_Lib",
    "Perf_Counter_Spec",
    "Benchmark_Spec",
    "Benchmark_Kind",
    "Benchmark_Run",
    "Statistics",
    "SegmentationFault",
    "SSH_Channel",
]

import argparse
import collections
import contextlib
import ctypes
import _ctypes
import fnmatch
import fractions
import itertools
import numpy
import os
import random
import re
import subprocess
import sys
import tempfile
import time
import typing as ty
import segv_recover as sgr
from typing import IO
import platform

from pipedream.benchmark.types import *
from pipedream.benchmark.ssh import SSH_Channel

from pipedream.asm.asmwriter import ASM_Writer
from pipedream.asm import allocator
from pipedream.asm import ir
from pipedream.utils import abc
from pipedream.utils import chunks
from pipedream.utils import pyperfs
from pipedream.utils import terminal


################################################################################
##### PUBLIC API
class SegmentationFault(Exception):
    pass


def raise_segfault(msg: str):
    raise SegmentationFault(msg)


def set_remote_connexion(
    host: str,
    user: str = "",
    port: int = 0,
    wrapper: str = None,
    keylocation: str = None,
    pinned_core: int = 2,
):
    if wrapper is None:
        wrapper = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                "../../../tools/benchmark-loader/benchmark-wrapper.c",
            )
        )

    _Benchmark_Runner._SSH_BENCHER = SSH_Channel(
        host, user, port, wrapper, keylocation, pinned_core
    )
    Perf_Counter_Spec._REMOTE = True


def get_remote_connexion():
    return _Benchmark_Runner._SSH_BENCHER


def unset_remote_connexion():
    _Benchmark_Runner._SSH_BENCHER = None
    Perf_Counter_Spec._REMOTE = False


def run_benchmarks(
    *,
    arch: ir.Architecture,
    benchmarks: ty.List[Benchmark_Spec],
    perf_counters: ty.Optional["Perf_Counter_Spec"],
    num_iterations: int,
    num_warmup_iterations: int,
    keep_raw_data: bool = False,
    outlier_low: int = 0,
    outlier_high: int = 100,
    verbose: bool = True,
    debug: bool = False,
    tmp_dir: str,
    is_lib_tmp: bool = False,
) -> ty.Iterable[Benchmark_Run]:
    """
    Generate a benchmark libary and run all benchmarks in it.
    This returns a generator, so benchmarks are only run if you actually use the results.

    Args:
      perf_counters:    Set of Perf events to measure.
                        If None no Perf events will be measured (we won't even try to link to libpapi).
      keep_raw_data:    Controls whether the Statistics fields of the retured Benchmark_Run objects should store
                        the measured raw values of performance counters or not.
      verbose:          Print status messages
      debug:            Generated code will check for errors.
    """

    runner = _Benchmark_Runner(verbose=verbose)

    total_num_iterations = num_iterations + num_warmup_iterations

    bench_lib = runner._gen_benchmark_lib(
        benchmark_specs=benchmarks,
        architecture=arch,
        num_iterations=total_num_iterations,
        gen_iaca_markers=False,
        perflib=perf_counters.perflib if perf_counters is not None else None,
        tmp_dir=tmp_dir,
        debug=debug,
        is_lib_tmp=is_lib_tmp,
    )
    return runner._run_benchmarks(
        arch=arch,
        bench_lib=bench_lib,
        perf_counters=perf_counters,
        num_iterations=num_iterations,
        num_warmup_iterations=num_warmup_iterations,
        keep_raw_data=keep_raw_data,
        outlier_low=outlier_low,
        outlier_high=outlier_high,
        tmp_dir=tmp_dir,
    )


def gen_benchmark_lib(
    *,
    arch: ir.Architecture,
    benchmarks: ty.List[Benchmark_Spec],
    num_iterations: int,
    dst_dir: str,
    verbose: bool = True,
    gen_iaca_markers: bool = False,
    gen_papi_calls: bool = True,
    debug: bool = False,
    is_lib_tmp: bool = False,
) -> "Benchmark_Lib":
    """
    Generate and load shared library with code for benchmarks.
    Does not actually run benchmarks.

    Args:
      num_iterations:   How many times every benchmark function runs each benchmark.
      dst_dir:          Where to write generated files to.
      gen_papi_calls:   Generate calls to PAPI/Perf to measure performance, if this is false NO timing or other measurements
                        will be performed!
      gen_iaca_markers: Generate machine code start/stop markers for Intel IACA before after the benchmark kernel.
                        These execute as nops, but are used by IACA to detect the kernel code for analysis.
      verbose:          Print status messages
      debug:            Generated code will check for errors.

    """

    lib = _Benchmark_Runner(verbose=verbose)._gen_benchmark_lib(
        benchmark_specs=benchmarks,
        architecture=arch,
        num_iterations=num_iterations,
        gen_iaca_markers=gen_iaca_markers,
        perflib=pyperfs.get_perf_lib() if gen_papi_calls else None,
        tmp_dir=dst_dir,
        debug=debug,
        is_lib_tmp=is_lib_tmp,
    )

    return lib


class Benchmark_Lib:
    """
    Little wrapper around the ctypes dynamic library we generate
    containing our benchmark kernels.
    """

    @property  # type:ignore
    def asm_file(self) -> pathlib.Path:
        """
        Path to assembly file containing benchmark functions.
        """

        return self._asm_file

    @property  # type:ignore
    def object_file(self) -> ty.Optional[pathlib.Path]:
        """
        Path to object file containing benchmark functions.
        """

        return self._object_file

    @property  # type:ignore
    def shared_lib_file(self) -> ty.Optional[pathlib.Path]:
        """
        Path to shared library containing benchmark functions.
        """

        return self._lib_file

    @property  # type:ignore
    def benchmarks(self) -> ty.Collection[Benchmark_Spec]:
        """
        Collection of all benchmarks this library contains
        """

        return self._benchmarks.keys()

    def benchmark_function(self, bench: Benchmark_Spec) -> ty.Callable:
        """
        Get a ctypes callable for a benchmark function.
        """

        self._load_dll()
        fn_name = self._benchmarks[bench]
        assert self._shared_lib is not None
        fn = self._benchmark_function_type(
            (fn_name, self._shared_lib),
            (
                (1, "papi_event_set", 0),
                (1, "num_papi_events", 0),
                (1, "papi_result_array", None),
                (1, "reg_values", None),
                (1, "load_area", None),
                (1, "store_area", None),
            ),
        )
        fn.__name__ = fn_name  # type:ignore
        return fn

    def benchmark_name(self, bench: Benchmark_Spec) -> str:
        """
        Get benchmark function name.
        """
        return self._benchmarks[bench]

    ## private parts

    def __init__(
        self,
        *,
        asm_file: pathlib.Path,
        object_file: ty.Optional[pathlib.Path],
        lib_file: ty.Optional[pathlib.Path],
        shared_lib: ctypes.CDLL = None,
        benchmarks: ty.Dict[Benchmark_Spec, str],
        unlink: bool = True,
    ):
        self._asm_file = asm_file
        self._object_file = object_file
        self._lib_file = lib_file
        self._shared_lib = shared_lib
        self._benchmarks = benchmarks
        self._unlink = unlink

        self._benchmark_function_type = ctypes.CFUNCTYPE(
            ## return type
            ctypes.c_int,
            ## argument types
            ctypes.c_int,
            ctypes.c_ssize_t,
            ctypes.POINTER(ctypes.c_longlong),
            ctypes.POINTER(ctypes.c_byte),
            ctypes.POINTER(ctypes.c_byte),
            ctypes.POINTER(ctypes.c_byte),
        )

    def _load_dll(self):
        assert self._lib_file is not None
        if self._shared_lib is not None:
            return
        self._shared_lib = ctypes.CDLL(self._lib_file)

    def __del__(self):
        """
        Unload the lib before closing
        """
        if self._shared_lib is not None:
            _ctypes.dlclose(self._shared_lib._handle)
        if self._unlink:
            for path in [self._asm_file, self._object_file, self._lib_file]:
                if path is not None:
                    if os.path.exists(path):
                        os.unlink(path)


class Perf_Counter_Spec:
    """
    Helper for setting up and managing perf counters for measuring benchmark performance.
    """

    @classmethod
    def make_throughput_counters(
        clss, extra_events: ty.List[str] = []
    ) -> "Perf_Counter_Spec":
        """
        Get set of counters for measuring a throughput benchmark's performance.
        """

        perflib = pyperfs.get_perf_lib()

        (
            cycle_counter,
            instruction_counter,
            fused_uop_counter,
            unfused_uop_counter,
        ) = clss._get_basic_counters(perflib)

        events = [instruction_counter]
        if fused_uop_counter:
            events += [fused_uop_counter]
        if unfused_uop_counter:
            events += [unfused_uop_counter]
        events += list(extra_events)

        return Perf_Counter_Spec(
            perflib=perflib,
            cycle_counter=cycle_counter,
            instruction_counter=instruction_counter,
            fused_uop_counter=fused_uop_counter,
            unfused_uop_counter=unfused_uop_counter,
            port_events={},
            extra_events=events,
            events_in_every_set=[cycle_counter],
        )

    @classmethod
    def make_latency_counters(
        clss, extra_events: ty.List[str] = []
    ) -> "Perf_Counter_Spec":
        """
        Get set of counters for measuring a latency benchmark's performance.
        """

        return clss.make_throughput_counters(extra_events)

    @classmethod
    def make_port_counters(
        clss, extra_events: ty.List[str] = []
    ) -> "Perf_Counter_Spec":
        """
        Get set of counters for measuring execution port usage of a benchmark.
        """

        perflib = pyperfs.get_perf_lib()

        (
            cycle_counter,
            instruction_counter,
            fused_uop_counter,
            unfused_uop_counter,
        ) = clss._get_basic_counters(perflib)

        events = [instruction_counter]
        if fused_uop_counter:
            events += [fused_uop_counter]
        if unfused_uop_counter:
            events += [unfused_uop_counter]
        events += list(extra_events)

        # FIXME: recent Intel CPUs only
        ports = {
            port: evt
            for port, evt in clss._get_uop_counters_per_port(perflib)
            if perflib.can_count_event(evt)
        }

        return Perf_Counter_Spec(
            perflib=perflib,
            cycle_counter=cycle_counter,
            instruction_counter=instruction_counter,
            fused_uop_counter=fused_uop_counter,
            unfused_uop_counter=unfused_uop_counter,
            port_events=ports,
            extra_events=events,
            events_in_every_set=[cycle_counter],
        )

    ##### access registered PAPI event set

    @property  # type:ignore
    def port_counters(self) -> ty.Dict[int, str]:
        """
        Per execution port counters.
        """

        return self._port_events

    @property  # type:ignore
    def event_sets(self) -> ty.Iterable[pyperfs.PerfEventSet]:
        return iter(self.evts.event_sets)

    @property  # type:ignore
    def num_event_sets(self) -> int:
        return self.evts.num_event_sets

    ##### names of PAPI perf counters

    def cycle_counter(self) -> str:
        return self._cycle_counter

    def instruction_counter(self) -> str:
        return self._instruction_counter

    def unfused_uop_counter(self) -> ty.Optional[str]:
        """Count muops in the unfused domain (after fission of fused micro ops)"""
        return self._unfused_uop_counter

    def fused_uop_counter(self) -> ty.Optional[str]:
        """Count muops in the fused domain (fused micro ops in ROB, etc.)"""
        return self._fused_uop_counter

    ##### private parts

    _REMOTE: bool = False

    _perf_obj_: ty.Optional[pyperfs.PerfMeasurementLibrary] = None

    class NO_DEFAULT_TYPE:
        def __init__(self):
            pass

    NO_DEFAULT = NO_DEFAULT_TYPE()

    @classmethod
    def _get_basic_counters(
        clss, perflib
    ) -> ty.Tuple[str, str, ty.Optional[str], ty.Optional[str]]:
        """
        Return tuple: (cycle counter, instruction counter, fused muop counter, unfused muop counter)
        """

        if clss._REMOTE:
            # For remote execution, we fall back to universal defaults
            return (
                "PAPI_TOT_CYC",
                "PAPI_TOT_INS",
                None,
                None,
            )

        cycle_counter = clss._find_first_working_counter(
            perflib,
            "cycle counter",
            [
                ## recent intel CPUs only
                "CPU_CLK_THREAD_UNHALTED:u=1",
                ## works on recent AMD
                "CYCLES_NOT_IN_HALT",
                ## fallback
                "PAPI_TOT_CYC",
            ],
        )
        assert cycle_counter is not None

        instruction_counter = clss._find_first_working_counter(
            perflib,
            "instruction counter",
            [
                ## recent intel CPUs only
                "INSTRUCTIONS_RETIRED",
                ## works on recent AMD
                "RETIRED_INSTRUCTIONS",
                ## fallback
                "PAPI_TOT_INS",
            ],
        )
        assert instruction_counter is not None

        fused_uop_counter = clss._find_first_working_counter(
            perflib,
            "fused uop counter",
            [
                # FIXME: recent Intel CPUs only
                "UOPS_RETIRED:RETIRE_SLOTS",
                # "UOPS_ISSUED",
            ],
            default=None,
        )

        unfused_uop_counter = clss._find_first_working_counter(
            perflib,
            "unfused uop counter",
            [
                # FIXME: recent Intel CPUs only
                # "UOPS_EXECUTED",
                "UOPS_RETIRED:ALL",
                # "UOPS_RETIRED:RETIRE_SLOTS",
                # "UOPS_ISSUED",
            ],
            default=None,
        )

        return (
            cycle_counter,
            instruction_counter,
            fused_uop_counter,
            unfused_uop_counter,
        )

    @classmethod
    def _get_uop_counters_per_port(
        clss, perflib: pyperfs.PerfMeasurementLibrary
    ) -> ty.Sequence[ty.Tuple[int, str]]:
        """Count unfused muops executed per execution port"""

        if clss._REMOTE:
            return []

        if perflib.can_count_event("UOPS_EXECUTED_PORT:PORT_0"):
            return tuple(
                enumerate(
                    [
                        "UOPS_EXECUTED_PORT:PORT_0",
                        "UOPS_EXECUTED_PORT:PORT_1",
                        "UOPS_EXECUTED_PORT:PORT_2",
                        "UOPS_EXECUTED_PORT:PORT_3",
                        "UOPS_EXECUTED_PORT:PORT_4",
                        "UOPS_EXECUTED_PORT:PORT_5",
                        "UOPS_EXECUTED_PORT:PORT_6",
                        "UOPS_EXECUTED_PORT:PORT_7",
                    ]
                )
            )

        if perflib.can_count_event("UOPS_DISPATCHED_PORT:PORT_0"):
            return tuple(
                enumerate(
                    [
                        "UOPS_DISPATCHED_PORT:PORT_0",
                        "UOPS_DISPATCHED_PORT:PORT_1",
                        "UOPS_DISPATCHED_PORT:PORT_2",
                        "UOPS_DISPATCHED_PORT:PORT_3",
                        "UOPS_DISPATCHED_PORT:PORT_4",
                        "UOPS_DISPATCHED_PORT:PORT_5",
                        "UOPS_DISPATCHED_PORT:PORT_6",
                        "UOPS_DISPATCHED_PORT:PORT_7",
                    ]
                )
            )

        return []

    @classmethod
    def _find_first_working_counter(
        clss,
        perflib,
        name: str,
        candidates: ty.List[str],
        default: ty.Union[None, str, NO_DEFAULT_TYPE] = NO_DEFAULT,
    ) -> ty.Optional[str]:
        """
        Get first counter from list we can actually count.
        """

        for counter in candidates:
            if perflib.can_count_event(counter):
                return counter

        if default is clss.NO_DEFAULT:
            raise ValueError(f"PAPI/Perf does not have any decent {name}")
        else:
            assert not isinstance(default, clss.NO_DEFAULT_TYPE)
            return default

    def __init__(
        self,
        *,
        perflib: pyperfs.PerfMeasurementLibrary,
        cycle_counter: str,
        instruction_counter: str,
        fused_uop_counter: ty.Optional[str],
        unfused_uop_counter: ty.Optional[str],
        port_events: ty.Dict[int, str],
        extra_events: ty.List[str],
        events_in_every_set: ty.List[str],
    ):
        assert type(port_events) is dict

        self.perflib = perflib

        self._port_events = port_events

        self._cycle_counter = cycle_counter
        self._instruction_counter = instruction_counter
        self._fused_uop_counter = fused_uop_counter
        self._unfused_uop_counter = unfused_uop_counter

        self.evts = self.perflib.make_event_manager(
            events=[evt for port, evt in port_events.items()] + list(extra_events),
            events_in_every_set=events_in_every_set,
            no_op=self._REMOTE,
        )


################################################################################
##### PRIVATE PARTS


def get_max_addr_offset(list_instr: ty.List[ir.Instruction]) -> ty.Optional[int]:
    maxi = None
    for ins in list_instr:
        for op in ins.operands:
            if isinstance(op, ir.Memory_Operand):
                for sop in op.sub_operands:
                    if isinstance(sop, ir.ImmMinMax_Operand):
                        newmax = sop.max()
                        if maxi is None:
                            maxi = newmax
                        else:
                            maxi = min(maxi, sop.max())
    return maxi


class _Benchmark_Runner:
    _SSH_BENCHER: ty.Optional[SSH_Channel] = None

    def __init__(self, verbose: bool):
        # Number of instruction outside the benchmark loop after the
        # `measurement_start()` call
        self._num_prologue_inst = 0
        self.info: ty.Any
        if verbose:
            self.info = terminal.Info_Line()
        else:
            self.info = lambda *args, **kwargs: None
            self.info.clear = self.info
        self.no_obj = False
        self.no_link = False
        if self._SSH_BENCHER is not None:
            self.no_link = True

    def _run_benchmarks(
        self,
        *,
        arch: ir.Architecture,
        bench_lib: Benchmark_Lib,
        perf_counters: ty.Optional[Perf_Counter_Spec],
        num_iterations: int,
        num_warmup_iterations: int,
        keep_raw_data: bool,
        outlier_low: int,
        outlier_high: int,
        tmp_dir: str,
    ) -> ty.Iterable[Benchmark_Run]:
        num_benchmarks = len(bench_lib.benchmarks)

        for i, benchmark in enumerate(bench_lib.benchmarks, 1):
            result = self._run_benchmark(
                arch=arch,
                benchmark_index=i,
                num_benchmarks=num_benchmarks,
                perf_counters=perf_counters,
                benchmark_lib=bench_lib,
                benchmark=benchmark,
                num_iterations=num_iterations,
                num_warmup_iterations=num_warmup_iterations,
                keep_raw_data=keep_raw_data,
                outlier_low=outlier_low,
                outlier_high=outlier_high,
            )
            self.info.clear()
            yield result

    def _create_benchmark_specs(
        self, arch: ir.Architecture, params, kind: Benchmark_Kind
    ) -> ty.List[Benchmark_Spec]:
        self.info("generate benchmark specs")

        ALL_BENCHMARKS = []

        def mk_benchmark(**kwargs):
            benchmark = Benchmark_Spec.from_instructions(
                **kwargs,
                kind=kind,
                arch=arch,
            )

            ALL_BENCHMARKS.append(benchmark)

        for dynamic_instructions in params.dynamic_instructions:
            for kernel_size in params.kernel_size:
                for insts in params.kernels:
                    mk_benchmark(
                        num_dynamic_instructions=dynamic_instructions,
                        unrolled_length=kernel_size,
                        instructions=insts,
                    )

                if params.tags:
                    instructions = glob_instruction_tags(arch, params.tags)

                    if not instructions:
                        self._die(
                            "no instruction found for tag(s): "
                            + ", ".join(map(repr, params.tags))
                        )

                    for (
                        number_of_different_instructions
                    ) in params.number_of_different_instructions:
                        for insts in itertools.combinations_with_replacement(
                            instructions, number_of_different_instructions
                        ):
                            mk_benchmark(
                                num_dynamic_instructions=dynamic_instructions,
                                unrolled_length=kernel_size,
                                instructions=insts,
                            )

            for unroll_factor in params.unroll_factor:
                for insts in params.kernels:
                    mk_benchmark(
                        num_dynamic_instructions=dynamic_instructions,
                        unroll_factor=unroll_factor,
                        instructions=insts,
                    )

                if params.tags:
                    instructions = glob_instruction_tags(arch, params.tags)

                    if not instructions:
                        self._die(
                            "no instruction found for tag(s): "
                            + ", ".join(map(repr, params.tags))
                        )

                    for (
                        number_of_different_instructions
                    ) in params.number_of_different_instructions:
                        if not instructions:
                            raise ValueError(
                                "no instruction found for tags: "
                                + ", ".join(map(repr, params.tags))
                            )

                        for insts in itertools.combinations_with_replacement(
                            instructions, number_of_different_instructions
                        ):
                            mk_benchmark(
                                num_dynamic_instructions=dynamic_instructions,
                                unroll_factor=unroll_factor,
                                instructions=insts,
                            )

        return ALL_BENCHMARKS

    def _parse_cmd_line_args(self, argv):
        parser = argparse.ArgumentParser(
            "run-benchmarks",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )

        #####

        class List_Action(argparse.Action):
            """
            https://bugs.python.org/issue16399
            argparse 'append' with a default list appends to that default.
            We want it replaced if a value occurs.
            """

            def __init__(self, *args, default, **kwargs):
                super().__init__(*args, default=default, **kwargs)
                self.reset_dest = True

            def __call__(self, parser, namespace, values, option_string=None):
                if self.reset_dest:
                    setattr(namespace, self.dest, [])
                    self.reset_dest = False
                getattr(namespace, self.dest).append(values)

        class Set_Action(argparse.Action):
            """
            https://bugs.python.org/issue16399
            argparse 'add' with a default set appends to that default.
            We want it replaced if a value occurs.
            """

            def __init__(self, *args, default, **kwargs):
                super().__init__(*args, default=frozenset(default), **kwargs)
                self.reset_dest = True

            def __call__(self, parser, namespace, values, option_string=None):
                if self.reset_dest:
                    setattr(namespace, self.dest, set())
                    self.reset_dest = False
                setattr(
                    namespace,
                    self.dest,
                    getattr(namespace, self.dest) | frozenset(values),
                )

        parser.register("action", "set", Set_Action)
        parser.register("action", "list", List_Action)

        #####

        def percentile_parser(txt: str) -> float:
            """
            argparse argument type for percentiles.
            Accepts either an int between 0-100 or a float between 0-1.
            """
            val: ty.Union[int, float]
            try:
                val = int(txt)

                if val < 0 or 100 < val:
                    raise argparse.ArgumentTypeError(
                        repr(txt) + " is not a valid percentile, not in [0, 100]"
                    )

                return val
            except ValueError:
                pass

            try:
                val = float(txt)

                if val < 0 or 100 < val:
                    raise argparse.ArgumentTypeError(
                        repr(txt) + " is not a valid percentile, not in [0.0, 1.0]"
                    )

                return val
            except ValueError:
                pass

            raise argparse.ArgumentTypeError(
                repr(txt) + " is not a valid percentile, neither an int nor float."
            )

        parser.add_argument(
            "--help-insts",
            help="list all instruction names for current architecture and exit",
            action="store_true",
            default=argparse.SUPPRESS,
        )

        parser.add_argument(
            "--help-tags",
            help="list all tags for current architecture and exit",
            action="store_true",
            default=argparse.SUPPRESS,
        )

        #####
        run = parser.add_argument_group("run args")

        kernels = run.add_argument(
            "-k",
            "--kernel",
            dest="kernels",
            help="""Run kernel described in kernel spec
              (can be specified multiple times).""",
            type=str,
            action="append",
            default=[],
        )

        tags = run.add_argument(
            "-t",
            "--tag",
            dest="tags",
            help="""Tags for selecting instructions to run benchmarks for.
              Value can be a tag,
              (for example 'add-i64-rr' for 64-bit integer addition via registers)
              or a Unix style glob
              (for example 'add-*-rr' for integer addition via registers of any width).
              You can also give simple boolean expressions combinings tags and globs using
              '&' (only matches if both operands match) and
              '|' (matches if either operand matches)
              (for example '(add-*-rr & *-i8-*) | sub-i64-*').
              '|' binds stronger than '&', you can uses parenthesis to disambiguate.
              Can be specified multiple times, the resulting query matches
              all instructions matching ANY of the given queries.""",
            type=str,
            action="append",
            default=[],
        )

        events = run.add_argument(
            "-e",
            "--event",
            dest="extra_events",
            help="""Extra PAPI/Perf event to measure""",
            type=str,
            action="append",
            default=[],
        )

        different_instructions = run.add_argument(
            "-n",
            "--number-of-different-instructions",
            help="""number of different instructions to put in each kernel
              (can be specified multiple times to run multiple experiments).""",
            type=int,
            default=[1],
            action="list",
        )

        mut = run.add_mutually_exclusive_group()

        unroll_factor = mut.add_argument(
            "-u",
            "--unroll-factor",
            help="""How many times the kernel should be unrolled.
              (can be specified multiple times to run multiple experiments).""",
            type=int,
            default=[],
            action="list",
        )

        kernel_size = mut.add_argument(
            "-s",
            "--kernel-size",
            help="""how many instructions should be in inner loop of the benchmark kernel.
              Kernels are unrolled to have that many instructions.
              (can be specified multiple times to run multiple experiments).""",
            type=int,
            default=[],
            action="list",
        )

        dynamic_instructions = run.add_argument(
            "-i",
            "--dynamic-instructions",
            help="""how many dynamic instructions should be executed for each kernel
              (can be specified multiple times to run multiple experiments).""",
            type=int,
            default=[1_000_000],
            action="list",
        )

        num_iterations = run.add_argument(
            "-N",
            "--num-iterations",
            help="""how many times the benchmark function should be run.""",
            type=int,
            default=500,
        )
        num_warmup_iterations = run.add_argument(
            "-W",
            "--num-warmup-iterations",
            help="""how many warmup iterations should be run for the benchmark function before starting measurements.""",
            type=int,
            default=100,
        )
        outlier_lo = run.add_argument(
            "--outlier-low",
            "--olo",
            help="""If the cycle count of a run is below this percentile the run is considered an outlier and dropped.""",
            type=percentile_parser,
            default=0,
        )
        outlier_hi = run.add_argument(
            "--outlier-high",
            "--ohi",
            help="""If the cycle count of a run is above this percentile the run is considered an outlier and dropped.""",
            type=percentile_parser,
            default=100,
        )

        #####

        output = parser.add_argument_group("output options")

        output_file = output.add_argument(
            "-o",
            "--output",
            help="""file to print benchmark results to""",
        )
        output_append = output.add_argument(
            "-a",
            "--append",
            help="""append to output file insted of overwriting it""",
            action="store_true",
            default=False,
        )

        gen_iaca_markers = output.add_argument(
            "--gen-iaca-markers",
            help="""Insert Intel IACA start/stop markers before/after every benchmark kernel.""",
            action="store_true",
            default=False,
        )

        gen_papi_calls = output.add_argument(
            "--no-gen-papi-calls",
            dest="gen_papi_calls",
            help="""
        Do NOT generate PAPI/Perf calls to measure benchmark performance.
        With this option all benchmark measurements will be meaningless.
      """,
            action="store_false",
            default=True,
        )

        tmp_dir = output.add_argument(
            "--tmp-dir",
            help="""directory for temporary files""",
            default=tempfile.mkdtemp(prefix="pipedream.", dir="/tmp/"),
        )

        debug = parser.add_argument(
            "--debug",
            action="store_const",
            const=True,
            default=False,
        )

        #####

        args = parser.parse_args(argv)

        arch = ir.Architecture.for_name("x86")

        if getattr(args, "help_insts", False):
            if args.tags:
                insts = glob_instruction_tags(arch, args.tags)
            else:
                insts = sorted(
                    arch.instruction_set().instructions(), key=lambda i: i.name
                )

            for inst in insts:
                if not inst.can_benchmark:
                    continue
                print(inst.name)
            exit(0)

        if getattr(args, "help_tags", False):
            for tag in arch.instruction_set().all_tags:
                print(tag)
            exit(0)

        if not args.tags and not args.kernels:
            print("error: you did not specify any kernel or tag", file=sys.stderr)
            parser.print_usage(file=sys.stderr)
            exit(1)

        ##

        if not args.kernel_size and not args.unroll_factor:
            args.kernel_size = [100]

        ##

        kernel_list = []

        for kernel in args.kernels:
            try:
                insts = Benchmark_Spec.from_name_str(arch.instruction_set(), kernel)
            except ValueError as e:
                parser.error(str(e))
                exit(1)

            kernel_list.append(insts)

        args.kernels = kernel_list

        ##

        if not args.output:
            args.output = sys.stdout
        else:
            mode = "a" if args.append else "w"
            try:
                args.output = argparse.FileType(mode)(args.output)
            except argparse.ArgumentTypeError as e:
                parser.error(str(e))

        ##

        class Immutable_Bag(collections.abc.Mapping):
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)

            def __iter__(self):
                return iter(self.__dict__)

            def __getitem__(self, item):
                return self.__dict__[item]

            def __len__(self):
                return len(self.__dict__)

            def __setattr__(self, attr, value):
                raise RuntimeError("Don't mess with the command line options, please!")

        class Immutable_Namespace(Immutable_Bag):
            """
            immutable version of argparse.Namespace
            """

            def __init__(self, *options):
                for opt in options:
                    object.__setattr__(self, opt.dest, getattr(args, opt.dest))

        out = Immutable_Bag(
            debug=args.debug,
            architecture=arch,
            run=Immutable_Namespace(
                kernels,
                tags,
                kernel_size,
                unroll_factor,
                different_instructions,
                dynamic_instructions,
                num_iterations,
                num_warmup_iterations,
                outlier_lo,
                outlier_hi,
                events,
            ),
            output=Immutable_Namespace(
                tmp_dir,
                debug,
                output_file,
                gen_iaca_markers,
                gen_papi_calls,
            ),
        )
        return out

    def _gen_benchmark_paramaters(
        self,
        arch: ir.Architecture,
        num_iterations: int,
        *,
        perflib: ty.Optional[pyperfs.PerfMeasurementLibrary],
    ) -> ty.List["_ASM_Builder.ASM_Statement"]:
        out = _ASM_Builder(arch=arch)

        params_name = "benchmarks_params"
        out.comment("*" * 70)
        out.comment("*" * 3, "BGN GLOBAL PARAMATERS", params_name)
        out.comment("*" * 3, "init_area_align, init_area_size")
        out.comment(
            "*" * 3, "load_store_area_align, load_store_area_size, store_area_offset"
        )
        out.comment("*" * 3, "has_perflib, num_iterations")
        init_area_align = arch.max_vector_size
        init_area_size = arch.nb_vector_reg * arch.max_vector_size
        load_store_area_align = arch.max_vector_size
        load_store_area_size = allocator.Memory_Arena_Allocator.MAX_ARENA_SIZE
        store_area_offset = allocator.Memory_Arena_Allocator.LD_TO_STORE_OFFSET
        out.global_int_values(
            params_name,
            [
                init_area_align,
                init_area_size,
                load_store_area_align,
                load_store_area_size,
                store_area_offset,
                1 if perflib is not None else 0,
                num_iterations,
            ],
        )
        out.comment("*" * 3, "END GLOBAL PARAMATERS")
        out.comment("*" * 70)
        out.newline()
        return out.take_code()

    def _gen_benchmark_asm_function(
        self,
        arch: ir.Architecture,
        labels: "_ASM_Label_Manager",
        fn_name: str,
        benchmark: Benchmark_Spec,
        num_iterations: int,
        *,
        gen_iaca_markers: bool,
        perflib: ty.Optional[pyperfs.PerfMeasurementLibrary],
        debug=False,
    ) -> ty.List["_ASM_Builder.ASM_Statement"]:
        out = _ASM_Builder(arch=arch, labels=labels)

        num_prologue_insts = 0
        perf_event_set = out.allocate_argument(0)
        num_events = out.allocate_argument(1)
        results = out.allocate_argument(2)
        reg_values = out.allocate_argument(3)
        mem_load_area = out.allocate_argument(4)
        mem_store_area = out.allocate_argument(5)

        perf_start_error = out.make_label(fn_name + ".start.error")
        perf_stop_error = out.make_label(fn_name + ".stop.error")
        stack_start_error: ty.Dict = dict()

        out.newline()
        out.comment("*" * 70)
        out.comment("*" * 3, "BGN FUNCTION", fn_name)
        out.comment(
            "*" * 3,
            benchmark.name,
            f"k={benchmark.unroll_factor:_}",
            f"N={benchmark.kernel_iterations:_}",
            f"PerfMeasure={perflib is not None}",
        )
        out.begin_function(fn_name)

        out.comment("ARG perf_event_set ", perf_event_set)
        out.comment("ARG num_events     ", num_events)
        out.comment("ARG results        ", results)
        out.comment("ARG reg_values     ", reg_values)
        out.comment("ARG mem_load_area  ", mem_load_area)
        out.comment("ARG mem_store_area ", mem_store_area)

        out.push_callee_saves()

        out.newline()
        with out.regs_in_stack(mem_load_area, mem_store_area):
            # Set of registers that are currently used, but will not during the execution
            # of the kernel (=> used only during initialisation)
            unused_registers_kernel = set()

            if arch.name == "x86":
                from pipedream.asm.x86 import RDX  # type:ignore

                assert results is RDX

            PERF_EVENT_SET = out.get_next_scratch_register()
            out.comment("perf_event_set -> ", PERF_EVENT_SET)
            PERF_EVENT_SET = out.move_to(perf_event_set, PERF_EVENT_SET)
            unused_registers_kernel.add(PERF_EVENT_SET)

            num_events_reg = out.get_next_scratch_register()
            out.comment("num_events     -> ", num_events_reg)
            num_events = out.move_to(num_events, num_events_reg)
            unused_registers_kernel.add(num_events)

            RESULTS = out.get_next_scratch_register()
            out.comment("results        -> ", RESULTS)
            RESULTS = out.move_to(results, RESULTS)
            unused_registers_kernel.add(RESULTS)

            REG_VALUES = out.get_next_scratch_register()
            out.comment("reg_values     -> ", REG_VALUES)
            REG_VALUES = out.move_to(reg_values, REG_VALUES)
            unused_registers_kernel.add(REG_VALUES)

            LOOP_COUNTER = out.get_next_scratch_register()
            # Avoid further usage of `LOOP_COUNTER`
            out.take_reg(LOOP_COUNTER)

            out.comment("Stride: size of one row of results table in bytes")
            STRIDE = out.mul_reg_with_const(num_events, 8)

        memory_allocator = (
            allocator.Memory_Arena_Punctual_Allocator()
            if benchmark.kind == Benchmark_Kind.LATENCY
            else allocator.Memory_Arena_Round_Robin_Allocator(
                arch.l1_cache_config, get_max_addr_offset(benchmark.instructions)
            )
        )
        out.analyse_mem_needs(benchmark.instructions)
        out.clear_memory_areas(mem_load_area, mem_store_area, memory_allocator)
        out.move_mem_reg(mem_load_area, mem_store_area, memory_allocator)
        with out.mem_regs_in_stack():
            out.free_reg(LOOP_COUNTER)
            with out.counting_loop("measurement", LOOP_COUNTER, num_iterations) as loop:
                out.pop_from_stack_mem_regs()
                out.comment("push loop counter")
                with out.regs_in_stack(LOOP_COUNTER):
                    ## Pre-allocate kernel and its related variables, as some register information may
                    ## be used by prologue and/or pre-prologue
                    stolen_regs, preallocated_kernel = out.preallocate_benchmark(
                        benchmark.instructions,
                    )

                    ## first generate code benchmark (but don't actually put it in ASM yet)
                    kernel_code: ty.List[_ASM_Builder.ASM_Statement]
                    (
                        kernel_code,
                        fully_allocated_kernel,
                    ) = self._gen_inner_benchmark_loop(
                        out,
                        benchmark,
                        preallocated_kernel,
                        LOOP_COUNTER,
                        memory_allocator,
                        unused_registers_kernel,
                        set(),
                        gen_iaca_markers=gen_iaca_markers,
                    )

                    out.comment("push loop stride")
                    with out.regs_in_stack(STRIDE):
                        ## loop prologue initialise every register not used by `measurement_start()`
                        ## i.e. vectors + some GPR that stay untouched during the call.
                        ## allow pre-prologue generator to see real instructions with allocated
                        ## registers, etc.
                        ## Also push `REG_VALUES` on the stack
                        out.emit_loop_prologue(
                            fully_allocated_kernel,
                            REG_VALUES,
                        )
                        out.free_reg(REG_VALUES)

                        out.comment("push perf_results and perf_event_set")
                        with out.regs_in_stack(RESULTS, PERF_EVENT_SET):
                            with out.mem_regs_in_stack():
                                nb_inst_before = out.nb_inst
                                if perflib is not None:
                                    out.sequentialize_cpu()

                                    out.comment("start PAPI/Perf counters")
                                    nb_inst_before = out.nb_inst
                                    ret = out.call(
                                        perflib.start_function_name, PERF_EVENT_SET
                                    )
                                    if debug:
                                        out.comment("check for PAPI/Perf error")
                                        with out.with_register(ret):
                                            out.branch_if_not_zero(
                                                ret,
                                                perf_start_error,
                                            )

                            ## allow prologue generator to see real instructions with allocated registers, etc.
                            out.emit_benchmark_prologue(
                                fully_allocated_kernel,
                            )

                            ## free registers stolen by backend
                            out.free_stolen_benchmark_registers(stolen_regs)
                            num_prologue_inst = out.nb_inst - nb_inst_before

                            out.comment("*" * 40)
                            out.comment("BGN BENCHMARK")
                            out.comment("LLVM-MCA-BEGIN pipedream_kernel")

                            ## actually emit kernel
                            out.splice_in_code(kernel_code)

                            out.comment("LLVM-MCA-END")
                            out.comment("END BENCHMARK")
                            out.comment("*" * 40)

                            out.emit_benchmark_epilogue(fully_allocated_kernel)

                            nb_inst_before = out.nb_inst
                            out.comment("pop perf_event_set and perf_results")

                        if perflib is not None:
                            out.comment("stop & read PAPI/Perf counters")
                            with out.mem_regs_in_stack():
                                out.sequentialize_cpu()
                                ret = out.call(
                                    perflib.stop_function_name, PERF_EVENT_SET, RESULTS
                                )
                                num_prologue_inst += out.nb_inst - nb_inst_before
                                if debug:
                                    stack_start_error = out.dump_stack_state()
                                    out.comment("check for PAPI/Perf error")
                                    with out.with_register(ret):
                                        out.branch_if_not_zero(
                                            ret,
                                            perf_stop_error,
                                        )

                        out.comment("")
                        out.comment("pop reg_values (loop epilogue)")
                        out.emit_loop_epilogue(fully_allocated_kernel, REG_VALUES)
                        out.comment("pop stride")
                    out.comment("pop loop counter")

                out.newline()
                out.add_registers(STRIDE, RESULTS)
                out.push_to_stack_mem_regs()
            out.newline()
        out.free_mem_regs()

        out.comment("All went well, set return code to zero")
        with out.with_register(out.return_register()) as ret:
            out.put_const_in_register(0, ret)

        out.free_reg(
            PERF_EVENT_SET,
            RESULTS,
            STRIDE,
        )

        out.comment("Restore calle-saves")
        out.pop_callee_saves()
        out.return_(out.return_register())

        out.end_function(fn_name)
        out.comment("*" * 3, "END FUNCTION", fn_name)
        out.comment("*" * 70)
        out.newline()

        out.comment("*" * 70)
        out.comment("*" * 3, "BGN FUNCTION PARAMATERS", fn_name)
        out.comment("*" * 3, "unroll_factor, kernel_iterations, num_prologue_insts")
        out.global_int_values(
            f"{fn_name}_params",
            [benchmark.unroll_factor, benchmark.kernel_iterations, num_prologue_insts],
        )
        out.comment("*" * 3, "END FUNCTION PARAMATERS", fn_name)
        out.comment("*" * 70)
        out.newline()

        self._num_prologue_insts = num_prologue_insts
        benchmark.loop_overhead = arch.loop_overhead(
            benchmark.kernel_iterations, self._num_prologue_inst
        )
        if debug and (perflib is not None):
            out.load_stack_state(stack_start_error)
            out.comment("*" * 3, "PAPI ERROR HANDLING")
            out.emit_label(perf_start_error)
            out.comment("Removing perf_result and perf_event_set from the stack")
            out.shrink_stack(PERF_EVENT_SET.width * 2)

            out.emit_label(perf_stop_error)
            out.shrink_stack_by_mem_regs()
            out.comment(
                "Removing reg_values, loop stride and loop counter from the stack"
            )
            out.shrink_stack(REG_VALUES.width * 3)

            for reg in iter(out.arch.register_set().callee_save_registers()):
                out.free_reg(reg)
            out.comment("Restore calle-saves")
            out.pop_callee_saves()
            out.return_(out.return_register())
            out.newline()

        return out.take_code()

    def _gen_inner_benchmark_loop(
        self,
        out: "_ASM_Builder",
        benchmark: Benchmark_Spec,
        instructions: ty.List[ir.Instruction],
        scratch: ir.Register,
        memory_allocator: ty.Optional[allocator.Memory_Arena_Allocator],
        unused_registers: ty.Set[ir.Register],
        used_registers: ty.Set[ir.Register],
        gen_iaca_markers: bool,
    ) -> ty.Tuple[ty.List["_ASM_Builder.ASM_Statement"], ty.List[ir.Instruction]]:
        """
        Generate inner benchmark loop.
        Does *NOT* emit any code into :out:, code is returned and can later be spliced in
        `unused_registers` denote registers that are currently used when the function
        is called, but will when the `kernel` code is actually generated, the same
        goes invertedly for used_register.
        WARNING: `used_register` only cancels `unused_registers` (i.e. a register
        being in both sets is supposed to be currently used and will not be used
        when generating)
        """
        for reg in unused_registers:
            out.free_reg(reg)
        for reg in used_registers:
            out.take_reg(reg)

        if benchmark.kernel_iterations < 1:
            raise ValueError("benchmark.kernel_iterations must be > 0")

        parent, out = out, out.sub_builder()

        SP = out.arch.register_set().stack_pointer_register()
        LOOP_COUNTER = scratch

        ## holds fully allocated instructions in kernel
        kernel_instructions: ty.List[ir.Instruction]

        with out.with_register(SP), out.counting_loop(
            ".kernel", LOOP_COUNTER, benchmark.kernel_iterations
        ):
            if gen_iaca_markers:
                out.emit_iaca_start_marker()

            out.comment("unroll factor:", benchmark.unroll_factor)

            for inst in instructions:
                out.comment(inst)

            insts = list(instructions) * benchmark.unroll_factor

            read_only_registers = tuple(
                out.arch.register_set().stack_pointer_register_class()
            )
            Allocator_Class = {
                (
                    Benchmark_Kind.THROUGHPUT,
                    True,
                ): allocator.Minimize_Deps_Register_Allocator,
                (
                    Benchmark_Kind.THROUGHPUT,
                    False,
                ): allocator.Minimize_Deps_Pooled_Register_Allocator,
                (
                    Benchmark_Kind.LATENCY,
                    True,
                ): allocator.Maximize_Deps_Register_Allocator,
            }[(benchmark.kind, benchmark.register_pools is None)]
            alloc = Allocator_Class(
                out.arch.make_register_pools(
                    out.iter_free_registers(),
                    read_only_registers,
                    benchmark.register_pools,
                ),
                memory_regs=out._memory_caracs,
                memory_allocator=memory_allocator,
                regs_ro=read_only_registers,
            )
            kernel_instructions = alloc.allocate(insts, out.ir_builder)

            out.newline()

            for idx, chunk in enumerate(chunks(kernel_instructions, len(instructions))):
                out.comment("----- unroll ", idx)

                out.insts(chunk)

                if benchmark.align_kernel:
                    out.align()

            if gen_iaca_markers:
                out.emit_iaca_stop_marker()

        for reg in used_registers:
            out.free_reg(reg)
        for reg in unused_registers:
            out.take_reg(reg)

        return out.take_code(), kernel_instructions

    _PAGE_SIZE = 4096

    def _gen_benchmark_lib(
        self,
        *,
        benchmark_specs: ty.List[Benchmark_Spec],
        architecture: ir.Architecture,
        num_iterations: int,
        gen_iaca_markers: bool,
        perflib: ty.Optional[pyperfs.PerfMeasurementLibrary],
        debug: bool,
        tmp_dir: str,
        is_lib_tmp: bool,
    ) -> Benchmark_Lib:
        """
        Generate shared library with benchmark code.
          1. generate textual ASM code with benchmarks
          2. assembly ASM to an object file.
          3. link into a shared library.

        Args:
          gen_iaca_markers: Surround code for each benchmark with Intel IACA start/stop markers.
        """

        self.info("generate benchmark ASM code")

        os.makedirs(tmp_dir, exist_ok=True)
        if is_lib_tmp:
            fd, filename = tempfile.mkstemp(dir=tmp_dir, prefix="benchmarks.")
            os.close(fd)
            os.unlink(filename)
            asm_file = filename + ".s"
            obj_file = filename + ".o"
            lib_file = filename + ".so"
        else:
            asm_file = os.path.join(tmp_dir, "benchmarks.s")
            obj_file = os.path.join(tmp_dir, "benchmarks.o")
            lib_file = os.path.join(tmp_dir, "benchmarks.so")

        assert benchmark_specs

        benchmark_id = 0
        benchmark_functions = {}

        labels = _ASM_Label_Manager()

        with open(asm_file, "w") as file:
            # Use first dialect as default
            asm_writer = architecture.make_asm_writer(
                architecture.asm_dialects()[0], file
            )
            asm_writer.begin_file(asm_file)

            for i, benchmark in enumerate(benchmark_specs, 1):
                self.info(f"generate benchmark ASM code ({i}/{len(benchmark_specs)})")

                fn_name = "benchmark_kernel_" + str(benchmark_id)
                benchmark_id += 1

                asm = self._gen_benchmark_asm_function(
                    architecture,
                    labels,
                    fn_name,
                    benchmark,
                    num_iterations,
                    gen_iaca_markers=gen_iaca_markers,
                    perflib=perflib,
                    debug=debug,
                )

                for stmt in asm:
                    stmt.emit(asm_writer)

                benchmark_functions[benchmark] = fn_name

            asm = self._gen_benchmark_paramaters(
                architecture,
                num_iterations,
                perflib=perflib,
            )
            for stmt in asm:
                stmt.emit(asm_writer)

            asm_writer.end_file(asm_file)

        if self.no_obj:
            self.no_link = True
        else:
            self.info("assemble benchmark library")

            subprocess.check_call(
                [architecture.tools_config.as_cmd, asm_file, "-o", obj_file],
            )

            if not self.no_link:
                ld_args = [
                    architecture.tools_config.ld_cmd,
                    "-shared",
                    obj_file,
                ]
                if perflib is not None:
                    ld_args += self._ldflags_for_lib(architecture, perflib)
                ld_args += ["-o", lib_file]

                subprocess.check_call(ld_args)

        return Benchmark_Lib(
            asm_file=pathlib.Path(asm_file),
            object_file=pathlib.Path(obj_file) if not self.no_obj else None,
            lib_file=pathlib.Path(lib_file) if not self.no_link else None,
            benchmarks=benchmark_functions,
            unlink=is_lib_tmp,
        )

    def _is_cross_compile(self, architecture: ir.Architecture) -> bool:
        archs = dict(x86_64="x86", aarch64="ARMv8a")
        processor = platform.machine()
        arch = archs.get(processor)
        assert arch is not None, f"unexpected architecture"
        return arch != architecture.name

    def _ldflags_for_lib(
        self, architecture: ir.Architecture, perflib: pyperfs.PerfMeasurementLibrary
    ) -> ty.List[str]:
        if self._is_cross_compile(architecture):
            return []
        name = perflib.lib_name
        return [f"-l{name}"]

    # Assign and pre-fil with random values an array
    @staticmethod
    def aligned_alloc_byte_array(length, align: int, rand_values=False):
        buf = (ctypes.c_byte * (length + align - 1))()
        addr = ctypes.addressof(buf)
        offset = (align - (addr % align)) % align
        assert (addr + offset) % align == 0
        buf = (ctypes.c_byte * length).from_buffer(buf, offset)

        if rand_values:
            random.seed(42)
            for i in range(length):
                buf[i] = random.getrandbits(8)

        addr = ctypes.addressof(buf)
        assert addr % align == 0
        return buf

    @staticmethod
    def offset_byte_array(buf, offset, org_length):
        bufnew = (ctypes.c_byte * (org_length - offset)).from_buffer(buf, offset)
        return bufnew

    def _run_benchmark(
        self,
        *,
        arch: ir.Architecture,
        benchmark_index: int,
        num_benchmarks: int,
        perf_counters: ty.Optional[Perf_Counter_Spec],
        benchmark_lib: Benchmark_Lib,
        benchmark: Benchmark_Spec,
        num_iterations: int,
        num_warmup_iterations: int,
        keep_raw_data: bool = False,
        outlier_low: int = 0,
        outlier_high: int = 100,
    ) -> Benchmark_Run:
        def _total(arrays, keep_raw_data: bool = False):
            if arrays:
                array = numpy.hstack(arrays)
                return Statistics.from_array(array, keep_raw_data)
            else:
                return Statistics(mean=0, stddev=0)

        assert type(outlier_low) is int and 0 <= outlier_low <= 100
        assert type(outlier_high) is int and 0 <= outlier_high <= 100
        assert outlier_low < outlier_high

        total_cycles: ty.List[int] = []
        total_insts: ty.List[numpy.ndarray[ty.Any, numpy.dtype[ty.Any]]] = []
        total_fused_muops: ty.List[numpy.ndarray[ty.Any, numpy.dtype[ty.Any]]] = []
        total_unfused_muops: ty.List[numpy.ndarray[ty.Any, numpy.dtype[ty.Any]]] = []

        total_clean_IPC: ty.List[float] = []
        total_clean_fMPC: ty.List[float] = []
        total_clean_uMPC: ty.List[float] = []

        total_IPC: ty.List[float] = []
        total_fMPC: ty.List[float] = []
        total_uMPC: ty.List[float] = []
        all_measurements: ty.List[Event_Set_Run] = []

        runtime: int = 0

        timestamp_bgn = datetime.datetime.now()

        event_sets: ty.List[pyperfs.PerfEventSet]

        if perf_counters is not None:
            event_sets = list(perf_counters.event_sets)
        else:
            event_sets = [pyperfs.make_null_event_set()]

        for idx, evt_set in enumerate(event_sets, 1):
            self.info(
                f"BENCHMARK ({benchmark_index}/{num_benchmarks}):",
                repr(benchmark.name),
                f"N: {num_iterations:_},",
                f"unroll factor: {benchmark.unroll_factor:_},",
                f"kernel iterations: {benchmark.kernel_iterations:_}",
                "-",
                "EVENTS:",
                "(" + str(idx) + "/" + str(len(event_sets)) + ")",
                *evt_set.event_names,
                rprompt="[" + str(runtime) + "s]",
            )

            event_set_id = evt_set.id
            num_events = evt_set.num_events
            total_num_iterations = num_iterations + num_warmup_iterations

            if self._SSH_BENCHER is None:
                benchmark_fn = benchmark_lib.benchmark_function(benchmark)

                result_array: numpy.typing.NDArray = numpy.ndarray(
                    shape=[total_num_iterations, num_events],
                    dtype=ctypes.c_longlong,
                    order="C",
                )

                ## will be overwritten by benchmark (except if PAPI calls are disabled)
                result_array.fill(42)

                ## values used to initialise vector registers
                ## align on max_vector_size * 2, we are never too cautious
                ALIGNMENT = benchmark.arch.max_vector_size
                init_values = self.aligned_alloc_byte_array(
                    benchmark.arch.nb_vector_reg * benchmark.arch.max_vector_size,
                    ALIGNMENT,
                    rand_values=True,
                )
                load_area = self.aligned_alloc_byte_array(
                    allocator.Memory_Arena_Allocator.MAX_ARENA_SIZE,
                    ALIGNMENT,
                )
                store_area = self.offset_byte_array(
                    load_area,
                    allocator.Memory_Arena_Allocator.LD_TO_STORE_OFFSET,
                    allocator.Memory_Arena_Allocator.MAX_ARENA_SIZE,
                )

                time_before = time.perf_counter()

                ret = sgr.safe_call(
                    benchmark_fn,
                    [
                        event_set_id,
                        num_events,
                        result_array.ctypes.data_as(ctypes.POINTER(ctypes.c_longlong)),
                        init_values,
                        load_area,
                        store_area,
                    ],
                    raise_segfault,
                    [benchmark.instructions],
                )
                time_after = time.perf_counter()
                assert ret is not None

                runtime = int(time_after - time_before)

                if ret != 0:
                    if perf_counters is not None:
                        perf_counters.perflib._handle_error(ret)

                    raise RuntimeError(
                        "Benchmark kernel "
                        + benchmark.name
                        + " failed! "
                        + f"(return code: {ret})"
                    )

            else:
                time_before = time.perf_counter()
                results: ty.List[int] = []
                # TODO: passing the .o file seems a good tradeoff, .s or .so is generally 4-5x times larger
                # we may pass a zipped source also, to be evaluated
                assert benchmark_lib.object_file is not None
                ret = self._SSH_BENCHER.exec_bench(
                    benchmark_lib.object_file.as_posix(),
                    arch=arch.name,
                    nb_reps=num_iterations + num_warmup_iterations,
                    benchmark_index=benchmark_index
                    - 1,  # Actual benchmark index should start at 0
                    perf_lib="none"
                    if perf_counters is None
                    else perf_counters.perflib.lib_name,
                    events=evt_set.event_names,
                    results=results,
                )

                time_after = time.perf_counter()
                runtime = int(time_after - time_before)

                if ret != 0:
                    raise RuntimeError(
                        "Benchmark kernel "
                        + benchmark.name
                        + " failed! "
                        + f"(return code: {ret})"
                    )
                assert len(results) == total_num_iterations * num_events
                result_array = numpy.array(results).reshape(
                    total_num_iterations, num_events
                )

            if not perf_counters:
                continue

            # discard warmup iterations
            result_array = result_array[num_warmup_iterations:]

            def column_index(name):
                return evt_set.index_of(name)

            def column_by_name(name, array):
                return array[:, column_index(name)]

            assert len(result_array)

            # drop outlier values (below/above lo/hi percentiles)
            clk_index = column_index(perf_counters.cycle_counter())
            res: numpy.ndarray[ty.Any, numpy.dtype[ty.Any]] = result_array[:, clk_index]
            lo_p, hi_p = numpy.percentile(res, [outlier_low, outlier_high])
            result_array = result_array[(res >= lo_p) & (res <= hi_p)]

            cycles = column_by_name(perf_counters.cycle_counter(), result_array)

            total_cycles.append(cycles)
            overhead = benchmark.arch.loop_overhead(
                benchmark.kernel_iterations, self._num_prologue_inst
            )

            try:
                muops_idx = evt_set.index_of(perf_counters.unfused_uop_counter())
            except IndexError:
                unfused_muops = None
                uMPC = None
                clean_uMPC = None
            else:
                muops = result_array[:, muops_idx]
                # this assumes that the loop control muops
                # do not contend significantly with the benchmark kernel.
                clean_MPC = (muops - overhead.muops) / cycles
                MPC = muops / cycles

                total_fused_muops.append(muops)
                total_clean_fMPC.append(clean_MPC)
                total_uMPC.append(MPC)

                unfused_muops = Statistics.from_array(muops, keep_raw_data)
                uMPC = Statistics.from_array(MPC, keep_raw_data)
                clean_uMPC = Statistics.from_array(clean_MPC, keep_raw_data)

            try:
                muops_idx = evt_set.index_of(perf_counters.fused_uop_counter())
            except IndexError:
                fused_muops = None
                fMPC = None
                clean_fMPC = None
            else:
                muops = result_array[:, muops_idx]
                # this assumes that the loop control muops
                # do not contend significantly with the benchmark kernel.
                clean_MPC = (muops - overhead.muops) / cycles
                MPC = muops / cycles

                total_fused_muops.append(muops)
                total_clean_fMPC.append(clean_MPC)
                total_fMPC.append(MPC)

                fused_muops = Statistics.from_array(muops, keep_raw_data)
                fMPC = Statistics.from_array(MPC, keep_raw_data)
                clean_fMPC = Statistics.from_array(clean_MPC, keep_raw_data)

            try:
                insts_idx = evt_set.index_of(perf_counters.instruction_counter())
            except IndexError:
                instructions = None
                IPC = None
                clean_IPC = None
            else:
                insts = result_array[:, insts_idx]

                clean_IPC = (insts - overhead.instructions) / cycles

                IPC = insts / cycles

                total_insts.append(insts)
                total_clean_IPC.append(clean_IPC)
                total_IPC.append(IPC)

                instructions = Statistics.from_array(insts, keep_raw_data)
                IPC = Statistics.from_array(IPC, keep_raw_data)
                clean_IPC = Statistics.from_array(clean_IPC, keep_raw_data)

            measurement = Event_Set_Run(
                cycles=Statistics.from_array(cycles),
                unfused_muops=unfused_muops,
                uMPC=uMPC,
                clean_uMPC=clean_uMPC,
                fused_muops=fused_muops,
                fMPC=fMPC,
                clean_fMPC=clean_fMPC,
                instructions=instructions,
                IPC=IPC,
                clean_IPC=clean_IPC,
            )

            for port, evt in perf_counters.port_counters.items():
                if evt not in evt_set.event_names:
                    continue

                data = column_by_name(evt, result_array)

                mean_and_stddev = Statistics.from_array(data)

                assert port not in measurement.port_muops

                measurement.port_muops[port] = mean_and_stddev

            for evt in evt_set.event_names:
                if evt == perf_counters.fused_uop_counter():
                    continue
                if evt == perf_counters.instruction_counter():
                    continue
                if evt == perf_counters.cycle_counter():
                    continue
                if evt in perf_counters.port_counters.values():
                    continue

                assert type(evt) is str, evt
                measurement.misc[evt] = Statistics.from_array(
                    column_by_name(evt, result_array)
                )

            all_measurements.append(measurement)

        timestamp_end = datetime.datetime.now()

        return Benchmark_Run(
            benchmark=benchmark,
            timestamp=timestamp_bgn,
            runtime=timestamp_end - timestamp_bgn,
            cycles=_total(total_cycles, keep_raw_data),
            instructions=_total(total_insts, keep_raw_data),
            fused_muops=_total(total_fused_muops, keep_raw_data),
            unfused_muops=_total(total_unfused_muops, keep_raw_data),
            clean_IPC=_total(total_clean_IPC, keep_raw_data),
            clean_fMPC=_total(total_clean_fMPC, keep_raw_data),
            clean_uMPC=_total(total_clean_uMPC, keep_raw_data),
            IPC=_total(total_IPC, keep_raw_data),
            fMPC=_total(total_fMPC, keep_raw_data),
            uMPC=_total(total_uMPC, keep_raw_data),
            event_sets=all_measurements,
        )

    def _die(self, *msg):
        self.info.clear()
        print("error:", *msg, file=sys.stderr)
        exit(1)


class _ASM_Label_Manager:
    """
    Manages labels for a whole ASM file.
    Creates unique names for function local jump labels.
    """

    def __init__(self):
        self._labels = set()

    def make_label(self, name: str) -> ir.Label:
        """
        Get a new unique label
        (:name: will be altered until there is no other label clashing with it).
        """

        label_name = ".L"
        if not name.startswith("."):
            label_name += "."
        label_name += name

        if label_name not in self._labels:
            lbl = ir.Label(label_name)
            self._labels.add(label_name)
            return lbl

        suffix = 0

        while True:
            tmp = label_name + "." + str(suffix)

            if tmp not in self._labels:
                lbl = ir.Label(tmp)
                self._labels.add(tmp)
                return lbl

            suffix += 1

    def __iter__(self):
        for name in self._labels:
            yield ir.Label(name)

    def __contains__(self, label: ir.Label):
        assert type(label) is ir.Label
        return label.name in self._labels


class _ASM_Builder:
    """
    Simple helper wrapping Register_Liveness_Tracker, ASM_Writer, & IR_Builder
    providing a slightly more high-level interface.
    """

    class ASM_Statement(abc.ABC):
        """
        base class for all statement in an assembler program
        """

        @abc.abstractmethod
        def emit(self, asm: ASM_Writer):
            raise NotImplementedError()

    class ASM_Insts(ASM_Statement):
        def __init__(self, insts: ty.Sequence[ir.Instruction]):
            assert isinstance(insts, (list, tuple, itertools.islice)), insts  # type: ignore

            if isinstance(insts, itertools.islice):  # type: ignore
                insts = tuple(insts)

            for i in insts:
                for op in i.operands:
                    if op.is_virtual:
                        assert False, [i, op]

            self.insts = insts

        @abc.override
        def emit(self, asm: ASM_Writer):
            asm.insts(self.insts)

    class ASM_Comment(ASM_Statement):
        def __init__(self, *msg: str):
            self.msg = msg

        @abc.override
        def emit(self, asm: ASM_Writer):
            asm.comment(*self.msg)

    class ASM_Newline(ASM_Statement):
        """Empty line"""

        @abc.override
        def emit(self, asm: ASM_Writer):
            asm.newline()

    class ASM_Label(ASM_Statement):
        def __init__(self, label: ir.Label):
            self.label = label

        @abc.override
        def emit(self, asm: ASM_Writer):
            asm.emit_label(self.label)

    class ASM_Directive(ASM_Statement):
        """Abstract base class for all ASM directives"""

    class ASM_Align(ASM_Directive):
        """Abstract base class for all ASM directives"""

        @abc.override
        def emit(self, asm: ASM_Writer):
            asm.align()

    class ASM_Begin_Function(ASM_Directive):
        def __init__(self, name: str):
            self.name = name

        @abc.override
        def emit(self, asm: ASM_Writer):
            asm.begin_function(self.name)

    class ASM_End_Function(ASM_Directive):
        def __init__(self, name: str):
            self.name = name

        @abc.override
        def emit(self, asm: ASM_Writer):
            asm.end_function(self.name)

    class ASM_Global_Int_Values(ASM_Directive):
        def __init__(self, name: str, values: ty.List[int]):
            self.name = name
            self.values = values

        @abc.override
        def emit(self, asm: ASM_Writer):
            asm.global_int_values(self.name, self.values)

    def __init__(
        self,
        *,
        arch: ir.Architecture,
        alloc: ir.Register_Liveness_Tracker = None,
        irb: ir.IR_Builder = None,
        labels: _ASM_Label_Manager = None,
        memory_caracs: allocator.Mem_Access_Regs = None,
    ):
        self._arch = arch
        self._alloc = alloc or arch.make_register_allocator()
        self._irb = irb or arch.make_ir_builder()
        self._labels = labels or _ASM_Label_Manager()
        self._code: ty.List["_ASM_Builder.ASM_Statement"] = []
        self._scratch_regs_avail: ty.List[bool] = []
        self._scratch_regs_list: ty.List[ir.Register] = []
        self._memory_caracs = memory_caracs or allocator.Mem_Access_Regs()

    @property  # type:ignore
    def nb_inst(self):
        return sum(
            map(
                lambda i: len(i.insts) if isinstance(i, self.ASM_Insts) else 0,
                self._code,
            )
        )

    @property  # type:ignore
    def arch(self) -> ir.Architecture:
        return self._arch

    @property  # type:ignore
    def ir_builder(self) -> ir.IR_Builder:
        return self._irb

    ##### MISC HELPERS FOR EMITTING ASM STATEMENTS OUT OF ORDER

    def sub_builder(self) -> "_ASM_Builder":
        """
        Create a new ASM builder that shares its register allocator & labels with :self:.
        I.e. if self creates a label it will be visible to the new builder & vice versa.
        I.e. if self allocates/frees a register it will be visible to the new builder & vice versa.
        Code emitted by the new builder is not visible to :self: & vice versa.
        """

        return _ASM_Builder(
            arch=self.arch,
            alloc=self._alloc,
            irb=self._irb,
            labels=self._labels,
            memory_caracs=self._memory_caracs,
        )

    def take_code(self) -> ty.List[ASM_Statement]:
        """
        Take list of statements generated by this ASM builder.
        Afterwards the ASM builder will have no more code (as if it was freshly allocated)!
        """
        out = self._code
        self._code = []
        return out

    def splice_in_code(self, code: ty.List[ASM_Statement]):
        """
        Splice in code generated by another ASM builder.
        """
        self._code.extend(code)

    def emit_asm(self, writer: ASM_Writer):
        for stmt in self._code:
            stmt.emit(writer)

    ##### MISC ASM HELPERS

    def insts(self, insts: ty.Iterable[ir.Instruction]):
        self._code.append(self.ASM_Insts(list(insts)))

    def align(self):
        self._code.append(self.ASM_Align())

    def comment(self, *args):
        self._code.append(self.ASM_Comment(*args))

    def newline(self):
        self._code.append(self.ASM_Newline())

    def begin_function(self, name: str):
        self._code.append(self.ASM_Begin_Function(name))

    def end_function(self, name: str):
        self._code.append(self.ASM_End_Function(name))

    def global_int_values(self, name: str, values: ty.List[int]):
        self._code.append(self.ASM_Global_Int_Values(name, values))

    ##### LABEL MANAGEMENT

    def make_label(self, name: str) -> ir.Label:
        """
        Get a new unique label
        (:name: will be altered until there is no other label clashing with it).
        """

        return self._labels.make_label(name)

    def emit_label(self, label: ir.Label):
        """
        Emit assembly to define label :label:
        """
        assert label in self._labels, f"emitting undefined label: {label.name:r}"

        self._code.append(self.ASM_Label(label))

    def emit_iaca_start_marker(self):
        """
        Generate an Intel IACA benchmark kernel start marker.
        """

        assert self._arch.name == "x86", "IACA only supports X86"

        import pipedream.asm.x86 as x86

        self.newline()
        self.comment("IACA start marker")
        self.put_const_in_register(111, x86.EBX)
        self.insts([x86.Harness.IACA_START_STOP_NOP])
        self.newline()

    def emit_iaca_stop_marker(self):
        """
        Generate an Intel IACA benchmark kernel stop marker.
        """

        assert self._arch.name == "x86", "IACA only supports X86"

        import pipedream.asm.x86 as x86

        self.newline()
        self.comment("IACA stop marker")
        self.put_const_in_register(222, x86.EBX)
        self.insts([x86.Harness.IACA_START_STOP_NOP])
        self.newline()

    ##### SIMULATE HIGHER LEVEL CONTROL FLOW CONSTRUCTS

    @contextlib.contextmanager
    def loop(self, name: str = "loop"):
        head = self.make_label(name + ".loop.head")
        exit = self.make_label(name + ".loop.exit")

        loop = ir.Loop(name, head, exit)

        self.newline()
        self.comment("BGN LOOP", name)

        self.align()
        self.emit_label(head)

        yield loop

        self.comment("END LOOP", name)
        self.newline()

        self.align()
        self.emit_label(exit)

    @contextlib.contextmanager
    def counting_loop(self, name: str, loop_counter: ir.Register, num_iterations: int):
        if num_iterations <= 0:
            raise ValueError("num_iterations must be > 0")

        with self.with_register(loop_counter):
            self.comment("initialize loop counter")
            self.put_const_in_register(num_iterations, loop_counter)

            with self.loop(name) as loop:
                yield loop

                self.newline()
                self.comment("decrement loop counter & iterate")
                self.substract_one_from_reg_and_branch_if_not_zero(
                    loop_counter, loop.head
                )

    ##### EMIT INSTRUCTIONS

    def emit_loop_prologue(
        self,
        instructions: ty.Sequence[ir.Instruction],
        reg_values: ir.Register,
    ):
        """
        Allow backend to emit some initialization code for benchmark kernel.

        One example:
          Intel integer division (div/idiv) divides RDX:RAX by its argument.
          If the divisor is zero an exception is raised by the CPU.
          In a benchmark we perform a long sequence of divisions so we need to make sure
          none of the possible argument registers hold 0.
          One possible prologue thus stores 1 into all GPRs if the benchmark contains a division.
        """

        prologue = self._irb.emit_loop_prologue(
            instructions,
            list(self._alloc.iter_free_registers()),
            reg_values,
        )

        if prologue:
            self.newline()
            self.comment("** bgn loop prologue: set up values in registers")
            self.insts(prologue)
            self.comment("** end loop prologue")
            self.newline()

    def emit_benchmark_prologue(
        self,
        instructions: ty.Sequence[ir.Instruction],
    ):
        """
        Allow backend to emit some initialization code for benchmark kernel.

        One example:
          Intel integer division (div/idiv) divides RDX:RAX by its argument.
          If the divisor is zero an exception is raised by the CPU.
          In a benchmark we perform a long sequence of divisions so we need to make sure
          none of the possible argument registers hold 0.
          One possible prologue thus stores 1 into all GPRs if the benchmark contains a division.
        """

        prologue = self._irb.emit_benchmark_prologue(
            instructions,
            list(self._alloc.iter_free_registers()),
        )

        if prologue:
            self.newline()
            self.comment("** bgn bench prologue: set up values in registers")
            self.insts(prologue)
            self.comment("** end bench prologue")
            self.newline()

    def emit_benchmark_epilogue(self, instructions: ty.Sequence[ir.Instruction]):
        """
        Allow backend to emit some finalization code for benchmark kernel.
        """

        epilogue = self._irb.emit_benchmark_epilogue(
            instructions,
            list(self._alloc.iter_free_registers()),
        )

        if epilogue:
            self.newline()
            self.comment("** bgn bench epilogue")
            self.insts(epilogue)
            self.comment("** end bench epilogue")
            self.newline()

    def emit_loop_epilogue(
        self, instructions: ty.Sequence[ir.Instruction], reg_values: ir.Register
    ):
        """
        Allow backend to emit some finalization code for benchmark kernel.
        """

        epilogue = self._irb.emit_loop_epilogue(
            instructions,
            list(self._alloc.iter_free_registers()),
            reg_values,
        )

        if epilogue:
            self.newline()
            self.comment("** bgn loop epilogue")
            self.insts(epilogue)
            self.comment("** end loop epilogue")
            self.newline()

    def preallocate_benchmark(
        self, instructions: ty.Sequence[ir.Instruction]
    ) -> ty.Tuple[object, ty.List[ir.Instruction]]:
        return self._irb.preallocate_benchmark(self._alloc, instructions)

    def free_stolen_benchmark_registers(self, stolen_regs):
        return self._irb.free_stolen_benchmark_registers(self._alloc, stolen_regs)

    def copy_to(self, src: ir.Register, dst: ir.Register) -> ir.Register:
        """
        Emit code to copy value in :src: to :dst:.
        Takes :dst:
        Returns :dst:.
        """
        self._alloc.take(dst)
        self.insts(self._irb.emit_copy(src, dst))

        return dst

    def move_to(self, src: ir.Register, dst: ir.Register) -> ir.Register:
        """
        Emit code to move value in :src: to :dst:.
        Frees :src: and takes :dst:
        Returns :dst:.
        """
        if src.widest != dst.widest:
            self.copy_to(src, dst)
            self._alloc.free(src)

        return dst

    def spill_to_stack(self, *srcs: ir.Register):
        """
        Emit code to spill value in :src: on to the stack.
        Does not free :src::
        """
        for src in srcs:
            self.insts(self._irb.emit_push_to_stack(src))

    def reload_from_stack(self, dst: ir.Register):
        """
        Emit code to load spilled value from the stack into :dst:.
        :dst: must be taken.
        Return :dst:.
        """
        assert self._alloc.is_taken(dst), f"{dst} is not taken."
        self.insts(self._irb.emit_pop_from_stack(dst))
        return dst

    def push_to_stack(self, *srcs: ir.Register):
        """
        Emit code to spill value in :src: on to the stack.
        Frees :src::
        """
        self.spill_to_stack(*srcs)
        for src in srcs:
            self._alloc.free(src)

    def pop_from_stack(self, dst: ir.Register):
        """
        Emit code to load spilled value from the stack into :dst:.
        Takes & returns :dst:.
        """
        self._alloc.take(dst)
        return self.reload_from_stack(dst)

    def shrink_stack(self, size: int):
        """
        Shrink the stack by :size: bits
        """
        assert size % 8 == 0
        return self.insts(
            self._irb.emit_add_imm_to_reg(
                self.arch.register_set().stack_pointer_register(), size // 8
            )
        )

    def add_imm_to_reg(self, reg: ir.Register, imm: int):
        return self.insts(self._irb.emit_add_imm_to_reg(reg, imm))

    def sub_imm_to_reg(self, reg: ir.Register, imm: int):
        return self.insts(self._irb.emit_sub_imm_to_reg(reg, imm))

    @contextlib.contextmanager
    def with_spill(self, *regs: ir.Register):
        for reg in regs:
            self.push_to_stack(reg)
        yield
        for reg in reversed(regs):
            self.pop_from_stack(reg)

    def push_callee_saves(self):
        self.comment("spill callee save registers")
        for reg in self._arch.register_set().callee_save_registers():
            self.push_to_stack(reg)

    def pop_callee_saves(self):
        self.comment("load callee save registers from stack")
        for reg in reversed(self.arch.register_set().callee_save_registers()):
            self.pop_from_stack(reg)

    def put_const_in_register(self, const: int, reg: ir.Register):
        """
        Emit code to put constant int in register.
        """
        return self.insts(self._irb.emit_put_const_in_register(const, reg))

    def dump_stack_state(self) -> ty.Dict:
        """
        Dump information about the stack, usefull to maintain consistency accross
        labels
        """
        return self._irb.dump_stack_state()

    def load_stack_state(self, state: ty.Dict):
        """
        Load a previously saved stack state :state:
        """
        self._irb.load_stack_state(state)

    def mul_reg_with_const(self, reg: ir.Register, const: int) -> ir.Register:
        """
        Emit code to multiply contents of :reg: with signed integer constant :const:.
        Result is stored in :reg:.
        Returns :reg:
        """
        if self._arch.name == "x86":
            self.insts(self._irb.emit_mul_reg_const(reg, const))
        elif self._arch.name == "ARMv8a":
            const_reg = self.allocate_argument(4)
            self.insts(self._irb.emit_put_const_in_register(const, const_reg))
            self.insts(self._irb.emit_mul_reg_reg(reg, const_reg))
            self.free_reg(const_reg)
        else:
            raise Exception(f"Unknown arch: {self._arch.name}")
        return reg

    def add_registers(self, src: ir.Register, src_dst: ir.Register):
        """
        emit code to add two registers
        """
        self.insts(self._irb.emit_add_registers(src, src_dst))

    def substract_one_from_reg_and_branch_if_not_zero(
        self, loop_counter: ir.Register, dst: ir.Label
    ):
        """
        emit code to subtract one from contents of register and branch if contents drop to zero.
        """
        self.insts(
            self._irb.emit_substract_one_from_reg_and_branch_if_not_zero(
                loop_counter, dst
            )
        )

    def sequentialize_cpu(self):
        """
        Emit code to flush the CPU pipeline.
        """
        self.comment("sequentialize CPU")
        self.insts(self._irb.emit_sequentialize_cpu(self._alloc))

    def branch_if_not_zero(self, reg: ir.Register, dst: ir.Label):
        self.insts(self._irb.emit_branch_if_not_zero(reg, dst))

    def get_argument_register(self, idx: int) -> ir.Register:
        reg = self._irb.get_argument_register(idx)
        return reg

    def allocate_argument(self, idx: int) -> ir.Register:
        reg = self._irb.get_argument_register(idx)
        self._alloc.take(reg)
        return reg

    def call(self, fn: str, *args: ir.Register):
        """
        Emit code to call the function with name :fn: passing arguments :args: through registers.
        """

        dst = ir.Label(fn)

        for idx, arg in enumerate(args):
            reg = self._irb.get_argument_register(idx)
            if isinstance(arg, ir.Register):
                self.copy_to(arg, reg)
            elif isinstance(arg, int):
                self.put_const_in_register(arg, reg)

        self.insts(self._irb.emit_call(dst))

        for idx, arg in enumerate(args):
            reg = self._irb.get_argument_register(idx)
            if isinstance(arg, ir.Register) and reg.widest != arg.widest:
                self._alloc.free(reg)

        return self._irb.get_return_register()

    def return_(self, reg: ir.Register):
        """
        Emit code to return from the current function
        """

        self.insts(self._irb.emit_return(reg))

    ##### REGISTERS

    def iter_free_registers(self, with_ro: bool = False) -> ty.Iterable[ir.Register]:
        return self._alloc.iter_free_registers(with_ro=with_ro)

    def return_register(self) -> ir.Register:
        return self._irb.get_return_register()

    def get_next_scratch_register(self) -> ir.Register:
        for i, reg in enumerate(self._scratch_regs_list):
            if self._scratch_regs_avail[i]:
                self._scratch_regs_avail[i] = False
                return reg
        reg = self._irb.get_scratch_register(len(self._scratch_regs_list))
        self._scratch_regs_list.append(reg)
        self._scratch_regs_avail.append(False)
        return reg

    def free_scratch_register(self, reg):
        for i, reg_test in enumerate(self._scratch_regs_list):
            if reg_test == reg:
                self._scratch_regs_avail[i] = True
                self.free_reg(reg)
                return
        assert False

    @contextlib.contextmanager
    def with_register(self, reg: ir.Register):
        self._alloc.take(reg)
        yield reg
        self._alloc.free(reg)

    @contextlib.contextmanager
    def regs_in_stack(self, *regs: ir.Register):
        for reg in regs:
            self.push_to_stack(reg)
        yield
        for reg in reversed(regs):
            self.pop_from_stack(reg)

    @contextlib.contextmanager
    def mem_regs_in_stack(self):
        self.push_to_stack_mem_regs()
        yield
        self.pop_from_stack_mem_regs()

    def take_reg(self, reg: ir.Register) -> ir.Register:
        """
        Mark register as used.
        """
        self._alloc.take(reg)
        return reg

    def free_reg(self, *regs: ir.Register):
        """
        Mark register as free.
        """
        for reg in regs:
            self._alloc.free(reg)

    def free_mem_regs(self):
        for reg in self._memory_caracs.used_regs():
            self.free_reg(reg)

    def shrink_stack_by_mem_regs(self):
        tot_size = 0
        for reg in self._memory_caracs.used_regs():
            tot_size += reg.width
        self.shrink_stack(tot_size)

    # Store in :self.mem_caracs: the number of register needed in the kernel
    # to handle all types of memory accesses
    def analyse_mem_needs(self, instructions: ty.List[ir.Instruction]):
        need_pures = any(
            any(
                isinstance(o, ir.Memory_Operand)
                and (not isinstance(o, ir.Base_Displacement_Operand))
                and (not isinstance(o, ir.Base_Offset_Memory_Operand))
                for o in i.operands
            )
            for i in instructions
        )
        max_addr_offset = get_max_addr_offset(instructions)
        if max_addr_offset is not None:
            need_pures = need_pures or (
                max_addr_offset < allocator.Memory_Arena_Allocator.OFFSET_MEMORY
            )
        need_disps = any(
            any(
                (isinstance(o, ir.Base_Displacement_Operand))
                and (not isinstance(o, ir.Base_Offset_Memory_Operand))
                for o in i.operands
            )
            for i in instructions
        )
        need_offset_load = any(
            any(
                (not isinstance(o, ir.Base_Displacement_Operand))
                and (isinstance(o, ir.Base_Offset_Memory_Operand))
                and o.is_use
                for o in i.operands
            )
            for i in instructions
        )
        need_offset_store = any(
            any(
                (not isinstance(o, ir.Base_Displacement_Operand))
                and (isinstance(o, ir.Base_Offset_Memory_Operand))
                and o.is_def
                for o in i.operands
            )
            for i in instructions
        )

        # No choice, a register has to be used
        if need_pures or (need_disps and (not (need_offset_load or need_offset_store))):
            self._memory_caracs.pure_ld = self.ir_builder.select_memory_base_register(
                instructions,
                set(self.iter_free_registers()) - set(self._scratch_regs_list),
                64,
            )
            self._memory_caracs.pure_st = self.ir_builder.select_memory_base_register(
                instructions,
                set(self.iter_free_registers())
                - set(self._scratch_regs_list)
                - set(self._memory_caracs.used_regs()),
                64,
            )
        # One register is used as common base for all disp. (reg+imm) and offset
        # (reg+reg) operands. Depending if the inst. is a ld/st, different disp./offset
        # are used
        if need_offset_load or need_offset_store:
            self._memory_caracs.base_area = self.ir_builder.select_memory_base_register(
                instructions,
                set(self.iter_free_registers())
                - set(self._scratch_regs_list)
                - set(self._memory_caracs.used_regs()),
                64,
            )
            if need_offset_load:
                self._memory_caracs.reg_off_ld = (
                    self.ir_builder.select_memory_base_register(
                        instructions,
                        set(self.iter_free_registers())
                        - set(self._scratch_regs_list)
                        - set(self._memory_caracs.used_regs()),
                        64,
                    )
                )
            if need_offset_store:
                self._memory_caracs.reg_off_st = (
                    self.ir_builder.select_memory_base_register(
                        instructions,
                        set(self.iter_free_registers())
                        - set(self._scratch_regs_list)
                        - set(self._memory_caracs.used_regs()),
                        64,
                    )
                )

    def move_mem_reg(self, mem_load_area, mem_store_area, memory_allocator):
        self.newline()
        self.comment("initializing mem regs")
        self.push_to_stack(mem_store_area)
        self.push_to_stack(mem_load_area)
        if self._memory_caracs.pure_ld:
            self.comment("pure_ld    -> ", self._memory_caracs.pure_ld)
            self.pop_from_stack(self._memory_caracs.pure_ld)
            mem_load_area = self._memory_caracs.pure_ld
        else:
            self.shrink_stack(mem_load_area.width)

        if self._memory_caracs.pure_st:
            self.comment("pure_st    -> ", self._memory_caracs.pure_st)
            self.pop_from_stack(self._memory_caracs.pure_st)
            mem_store_area = self._memory_caracs.pure_st
        else:
            self.shrink_stack(mem_store_area.width)

        if self._memory_caracs.base_area:
            self.comment("base_area    -> ", self._memory_caracs.base_area)
            self.copy_to(mem_load_area, self._memory_caracs.base_area)
            self.sub_imm_to_reg(
                self._memory_caracs.base_area, memory_allocator.OFFSET_MEMORY
            )

            if self._memory_caracs.reg_off_ld:
                self.comment("reg_off_ld -> ", self._memory_caracs.reg_off_ld)
                self.take_reg(self._memory_caracs.reg_off_ld)
                self.put_const_in_register(
                    memory_allocator.OFFSET_MEMORY, self._memory_caracs.reg_off_ld
                )
                self._memory_caracs.cst_disp_reg_off_ld = memory_allocator.OFFSET_MEMORY
                self.comment(
                    "cst_disp_reg_off_ld -> ", self._memory_caracs.cst_disp_reg_off_ld
                )
            if self._memory_caracs.reg_off_st:
                self.comment("reg_off_st -> ", self._memory_caracs.reg_off_st)
                self.take_reg(self._memory_caracs.reg_off_st)
                self.put_const_in_register(
                    memory_allocator.OFFSET_MEMORY
                    + memory_allocator.LD_TO_STORE_OFFSET,
                    self._memory_caracs.reg_off_st,
                )
                self._memory_caracs.cst_disp_reg_off_st = (
                    memory_allocator.OFFSET_MEMORY + memory_allocator.LD_TO_STORE_OFFSET
                )
                self.comment(
                    "cst_disp_reg_off_ld -> ", self._memory_caracs.cst_disp_reg_off_st
                )
        if self._memory_caracs.pure_ld:
            self._memory_caracs.cst_disp_ld = 0
            self.comment("cst_disp_ld -> ", self._memory_caracs.cst_disp_ld)
        if self._memory_caracs.pure_st:
            self._memory_caracs.cst_disp_st = 0
            self.comment("cst_disp_st -> ", self._memory_caracs.cst_disp_st)

    def push_to_stack_mem_regs(self):
        for reg in self._memory_caracs.used_regs():
            self.push_to_stack(reg)

    def pop_from_stack_mem_regs(self):
        for reg in reversed(list(self._memory_caracs.used_regs())):
            self.pop_from_stack(reg)

    def clear_memory_areas(self, mem_load_area, mem_store_area, memory_allocator):
        self.newline()
        self.comment("clear mem_store_area")
        # Saving `mem_store_area` and `mem_store_area` as they may be overwritten by
        # the call
        with self.regs_in_stack(mem_store_area, mem_load_area):
            ## void *memset(void *s, int c, size_t n);
            self.call("memset", mem_store_area, 0, memory_allocator.store_alloc_size)

        self.newline()
        self.comment("clear mem_load_area")
        # saving `mem_load_area` and `mem_store_area`
        with self.regs_in_stack(mem_store_area, mem_load_area):
            self.call("memset", mem_load_area, 0, memory_allocator.load_alloc_size)
        self.newline()


def glob_instruction_tags(
    arch_or_instructions: ty.Union[ir.Architecture, ty.Iterable[ir.Instruction]],
    tags: ty.Iterable[str],
) -> ty.Iterable[ir.Instruction]:
    if isinstance(arch_or_instructions, ir.Architecture):
        arch = arch_or_instructions
        instructions = list(arch.instruction_set().benchmark_instructions())
    else:
        instructions = list(arch_or_instructions)
    out = set()

    for tag in tags:
        matcher = _Tag_Parser.parse(tag)

        out |= set(inst for inst in instructions if matcher.matches(inst.tags))

    return sorted(out, key=lambda i: i.name)  # type:ignore


class _Tag_Parser:
    """
    Parser for the tag matcher mini language

    tag_matcher ::= and_expr

    and_expr ::= or_expr
               | or_expr '&' and_expr

    or_expr ::= neg_expr
              | neg_expr '|' or_expr

    neg_expr ::= tag_expr
               | '!' tag_expr

    tag_expr ::= glob
               | '(' and_expr ')'
    """

    class Tag_Matcher(abc.ABC):
        @abc.abstractmethod
        def matches(self, tags: ty.Sequence[str]) -> bool: ...

    class Glob_Tag_Matcher(Tag_Matcher):
        def __init__(self, glob: str):
            self._glob = glob

        @abc.override
        def matches(self, tags: ty.Sequence[str]) -> bool:
            return bool(fnmatch.filter(tags, self._glob))

        def __repr__(self):
            return self._glob

    class Intersect_Tag_Matcher(Tag_Matcher):
        def __init__(self, *matchers: "_Tag_Parser.Tag_Matcher"):
            self._matchers = matchers

        @abc.override
        def matches(self, tags: ty.Sequence[str]) -> bool:
            out = True

            for matcher in self._matchers:
                out &= matcher.matches(tags)

            return out

        def __repr__(self):
            return "(" + " & ".join(map(str, self._matchers)) + ")"

    class Union_Tag_Matcher(Tag_Matcher):
        def __init__(self, *matchers: "_Tag_Parser.Tag_Matcher"):
            self._matchers = matchers

        @abc.override
        def matches(self, tags: ty.Sequence[str]) -> bool:
            out = False

            for matcher in self._matchers:
                out |= matcher.matches(tags)

            return out

        def __repr__(self):
            return "(" + " | ".join(map(str, self._matchers)) + ")"

    class Negation_Tag_Matcher(Tag_Matcher):
        def __init__(self, matcher: "_Tag_Parser.Tag_Matcher"):
            self._matcher = matcher

        @abc.override
        def matches(self, tags: ty.Sequence[str]) -> bool:
            return not self._matcher.matches(tags)

        def __repr__(self):
            return "!" + repr(self._matcher)

    class Tokens:
        def __init__(self, tokens):
            self._tokens = tokens

        def __iter__(self):
            return self._tokens

        def remaining(self):
            out = tuple(self._tokens)
            self._tokens = iter(out)
            return out

        def next(self):
            return next(self._tokens, None)

        def unnext(self, token):
            self._tokens = itertools.chain([token], self._tokens)

    @staticmethod
    def _tokenize(tag_spec: str) -> "_Tag_Parser.Tokens":
        def split(tag_spec: str):
            # split & drop whitespace
            for pre_token in tag_spec.split():
                assert pre_token

                # split by delimiter & keep delimiters
                for token in re.split(r"([!(|&)])", pre_token):
                    if token:
                        yield token

        return _Tag_Parser.Tokens(split(tag_spec))

    @classmethod
    def parse(clss, tag_spec: str) -> Tag_Matcher:
        tokens = clss._tokenize(tag_spec)
        expr = clss._parse_expr(tokens)

        tok = tokens.next()
        if tok is not None:
            clss._raise_err_bad_tok("end of input", tok)

        return expr

    @classmethod
    def _parse_expr(clss, tokens) -> Tag_Matcher:
        return clss._parse_and_expr(tokens)

    @classmethod
    def _parse_and_expr(clss, tokens) -> Tag_Matcher:
        left = clss._parse_or_expr(tokens)

        tok = tokens.next()

        if tok != "&":
            tokens.unnext(tok)
            return left

        right = clss._parse_and_expr(tokens)

        return clss.Intersect_Tag_Matcher(left, right)

    @classmethod
    def _parse_or_expr(clss, tokens) -> Tag_Matcher:
        left = clss._parse_neg_expr(tokens)

        tok = tokens.next()

        if tok != "|":
            tokens.unnext(tok)
            return left

        right = clss._parse_or_expr(tokens)

        return clss.Union_Tag_Matcher(left, right)

    @classmethod
    def _parse_neg_expr(clss, tokens) -> Tag_Matcher:
        tok = tokens.next()

        if tok == "!":
            expr = clss._parse_tag_expr(tokens)
            return clss.Negation_Tag_Matcher(expr)

        tokens.unnext(tok)
        return clss._parse_tag_expr(tokens)

    @classmethod
    def _parse_tag_expr(clss, tokens) -> Tag_Matcher:
        tok = tokens.next()

        if tok is None:
            raise ValueError(
                f'invalid tag spec: unexpected end of tag_spec (empty or missing ")")'
            )

        if tok == "(":
            expr = clss._parse_expr(tokens)
            tok = tokens.next()
            if tok != ")":
                clss._raise_err_bad_tok('")"', tok)
            return expr

        if re.match(r"[?*a-zA-Z0-9_-]+", tok):
            return clss.Glob_Tag_Matcher(tok)

        clss._raise_err_bad_tok('a tag or "("', tok)
        raise Exception()

    @classmethod
    def _raise_err_bad_tok(clss, want, have):
        assert want != have

        if want is None:
            want = "end of input"

        if have is None:
            have = "end of input"

        raise ValueError(f"invalid tag spec: expected {want!r}, got {have!r}")
