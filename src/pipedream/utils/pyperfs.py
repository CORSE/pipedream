"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import ctypes
import ctypes.util
from ctypes import c_int, c_char_p, POINTER, pointer
import numbers
import typing as ty
from contextlib import contextmanager

from pipedream.utils import nub
from pipedream.utils import abc

__all__ = [
    "PerfMeasurementLibrary",
    "PerfException",
    "PerfEventManager",
    "PerfEventSet",
    "EventSet",
    "make_null_event_set",
    "set_perf_lib",
    "get_perf_lib",
    "perf_lib_context",
]

T = ty.TypeVar("T")

_PERF_OK = 0  # PAPI_OK and PERFPIPEDREAM_SUCCESS
_PERF_NULL = -1  # PAPI_NULL and PERPIPEDREAM_NULL

_current_lib: ty.Optional["PerfMeasurementLibrary"] = None


class PerfException(Exception):
    pass


class PerfMeasurementLibrary(abc.ABC):
    def __init__(self):
        self.lib: ctypes.CDLL
        self.start_function_name: str
        self.stop_function_name: str
        self.lib_name: str
        self.perf_library_init: ty.Callable[Any, T]
        self.perf_shutdown: ty.Optional[ty.Callable[Any, T]] = None
        self.perf_is_initialized: ty.Callable[Any, T]
        self.perf_strerror: ty.Callable[Any, T]
        self.perf_event_name_to_code: ty.Callable[Any, T]
        self.perf_add_event: ty.Callable[Any, T]
        self.perf_create_eventset: ty.Callable[Any, T]
        self.perf_cleanup_eventset: ty.Callable[Any, T]
        self.perf_destroy_eventset: ty.Callable[Any, T]
        self.perf_query_event: ty.Callable[Any, T]

        lib = ctypes.cdll.LoadLibrary(ctypes.util.find_library(self.lib_name))
        self.lib = lib

    @abc.abstractmethod
    def init_lib(self) -> int:
        ...

    def __del__(self):
        if self.perf_shutdown is not None:
            self.perf_shutdown()

    def can_count_event(self, name: str):
        event_name_bytes = name.encode("utf-8")

        event_code = pointer(c_int(_PERF_NULL))

        # PAPI_even_name_to_code may use thread-local variables which are not
        # properly handled when using intricate mutilprocessing/threading
        # Initialising PAPI again seems to fix it.
        self.init_lib()
        ret = self.perf_event_name_to_code(c_char_p(event_name_bytes), event_code)
        if ret != _PERF_OK:
            return False

        ret = self.perf_query_event(event_code[0])
        if ret != _PERF_OK:
            return False

        return True

    @abc.override
    def make_event_manager(
        self,
        events: ty.Sequence[str],
        events_in_every_set: ty.Sequence[str] = (),
        no_op: bool = False,
    ) -> "PerfEventManager":
        """
        create event manager with as many event sets as necessary to measure
        all given events.
        Every set contains all events from `events_in_every_set` and at least one
        event from `events`.
        """

        events_in_every_set = list(nub(events_in_every_set))
        events = [e for e in nub(events) if e not in events_in_every_set]

        if not events:
            if not events_in_every_set:
                _die("tried to create empty event set")
            else:
                event_set = self._make_event_set(events_in_every_set, no_op)

                return PerfEventManager(self, [event_set])
        else:
            event_sets = []

            while events:
                ## create new event set
                event_set = self._make_event_set(events_in_every_set, no_op)

                ## add events to set until it is full
                added_one = False

                while events:
                    event_name = events[0]

                    try:
                        self._add_event_to_set(event_set, event_name, no_op)
                        added_one = True
                        events.pop(0)
                    except PerfException:
                        if added_one:
                            # event set is full (or there are conflicts)
                            break
                        elif not self._event_exists(event_name):
                            _die("Invalid event name", repr(event_name))
                        else:
                            # can't fit event next to common events
                            _die(
                                "cannot fit event",
                                repr(event_name),
                                "into set with common events:",
                                ", ".join(map(repr, events_in_every_set)),
                            )

                event_sets.append(event_set)

            return PerfEventManager(self, event_sets)

    def make_exact_event_sets(
        self, event_sets: ty.Sequence[ty.Sequence[str]], no_op: bool = False
    ) -> "PerfEventManager":
        """
        given a list of lists of event names create an event manager with
        exactly these event sets.
        Fail with a PerfException if PAPI/PerfPipedream cannot create exactly these event sets.
        """

        event_sets = [list(set) for set in event_sets]

        if not event_sets:
            _die("tried to create event manager with no event sets")

            for set in event_sets:
                _die("tried to create empty event set")

        built_event_sets = []

        for events in event_sets:
            event_set = self._make_event_set(events, no_op)

            built_event_sets.append(event_set)

        return PerfEventManager(self, built_event_sets)

    ## private parts

    def _make_event_set(
        self, events: ty.Sequence[str], no_op: bool = False
    ) -> "PerfEventSet":
        """
        create new, possibly empty, event set.
        """
        if no_op:
            py_event_set = PerfEventSet(self, -1, ())
            for event_name in events:
                self._add_event_to_set(py_event_set, event_name, no_op)
            return py_event_set

        c_event_set = pointer(c_int(_PERF_NULL))
        c_event_set[0] = c_int(_PERF_NULL)

        ret = self.perf_create_eventset(c_event_set)
        if ret != _PERF_OK:
            self._handle_error(ret, "could not allocate event set")

        ## FIXME: mypy (reasonably) thinks that POINTER(c_int).__getitem__ returns a c_int,
        ##        but actually it is a special case that returns an int.
        event_set_id: int = ty.cast(int, c_event_set[0])

        py_event_set = PerfEventSet(self, event_set_id, ())

        ## add events to set
        for event_name in events:
            self._add_event_to_set(py_event_set, event_name)

        return py_event_set

    def _event_exists(self, event_name: str) -> bool:
        event_name_bytes = event_name.encode("ascii")

        event_code = pointer(c_int(_PERF_NULL))

        ret = self.perf_event_name_to_code(c_char_p(event_name_bytes), event_code)
        return ret != _PERF_OK

    def _add_event_to_set(
        self, event_set: "PerfEventSet", event_name: str, no_op: bool = False
    ) -> "PerfEvent":
        if no_op:
            event = PerfEvent(event_name, -1)
            event_set.events += (event,)
            return event

        event_name_bytes = event_name.encode("ascii")

        event_code = pointer(c_int(_PERF_NULL))

        ret = self.perf_event_name_to_code(c_char_p(event_name_bytes), event_code)
        if ret != _PERF_OK:
            self._handle_error(ret, "could not find event", repr(event_name))

        ret = self.perf_add_event(event_set.id, event_code[0])
        if ret != _PERF_OK:
            self._handle_error(ret, "could not add event to set", repr(event_name))

        event = PerfEvent(event_name, event_code[0])

        event_set.events += (event,)

        return event

    ## FIXME: calculate arg types of returned callable from *argtypes*.
    def _get_fn(self, name: str, restype: ty.Type, *argtypes) -> ty.Callable[[], T]:
        fn = getattr(self.lib, name)
        assert fn is not None

        fn.restype = restype
        fn.argtypes = tuple(argtypes)
        return fn

    def _handle_error(self, err_code: int, *args):
        msg: ty.Sequence[ty.Any]

        if err_code:
            msg = [
                self.perf_strerror(err_code).decode("ascii")
                + " ("
                + str(err_code)
                + "): "
            ]
        else:
            msg = ()

        _die(*msg, *args)

    def __repr__(self):
        return type(self).__name__


class Papi(PerfMeasurementLibrary):
    def __init__(self):
        self.version = 0
        self.lib_name = "papi"
        super().__init__()

        if self.lib._name is None:
            raise ImportError("Could not find PAPI shared library")

        self.start_function_name = "PAPI_start"
        self.stop_function_name = "PAPI_stop"

        self.perf_library_init = self._get_fn("PAPI_library_init", c_int, c_int)
        self.perf_shutdown = self._get_fn("PAPI_shutdown", None)
        self.perf_is_initialized = self._get_fn("PAPI_is_initialized", c_int)
        self.perf_strerror = self._get_fn("PAPI_strerror", c_char_p, ctypes.c_int)
        self.perf_event_name_to_code = self._get_fn(
            "PAPI_event_name_to_code", c_int, c_char_p, POINTER(c_int)
        )
        self.perf_add_event = self._get_fn("PAPI_add_event", c_int, c_int, c_int)

        self.perf_create_eventset = self._get_fn(
            "PAPI_create_eventset", c_int, POINTER(c_int)
        )
        self.perf_cleanup_eventset = self._get_fn("PAPI_cleanup_eventset", c_int, c_int)
        self.perf_destroy_eventset = self._get_fn(
            "PAPI_destroy_eventset", c_int, POINTER(c_int)
        )

        self.perf_query_event = self._get_fn("PAPI_query_event", c_int, c_int)

        ret = self.init_lib()
        if ret < 0:
            min_major, min_minor = min(ALLOWED_VERSIONS)
            max_major, max_minor = max(ALLOWED_VERSIONS)

            _die(
                f"PAPI has unsupported version, want >={min_major}.{min_minor} <={max_major}.{max_minor}"
            )

    @abc.override
    def init_lib(self) -> int:
        PAPI_EINVAL = -1

        success = 0

        ALLOWED_VERSIONS = [
            *[(5, v) for v in range(4, 42)],
            *[(6, v) for v in range(0, 10)],
        ]

        for major_version, minor_version in ALLOWED_VERSIONS:
            version = _PapiVersion(major_version, minor_version)

            ret = self.perf_library_init(version.encode())

            if ret == PAPI_EINVAL:
                ## bad PAPI version, try another one
                continue

            if _PapiVersion.decode(ret) != version:
                _die("PAPI_library_init failed with", ret)

            self.version = version
            success = ret
            break
        return success

    def __repr__(self):
        return type(self).__name__ + "('" + str(self.version) + "')"


class PerfPipedream(PerfMeasurementLibrary):
    def __init__(self):
        self.lib_name = "perf-pipedream"
        super().__init__()

        if self.lib._name is None:
            raise ImportError("Could not find Perf-pipedream shared library")

        self.start_function_name = "perf_pipedream_start"
        self.stop_function_name = "perf_pipedream_stop"

        self.perf_library_init = self._get_fn(
            "perf_pipedream_library_init", c_int, c_int
        )
        self.perf_shutdown = self._get_fn("perf_pipedream_shutdown", None)

        self.perf_is_initialized = self._get_fn("perf_pipedream_is_initialized", c_int)
        self.perf_strerror = self._get_fn(
            "perf_pipedream_strerror", c_char_p, ctypes.c_int
        )
        self.perf_event_name_to_code = self._get_fn(
            "perf_pipedream_event_name_to_code", c_int, c_char_p, POINTER(c_int)
        )
        self.perf_add_event = self._get_fn(
            "perf_pipedream_add_event", c_int, c_int, c_int
        )

        self.perf_create_eventset = self._get_fn(
            "perf_pipedream_create_eventset", c_int, POINTER(c_int)
        )
        self.perf_cleanup_eventset = self._get_fn(
            "perf_pipedream_cleanup_eventset", c_int, c_int
        )
        self.perf_destroy_eventset = self._get_fn(
            "perf_pipedream_destroy_eventset", c_int, POINTER(c_int)
        )

        self.perf_query_event = self._get_fn("perf_pipedream_query_event", c_int, c_int)

        ret = self.init_lib()
        if ret < 0:
            _die(
                "Could not init PerfPipedream: "
                + self.perf_strerror(ret).decode("ascii")
            )

    @abc.override
    def init_lib(self) -> int:
        PERFPIPEDREAM_VERSION = 1
        ret = self.perf_library_init(PERFPIPEDREAM_VERSION)
        if ret != 0 and "already initialized" in self.perf_strerror(ret).decode(
            "ascii"
        ):
            return 0
        return ret


def make_null_event_set() -> "PerfEventSet":
    """
    Create a fake empty PerfEventSet object, corresponds to _PERF_NULL.
    It does not contain any events, you cannot add any events.
    """

    return PerfEventSet.make_null()


class PerfEventManager:
    def __init__(self, perflib, event_sets: ty.List["PerfEventSet"]):
        self._perflib = perflib
        self._event_sets = event_sets

    @property
    def event_sets(self) -> ty.Iterable["PerfEventSet"]:
        """
        Iterator event sets of this manager
        """

        return iter(self._event_sets)

    @property
    def num_event_sets(self) -> int:
        """
        Number of event sets of this manager
        """

        return len(self._event_sets)

    @property
    def event_names(self) -> ty.Iterable[str]:
        """
        Iterator over names of all events in all event sets of this manager.
        """

        for set in self.event_sets:
            yield from set.event_names

    def __repr__(self):
        return (
            type(self).__name__
            + "("
            + ", ".join(repr(list(set.event_names)) for set in self.event_sets)
            + ")"
        )


class PerfEventSet:
    def __init__(self, perflib, id: int, events: ty.Sequence["PerfEvent"]):
        self.perflib = perflib
        self.id = c_int(id)
        self.events = tuple(events)

    @classmethod
    def make_null(clss):
        return clss(perflib=None, id=0, events=())

    def __del__(self):
        if self.id.value < 0:
            return
        if self.perflib is not None:
            set_ptr = pointer(self.id)

            ret = self.perflib.perf_cleanup_eventset(self.id)
            if ret != _PERF_OK:
                self.perflib._handle_error(ret, "Could not destroy event set", self)

            ret = self.perflib.perf_destroy_eventset(set_ptr)
            if ret != _PERF_OK:
                self.perflib._handle_error(ret, "Could not destroy event set", self)

    def __iter__(self) -> ty.Iterator["PerfEvent"]:
        return iter(self.events)

    def all_present(self, events) -> ty.Iterable[str]:
        """
        returns sequence of event names present in both *self* and *events*
        (in the order they are given in *events*).
        """

        for evt in events:
            if evt in self.event_names:
                yield evt

    def index_of(self, event_name) -> int:
        for i, evt in enumerate(self):
            if evt.name == event_name:
                return i

        raise IndexError()

    @property
    def event_names(self):
        for event in self:
            yield event.name

    @property
    def num_events(self):
        return len(self.events)

    def __repr__(self):
        return (
            type(self).__name__
            + "("
            + str(self.id)
            + ", ["
            + ", ".join(evt.name for evt in self.events)
            + "])"
        )


class PerfEvent:
    def __init__(self, name, code):
        self.code = c_int(code)
        self.name = name

    def __repr__(self):
        return type(self).__name__ + "(" + repr(self.name) + ")"


class _PapiVersion:
    def __init__(self, major: int, minor: int, revision: int = 0, increment: int = 0):
        self.major = int(major)
        self.minor = int(minor)
        self.revision = int(revision)
        self.increment = int(increment)

    @staticmethod
    def decode(version):
        major = (version >> 24) & 0xFF
        minor = (version >> 16) & 0xFF
        revision = (version >> 8) & 0xFF
        increment = version & 0xFF

        return _PapiVersion(major, minor, revision, increment)

    def encode(self):
        return (
            (self.major << 24)
            | (self.minor << 16)
            | (self.revision << 8)
            | self.increment
        )

    def __eq__(self, that):
        if isinstance(that, _PapiVersion):
            return self.encode() == that.encode()

        if isinstance(that, numbers.Integral):
            return self.encode() == that

        return NotImplemented

    def __str__(self):
        return ".".join(
            map(str, [self.major, self.minor, self.increment, self.revision])
        )

    def __repr__(self):
        txt = type(self).__name__ + "("
        txt += (
            ", ".join(map(str, [self.major, self.minor, self.increment, self.revision]))
            + ")"
        )
        return txt


def set_perf_lib(lib_name: ty.Optional[str]) -> None:
    global _current_lib
    if lib_name is None:
        _current_lib = None
        return
    assert lib_name in ["auto", "papi", "perf-pipedream"]

    current_lib: ty.Optional[PerfMeasurementLibrary] = None
    if lib_name == "papi":
        try:
            current_lib = Papi()
        except:
            raise ImportError("Could not find PAPI library")
    elif lib_name == "perf-pipedream":
        try:
            current_lib = PerfPipedream()
        except:
            raise ImportError("Could not find PerfPipedream library")
    else:
        try:
            current_lib = Papi()
        except ImportError:
            try:
                current_lib = PerfPipedream()
            except ImportError:
                raise ImportError(
                    "Could not find neither PAPI nor PerfPipedream library"
                )
    assert current_lib is not None
    _current_lib = current_lib


def get_perf_lib() -> PerfMeasurementLibrary:
    global _current_lib
    if _current_lib is None:
        set_perf_lib("auto")
    assert _current_lib is not None
    return _current_lib


@contextmanager
def perf_lib_context(perf_lib: str = "auto"):
    global _current_lib
    # backup and set
    current = _current_lib
    set_perf_lib(perf_lib)
    try:
        yield _current_lib
    finally:
        # restore
        _current_lib = current


def _die(*args) -> ty.NoReturn:
    raise PerfException(" ".join(map(str, args)))
