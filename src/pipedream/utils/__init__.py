"""
Pipedream -- CPU architecture performance evaluation tool
Copyright (C) 2022  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import itertools
import typing as ty

__all__ = [
    "chunks",
    "nub",
    "Sentinel",
]

T = ty.TypeVar("T")


def chunks(seq: ty.Sequence[T], n: int) -> ty.Iterable[ty.Iterable[T]]:
    """
    Generate an iterable of chunks of length :n: from :seq:.
    The last chunk will have length :len(seq) % n:.
    """
    if n == 0:
        return

    it = iter(seq)
    while True:
        chunk = tuple(itertools.islice(it, n))
        if not chunk:
            break
        yield chunk


def nub(seq: ty.Iterable[T]) -> ty.Iterable[T]:
    """
    Filter duplicate elements from iterable.
    Elements in :seq: must be hashable.
    """

    seen = set()

    for elem in seq:
        if elem in seen:
            continue
        seen.add(elem)
        yield elem


def flatten(items: ty.Iterable[ty.Any]) -> ty.Iterable[ty.Any]:
    """
    Yield items from any nested iterable.
    Reference: https://stackoverflow.com/a/2158532/4531270
    """
    for x in items:
        if isinstance(x, ty.Iterable) and not isinstance(x, (str, bytes)):
            yield from x
        else:
            yield x


class Sentinel:
    """
    Simple helper class for making unique objects with a name attached.
    A sentinel is always equal ('==') to itself, but never to any other object
    (i.e. '==' behaves like 'is').
    """

    _instances: ty.ClassVar[ty.Dict[str, "Sentinel"]] = {}

    def __init__(self, name):
        if type(name) is not str:
            raise TypeError(
                "Sentinel name must be a string, not " + repr(type(name).__name__)
            )

        self.__name = name

    def __repr__(self):
        return "<" + self.__name + ">"

    def __new__(clss, name):
        try:
            return clss._instances[name]
        except KeyError:
            obj = super().__new__(clss)
            clss._instances[name] = obj
            return obj
