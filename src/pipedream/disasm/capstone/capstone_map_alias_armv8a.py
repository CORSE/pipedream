from typing import *

map: Dict[str, str] = dict()

op_aliases = dict(
    IMM=[""],
    SFT=[""],
    EXT=[""],
    W=["W_IMM", "W_SFT", "W_EXT", "WSP"],
    X=["X_IMM", "X_SFT", "X_EXT", "XSP", "SP"],
    SP=["X", "XSP"],
    XSP=["X", "SP"],
    WSP=["W"],
)
