import logging
import capstone as cs
from typing import *
from dataclasses import dataclass
from ...asm.ir import Architecture
from .utils import cs_insn_class

__all__ = [
    "DisasOp",
    "Disassembler",
]

logger = logging.getLogger(__name__)


@dataclass
class DisasOp:
    cs_insn: Optional[cs.CsInsn]
    offset: int
    binary: bytes
    insn_class: str

    @property
    def disasm(self) -> str:
        return (
            f"{self.cs_insn.mnemonic} {self.cs_insn.op_str}".strip()
            if self.cs_insn is not None
            else ""
        )


class Disassembler:
    _machines_map = dict(
        ARMv8a=(cs.CS_ARCH_ARM64, cs.CS_MODE_ARM | cs.CS_MODE_LITTLE_ENDIAN),
        x86=(cs.CS_ARCH_X86, cs.CS_MODE_64),
    )

    _inst_size_map = dict(ARMv8a=4)  # undefined for variable length architectures

    def __init__(self, arch: Architecture) -> None:
        cs_arch_mode = self._machines_map.get(arch.name)
        if cs_arch_mode is None:
            raise Exception(f"architecture not supported for {arch.name}")
        cap = cs.Cs(*cs_arch_mode)
        cap.detail = True
        self.cs = cap
        self.inst_size = self._inst_size_map.get(arch.name, 0)
        self.arch = arch

    def gen_binary_dis(
        self, binary: bytes, skip: bool = False, addr: int = 0
    ) -> Generator[DisasOp, None, None]:
        offset = 0
        size = len(binary)
        while offset < size:
            insts = [x for x in self.cs.disasm(binary[offset:], addr + offset, 1)]
            if len(insts) == 0:
                if skip and self.inst_size > 0:
                    yield DisasOp(
                        None, offset, binary[offset : offset + self.inst_size], ""
                    )
                    offset += self.inst_size
                else:
                    break
            else:
                for inst in insts:
                    insn_class = cs_insn_class(inst)
                    yield DisasOp(
                        inst, offset, binary[offset : offset + inst.size], insn_class
                    )
                    offset += inst.size

    def get_binary_dis(
        self, binary: bytes, skip: bool = False, addr: int = 0
    ) -> List[DisasOp]:
        return [op for op in self.gen_binary_dis(binary, skip)]
