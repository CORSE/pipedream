import logging
import re
from typing import *
import capstone as cs

__all__ = [
    "cs_insn_class",
    "cs_insn_class_pipedream",
]

logger = logging.getLogger(__name__)


def _op_class_arm64(insn: cs.CsInsn, op: cs.arm64.Arm64Op) -> List[str]:

    def imm_cls(imm: int) -> str:
        cls_str = "imm"
        return cls_str

    def reg_cls(regname: str) -> str:
        # When not a numeric register name, keep register alias name
        if len(regname) > 1 and regname[1] not in "0123456789":
            return regname
        return regname[0]

    if op.type == cs.CS_OP_IMM:
        cls_str = [imm_cls(op.imm)]
    elif op.type == cs.CS_OP_FP:
        cls_str = ["fp"]
    elif op.type == cs.CS_OP_REG:
        vas_str = {
            0: "",
            1: "8b",
            2: "16b",
            3: "4h",
            4: "8h",
            5: "2s",
            6: "4s",
            7: "1d",
            8: "2d",
            9: "1q",
        }[op.vas]
        cls = reg_cls(insn._cs.reg_name(op.reg))
        vess_str = {0: "", 1: "b", 2: "h", 3: "s", 4: "d"}[op.vess]
        cls = reg_cls(insn._cs.reg_name(op.reg))
        cls_str = [f"{cls}{vas_str}{vess_str}"]
    elif op.type == cs.CS_OP_MEM:
        cls_base = reg_cls(insn._cs.reg_name(op.mem.base))
        cls_index = ("" if op.mem.index == 0 else reg_cls(
            insn._cs.reg_name(op.mem.index)))
        cls_disp = "" if op.mem.disp == 0 else imm_cls(op.mem.disp)
        cls_str = [c for c in [cls_base, cls_index, cls_disp] if c]
    elif op.type == cs.arm64_const.ARM64_OP_PREFETCH:
        cls_str = ["pft"]
    else:
        assert (
            0
        ), f"TODO: unexpected operand type: {op.type} for insn: {insn.mnemonic} {insn.op_str}"
    if op.shift.type != 0:
        cls_str += ["sft"]
    if op.ext != 0:
        cls_str += ["ext"]
    return cls_str


def _insn_ops_classes_arm64(insn: cs.CsInsn) -> List[str]:
    ops_classes = []
    for cs_op in insn.operands:
        ops_classes += _op_class_arm64(insn, cs_op)
    if insn.cc != 0:
        ops_classes.append("cond")
    return ops_classes


_insn_fixup_arm64_re = None


def _insn_fixup_arm64(insn: cs.CsInsn) -> None:
    global _insn_fixup_arm64_re
    if _insn_fixup_arm64_re is None:
        _insn_fixup_arm64_re = re.compile(r"v\d+(?P<vas>\.\d+\w)?$")
    cs_str = insn.op_str
    for c in ["[", "]"]:
        cs_str = cs_str.replace(c, "")
    cs_ops = cs_str.split(", ") if cs_str else []
    mnemo = insn.mnemonic
    # Capstone do not fill correctly vas information for some opcodes,
    # may need update for other opcodes/cases
    if mnemo in ["mov", "mvn"]:
        eltsiz = {
            0: "",
            1: ".8b",
            2: ".16b",
            3: ".4h",
            4: ".8h",
            5: ".2s",
            6: ".4s",
            7: ".1d",
            8: ".2d",
            9: ".1q",
        }
        vas_map = {v: k for k, v in eltsiz.items()}
        re_vas = re.compile(r"v\d+(?P<vas>\.\d+\w)?$")
        for i, op in enumerate(cs_ops):
            m = re_vas.match(op)
            if m is not None:
                vas_str = m["vas"]
                insn.operands[i].vas = vas_map.get(vas_str, 0)


def cs_insn_ops_classes(insn: cs.CsInsn) -> List[str]:
    if insn._cs.arch == cs.CS_ARCH_ARM64:
        _insn_fixup_arm64(insn)
        ops_classes = _insn_ops_classes_arm64(insn)
    else:
        logger.warning(
            f"architecture not implemented {cs.CS_ARCH[insn._cs.arch]}, instruction class retricted to mnemonic name"
        )
        ops_classes = []
    return [x.upper() for x in ops_classes]


def cs_insn_class(insn: cs.CsInsn) -> str:
    ops_classes = cs_insn_ops_classes(insn)
    insn_class = "_".join([insn.mnemonic.upper()] + ops_classes)
    return insn_class


def cs_insn_class_pipedream(insn: cs.CsInsn) -> str:
    if insn._cs.arch == cs.CS_ARCH_ARM64:
        from .capstone_map_armv8a import map as capstone_map_armv8a
        from .capstone_map_alias_armv8a import op_aliases as op_aliases

        ops_cls = cs_insn_ops_classes(insn)
        insn_cls = "_".join([insn.mnemonic.upper()] + ops_cls)
        pipedream_cls = capstone_map_armv8a.get(insn_cls, [""])[0]
        if pipedream_cls:
            return pipedream_cls
        for i, op in enumerate(ops_cls):
            aliases = op_aliases.get(op, [])
            for alias in aliases:
                alias_ops_cls = [
                    x for x in ops_cls[:i] + [alias] + ops_cls[i + 1:] if x
                ]
                insn_cls = "_".join([insn.mnemonic.upper()] + alias_ops_cls)
                pipedream_cls = capstone_map_armv8a.get(insn_cls, [""])[0]
                if pipedream_cls:
                    return pipedream_cls
    else:
        logger.warning(
            f"architecture not implemented {cs.CS_ARCH[insn._cs.arch]}, instruction class mapping not available"
        )
        pipedream_cls = ""
    return pipedream_cls
