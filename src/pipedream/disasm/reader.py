import logging
from dataclasses import dataclass
from enum import Enum
from typing import *
from elftools.elf.elffile import ELFFile
from elftools.elf.sections import SymbolTableSection, Symbol, Section
from io import IOBase, TextIOBase, FileIO

__all__ = [
    "SymType",
    "ObjSymbol",
    "ObjReader",
]

logger = logging.getLogger(__name__)


class SymType(Enum):
    UNDEF = 0
    SECTION = 1
    FUNC = 2
    OBJ = 3


@dataclass
class ObjSymbol:
    name: str
    type: SymType
    addr: int
    size: int
    binary: Optional[bytes]


class ObjReader:
    def __init__(self, exe_io: FileIO) -> None:
        self.exe_io = exe_io
        self.exe_name = str(exe_io.name) if hasattr(exe_io, "name") else "<undef>"
        self.elffile = ELFFile(self.exe_io)

    def _get_section(self, name_or_shndx: Union[str, int]) -> Section:
        if isinstance(name_or_shndx, str):
            section = self.elffile.get_section_by_name(name_or_shndx)
            if section is None:
                raise Exception(
                    f"no section with name {name_or_shndx} found in {self.exe_name}"
                )
        else:
            section = self.elffile.get_section(name_or_shndx)
            if section is None:
                raise Exception(
                    f"no section with idx {name_or_shndx} found in {self.exe_name}"
                )
        return section

    def _get_symtab(self) -> SymbolTableSection:
        symtab = self.elffile.get_section_by_name(".symtab")
        if symtab is None or not isinstance(symtab, SymbolTableSection):
            raise Exception(
                f"no symbol table found in {self.exe_name}, unable to analyse stripped program"
            )
        return symtab

    def _get_symbols(self, filter: Callable[[Symbol], bool] = None) -> List[Symbol]:
        symtab = self._get_symtab()
        return [
            s
            for s in [symtab.get_symbol(i) for i in range(symtab.num_symbols())]
            if filter is None or filter(s)
        ]

    def _get_functions(self) -> List[Symbol]:
        symtab = self._get_symtab()
        return [s for s in self._get_symbols() if s.entry.st_info.type == "STT_FUNC"]

    def _get_function_symbol(self, name: str) -> Symbol:
        def filter(sym: Symbol) -> bool:
            return sym.name == name and sym.entry.st_info.type == "STT_FUNC"

        symbols = self._get_symbols(filter)
        if len(symbols) > 1:
            raise Exception(f"multiple symbols found for symbol name: {name}")
        if len(symbols) == 0:
            raise Exception(f"no symbol found for symbol name: {name}")
        return symbols[0]

    def _get_section_symbol(self, shndx: int) -> Symbol:
        def filter(sym: Symbol) -> bool:
            return (
                sym.entry.st_shndx == shndx and sym.entry.st_info.type == "STT_SECTION"
            )

        symbols = self._get_symbols(filter)
        return symbols[0]

    def _get_function_obj(self, sym: Symbol) -> ObjSymbol:
        shndx, addr, size = sym.entry.st_shndx, sym.entry.st_value, sym.entry.st_size
        section_addr = self._get_section_symbol(shndx).entry.st_value
        section_offset = self._get_section(shndx).header.sh_offset
        file_addr = addr - section_addr + section_offset
        self.exe_io.seek(file_addr)
        binary = self.exe_io.read(size)
        return ObjSymbol(sym.name, SymType.FUNC, addr, size, binary)

    def get_function(self, func: str) -> ObjSymbol:
        sym = self._get_function_symbol(func)
        return self._get_function_obj(sym)

    def gen_functions(self) -> Generator[ObjSymbol, None, None]:
        for f in self._get_functions():
            yield self._get_function_obj(f)

    def get_functions(self) -> List[ObjSymbol]:
        return [o for o in self.gen_functions()]
