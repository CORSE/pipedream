import os
import shutil


def test_armv8_registers_list():
    from pipedream.asm.armv8a.registers import _ALL_REGISTERS_ as armv8a_regs

    dirname = os.path.dirname(__file__)
    fname = "list-registers-armv8a.csv"
    ref_fname = f"{dirname}/refs/{fname}"
    regs, ref_regs = {}, {}
    with open(fname, "w") as f:
        f.write("reg;subs;supers;aliases;as_width;widest\n")
        for reg in armv8a_regs:
            subs, supers, aliases = [
                sorted(x)
                for x in [
                    [x.name for x in reg._subs],
                    [x.name for x in reg._supers],
                    [x.name for x in reg._aliases],
                ]
            ]
            as_width = {k: r.name for k, r in sorted(reg._as_width.items())}
            widest = reg.widest.name
            regs[reg.name] = (subs, supers, aliases, as_width, widest)
            f.write(f"'{reg.name}';{subs};{supers};{aliases};{as_width};'{widest}'\n")

    if os.environ.get("TEST_UPDATE_REF"):
        os.makedirs(os.path.dirname(ref_fname), exist_ok=True)
        shutil.copy(fname, ref_fname)

    with open(ref_fname) as f:
        for l in [x.strip() for x in f.readlines()][1:]:
            reg, subs, supers, aliases, as_width, widest = [
                eval(x) for x in l.split(";")
            ]
            ref_regs[reg] = (subs, supers, aliases, as_width, widest)
    for reg in ref_regs:
        assert reg in regs, f"register {reg} missing"
        assert ref_regs[reg][0] == regs[reg][0], f"subs registers mismatch for {reg}"
        assert ref_regs[reg][1] == regs[reg][1], f"supers registers mismatch for {reg}"
        assert ref_regs[reg][2] == regs[reg][2], f"aliases registers mismatch for {reg}"
        assert ref_regs[reg][3] == regs[reg][3], f"as_width mismatch for {reg}"
        assert ref_regs[reg][4] == regs[reg][4], f"widest mismatch for {reg}"

    for reg in regs:
        assert reg in ref_regs, f"register {reg} missing in references"
