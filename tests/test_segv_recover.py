import pytest
import segv_recover as sgr
from pipedream.benchmark import SegmentationFault


def raise_test_exp():
    raise SegmentationFault()


def test_segv_recover_return_fail():
    ret = sgr.safe_call(sgr.do_segfault, (), lambda: 42, ())
    assert ret == 42


def test_segv_recover():
    with pytest.raises(SegmentationFault):
        sgr.safe_call(sgr.do_segfault, (), raise_test_exp, ())


def test_segv_recover_return_nofail():
    ret = sgr.safe_call(lambda: 39, (), lambda: 42, ())
    assert ret == 39
