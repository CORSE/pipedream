import pytest
from pssh.clients import SSHClient
from gevent import Timeout as GTimeout
import traceback
import time

from ssh_config import *


class Timeout(Exception):
    pass


def unstable(fun):
    return pytest.mark.skipif(True, reason="unstable implementation")(fun)


def run_remote(ssh_client, cmd, timeout=None):
    out = ssh_client.run_command(cmd, shell="bash -c", read_timeout=timeout)
    stdout = "\n".join(out.stdout)
    stderr = "\n".join(out.stderr)
    exit_code = out.exit_code
    ssh_client.close_channel(out.channel)
    return exit_code, stdout, stderr


@unstable
def test_ssh():
    ssh_client = SSHClient(host=HOST, port=PORT, user=USER, pkey=KEYLOCATION)
    exit_code, stdout, stderr = run_remote(
        ssh_client, "echo Hello Stdout && echo Hello Stderr >&2"
    )
    assert exit_code == 0, f"Unexpected exit code {exit_code}"
    assert stdout == "Hello Stdout"
    assert stderr == "Hello Stderr"
    exit_code, stdout, stderr = run_remote(
        ssh_client, "echo Hello Stdout && echo Hello Stderr >&2"
    )
    assert exit_code == 0, f"Unexpected exit code {exit_code}"
    assert stdout == "Hello Stdout"
    assert stderr == "Hello Stderr"
    ssh_client.disconnect()


def run_thread(tid, in_queue, out_queue, catch=True, ssh_client=None):
    if catch:
        ssh_client = None
        try:
            print(f"TID {tid}: start SSHClient")
            with GTimeout(
                seconds=3, exception=Timeout(f"Timeout connecting thread {tid}")
            ):
                ssh_client = SSHClient(host=HOST, port=PORT)  # user=USER, keylocation
                print(f"TID {tid}: done SSHClient")
                run_thread(tid, in_queue, out_queue, catch=False, ssh_client=ssh_client)
        except Exception as e:
            tb = traceback.format_exc()
            out_queue.put((tid, "ERR", f"{tb}: Exception {e}"))
        if ssh_client is not None:
            ssh_client.disconnect()
            del ssh_client
            out_queue.put((tid, "END", None))
        return
    while True:
        typ, cmd = in_queue.get()
        print(f"Tid: {typ} {cmd}")
        if typ == "END":
            break
        elif typ == "CMD":
            with GTimeout(
                seconds=3,
                exception=Timeout(f"Timeout executing command thread {tid}: {cmd}"),
            ):
                exit_code, stdout, stderr = run_remote(ssh_client, cmd, timeout=3)
            out_queue.put((tid, "RES", (exit_code, stdout, stderr)))


@unstable
def test_mp_ssh():
    from multiprocessing import Process as Thread, Queue, set_start_method

    # set_start_method('forkserver')

    n_threads = 4  # not robust above 4 processes, dead lock in libssh2
    out_queue = Queue()
    threads, in_queues = [], []
    for n in range(n_threads):
        in_queue = Queue()
        thread = Thread(target=run_thread, args=(n, in_queue, out_queue))
        thread.start()
        threads.append(thread)
        in_queues.append(in_queue)

    for n in range(n_threads):
        in_queues[n].put(("CMD", f"echo Hello From {n}"))
        in_queues[n].put(("END", None))

    running = n_threads
    failed = 0
    while running > 0:
        tid, typ, result = out_queue.get()
        if typ == "END":
            running -= 1
        elif typ == "ERR":
            print(f"ERROR {tid}: {result}")
            failed += 1
        elif typ == "RES":
            exit_code, stdout, stderr = result
            if exit_code != 0:
                print(f"ERROR: Unexpected exit_code from thread {tid}, err: '{stderr}'")
                failed += 1
            elif stdout != f"Hello From {tid}":
                print(f"ERROR: Unexpected output from thread {tid}: '{stdout}'")
                failed += 1

    for n in range(n_threads):
        threads[n].join()

    assert failed == 0, f"some threads failed: {failed}/{n_threads}"


if __name__ == "__main__":
    test_ssh()
