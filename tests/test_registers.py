def test_armv8_registers():
    from pipedream.asm.armv8a.registers import _ALL_REGISTERS_ as armv8a_regs
    from pipedream.asm.armv8a.registers import r_LR

    reg_names = {x.name: x for x in armv8a_regs}
    reg_names["r_LR"] = r_LR
    for i in range(31):
        assert f"W{i}" in reg_names
        assert f"X{i}" in reg_names
        assert [f"W{i}"] == [x.name for x in reg_names[f"X{i}"].sub_registers]
        assert [f"X{i}"] == [x.name for x in reg_names[f"W{i}"].super_registers]
        assert [f"X{i}"] == [x.name for x in reg_names[f"W{i}"].aliases]
        assert [f"W{i}"] == [x.name for x in reg_names[f"X{i}"].aliases]
        assert f"X{i}" == reg_names[f"X{i}"].widest.name
        assert f"X{i}" == reg_names[f"W{i}"].widest.name
        assert f"W{i}" == reg_names[f"W{i}"].as_width(32).name
        assert f"X{i}" == reg_names[f"W{i}"].as_width(64).name
        assert f"X{i}" == reg_names[f"X{i}"].as_width(64).name
    assert reg_names["r_LR"] is reg_names["X30"]
    assert "X31" not in reg_names
    assert "W31" not in reg_names


def test_armv8_simd_registers():
    from pipedream.asm.armv8a.registers import _ALL_REGISTERS_ as armv8a_regs

    reg_names = {x.name: x for x in armv8a_regs}
    z_bytes = reg_names["Z0"].width // 8
    q_bytes = reg_names["Q0"].width // 8
    for i in range(32):
        for k, s in enumerate("BHSDQZ"):
            assert f"{s}{i}" in reg_names  # Scalar access
            assert f"Z{i}" == reg_names[f"{s}{i}"].widest.name
            assert f"Z{i}" == reg_names[f"{s}{i}"].as_width(z_bytes * 8).name
            assert (
                s in ["Q", "Z"] or f"V{i}.{s}[0]" in reg_names
            )  # Single vector element access
            assert (
                s in ["Z"] or f"Z{i}.{s}[0]" in reg_names
            )  # Single vector element access
            if s != "Z":
                elt_bytes = 2**k
                num = q_bytes // elt_bytes
                assert f"V{i}.{num}{s}" in reg_names  # Vector access
                assert f"Z{i}.{s}" in reg_names  # Z Vector access
        for k in range(4):
            assert f"V{i}.4B[{k}]" in reg_names  # Int8MM 8-bytes elements access
        assert [] == [x.name for x in reg_names[f"B{i}"].sub_registers]
        assert [f"B{i}"] == [x.name for x in reg_names[f"H{i}"].sub_registers]
        assert [f"B{i}", f"H{i}"] == sorted(
            [x.name for x in reg_names[f"S{i}"].sub_registers]
        )
        assert [f"B{i}", f"H{i}", f"S{i}"] == sorted(
            [x.name for x in reg_names[f"D{i}"].sub_registers]
        )
        assert [f"B{i}", f"D{i}", f"H{i}", f"S{i}"] == sorted(
            [x.name for x in reg_names[f"Q{i}"].sub_registers]
        )
        assert [f"D{i}", f"H{i}", f"Q{i}", f"S{i}", f"Z{i}"] == sorted(
            [x.name for x in reg_names[f"B{i}"].super_registers]
        )
        assert f"D{i}" == reg_names[f"B{i}"].as_width(64).name
        assert f"Q{i}" == reg_names[f"H{i}"].as_width(128).name
        assert sorted(
            [
                f"V{i}.16B",
                f"V{i}.8H",
                f"V{i}.4S",
                f"V{i}.2D",
                f"V{i}.8B",
                f"V{i}.4H",
                f"V{i}.2H",
                f"V{i}.2S",
                f"V{i}.1D",
            ]
        ) == sorted([x.name for x in reg_names[f"V{i}.1Q"].sub_registers])
        assert sorted(
            [f"V{i}.B[1]", f"V{i}.B[0]", f"Z{i}.B[1]", f"Z{i}.B[0]"]
        ) == sorted([x.name for x in reg_names[f"V{i}.H[0]"].sub_registers])
        for elt in range(4):
            assert sorted(
                [
                    x.name
                    for x in reg_names[f"V{i}.4B[{elt}]"]._aliases
                    if x.name != f"V{i}.S[{elt}]"
                ]
            ) == sorted(
                [
                    x.name
                    for x in reg_names[f"V{i}.S[{elt}]"]._aliases
                    if x.name != f"V{i}.4B[{elt}]"
                ]
            ), f"aliases mismatch for V{i}.4B[{elt}] and V{i}.S[{elt}]"
            assert sorted(
                [x.name for x in reg_names[f"V{i}.4B[{elt}]"]._subs]
            ) == sorted([x.name for x in reg_names[f"V{i}.S[{elt}]"]._subs])
            assert sorted(
                [x.name for x in reg_names[f"V{i}.4B[{elt}]"]._supers]
            ) == sorted([x.name for x in reg_names[f"V{i}.S[{elt}]"]._supers])
        bk = hk = sk = dk = 0
        aliases_b0 = sorted(
            set(
                [
                    f"B{i}",
                    f"H{i}",
                    f"S{i}",
                    f"D{i}",
                    f"Q{i}",
                    f"Z{i}",
                    f"V{i}.B[{bk}]",
                    f"V{i}.H[{hk}]",
                    f"V{i}.S[{sk}]",
                    f"V{i}.D[{dk}]",
                    f"V{i}.16B",
                    f"V{i}.8H",
                    f"V{i}.4S",
                    f"V{i}.2D",
                    f"V{i}.1Q",
                    f"V{i}.8B",
                    f"V{i}.4H",
                    f"V{i}.2S",
                    f"V{i}.1D",
                    f"V{i}.2H",
                    f"V{i}.4B[{sk}]",
                    f"Z{i}.B",
                    f"Z{i}.H",
                    f"Z{i}.S",
                    f"Z{i}.D",
                    f"Z{i}.Q",
                    f"Z{i}.B[{bk}]",
                    f"Z{i}.H[{hk}]",
                    f"Z{i}.S[{sk}]",
                    f"Z{i}.D[{dk}]",
                    f"Z{i}.Q[{dk}]",
                ]
            )
        )
        assert aliases_b0 == sorted(
            [x.name for x in reg_names[f"B{i}"].aliases] + [f"B{i}"]
        )
        assert aliases_b0 == sorted(
            [x.name for x in reg_names[f"V{i}.B[0]"].aliases] + [f"V{i}.B[0]"]
        )

    assert "B32" not in reg_names
    assert "Q32" not in reg_names
    assert "Z32" not in reg_names
    assert "V0" not in reg_names
    assert "V0.Q[0]" not in reg_names
    assert "V0.Q[1]" not in reg_names
    assert "V0.32B" not in reg_names
    assert "V0.4D" not in reg_names
    assert "V0.B[16]" not in reg_names
    assert "V0.D[2]" not in reg_names
