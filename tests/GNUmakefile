ARCH=$(shell uname -m)
ifeq ($(ARCH), x86_64)
TEST_X86_INS=test-example-list-instructions-x86
TEST_X86_GEN=test-example-generate-benchmark-lib-x86
endif

ifneq ($(DEBUG),)
LIST_DEBUG=--debug
endif

.PHONY:
.SUFFIXES:

help:
	@echo ""
	@echo "usage: make TARGET"
	@echo "  where TARGET is one of"
	@echo "    test: test all"
	@echo "    test-pytest: unit tests"
	@echo "    test-examples: test examples"
	@echo "    ref: update references for tests which have reference files in refs/"
	@echo "    test-pytest-coverage: coverage metric of unit tests"

test: test-pytest test-examples

test-pytest:
	pytest $(PYTEST_OPTS) test_*.py
	for x in testexc_*.py; do pytest $(PYTEST_OPTS) $$x || exit 1; done

test-pytest-%:
	if [ -f testexc_$*.py ]; then pytest $(PYTEST_OPTS) testexc_$*.py; fi
	if [ -f test_$*.py ]; then pytest $(PYTEST_OPTS) test_$*.py; fi

test-pytest-coverage:
	pytest --cov=pipedream *.py

test-examples: test-example-list-instructions test-example-generate-benchmark-lib test-example-parse-tb

test-example-list-instructions: $(TEST_X86_INS) test-example-list-instructions-arm

test-example-list-instructions-x86: test-example-list-instructions-x86-intel test-example-list-instructions-x86-att-punctual test-example-list-instructions-x86-att-throughput test-example-list-instructions-x86-att-latency

test-example-list-instructions-x86-intel:
	../examples/example_list_instructions.py --arch x86 --dialect intel --validate $(LIST_DEBUG)

test-example-list-instructions-x86-att-%:
	../examples/example_list_instructions.py --arch x86 --allocator $* --validate $(LIST_DEBUG)

test-example-list-instructions-arm: test-example-list-instructions-arm-none test-example-list-instructions-arm-punctual test-example-list-instructions-arm-throughput test-example-list-instructions-arm-latency

test-example-list-instructions-arm-%:
	../examples/example_list_instructions.py --arch ARMv8a --allocator $* --validate $(LIST_DEBUG)

test-example-generate-benchmark-lib: $(TEST_X86_GEN) test-example-generate-benchmark-lib-arm

test-example-generate-benchmark-lib-x86:
	../examples/example_generate_benchmark_lib.py --arch x86 --perf papi $(LIST_DEBUG)

test-example-generate-benchmark-lib-arm:
	../examples/example_generate_benchmark_lib.py --arch ARMv8a --perf papi $(LIST_DEBUG)

test-example-parse-tb: test-example-parse-tb-arm

test-example-parse-tb-arm:
	../examples/example_parse_tb.py --arch ARMv8a ../examples/data/armv8a_thunderx2.tb $(LIST_DEBUG)

ref:
	env TEST_UPDATE_REF=1 pytest test_list_all_registers.py

