def test_mp_fs_ssh():
    from multiprocessing import set_start_method
    from test_raw_ssh import run_mp_ssh

    set_start_method("forkserver")
    run_mp_ssh(n_threads=32)
