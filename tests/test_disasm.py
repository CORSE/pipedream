import subprocess
from pipedream.asm.ir import Architecture
from pipedream.disasm.reader import ObjReader
from pipedream.disasm.capstone.disassembler import Disassembler
from pipedream.disasm.capstone.utils import cs_insn_class, cs_insn_class_pipedream


map_test = {
    "ARMv8a": [
        (
            b"\x00\x1c\xa0\x4e",
            "mov v0.16b, v0.16b",
            "MOV_V16B_V16B",
            "MOV_VD_V_16B_VN_V_16B",
        ),
        (
            b"\x20\x00\x02\xab",
            "adds x0, x1, x2",
            "ADDS_X_X_X",
            "ADDS_RD_X_RN_SP_X_SP_RM_EXT_X",
        ),
        (
            b"\xe2\x8f\x40\xa9",
            "ldp x2, x3, [sp, #8]",
            "LDP_X_X_SP_IMM",
            "",
        ),  # TODO: ld/st pairs not defined in pipedream as of now
    ]
}
assembly_pre = f"""
    .text
    .global test
test:
"""
assembly_post = f"""
    .type test, @function
    .size test, . - test
"""


def test_disasm_arm():
    arch = Architecture.for_name("ARMv8a")
    binary_insns = map_test["ARMv8a"]
    dis = Disassembler(arch)
    binary = b"".join([info[0] for info in binary_insns])
    insns = dis.get_binary_dis(binary)
    assert len(insns) == len(binary_insns)
    for insn, info in zip(insns, binary_insns):
        assert insn.binary == info[0]
        assert insn.disasm == info[1]


def test_disasm_reader(tmpdir):
    arch = Architecture.for_name("ARMv8a")
    binary_insns = map_test["ARMv8a"]
    assembly = assembly_pre
    for info in binary_insns:
        assembly += f"{info[1]}\n"
    assembly += assembly_post
    o_name = tmpdir / "test.o"
    s_name = tmpdir / "test.s"
    with open(s_name, "w") as f:
        f.write(assembly)
    p = subprocess.run([arch.tools_config.as_cmd, "-o", o_name, s_name], text=True)
    assert p.returncode == 0
    with open(o_name, "rb") as f:
        reader = ObjReader(f)
        funcs = reader.get_functions()
    assert len(funcs) == 1
    assert funcs[0].name == "test"
    dis = Disassembler(arch)
    insns = dis.get_binary_dis(funcs[0].binary)
    for insn, info in zip(insns, binary_insns):
        assert insn.binary == info[0]


def test_disasm_insn_class():
    arch = Architecture.for_name("ARMv8a")
    binary_insns = map_test["ARMv8a"]
    dis = Disassembler(arch)
    binary = b"".join([info[0] for info in binary_insns])
    insns = dis.get_binary_dis(binary)
    assert len(insns) == len(binary_insns)
    for insn, info in zip(insns, binary_insns):
        assert insn.insn_class == info[2]
        assert cs_insn_class(insn.cs_insn) == info[2]


def test_disasm_insn_map():
    arch = Architecture.for_name("ARMv8a")
    binary_insns = map_test["ARMv8a"]
    dis = Disassembler(arch)
    binary = b"".join([info[0] for info in binary_insns])
    insns = dis.get_binary_dis(binary)
    assert len(insns) == len(binary_insns)
    for insn, info in zip(insns, binary_insns):
        assert cs_insn_class_pipedream(insn.cs_insn) == info[3]
