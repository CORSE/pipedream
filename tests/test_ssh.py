import pytest
import time
import sys

from test_schedule import x86_only, gen_benchspec_for_arch, run_for_arch
from ssh_config import *

try:
    import pipedream
except ImportError:
    sys.path.insert(0, str(pathlib.Path(__file__).parent.parent / "src"))
    print(sys.path)

import pipedream.benchmark as pipebench


@x86_only
def test_remote_bench(arch_name="x86", repeat=1):
    from pipedream.benchmark.common import _Benchmark_Runner
    import logging

    logging.getLogger("pipedream.benchmark.ssh").setLevel(logging.DEBUG)
    pipebench.set_remote_connexion(
        HOST,
        USER,
        PORT,
        keylocation=KEYLOCATION,
        pinned_core=2,
    )

    if arch_name == "x86":
        inputs = zip(
            [
                "ADD_GPR32i32_IMMi8",
                "ADD_GPR64i64_GPR64i64",
                "ADC_GPR32i32_MEM64i32",
                "ADC_MEM64i64_GPR64i64",
            ],
            [4, 4, 1, 1],
        )
    elif arch_name == "ARMv8a":
        inputs = zip(["SMADDL_RD_X_RN_W_RM_W_RA_X"], [2])

    for ins, ipc_val in inputs:
        benchmark = gen_benchspec_for_arch([ins], arch_name)
        start = time.time()
        for i in range(repeat):
            ipc = run_for_arch(benchmark, arch_name)
            # Assert that rounded IPC match expected values
            print(f"  it {i}: mean IPC {ins}: {ipc}")
            assert round(ipc) == ipc_val
        end = time.time()
        print(f"Did {repeat/(end-start)} it/s")


if __name__ == "__main__":
    assert len(sys.argv) == 2, f"Usage: python {sys.argv[0]} <arch>"
    arch_name = sys.argv[1]
    test_remote_bench(arch_name, repeat=10)
