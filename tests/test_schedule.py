import sys, pathlib
import platform
import pytest
from subprocess import CalledProcessError

HOST = platform.uname().machine

try:
    import pipedream
except ImportError:
    sys.path.insert(0, str(pathlib.Path(__file__).parent.parent / "src"))
    print(sys.path)

import pipedream.benchmark as pipebench
import pipedream.asm.ir as pipeasm
import pipedream.utils.pyperfs as pyperfs


def has_perf_lib(perf_lib):
    def wrapper(fun):
        try:
            with pyperfs.perf_lib_context(perf_lib):
                found = True
        except ImportError:
            found = False
        return pytest.mark.skipif(
            not found, reason=f"library {perf_lib} not available"
        )(fun)

    return wrapper


def x86_only(fun):
    return pytest.mark.skipif(HOST != "x86_64", reason="x86 host required")(fun)


def aarch64_only(fun):
    return pytest.mark.skipif(HOST != "aarch64", reason="ARMv8a host required")(fun)


def not_aarch64(fun):
    return pytest.mark.skipif(
        HOST == "aarch64", reason="Will be performed in another test"
    )(fun)


def gen_benchspec_for_arch(
    instruction_names, arch_name, with_mapping=False, is_throughput=True
):
    arch = pipeasm.Architecture.for_name(arch_name)
    inst_set = arch.instruction_set()
    instructions = [inst_set.instruction_for_name(name) for name in instruction_names]
    if with_mapping:
        instr_distrib = {instr: 1 for instr in instructions}
    else:
        instr_distrib = None

    benchmark = pipebench.Benchmark_Spec.from_instructions(
        arch=arch,
        kind=pipebench.Benchmark_Kind.THROUGHPUT
        if is_throughput
        else pipebench.Benchmark_Kind.LATENCY,
        ## kernel will contain these instructions, in this order
        instructions=instructions,
        register_pools=instr_distrib,
        ## benchmark will execute this many instructions in total (~approximate)
        num_dynamic_instructions=500_000,
        ## benchmark kernel will be unrolled to be this many instructions long
        unrolled_length=100,
    )
    return benchmark


def gen_lib_for_arch(
    instruction_names, arch_name, with_mapping=False, is_throughput=True
):
    arch = pipeasm.Architecture.for_name(arch_name)
    inst_set = arch.instruction_set()
    instructions = [inst_set.instruction_for_name(name) for name in instruction_names]

    benchmark = gen_benchspec_for_arch(
        instruction_names,
        arch_name,
        with_mapping=with_mapping,
        is_throughput=is_throughput,
    )
    lib = pipebench.gen_benchmark_lib(
        arch=arch,
        benchmarks=[benchmark],
        num_iterations=20,
        dst_dir="/tmp",
        verbose=True,
        debug=True,
        is_lib_tmp=True,
    )
    return lib


def run_for_arch(benchmark, arch_name, with_papi=True):
    arch = pipeasm.Architecture.for_name(arch_name)
    inst_set = arch.instruction_set()
    ## run benchmarks
    if with_papi:
        perf_counters = pipebench.Perf_Counter_Spec.make_throughput_counters()
    else:
        perf_counters = None

    results = pipebench.run_benchmarks(
        arch=arch,
        benchmarks=[benchmark],
        perf_counters=perf_counters,
        ## how often is each benchmark run?
        num_iterations=20,
        num_warmup_iterations=1,
        ## optional:
        ##   Integers between 0 and 100.
        ##   Results below/above the given percentiles are dropped.
        ##   Helps filter out noise, like runs where the OS scheduler did something weird.
        outlier_low=5,
        outlier_high=100,
        ## where should we store the temporay files we generate?
        tmp_dir="/tmp",
        ## extra log messages
        verbose=True,
        ## generated code contains errors checks
        debug=True,
    )
    return list(results)[0].ipc.mean


def run_insts_ipcs(insts_ipcs, arch_name):
    for ins, ipc_val in insts_ipcs:
        if isinstance(ins, str):
            ins = [ins]
        benchmark = gen_benchspec_for_arch(ins, arch_name)
        ipc = run_for_arch(benchmark, arch_name)

        print(f"  mean IPC {ins}: {ipc}")
        # Assert that IPC is ok when specified
        if ipc_val:
            assert round(ipc) == ipc_val


@x86_only
def test_x86_throughput_add_gpr():
    run_insts_ipcs([("ADD_GPR64i64_GPR64i64", 4)], "x86")


@x86_only
@has_perf_lib("papi")
def test_x86_throughput_add_gpr_with_papi():
    with pyperfs.perf_lib_context("papi"):
        run_insts_ipcs([("ADD_GPR64i64_GPR64i64", 4)], "x86")


@x86_only
@has_perf_lib("perf-pipedream")
def test_x86_throughput_add_gpr_with_perf_pipedream():
    with pyperfs.perf_lib_context("perf-pipedream") as lib:
        run_insts_ipcs([("ADD_GPR64i64_GPR64i64", 4)], "x86")


@x86_only
def test_x86_throughput_add_gpr_imm_mem():
    run_insts_ipcs(
        [
            ("ADD_GPR32i32_IMMi8", 4),
            ("ADD_GPR64i64_GPR64i64", 4),
            ("ADC_GPR32i32_MEM64i32", 1),
            ("ADC_MEM64i64_GPR64i64", 1),
        ],
        "x86",
    )


@x86_only
def test_x86_throughput_vaddpd_vr128():
    run_insts_ipcs([("VADDPD_VR128f64x2_VR128f64x2_VR128f64x2", 2)], "x86")


@x86_only
def test_x86_latency_vaddpd_vr128():
    arch_name = "x86"
    instruction_names = ["VADDPD_VR128f64x2_VR128f64x2_VR128f64x2"]
    benchmark = gen_benchspec_for_arch(
        instruction_names, arch_name, is_throughput=False
    )
    ipc = run_for_arch(benchmark, arch_name)
    print(f"  latency {instruction_names[0]}: {ipc}")


@x86_only
def test_x86_memop():
    arch_name = "x86"
    instruction_names = ["SHR_MEM64u8_CLi8"]
    benchmark = gen_benchspec_for_arch(
        instruction_names, arch_name, is_throughput=False
    )
    ipc = run_for_arch(benchmark, arch_name)
    benchmark = gen_benchspec_for_arch(instruction_names, arch_name)
    ipc = run_for_arch(benchmark, arch_name)
    benchmark = gen_benchspec_for_arch(instruction_names, arch_name, with_mapping=True)
    ipc = run_for_arch(benchmark, arch_name)


@x86_only
def test_x86_only_rdx():
    arch_name = "x86"
    instruction_names = ["PCMPESTRI64_VR128i32x4_MEM64i32x4_IMMu8"]
    benchmark = gen_benchspec_for_arch(instruction_names, arch_name)
    ipc = run_for_arch(benchmark, arch_name)


@x86_only
def test_x86_produces_code():
    arch_name = "x86"
    instruction_names = []
    benchmark = gen_benchspec_for_arch(instruction_names, arch_name)


@pytest.mark.skipif(HOST != "aarch64", reason="ARMv8a host required")
def test_arvm8a_add_x_x_imm():
    arch_name = "ARMv8a"
    instruction_names = ["ADD_RD_SP_X_SP_RN_SP_X_SP_AIMM"]
    benchmark = gen_benchspec_for_arch(instruction_names, arch_name)
    ipc = run_for_arch(benchmark, arch_name)
    print(f"  mean IPC {instruction_names[0]}: {ipc}")
    assert round(ipc) == 4


def gen_armv8a_benchs(instruction_names):
    arch_name = "ARMv8a"
    gen_lib_for_arch(instruction_names, arch_name)
    gen_lib_for_arch(instruction_names, arch_name, is_throughput=False)
    gen_lib_for_arch(instruction_names, arch_name, with_mapping=True)


def launch_armv8a_benchs(instruction_names, with_papi=True):
    arch_name = "ARMv8a"
    bench = gen_benchspec_for_arch(instruction_names, arch_name)
    ret1 = run_for_arch(bench, arch_name, with_papi)
    bench = gen_benchspec_for_arch(instruction_names, arch_name, is_throughput=False)
    ret2 = run_for_arch(bench, arch_name, with_papi)
    bench = gen_benchspec_for_arch(instruction_names, arch_name, with_mapping=True)
    ret3 = run_for_arch(bench, arch_name, with_papi)
    if with_papi:
        assert ret1 > 0 and ret1 < 100
        assert ret2 > 0 and ret2 < 100
        assert ret3 > 0 and ret3 < 100


@aarch64_only
def test_launch_armv8a_addrsimple_nopapi():
    launch_armv8a_benchs(["LDUMINA_RS_X_RT_X_ADDR_SIMPLE"], with_papi=False)


@aarch64_only
def test_launch_armv8a_addrsimple():
    launch_armv8a_benchs(["LDUMINA_RS_X_RT_X_ADDR_SIMPLE"])
    launch_armv8a_benchs(["CAS_RS_X_RT_X_ADDR_SIMPLE"])
    launch_armv8a_benchs(["CASAL_RS_W_RT_W_ADDR_SIMPLE"])


@not_aarch64
def test_armv8a_addrsimple():
    gen_armv8a_benchs(["LDUMINA_RS_X_RT_X_ADDR_SIMPLE"])
    gen_armv8a_benchs(["CAS_RS_X_RT_X_ADDR_SIMPLE"])
    gen_armv8a_benchs(["CASAL_RS_W_RT_W_ADDR_SIMPLE"])


@aarch64_only
def test_launch_armv8a_addroffset():
    launch_armv8a_benchs(["STR_FT_D_ADDR_REGOFF", "LDR_FT_D_ADDR_REGOFF"])


@not_aarch64
def test_armv8a_addroffset():
    gen_armv8a_benchs(["STR_FT_D_ADDR_REGOFF", "LDR_FT_D_ADDR_REGOFF"])


@aarch64_only
def test_launch_armv8a_ldstpair():
    launch_armv8a_benchs(["LDP_FT_D_FT2_D_ADDR_SIMM7", "STP_FT_D_FT2_D_ADDR_SIMM7"])


@not_aarch64
def test_armv8a_ldstpair():
    gen_armv8a_benchs(["LDP_FT_D_FT2_D_ADDR_SIMM7", "STP_FT_D_FT2_D_ADDR_SIMM7"])


@aarch64_only
def test_launch_armv8a_reglist():
    launch_armv8a_benchs(
        [
            "LD1_LET_0_VB_SIMD_ADDR_SIMPLE",
            "ST1_LET_0_VB_SIMD_ADDR_SIMPLE",
            "LD1R_LVT_AL_0_V_16B_SIMD_ADDR_SIMPLE",
            "LD1_LVT_0_V_16B_SIMD_ADDR_SIMPLE",
            "ST1_LVT_0_V_16B_SIMD_ADDR_SIMPLE",
        ]
    )


@not_aarch64
def test_armv8a_ldstpair():
    gen_armv8a_benchs(
        [
            "LD1_LET_0_VB_SIMD_ADDR_SIMPLE",
            "ST1_LET_0_VB_SIMD_ADDR_SIMPLE",
            "LD1R_LVT_AL_0_V_16B_SIMD_ADDR_SIMPLE",
            "LD1_LVT_0_V_16B_SIMD_ADDR_SIMPLE",
            "ST1_LVT_0_V_16B_SIMD_ADDR_SIMPLE",
        ]
    )


@not_aarch64
def test_armv8a_vh_16():
    gen_armv8a_benchs(["SQRDMULH_VD_V_4H_VN_V_4H_EM16_VH_16"])


@aarch64_only
def test_launch_armv8a_pcrel21():
    launch_armv8a_benchs(["ADR_RD_X_ADDR_PCREL21"])


@not_aarch64
def test_armv8a_pcrel21():
    gen_armv8a_benchs(["ADR_RD_X_ADDR_PCREL21"])


@aarch64_only
def test_launch_armv8a_x_w():
    launch_armv8a_benchs(["SMADDL_RD_X_RN_W_RM_W_RA_X"])


@not_aarch64
def test_armv8a_x_w():
    gen_armv8a_benchs(["SMADDL_RD_X_RN_W_RM_W_RA_X"])


@aarch64_only
def test_launch_armv8a_imm_addr_simm9():
    launch_armv8a_benchs(["LDR_RT_X_ADDR_SIMM9"])


@not_aarch64
def test_armv8a_imm_addr_simm9():
    gen_armv8a_benchs(["LDR_RT_X_ADDR_SIMM9"])


@aarch64_only
def test_launch_armv8a_regoff():
    launch_armv8a_benchs(["LDRSB_RT_X_ADDR_REGOFF"])


@not_aarch64
def test_armv8a_regoff():
    gen_armv8a_benchs(["LDRSB_RT_X_ADDR_REGOFF"])


@aarch64_only
def test_steor():
    launch_armv8a_benchs(["STEOR_RS_X_ADDR_SIMPLE"])


def test_armv8a_sp_alloc():
    gen_armv8a_benchs(["MOV_RD_SP_X_SP_RN_SP_SP"])


# Operations modifying stack are not supported
def test_armv8a_wsp():
    with pytest.raises(ValueError):
        gen_armv8a_benchs(["MOV_RD_SP_WSP_RN_SP_W_SP"])


# LDRAA is not supported, this should crash
def test_armv8a_imm_addr_simm10():
    with pytest.raises(ValueError):
        gen_armv8a_benchs(["LDRAA_RT_X_ADDR_SIMM10"])


def test_armv8a_multiple_ins():
    gen_armv8a_benchs(["ADR_RD_X_ADDR_PCREL21", "ADRP_RD_X_ADDR_ADRP"])


def test_gen_imm_addr_simm9():
    gen_armv8a_benchs(["LDRB_RT_W_ADDR_SIMM9", "LDRSB_RT_W_ADDR_REGOFF"])
