from ssh_config import *
from pipedream.benchmark.ssh_raw import SSH_Session
import traceback
import time
import pytest


class Timeout(Exception):
    pass


def new_ssh_client():
    return SSH_Session(host=HOST, port=PORT, user=USER, keylocation=KEYLOCATION)


def test_ssh():
    ssh_client = new_ssh_client()
    exit_code, stdout, stderr = ssh_client.remote_exec(
        "echo Hello Stdout && echo Hello Stderr >&2", shell=True
    )
    assert exit_code == 0, f"Unexpected exit code {exit_code}"
    assert stdout.strip() == "Hello Stdout"
    assert stderr.strip() == "Hello Stderr"
    ssh_client.disconnect()


def test_ssh_noshell():
    ssh_client = new_ssh_client()
    exit_code, stdout, stderr = ssh_client.remote_exec(["echo", "Hello Stdout"])
    assert exit_code == 0, f"Unexpected exit code {exit_code}"
    assert stdout.strip() == "Hello Stdout"
    assert stderr.strip() == ""
    ssh_client.disconnect()


def test_ssh_timeout():
    ssh_client = new_ssh_client()
    exit_code, stdout, stderr = ssh_client.remote_exec(
        "sleep 2", shell=True, timeout=0.5
    )
    assert exit_code == 1, f"Expected timeout returning exit code 1: {exit_code}"
    ssh_client.disconnect()


def test_ssh_timeout_exc():
    ssh_client = new_ssh_client()
    with pytest.raises(Timeout):
        exit_code, stdout, stderr = ssh_client.remote_exec(
            "sleep 2", shell=True, timeout=0.5, timeout_exception=Timeout
        )
    ssh_client.disconnect()


def test_ssh_timeout_exc_instance():
    ssh_client = new_ssh_client()
    with pytest.raises(Timeout):
        exit_code, stdout, stderr = ssh_client.remote_exec(
            "sleep 2", shell=True, timeout=0.5, timeout_exception=Timeout("Timeout")
        )
    ssh_client.disconnect()


def run_thread(tid, in_queue, out_queue, catch=True, ssh_client=None):
    if catch:
        ssh_client = None
        try:
            ssh_client = new_ssh_client()
            run_thread(tid, in_queue, out_queue, catch=False, ssh_client=ssh_client)
        except Exception as e:
            tb = traceback.format_exc()
            out_queue.put((tid, "ERR", f"{tb}: Exception {e}"))
        if ssh_client is not None:
            ssh_client.disconnect()
            out_queue.put((tid, "END", None))
        return
    while True:
        typ, cmd = in_queue.get()
        if typ == "END":
            break
        elif typ == "CMD":
            exit_code, stdout, stderr = ssh_client.remote_exec(
                cmd, shell=True, timeout=3
            )
            out_queue.put((tid, "RES", (exit_code, stdout, stderr)))


def run_mp_ssh(n_threads=4):
    from multiprocessing import Process as Thread, Queue

    assert (
        n_threads < 128
    ), f"above 128 threads, the limit is generally the user's max opened file limit"
    out_queue = Queue()
    threads, in_queues = [], []
    for n in range(n_threads):
        in_queue = Queue()
        thread = Thread(target=run_thread, args=(n, in_queue, out_queue))
        thread.start()
        threads.append(thread)
        in_queues.append(in_queue)

    for n in range(n_threads):
        in_queues[n].put(("CMD", f"echo Hello From {n}"))
        in_queues[n].put(("END", None))

    running = n_threads
    failed = 0
    while running > 0:
        tid, typ, result = out_queue.get()
        if typ == "END":
            running -= 1
        elif typ == "ERR":
            print(f"ERROR {tid}: {result}")
            failed += 1
        elif typ == "RES":
            exit_code, stdout, stderr = result
            if exit_code != 0:
                print(f"ERROR: Unexpected exit_code from thread {tid}, err: '{stderr}'")
                failed += 1
            elif stdout.strip() != f"Hello From {tid}":
                print(f"ERROR: Unexpected output from thread {tid}: '{stdout}'")
                failed += 1

    for n in range(n_threads):
        threads[n].join()

    assert failed == 0, f"some threads failed: {failed}/{n_threads}"


def test_mp_ssh():
    run_mp_ssh(n_threads=32)


if __name__ == "__main__":
    test_mp_ssh()
