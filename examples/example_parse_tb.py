#!/usr/bin/env python3
import sys
import logging
import argparse
from types import SimpleNamespace as ns
from pipedream.disasm.capstone.disassembler import Disassembler
from pipedream.disasm.capstone.utils import cs_insn_class_pipedream
import pipedream.asm.ir as pipeasm

logger = logging.getLogger(__name__)


def parse_tb(args):
    binary = []
    with open(args.input) as f:
        for l in f.readlines():
            if l.strip() == "":
                continue
            if l.startswith("TB") or l.startswith("End of TB"):
                continue
            bins = l.split("\t", 1)[0]
            bin_inst = b"".join([bytes.fromhex(h) for h in bins.split(" ")])
            binary.append(bin_inst)
    return binary


def parse_inst(args, binary):
    arch = pipeasm.Architecture.for_name(args.arch)
    disas = Disassembler(arch)
    unmapped = 0
    for bin_inst in binary:
        inst = disas.get_binary_dis(bin_inst)[0]
        pipe_id = cs_insn_class_pipedream(inst.cs_insn)
        # print(f"{inst.disasm}: {inst.insn_class} => {pipe_id}")
        if not pipe_id:
            logger.debug(
                f"Instruction has no mapping: {inst.disasm}: {inst.insn_class}"
            )
            unmapped += 1
    if unmapped > 0:
        logger.error(f"Unmapped instructions found: {unmapped}/{len(binary)}")
        return 1
    return 0


def main(argv):
    parser = argparse.ArgumentParser(description="parse target blocks")
    parser.add_argument(
        "--arch", required=True, type=str, help="Architecture name: x86, ARMv8a,..."
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Debug mode",
    )
    parser.add_argument(
        "input",
        help="Input TB file",
    )
    args = parser.parse_args(argv[1:])
    logging.basicConfig()
    if args.debug:
        logger.setLevel(logging.DEBUG)
    binary = parse_tb(args)
    res = parse_inst(args, binary)
    return res


if __name__ == "__main__":
    sys.exit(main(sys.argv))
