#!/usr/bin/env python3

__doc__ = """
  Example showing how to use the pipedream benchmark module as a library.
  Generates a benchmark library but does not run it.
"""
import sys
import os
import pathlib
import sys
import argparse
import platform
import tempfile
import traceback
import logging
import ctypes
from itertools import combinations
import subprocess
from types import SimpleNamespace as ns
from typing import *

from multiprocessing import Process as Thread
from multiprocessing import Queue

try:
    import pipedream
except ImportError:
    sys.path.append(str(pathlib.Path(__file__).parent.parent / "src"))

import pipedream.benchmark as pipebench
import pipedream.asm.ir as pipeasm
import pipedream.utils.pyperfs as pyperfs

logging.basicConfig(format="%(levelname)s: %(name)s: %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


ARCHS = dict(x86_64="x86", aarch64="ARMv8a")

INSTRUCTION_NAMES = {
    "x86": [
        "ADDPD_VR128f64x2_MEM64f64x2",
        "ADD_GPR64i64_MEM64i64",
        "SUB_GPR64i64_MEM64i64",
        "MOV_GPR16i16_MEM64i16",
        "MOV_MEM64i16_GPR16i16",
    ],
    "ARMv8a": [
        "LDR_RT_X_ADDR_SIMM9",
        "ADD_RD_SP_X_SP_RN_SP_X_SP_AIMM",
        "SUB_RD_SP_X_SP_RN_SP_X_SP_AIMM",
        "MUL_RD_X_RN_X_RM_X",
        "STR_RT_X_ADDR_SIMM9",
    ],
}


def proc_arch(processor=None):
    if processor is None:
        processor = platform.machine()
    return ARCHS[processor]


def bench_spec(args, instruction_names):
    arch = pipeasm.Architecture.for_name(args.arch)
    inst_set = arch.instruction_set()
    instructions = [inst_set.instruction_for_name(name) for name in instruction_names]

    benchmark: pipebench.Benchmark_Spec = pipebench.Benchmark_Spec.from_instructions(
        arch=arch,
        kind=pipebench.Benchmark_Kind.THROUGHPUT
        if args.allocator == "throughput"
        else pipebench.Benchmark_Kind.LATENCY,
        ## kernel will contain these instructions, in this order
        instructions=instructions,
        ## benchmark will execute this many instructions in total (~approximate)
        num_dynamic_instructions=args.dyn_insts,
        ## benchmark kernel will be unrolled to be this many instructions long
        unrolled_length=args.unroll,
    )
    return benchmark


def get_tmp_params(args):
    if args.debug and args.threads == 1:
        is_lib_tmp, tmpdir = False, os.getcwd()
    else:
        is_lib_tmp, tmpdir = True, tempfile.mkdtemp(prefix="pipedream.", dir="/tmp")
    return is_lib_tmp, tmpdir


def bench_build(args, benchmarks):

    is_lib_tmp, tmpdir = get_tmp_params(args)
    gen_counters = args.perf != "none"
    if gen_counters:
        pyperfs.set_perf_lib(args.perf)

    lib: Benchmark_Lib = pipebench.gen_benchmark_lib(
        arch=pipeasm.Architecture.for_name(args.arch),
        benchmarks=benchmarks,
        ## how often is each benchmark run?
        num_iterations=args.warm_up_its + args.its,
        ## where should we store the files we generate?
        dst_dir=tmpdir,
        is_lib_tmp=is_lib_tmp,
        ## extra log messages
        verbose=args.debug,
        ## generate error checks in around benchmark code
        debug=False,
        ## benchmarks don't call out to PAPI.
        gen_papi_calls=gen_counters,
    )
    return lib


def bench_run(args, benchmarks):

    is_lib_tmp, tmpdir = get_tmp_params(args)
    if args.perf != "none":
        pyperfs.set_perf_lib(args.perf)
        perf_counters = pipebench.Perf_Counter_Spec.make_throughput_counters()
    else:
        perf_counters = None

    results: Iterable[pipebench.Benchmark_Run] = pipebench.run_benchmarks(
        arch=pipeasm.Architecture.for_name(args.arch),
        benchmarks=benchmarks,
        perf_counters=perf_counters,
        ## how often is each benchmark run?
        num_iterations=args.its,
        num_warmup_iterations=args.warm_up_its,
        ## optional:
        ##   Integers between 0 and 100.
        ##   Results below/above the given percentiles are dropped.
        ##   Helps filter out noise, like runs where the OS scheduler did something weird.
        outlier_low=5,
        outlier_high=100,
        ## where should we store the temporay files we generate?
        tmp_dir=tmpdir,
        is_lib_tmp=is_lib_tmp,
        ## extra log messages
        verbose=args.debug,
    )
    return [r for r in results]


def thread_build_run(thread_id, args, input_queue, result_queue, catch=True):
    if catch:
        try:
            if args.remote:
                pipebench.set_remote_connexion(host=args.remote, pinned_core=thread_id)
            thread_build_run(thread_id, args, input_queue, result_queue, catch=False)
        except Exception as e:
            tb = traceback.format_exc()
            result_queue.put((thread_id, "ERR", (str(e), tb)))
        result_queue.put((thread_id, "END", None))
        return

    while True:
        typ, instruction_names = input_queue.get()
        if typ == "END":
            break
        elif typ == "INP":
            logger.debug(
                f"Thread {thread_id} Generate and run for {len(instruction_names)} benchmark function(s)"
            )
            benchmarks = [bench_spec(args, insts) for insts in instruction_names]
            if not args.run:
                lib = bench_build(args, benchmarks)
                result_queue.put((thread_id, "LIB", lib._lib_file))
                logger.debug(
                    f"Thread {thread_id} Generated ASM file with benchmark code: {lib.asm_file}"
                )
                logger.debug(
                    f"Thread {thread_id} Object file assembled from ASM file: {lib.object_file}"
                )
                logger.debug(
                    f"Thread {thread_id} Dynamic libray linked from object file: {lib.shared_lib_file}"
                )
                logger.debug(
                    f"Thread {thread_id} Dynamic library contains {len(lib.benchmarks)} benchmark function(s)"
                )
            else:
                results = bench_run(args, benchmarks)
                for result in results:
                    logger.debug(f"Thread {thread_id} generated result {result.name}")
                    result_queue.put(
                        (thread_id, "RES", ns(name=result.name, ipc=result.ipc.mean))
                    )


def build_run(args, instructions_names):
    if args.single:
        benchmarks = [[inst] for inst in instructions_names]
    elif args.quadra:
        benchmarks = [insts for insts in combinations(instructions_names, 2)]
    else:
        benchmarks = [instructions_names]
    benchmarks = benchmarks * args.n_runs

    result_queue = Queue()
    n_per_thread = (len(benchmarks) + args.threads - 1) // args.threads
    n_threads = (len(benchmarks) + n_per_thread - 1) // n_per_thread

    print(f"Launching {n_threads} threads for {len(benchmarks)} benchmarks")
    threads = []
    threads_queue = []
    threads_benchmarks = {}
    for i in range(n_threads):
        input_queue = Queue()
        thread_benchmarks = benchmarks[i * n_per_thread : (i + 1) * n_per_thread]
        thread = Thread(
            target=thread_build_run, args=(i, args, input_queue, result_queue)
        )
        thread.start()
        threads.append(thread)
        threads_queue.append(input_queue)
        threads_benchmarks[i] = []

    for i, benchmark in enumerate(benchmarks):
        threads_benchmarks[i % n_threads].append(benchmark)

    for i in range(n_threads):
        if args.no_group:
            for benchmark in threads_benchmarks[i]:
                threads_queue[i].put(("INP", [benchmark]))
        else:
            threads_queue[i].put(("INP", threads_benchmarks[i]))
        threads_queue[i].put(("END", None))

    failed = 0
    results = []
    threads_tasks = {i: 0 for i in range(n_threads)}
    running = len(threads)
    while running > 0:
        thread_id, typ, result = result_queue.get()
        if typ == "END":
            running -= 1
            if args.run and threads_tasks[thread_id] < len(
                threads_benchmarks[thread_id]
            ):
                logger.error(
                    f"Thread {thread_id} did not complete all tasks: {threads_tasks[thread_id]}/{len(threads_benchmarks[thread_id])}"
                )
                failed += 1
        elif typ == "ERR":
            msg, tb = result
            logger.error(f"Thread {thread_id}: {tb}")
            logger.error(f"Thread {thread_id}: {msg}")
        elif typ == "LIB":
            print(f"Thread {thread_id}: lib {result}")
        elif typ == "RES":
            print(f"Thread {thread_id}: res {result.name}")
            print("  mean IPC:", result.ipc)
            results.append(result)
            threads_tasks[thread_id] += 1

    for i in range(n_threads):
        threads[i].join()

    if failed > 0:
        logger.error(f"Failed {failed}/{len(benchmarks)} benchmarks")
        return 1

    return 0


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description="Benchmark lib generator")
    parser.add_argument(
        "--arch",
        type=str,
        default=proc_arch(),
        help=f"architecture, one of {', '.join(ARCHS.values())}. Default to host ({proc_arch()})",
    )
    parser.add_argument(
        "--perf",
        type=str,
        default="auto",
        help=f"perf measurement method, one of auto, papi, perf-pipedream. Default to auto",
    )
    parser.add_argument(
        "--inst-file", type=str, help=f"file containing instruction names, one per line"
    )
    parser.add_argument(
        "--all", action="store_true", help=f"generate for all instructions"
    )
    parser.add_argument(
        "--single", action="store_true", help=f"generate 1 benchamrk per instruction"
    )
    parser.add_argument(
        "--run", action="store_true", help=f"run the benchmark after generating lib"
    )
    parser.add_argument(
        "--dyn-insts", type=int, default=500_000, help=f"dyn insts, default to: 500,000"
    )
    parser.add_argument(
        "--unroll", type=int, default=500, help=f"unrolling, default to: 500"
    )
    parser.add_argument(
        "--its", type=int, default=50, help=f"number of iterations, default to: 50"
    )
    parser.add_argument(
        "--warm-up-its",
        type=int,
        default=10,
        help=f"number of warmup iterations, default to: 10",
    )
    parser.add_argument(
        "--n-runs",
        type=int,
        default=1,
        help=f"number of times to build/run the benchmarks",
    )
    parser.add_argument(
        "--no-group",
        action="store_true",
        help=f"do not group benchmarks in single runs per thread",
    )
    parser.add_argument(
        "--quadra", action="store_true", help="benchamrk all combinations of size 2"
    )
    parser.add_argument(
        "--allocator",
        choices=("throughput", "latency"),
        default="throughput",
        help="allocator kind",
    )
    parser.add_argument(
        "--threads", "-j", type=int, default=1, help="threads of execution, default 1"
    )
    parser.add_argument("--remote", type=str, help="remote ssh host for execution")
    parser.add_argument("--debug", action="store_true", help="enable debug output")
    args = parser.parse_args(argv[1:])

    if args.debug:
        logging.getLogger("pipedream").setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)

    ## set up benchmark

    if args.inst_file:
        with open(args.inst_file) as f:
            instruction_names = [
                i.split()[0]
                for i in [l.strip() for l in f.readlines()]
                if i and i[0] != "#"
            ]
    elif args.all:
        arch = pipeasm.Architecture.for_name(args.arch)
        instruction_names = list(
            [inst.name for inst in arch.instruction_set() if inst.can_benchmark]
        )
    else:
        instruction_names = INSTRUCTION_NAMES[args.arch]

    res = build_run(args, instruction_names)

    return res


if __name__ == "__main__":
    sys.exit(main())
