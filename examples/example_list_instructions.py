#!/usr/bin/env python3

__doc__ = """
  Example showing how to generate the full list of instructions with random registers.
  Generates an assembly file.
"""

import pathlib
import sys
import argparse
import random
import tempfile
from collections import defaultdict
import subprocess
import logging

try:
    import pipedream
except ImportError:
    sys.path.append(str(pathlib.Path(__file__).parent.parent / "src"))

import pipedream.benchmark as pipebench
import pipedream.asm.ir as pipeasm
import pipedream.asm.allocator as pipealloc

logging.basicConfig()
logger = logging.getLogger(__name__)


def get_memory_regs(irb, insts, all_regs):
    memory_regs = pipealloc.Mem_Access_Regs()
    mem_regs = []
    for i in range(5):
        mem_regs.append(irb.select_memory_base_register(insts, all_regs, 64))
    (
        memory_regs.pure_ld,
        memory_regs.pure_st,
        memory_regs.base_area,
        memory_regs.reg_off_ld,
        memory_regs.reg_off_st,
    ) = mem_regs
    (
        memory_regs.cst_disp_ld,
        memory_regs.cst_disp_st,
        memory_regs.cst_disp_reg_off_ld,
        memory_regs.cst_disp_reg_off_st,
    ) = (0, 0, 0, 0)
    return memory_regs


def generate_asm(args, io):
    rand = random.Random(args.seed)

    arch = pipeasm.Architecture.for_name(args.arch)
    inst_set = arch.instruction_set()
    insts = [inst for inst in inst_set if args.all or inst.can_benchmark]
    if args.shuffle:
        rand.shuffle(insts)

    func_name = "test"
    if args.dialect is None:
        dialect = arch.asm_dialects()[0]
    else:
        dialect = [d for d in arch.asm_dialects() if d.name == args.dialect][0]
    asm_writer = arch.make_asm_writer(dialect, io)

    if args.allocator != "none":
        regs = arch.register_set()
        all_regs = set(regs.all_registers())
        irb = arch.make_ir_builder()
        if args.allocator == "punctual":
            allocator = pipealloc.Punctual_Register_Allocator(all_regs, random=rand)
        elif args.allocator == "throughput":
            memory_allocator = pipealloc.Memory_Arena_Round_Robin_Allocator(
                arch.l1_cache_config, 8192  # arbitrary large
            )
            ro_regs = iter(regs.stack_pointer_register_class())
            memory_regs = get_memory_regs(irb, insts, all_regs)
            all_regs -= set(memory_regs.used_regs())
            allocator = pipealloc.Minimize_Deps_Register_Allocator(
                all_regs,
                memory_allocator=memory_allocator,
                memory_regs=memory_regs,
                regs_ro=ro_regs,
            )
        elif args.allocator == "latency":
            memory_allocator = pipealloc.Memory_Arena_Punctual_Allocator()
            ro_regs = iter(regs.stack_pointer_register_class())
            memory_regs = get_memory_regs(irb, insts, all_regs)
            all_regs -= set(memory_regs.used_regs())
            allocator = pipealloc.Maximize_Deps_Register_Allocator(
                all_regs,
                memory_allocator=memory_allocator,
                memory_regs=memory_regs,
                regs_ro=ro_regs,
            )
        allocated_insts = allocator.allocate(insts, irb)

    else:
        allocated_insts = []
        for inst in insts:
            allocated = inst
            regclass_def_num = defaultdict(int)
            for idx, op in enumerate(inst.operands):
                if not allocated.operands[idx].is_virtual:
                    continue
                if isinstance(op, pipeasm.Register_Operand) and op.is_def:
                    # Ensure that no reg def is allocated to the same register
                    regclass = op.register_class
                    reg_idx = regclass_def_num[regclass.name]
                    regclass_def_num[regclass.name] += 1
                    arbitrary = list(regclass)[reg_idx]
                else:
                    arbitrary = op.arbitrary_value(rand)
                allocated = allocated.set_operand(
                    op.name, op.with_operand_value(arbitrary)
                )
            allocated_insts.append(allocated)

    asm_writer.begin_file(io.name)
    asm_writer.begin_function(func_name)
    for inst, allocated in zip(insts, allocated_insts):
        if not args.no_comment:
            io.write(f"# {repr(inst)}\n")
            io.write(f"# {repr(allocated)}\n")
        asm_writer.insts([allocated])
    asm_writer.end_function(func_name)
    asm_writer.end_file(io.name)

    logger.debug(f"Generated {len(allocated_insts)} instructions")
    return allocated_insts


def assemble_file(args, io):
    arch = pipeasm.Architecture.for_name(args.arch)
    p = subprocess.run(
        [arch.tools_config.as_cmd, "-o", "/dev/null", io.name], text=True
    )
    return p.returncode


def generate_list(args, io):
    arch = pipeasm.Architecture.for_name(args.arch)
    inst_set = arch.instruction_set()

    for inst in inst_set:
        io.write(f"{inst.name}\n")


def main():
    parser = argparse.ArgumentParser(description="list all architecture instructions")
    parser.add_argument("--list", action="store_true", help="Only list names")
    parser.add_argument(
        "--arch", required=True, type=str, help="Architecture name: x86, ARMv8a,..."
    )
    parser.add_argument(
        "--dialect", type=str, help="Optional assembler dialect, for x86: att or intel"
    )
    parser.add_argument(
        "--validate",
        action="store_true",
        help="Generate and assemble file instead of emitting file",
    )
    parser.add_argument(
        "--all", action="store_true", help="Do not filter instruction lists"
    )
    parser.add_argument(
        "--allocator",
        choices=["none", "punctual", "latency", "throughput"],
        default="none",
        help="Optional register allocator",
    )
    parser.add_argument(
        "--shuffle",
        action="store_true",
        help="Shuffle generated instructions",
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=0,
        help="Random seed, default: 0",
    )
    parser.add_argument(
        "--no-comment",
        action="store_true",
        help="Do not emit comments in generated file",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Debug mode, generate tempfile locally with --validate",
    )
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)

    if args.dialect is None:
        arch = pipeasm.Architecture.for_name(args.arch)
        args.dialect = arch.asm_dialects()[0].name  # take first dialect as default

    if args.list:
        generate_list(args, sys.stdout)
    else:
        if args.validate:
            if args.debug:
                options = dict(
                    delete=False,
                    dir=".",
                    prefix=f"list-instructions-{args.arch}-{args.dialect}-",
                )
            else:
                options = {}
            with tempfile.NamedTemporaryFile(mode="w", suffix=".s", **options) as f:
                logger.debug(f"Generating asm file: {f.name}")
                generate_asm(args, f)
                f.flush()
                ret = assemble_file(args, f)
            return ret
        else:
            generate_asm(args, sys.stdout)
    return 0


if __name__ == "__main__":
    sys.exit(main())
