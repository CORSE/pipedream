black
msgpack-python
numpy
pygraphviz # graphviz headers must be installed in system
pytest-cov
PyYAML
scipy
statsmodels
mypy==0.971
segv-recover@git+https://gitlab.inria.fr/CORSE/segv-recover
parallel-ssh@git+https://github.com/NicolasDerumigny/parallel-ssh
capstone<5.0
pyelftools

# for convenience
bpython
