# Pipedream

## Install

### Tools dependencies

The following tools must be installed on the system first:

- python 3.8+
- gcc/g++ 9.x+
- make 4.x+
- CMake 3.14+
- graphviz dev files
- PAPI or Perf (See "Performance counters" subsection for more information)
- pySegv\_Recover (See `tools/segv_recover/README.md` for more information)

For instance on `deb` distros, install with:

    sudo apt install build-essential gcc g++ make cmake graphviz-dev [libpapi6.0] [perf]


### Generate the x86 instruction database

Before installing Pipedream, it is necessary to generate the database of x86
instructions from Intel XED:

```bash
  git submodule init
  git submodule update
  cd tools/extract-xed-instruction-database/
  cmake -B build -S .
  make -C build -j
  build/extract-xed-instruction-database print-instr-db -o ../../src/pipedream/asm/x86/instructions_xed.py
```

Alternatively, if you already have generated this file on another machine, you
may copy it over as `src/pipedream/asm/x86/instructions_xed.py`.

### Generate the ARMv8a instruction database

Refer to `tools/extract-binutils-instruction-database/README.md`

Alternatively, if you already have generated this file on another machine, you
may copy it over as `src/pipedream/asm/armv8a/instructions_binutils.py`.

### Installing pipedream

It is recommended to install python dependencies, as well as pipedream itself,
into a virtualenv:

```bash
  virtualenv -p python3 venv
  source venv/bin/activate
  pip install -r requirements.txt
  pip install -e .
```

Note that this will break if you did not first generate the x86 instructions
database (see above).

### Performance counters

Pipedream needs access to performance counters in order to measure the
number of instructions and the execution time of microbenchmarks. Two
libraries may be used for these measurements: [PAPI](https://icl.utk.edu/papi/)
or Perf-Pipedream, a PAPI-like interface over
[Perf](https://www.man7.org/linux/man-pages/man1/perf.1.html) for
CPUs no supported by PAPI. See `~/tools/perf-pipedream/README.md` for
more information about Perf-Pipedream and installation instructions.


### Test pipedream

Firt, before using pipedream performance reporting feature or running
unitary tests, one as to enable tracing of CPU events with:

    echo 0 | sudo tee /proc/sys/kernel/perf_event_paranoid

Run at least unitary tests in order to verify correct installations or
before commiting modifications:

    make test        # Run all tests
    make test-pytest # or run only unitary tests
    make test-examples # or run only test examples
    make test-mypy   # or run only type checking
    make test-black  # or run only indentation check with the black tool


### Repository branch management

Before pushing a commit or proposing a commit for review, ensure first that
tests are passing with: `make test` (see above)

Commits pushed to `master` branch should be only fast-forward commits,
merge commits on this branch are not allowed.

In particular, in order to keep a clean blame history, always ensure that
the code is correctly indented with `black` (tested with `make test` or `make test-black`).

### Legacy branches

History rewrites have been performed for ensuring correct indentation with `black` on the
`master` branch.

Revisions former to the history rewrites are availables in the heads stored under the
names `legacy/*`. This ensure that any previous revision is available after a clone and
can still be accessed.

Replaced commits have been pushed to `refs/replace/*`, in case one still has a development working
tree with legacy heads, fetch the replacement points with `git fetch refs/replace/*:refs/replace/*`.
This is not necessary on new clones or when working on non legacy branches.

The history rewrite was done with the `git-filter-repo` tool which is a faster equivalent
of `git filter-branch`, though the results are the same.

The command executed was with git-filter repo revision `d760d24c`:

    git clone https://github.com/newren/git-filter-repo.git ~/git-filter-repo
    PYTHONPATH=$HOME/git-filter-repo $HOME/git-filter-repo/contrib/filter-repo-demos/lint-history --relevant 'return filename.endswith(b".py")' bash -c 'black -t py37 $0 || true'
