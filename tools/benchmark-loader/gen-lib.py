import sys

import pipedream
import pipedream.benchmark as pipebench
import pipedream.asm.ir as pipeasm

assert len(sys.argv) == 2, f"Usage: python {sys.argv[0]} <arch>"
arch = pipeasm.Architecture.for_name(sys.argv[1])
inst_set = arch.instruction_set()
if sys.argv[1] == "ARMv8a":
    instructions = [inst_set.instruction_for_name("SMADDL_RD_X_RN_W_RM_W_RA_X")]
elif sys.argv[1] == "x86":
    instructions = [inst_set.instruction_for_name("ADD_GPR32i32_IMMi8")]

benchmark = pipebench.Benchmark_Spec.from_instructions(
    arch=arch,
    kind=pipebench.Benchmark_Kind.THROUGHPUT,
    instructions=instructions,
    num_dynamic_instructions=5000,
    unrolled_length=100,
)

lib = pipebench.gen_benchmark_lib(
    arch=arch,
    benchmarks=[benchmark],
    num_iterations=20,
    dst_dir=".",
    verbose=True,
    debug=True,
    is_lib_tmp=False,
)
