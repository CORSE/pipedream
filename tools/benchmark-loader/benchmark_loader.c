/// \file benchmark_loader.c
/// \brief THis file is designed to be modified in order to select the desired hardware
/// counters and linked with a Pipdream-generated benchmark in order to explore the reason
/// behind an unusual IPC

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef PAPI
#    include <papi.h>
#    define PERF_CURRENT_VERSION PAPI_VER_CURRENT
#    define PERF_OK PAPI_OK
#    define PERF_NULL PAPI_NULL
#    define perf_library_init(ver) PAPI_library_init(ver)
#    define perf_strerror(val) PAPI_strerror(val)
#    define perf_create_eventset(e) PAPI_create_eventset(e)
#    define perf_add_event(s, e) PAPI_add_event(s, e)
#    define perf_event_name_to_code(n, e) PAPI_event_name_to_code(n, e)
#    define perf_start(e) PAPI_start(e)
#    define perf_stop(e, m) PAPI_stop(e, m)
#else
#    include <perf-pipedream.h>
#    define PERF_CURRENT_VERSION PERFPIPEDREAM_CURRENT_VERSION
#    define PERF_OK PERFPIPEDREAM_SUCCESS
#    define PERF_NULL PERFPIPEDREAM_NULL
#    define perf_library_init(ver) perf_pipedream_library_init(ver)
#    define perf_strerror(val) perf_pipedream_strerror(val)
#    define perf_create_eventset(e) perf_pipedream_create_eventset(e)
#    define perf_add_event(s, e) perf_pipedream_add_event(s, e)
#    define perf_event_name_to_code(n, e) perf_pipedream_event_name_to_code(n, e)
#    define perf_start(e) perf_pipedream_start(e)
#    define perf_stop(e, m) perf_pipedream_stop(e, m)
#endif

int NUM_ITER = 21;
// ymm0..15 * sizeof(ymm0)
#define RANDOM_MEMORY_AREA_BYTES 512
// Should be enough
#define MAX_MEM_SIZE (32 * (2 << 10))

/// statement that GCC (currently) cannot reorder around
#define COMPILER_BARRIER() asm volatile("" ::: "memory");

extern int benchmark_kernel_0(int perf_event_set, int num_events, long long int *results,
                              unsigned char *random_memory_area,
                              unsigned char *memory_area_load,
                              unsigned char *memory_area_store);

int main(int argc, char *argv[]) {
    assert(argc == 2);
    NUM_ITER = atoi(argv[1]);

    int perf_event_set = 0;
    long long int *results = NULL;
    {
        const int retval = perf_library_init(PERF_CURRENT_VERSION);
        if (retval != PERF_CURRENT_VERSION) {
            const char *err = perf_strerror(retval);
            printf("%s:%d::perf_library_init failed: (%d) %s\n", __FILE__, __LINE__,
                   retval, err);
            exit(EXIT_FAILURE);
        }
    }

    const char *EVENT_NAMES[] = {
        "PAPI_TOT_CYC", //-> number of cycle (PAPI-specific method)
        "PAPI_TOT_INS", //-> number of instructions (PAPI-specific method)
        //"CPU_CLK_UNHALTED", //-> Execution time (in cycles)
        //"UOPS_ISSUED:ALL",                   //-> Number of uops executed
        //"UOPS_RETIRED:STALL_CYCLES",         //-> Stalled cycles where no instructions
        // are retired
        //"IDQ_UOPS_NOT_DELIVERED:CORE",       //-> Number of uops stalled
        // because the front end was too fast
        //"RESOURCE_STALLS:ALL",               //-> Stalled cycles due to Resources
        // Related Reasons
        //"RESOURCE_STALLS:RS",                //-> Stalled cycles due to Reservation
        // Station being full
        //"RESOURCE_STALLS:SB",                //-> Stalled cycles due to Store Buffer
        // being full
        //"RESOURCE_STALLS:ROB",               //-> Stalled cycles due to Reorder Buffer
        // being full
        //"INT_MISC:RECOVERY_CYCLES",          //-> Stalled cycles due to recovery of
        // checkpoints of the Resource Allocation Table (probably related to branch
        // prediction)
        //"INT_MISC:CLEAR_RESTEER_CYCLES",     //-> Stalled cycles of the issue stage
        // waiting for the front-end following a missprediction
        //"PAPI_L1_DCM",                       //-> L1 cache misses
        //"L1D_PEND_MISS:PENDING",             //-> Cycles with L1 load misses outstanding
        //"ICACHE_16B:IFDATA_STALL",           //-> Stalls due to instruction L1 cache
        // misses "ICACHE_64B:IFTAG_MISS",     //-> Instruction cache misses
        //"ICACHE_64B:IFTAG_STALL",            //-> Stalls due to instruction cache misses
        //"PAPI_L2_DCM",                       //-> L2 data cache misses
        //"PAPI_L2_DCA",                       //-> L2 data cache accesses
        //"PAPI_L3_TCM",                       //-> L3 total cache misses
        //"PAPI_TLB_DM",                       //-> DTLB miss
        //"PAPI_LST_INS",                      //-> Load/store ops completed
        //"PAPI_MEM_WCY",                      //-> Cyctes waiting for mem write
        //"PAPI_BR_MSP",                       //-> Branch misspredicts
        //"PAPI_PRF_DM",                       //-> Data Prefetch cache miss
        //"PAPI_RES_STL",                      //-> Nb of stalls waiting for a ressource
        //"CYCLE_ACTIVITY:STALLS_TOTAL",       //-> Stalled cycles
        //"CYCLE_ACTIVITY:CYCLES_L1D_MISS",    //-> Stalled cycles due to L1D misses
        //"CYCLE_ACTIVITY:CYCLES_L1D_PENDING", //-> Stalled cycles due to L1D misses
        //"CYCLE_ACTIVITY:CYCLES_L2_MISS",     //-> Stalled cycles due to L2 misses
        //"CYCLE_ACTIVITY:CYCLES_L2_PENDING",  //-> Stalled cycles due to L2 misses
        //"CYCLE_ACTIVITY:CYCLES_L3_MISS",     //-> Stalled cycles due to L3 misses
        //"CYCLE_ACTIVITY:CYCLES_LDM_PENDING", //-> Stalled cycles due to L3 misses
        //"CYCLE_ACTIVITY:STALLS_MEM_ANY",     //-> Nb of stalls due to memory
        //"CYCLE_ACTIVITY:STALLS_L1D_MISS",    //-> Nb of stalls due to L1D misses
        //"CYCLE_ACTIVITY:STALLS_L2_MISS",     //-> Nb of stalls due to L2 misses
        //"CYCLE_ACTIVITY:STALLS_L3_MISS",     //-> Nb of stalls due to L3 misses
        //"PARTIAL_RAT_STALLS:SCOREBOARD",     //-> Stalled cycles due to Ressource
        // Allocation Table Stalls
        //"ILD_STALL",                         //-> Nb of stalls due to the Instruction
        // Length Decoder
        //"DSB2MITE_SWITCHES:PENALTY_CYCLES",  //-> Stalled cycles due to DSB to MITE
        // switches (?)
        //"DTLB_LOAD_MISSES:WALK_ACTIVE",      //-> Cycles when one hardware walker
        // is active (loading data)
        //"DTLB_STORE_MISSES:WALK_ACTIVE",     //-> Cycles when one hardware walker is
        // active (storing data)
        //"ITLB:ITLB_FLUSH",                   //-> Nb of flush of
        // the Instruction TLB (independent of page size)
        //"ITLB_MISSES:WALK_ACTIVE",           //-> Cycles when at least one page walker
        // is busy (excluding EPT page walks)
    };

    static const int num_events = sizeof(EVENT_NAMES) / sizeof(EVENT_NAMES[0]);

    // Initialize the random memory area -- however, since this is a debug
    // wrapper, use deterministic and easy-to-verify bytes
    uint8_t *random_mem_area = aligned_alloc(32, RANDOM_MEMORY_AREA_BYTES);
    uint8_t *memory_area_load = aligned_alloc(32, MAX_MEM_SIZE);
    uint8_t *memory_area_store = (void *)memory_area_load + (MAX_MEM_SIZE / 2);
    for (size_t offset = 0; offset < RANDOM_MEMORY_AREA_BYTES; ++offset) {
        // ymm_i[j] should be 0xi[j%16]
        random_mem_area[offset] = (offset % 16) + (offset / 32) * 0x10;
    }

    results = malloc(sizeof(*results) * num_events * NUM_ITER);

    perf_event_set = PERF_NULL;

    COMPILER_BARRIER();
    {
        // create event set
        {
            const int retval = perf_create_eventset(&perf_event_set);
            if (retval != PERF_OK) {
                const char *err = perf_strerror(retval);
                printf("%s:%d::perf_create_eventset failed: (%d) %s\n", __FILE__,
                       __LINE__, retval, err);
                exit(EXIT_FAILURE);
            }
        }
        // add events
        for (size_t i = 0; i < num_events; i++) {
            const char *event_name = EVENT_NAMES[i];

            int event_code = 0;
            {
                const int retval = perf_event_name_to_code(event_name, &event_code);
                if (retval != PERF_OK) {
                    const char *err = perf_strerror(retval);
                    printf("%s:%d::perf_event_name_to_code(\"%s\") failed: (%d) %s\n",
                           __FILE__, __LINE__, event_name, retval, err);
                    exit(EXIT_FAILURE);
                }
            }
            {
                const int retval = perf_add_event(perf_event_set, event_code);
                if (retval != PERF_OK) {
                    const char *err = perf_strerror(retval);
                    printf("%s:%d::perf_add_event(%d) (\"%s\") failed: (%d) %s\n",
                           __FILE__, __LINE__, event_code, event_name, retval, err);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

    const int ret =
        benchmark_kernel_0(perf_event_set, num_events, results, random_mem_area,
                           memory_area_load, memory_area_store);
    if (ret != PERF_OK) {
        const char *err = perf_strerror(ret);
        printf("%s:%d::benchmark_kernel_0(%d, %d, %p, %p, %p, %p) failed: (%d) "
               "%s\n",
               __FILE__, __LINE__, perf_event_set, num_events, results, random_mem_area,
               memory_area_load, memory_area_store, ret, err);
        exit(EXIT_FAILURE);
    }

    for (unsigned j = 0; j < NUM_ITER; j++) {
        for (unsigned i = 0; i < num_events; i++) {
            printf("%-50s %10lli\n", EVENT_NAMES[i], results[j * num_events + i]);
        }
    }
    free(results);
    free(memory_area_load);
    free(random_mem_area);
    results = NULL;
    fflush(stdout);
    return (EXIT_SUCCESS);
}
