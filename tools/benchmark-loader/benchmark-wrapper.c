#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#if defined(NONE)
#    define PERF_CURRENT_VERSION 0
#    define PERF_OK 0
#    define PERF_NULL 0
#    define perf_library_init(ver) ({(void)(ver); 0;})
#    define perf_strerror(val) ({(void)(val); "";})
#    define perf_create_eventset(e) ({(void)(e); 0;})
#    define perf_add_event(s, e) ({(void)(s); (void)(e); 0;})
#    define perf_event_name_to_code(n, e) ({(void)(n); (void)(e); 0;})
#    define perf_start(e) ({(void)(e); 0;})
#    define perf_stop(e, m) ({(void)(e); (void)(m); 0;})
#elif defined(PAPI)
#    include <papi.h>
#    define PERF_CURRENT_VERSION PAPI_VER_CURRENT
#    define PERF_OK PAPI_OK
#    define PERF_NULL PAPI_NULL
#    define perf_library_init(ver) PAPI_library_init(ver)
#    define perf_strerror(val) PAPI_strerror(val)
#    define perf_create_eventset(e) PAPI_create_eventset(e)
#    define perf_add_event(s, e) PAPI_add_event(s, e)
#    define perf_event_name_to_code(n, e) PAPI_event_name_to_code(n, e)
#    define perf_start(e) PAPI_start(e)
#    define perf_stop(e, m) PAPI_stop(e, m)
#else
#    include <perf-pipedream.h>
#    define PERF_CURRENT_VERSION PERFPIPEDREAM_CURRENT_VERSION
#    define PERF_OK PERFPIPEDREAM_SUCCESS
#    define PERF_NULL PERFPIPEDREAM_NULL
#    define perf_library_init(ver) perf_pipedream_library_init(ver)
#    define perf_strerror(val) perf_pipedream_strerror(val)
#    define perf_create_eventset(e) perf_pipedream_create_eventset(e)
#    define perf_add_event(s, e) perf_pipedream_add_event(s, e)
#    define perf_event_name_to_code(n, e) perf_pipedream_event_name_to_code(n, e)
#    define perf_start(e) perf_pipedream_start(e)
#    define perf_stop(e, m) perf_pipedream_stop(e, m)
#endif

typedef int (*kernel_f)(int perf_event_set, int num_events, long long int *results, char *random_area, char *load_area, char *store_area);

typedef struct {
  long init_area_align;
  long init_area_size;
  long load_store_area_align;
  long load_store_area_size;
  long store_area_offset;
  long has_perf_lib;
  long num_iterations;
} global_params_t;

typedef struct {
  long unroll_factor;
  long kernel_iterations;
  long num_prologue_insts;
} kernel_params_t;

typedef struct {
  int num_iters;
  int benchmark_idx;
  int debug;
} options_t;

static int parse_options(options_t *options, int argc, char *argv[]) {
  static struct option long_options[] =
    {
     {"iters", required_argument, 0, 'n'},
     {"idx", required_argument, 0, 'i'},
     {"debug", no_argument, 0, 'd'},
     {0, 0, 0, 0}
    }; 
  options->num_iters = -1;
  options->benchmark_idx = -1;
  options->debug = 0;
  while(1) {
    int option_index = 0;
    int c = getopt_long(argc, argv, "", long_options, &option_index);
    if (c == -1)
      break;
    switch(c) {
    case 'n':
      options->num_iters = atoi(optarg);
      break;
    case 'i':
      options->benchmark_idx = atoi(optarg);
      break;
    case 'd':
      options->debug = 1;
      break;
    default:
      assert(0);
    }
  }
  return optind;
}

static void print_global_params(const global_params_t *params, FILE *f)
{
  fprintf(f, "init_area_align: %ld\n", params->init_area_align);
  fprintf(f, "init_area_size: %ld\n", params->init_area_size);
  fprintf(f, "load_store_area_align: %ld\n", params->load_store_area_align);
  fprintf(f, "load_store_area_size: %ld\n", params->load_store_area_size);
  fprintf(f, "store_area_offset: %ld\n", params->store_area_offset);
  fprintf(f, "has_perf_lib: %ld\n", params->has_perf_lib);
  fprintf(f, "num_iterations: %ld\n", params->num_iterations);
}

static void print_kernel_params(const kernel_params_t *params, FILE *f)
{
  fprintf(f, "unroll_factor: %ld\n", params->unroll_factor);
  fprintf(f, "kernel_iterations: %ld\n", params->kernel_iterations);
  fprintf(f, "num_prologue_insts: %ld\n", params->num_prologue_insts);
}

static void rand_init(char *buffer, int size, int seed)
{
  if (seed >= 0)
    srand(seed);
  for (int i = 0; i < size; i++) {
    buffer[i] = (char)rand();
  }
}

static char *lib_stdin_to_tmpfile()
{
  const char template[] = "/tmp/benchmark-lib-XXXXXX.so";
  char *tempfile = malloc(sizeof(template));
  memcpy(tempfile, template, sizeof(template));
  char buffer[4096];
  int out_fd  = mkstemps(tempfile, strlen(".so"));
  assert(out_fd >= 0);
  while (1) {
    int n = read(0, buffer, sizeof(buffer));
    assert(n >= 0);
    if (n > 0) {
      int w = write(out_fd, buffer, n);
      assert(n == w);
    }
    if (n < sizeof(buffer)) {
      break;
    }
  }
  close(out_fd);
  return tempfile;
}

static void handle(int retval)
{
  if (retval != PERF_OK) {
    fprintf(stderr, "ERROR: in perf library: %s\n", perf_strerror(retval));
    abort();
  }
}

int main(int argc, char *argv[])
{
  options_t options;
  int optind = parse_options(&options, argc, argv);
  assert(optind < argc);
  const char * lib_name = argv[optind++];
  char * tempfile = NULL;
  if (lib_name[0] == '-' && lib_name[1] == '\0') {
    lib_name = tempfile = lib_stdin_to_tmpfile();
  }
  void *lib = dlopen(lib_name, RTLD_NOW);
  assert(lib != NULL);
  char sym_name[256];
  const global_params_t *params = dlsym(lib, "benchmarks_params");
  assert(params != NULL);
  if (options.debug) {
    fprintf(stderr, "Global params:\n");
    print_global_params(params, stderr);
  }
  char *init_area = aligned_alloc(params->init_area_align, params->init_area_align);
  char *ldst_area = aligned_alloc(params->load_store_area_align, params->load_store_area_size);
  memset(ldst_area, 0, params->load_store_area_size);
  int num_iters = options.num_iters;
  num_iters = num_iters == -1 ? params->num_iterations: num_iters;
  num_iters = num_iters > params->num_iterations ? params->num_iterations: num_iters;
  long long int *results = NULL;
  int perf_event_set = PERF_NULL;
  int num_events = 0;
  char **event_names = &argv[optind];
  if (params->has_perf_lib) {
    int retval = perf_library_init(PERF_CURRENT_VERSION);
    if (retval < 0)
      handle(retval);
    assert(retval == PERF_CURRENT_VERSION);
    retval = perf_create_eventset(&perf_event_set);
    handle(retval);
    for (int i = optind; i < argc; i++) {
      const char *event_name = argv[i];
      int event_code;
      retval = perf_event_name_to_code(event_name, &event_code);
      handle(retval);
      retval = perf_add_event(perf_event_set, event_code);
      handle(retval);
      num_events++;
    }
    results = malloc(sizeof(*results) * num_events * params->num_iterations);
  }
  int idx = 0;
  while(1) {
    if (options.benchmark_idx >= 0)
      idx = options.benchmark_idx;
    snprintf(sym_name, sizeof(sym_name), "benchmark_kernel_%d", idx);
    kernel_f sym = dlsym(lib, sym_name);
    assert(sym != NULL || options.benchmark_idx == -1);
    if (sym == NULL) {
      break;
    }
    snprintf(sym_name, sizeof(sym_name), "benchmark_kernel_%d_params", idx);
    assert(sym != NULL);
    kernel_params_t *kernel_params = dlsym(lib, sym_name);
    assert(kernel_params != NULL);
    if (options.debug) {
      fprintf(stderr, "Params %s:\n", sym_name);
      print_kernel_params(kernel_params, stderr);
    }
    fprintf(stderr, "Running benchmark_kernel_%d (%ld x %ld insts) for %ld iters:\n",
	    idx, kernel_params->unroll_factor, kernel_params->kernel_iterations, params->num_iterations);
    rand_init(init_area, params->init_area_size, 42 /* seed */);
    memset(ldst_area, 0, params->load_store_area_size);
    if (params->has_perf_lib) {
      memset(results, 42, sizeof(*results) * num_events * params->num_iterations);
    }
    int retval = sym(perf_event_set, num_events, results, init_area, ldst_area, ldst_area + params->store_area_offset);
    assert(retval == 0);
    for (unsigned j = 0; j < num_iters; j++) {
      for (unsigned i = 0; i < num_events; i++) {
	printf("%-50s %10lli\n", event_names[i], results[j * num_events + i]);
      }
    }
    fflush(stdout);
    if (options.benchmark_idx >= 0)
      break;
    idx++;
  }
  dlclose(lib);
  if (lib_name == tempfile) {
    unlink(tempfile);
  }
  
  free(results);
  if (tempfile != NULL) {
    free(tempfile);
  }
  return 0;
}
