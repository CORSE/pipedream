benchmak-loader
===============

Harness to create a wrapper calling the first function of a Pipedream-generated
microbenchmark library.

# Author
- Nicolas Derumigny <nicolas.derumigny@inria.fr>

# Usage
Place one or more Pipedream-generated assembly files (`.s` format) in the directory,
then run:
- `make papi` to compile and link the harness with PAPI.
- `make perfpipedream` to compile and link the harness with perf-pipedream
It will create one instrumented binary executable for each assembly file present
in the directory.
- `python gen-lib <arch>` will create a library .s file for testing purposes (a target also
exists in the makefile fort that).
- Appending `-aarch64` will cross-compile for an Arm target.

Counters can be specified by uncommenting/adding elements to the array `EVENT_NAMES` in
`benchmark_loader.c`.
