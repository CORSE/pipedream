import sys
import subprocess
import wget  # type:ignore
from enum import Enum
from pathlib import Path
import shutil
import argparse
import logging
from typing import List, Dict, Any, Tuple, Optional, Set
from types import SimpleNamespace as ns
import re

logger = logging.getLogger()

URL = "https://sourceware.org/git?p=binutils-gdb.git;a=blob_plain;f=opcodes/aarch64-tbl.h;hb=f35674005e609660f5f45005a9e095541ca4c5fe"  # binutils tag binutils-2_36_1
# https://sourceware.org/git/?p=binutils-gdb.git;a=blob_plain;f=opcodes/aarch64-opc-2.c
# may be usefull too (signification of `op_types` field)
INFILE = "aarch64-tbl.h"
OUTFILE = "instructions_binutils.py"

UNSUPPORTED_SET = {"ldstpair_indexed"}
UNSUPPORTED_INS = {"stg", "stzg", "st2g", "stz2g", "casp", "caspa", "caspl", "caspal"}
UNSUPPORTED_INS_PFX = {}

op_class_to_mem_width_table = dict(S_B=8, S_H=16, S_S=32, S_D=64, S_Q=128)

# Map from role name to regexp for mnemonic when role must be considered a definition
re_any = re.compile(r"")
role_reg_def_map = {
    "Rd": re_any,
    "Ed": re_any,
    "Fd": re_any,
    "Vd": re_any,
    "Sd": re_any,
    "VdD1": re_any,
    "SVE_Pd": re_any,
    "SVE_Zd": re_any,
    "Rd_SP": re_any,
    "Rt": re.compile(r"^(swp|ld)"),
    "Rt2": re.compile(r"^(ld)"),
    "Rs": re.compile(r"^(cas)"),
    "Ft": re.compile(r"^(ld)"),
    "Ft2": re.compile(r"^(ld)"),
    "LVt": re.compile(r"^(ld)"),
    "LEt": re.compile(r"^(ld)"),
}


def brace_split(text, split_on=","):
    """A variant of `str.split` that respects braces"""

    def events(text):
        pos = 0

        while True:
            next_obrace = text.find("{", pos)
            next_cbrace = text.find("}", pos)
            next_split = text.find(split_on, pos)
            res_set = set([next_obrace, next_cbrace, next_split])
            res_set.discard(-1)
            if not res_set:  # no next event
                break
            next_event = min(res_set)

            if next_event == next_obrace:
                yield (next_event, "{")
            elif next_event == next_cbrace:
                yield (next_event, "}")
            else:
                yield (next_event, "")
            pos = next_event + 1

    last_split_pos = 0
    open_brace = 0

    for pos, ev_type in events(text):
        if ev_type == "{":
            open_brace += 1
        elif ev_type == "}":
            open_brace -= 1
            if open_brace < 0:
                raise Exception("Unmatched brace (col {}) in <{}>".format(pos, text))
        elif open_brace == 0:
            yield text[last_split_pos:pos]
            last_split_pos = pos + len(split_on)
    last_chunk = text[last_split_pos:]
    if last_chunk:
        yield last_chunk


def get_reg_list_size(details: Set[str]) -> int:
    for size in range(1, 5):
        if f"F_OD({size})" in details:
            return size
    return 0


def get_reg_has_extend(bin_op_name: str) -> bool:
    return bin_op_name.endswith("_EXT")


def get_reg_has_shift(bin_op_name: str) -> bool:
    return bin_op_name.endswith("_SFT")


def convert_register_name(
    binop_qlf_name: str, binop_op_name: str, details: Set[str]
) -> ns:
    reg_type = None
    reg_name = binop_op_name
    reg_modifier = None
    s_spec = None
    if binop_qlf_name.startswith("S_"):
        s_spec = binop_qlf_name[len("S_") :]
    if binop_op_name in ["En", "Em", "Ed", "LEt"]:
        reg_type = f"V{s_spec}"
    elif binop_op_name in ["Em16"]:
        reg_type = f"V{s_spec}_16"
    elif binop_op_name in ["VnD1", "VdD1"]:
        reg_type = f"VxD1"
    elif binop_op_name.startswith("SVE_Z"):
        if binop_op_name in [
            "SVE_Zn_INDEX",
            "SVE_Zm3_INDEX",
            "SVE_Zm4_INDEX",
            "SVE_Zm3_11_INDEX",
            "SVE_Zm4_11_INDEX",
            "SVE_Zm3_22_INDEX",
        ]:
            reg_type = f"Z{s_spec}"
        else:
            reg_type = f"Z_{s_spec}"
    elif binop_op_name.startswith("SVE_V") or binop_op_name.startswith("SVE_VZ"):
        reg_type = f"{s_spec}"
    elif binop_op_name in ["Fd", "Fn", "Fm", "Fa", "Ft", "Ft2", "Sd", "Sn", "Sm"]:
        reg_type = f"{s_spec}"
    elif binop_op_name.startswith("SVE_P"):
        if binop_qlf_name in ["P_Z", "P_M"]:
            reg_type, reg_name, reg_modifier = (
                "P",
                f"{binop_op_name}_{binop_qlf_name[-1]}",
                binop_qlf_name[-1],
            )
        elif binop_qlf_name == "NIL" or s_spec is not None:
            reg_type, reg_modifier = f"P_{s_spec}" if s_spec is not None else "P", None
    elif binop_op_name.startswith("SYSREG") and binop_qlf_name == "NIL":
        reg_type = "SYSREG"
    elif (
        binop_op_name.startswith("V") or binop_op_name.startswith("LV")
    ) and binop_qlf_name in [
        "V_8B",
        "V_16B",
        "V_4H",
        "V_8H",
        "V_4S",
        "V_2D",
        "V_2S",
        "V_1Q",
        "V_1D",
        "V_2H",
    ]:
        reg_type = binop_qlf_name
    elif binop_op_name.startswith("CR") and binop_qlf_name in ["CR"]:
        reg_type = binop_qlf_name
    elif binop_qlf_name in ["W", "X"]:
        if binop_op_name.endswith("_SP"):
            reg_type = f"{binop_qlf_name}_SP"
        else:
            reg_type = binop_qlf_name
    elif binop_qlf_name in ["WSP", "SP"]:
        reg_type = binop_qlf_name
    assert (
        reg_type is not None
    ), f"Unexpected (qlf, op) pair: {binop_qlf_name}, {binop_op_name}"

    reg_list_size = get_reg_list_size(details)
    has_extend = get_reg_has_extend(binop_op_name)
    has_shift = get_reg_has_shift(binop_op_name)

    return ns(
        reg_class=reg_type,
        reg_name=reg_name,
        reg_modifier=reg_modifier,
        n_regs=reg_list_size,
        has_extend=has_extend,
        has_shift=has_shift,
    )


def convert_addr_name(
    binop_qlf_name: str, binop_op_name: str
) -> Tuple[str, str, Optional[int]]:
    addr_class = None
    addr_name = None
    mem_width = op_class_to_mem_width_table.get(binop_qlf_name)

    s_spec = None
    if binop_qlf_name.startswith("S_"):
        s_spec = binop_qlf_name[len("S_") :]

    if binop_op_name.startswith("ADDR_") or binop_op_name.startswith("SIMD_ADDR_"):
        addr_name = addr_class = binop_op_name
    elif binop_op_name.startswith("SVE_ADDR_ZZ"):
        addr_name = addr_class = (
            f"{binop_op_name}_{s_spec}" if s_spec is not None else binop_op_name
        )
        mem_width = None
    elif binop_op_name.startswith("SVE_ADDR_"):
        addr_name = addr_class = (
            f"{binop_op_name}_{s_spec}" if s_spec is not None else binop_op_name
        )
        mem_width = None

    assert (
        addr_class is not None
    ), f"Unexpected (qlf, op) pair for addr: {binop_qlf_name}, {binop_op_name}"
    assert addr_name is not None
    return addr_class, addr_name, mem_width


def parse_operands_str(operands_str, is_type: bool) -> List[List[str]]:
    """Parse an instruction's operands"""

    operands_str = operands_str.strip()
    assert operands_str[0] == "{" and operands_str[-1] == "}"
    op_body = operands_str[1:-1].strip()
    if len(op_body) > 0 and op_body[-1] == ",":
        op_body = op_body[:-1]
    if not op_body:
        return []
    ops = op_body.split(", ")
    new_ops: List[List[str]] = []
    for op in ops:
        real_op = op
        if op.startswith("{"):
            new_ops.append([])
            real_op = real_op[1:]

        if op.endswith("}"):
            real_op = real_op[:-1]
        if is_type:
            real_op = real_op[len("AARCH64_OPND_") :]
        else:
            real_op = real_op[len("AARCH64_OPND_QLF_") :]
        if is_type:
            new_ops.append(real_op)
        else:
            new_ops[-1].append(real_op)
    return new_ops


class ADDR_TYPE(Enum):
    BR = 0
    MEM_R = 1
    MEM_W = 2
    MEM_RW = 3
    MEM_PRF = 4
    ARITH = 5


def mnemonic_addr_type(mnemonic: str) -> ADDR_TYPE:
    if mnemonic[0] == "b" or mnemonic[:2] in ["tb", "cb"]:
        return ADDR_TYPE.BR
    if mnemonic[:2] in ["ld"]:
        return ADDR_TYPE.MEM_R
    if mnemonic[:2] in ["st"]:
        return ADDR_TYPE.MEM_W
    if mnemonic[:3] in ["cas", "swp"]:
        return ADDR_TYPE.MEM_RW
    if mnemonic[:3] in ["prf"]:
        return ADDR_TYPE.MEM_PRF
    if mnemonic[:3] in ["adr"]:
        return ADDR_TYPE.ARITH
    assert False, f"unexpected addr type for mnemonic {mnemonic}"


class OP_TYPE(Enum):
    REG = 0
    IMM = 1
    IMM_VLS = 2
    ADDR = 3
    FLAG = 4
    REGLIST = 5
    NO_OP = 10
    OTHER = 11


def parse_operands_role(
    list_op_roles: List[str], mnemonic: str
) -> List[Tuple[OP_TYPE, List[str]]]:
    global role_reg_def_map
    list_carac: List[Tuple[OP_TYPE, List[str]]] = []
    for role in list_op_roles:
        role_re = role_reg_def_map.get(role)
        if role_re and role_re.match(mnemonic):
            list_carac.append((OP_TYPE.REG, [role, "W", "EXPLICIT"]))
        elif (
            role.startswith("ADDR_")
            or role.startswith("SIMD_ADDR_")
            or role.startswith("SVE_ADDR_")
        ):
            addr_type = mnemonic_addr_type(mnemonic)
            action = {
                ADDR_TYPE.MEM_R: "R",
                ADDR_TYPE.MEM_W: "W",
                ADDR_TYPE.MEM_RW: "RW",
                ADDR_TYPE.MEM_PRF: "R",
            }.get(addr_type, "")
            list_carac.append((OP_TYPE.ADDR, [role, action, "EXPLICIT"]))
        elif role in {
            "IMM_VLSR",
            "IMM_VLSL",
        }:
            list_carac.append((OP_TYPE.IMM_VLS, [role, "EXPLICIT"]))
        elif "IMM" in role or role in {
            "IDX",
            "WIDTH",
            "NZCV",
            "COND",
            "COND1",
            "FBITS",
            "HALF",
            "BIT_NUM",
            "MASK",
            "LIMM",
            "SVE_I1_HALF_ONE",
            "SVE_I1_HALF_TWO",
            "SVE_I1_ZERO_ONE",
            "SVE_FPIMM8",
            "SVE_PATTERN",
            "SVE_PATTERN_SCALED",
            "SVE_PRFOP",
            # FIXME: this would actually need separate instruction entries
            # depending on the prefetch operation
            "PRFOP",
        }:
            list_carac.append((OP_TYPE.IMM, [role, "EXPLICIT"]))
        elif role in ["ZVE_ZnxN"]:
            list_carac.append((OP_TYPE.REGLIST, [role, "R", "EXPLICIT"]))
        elif (
            (role[0] in {"R", "E", "V", "F", "S", "L"} and len(role) < 5)
            or role.startswith("SYSREG")
            or role.startswith("SVE_V")
            or role.startswith("SVE_Z")
            or role.startswith("SVE_P")
            or role.startswith("SVE_R")
            or role.startswith("SVE_ADDR")
            or role
            in {
                "LVt_AL",
                "Rn_SP",
                "Rm_SP",
                "Rm_SFT",
                "Rm_EXT",
                "Rt_SP",
                "Rt_LS64",
                "Rt_SYS",
                # Do not care, unsupported
                "CRn",
                "CRm",
            }
        ):
            list_carac.append((OP_TYPE.REG, [role, "R", "EXPLICIT"]))
        elif role in {"PAIRREG"}:
            # FIXME: this will not work because RegAlloc will never take this register
            # into account and R/W are broken
            list_carac.append((OP_TYPE.REG, [role, "R", "IMPLICIT"]))
        elif role in {}:
            list_carac.append((OP_TYPE.FLAG, [role, "IMPLICIT"]))
        else:
            list_carac.append((OP_TYPE.OTHER, [role]))
    return list_carac


def parse_operands(
    list_op_classes: List[List[Optional[str]]],
    op_roles: List[str],
    tied: int,
    details: Set[str],
    mnemonic: str,
) -> List[List[str]]:
    op_carac = parse_operands_role(op_roles, mnemonic)
    if not all(
        [len(list_op_classes[i]) == len(op_carac) for i in range(len(list_op_classes))]
    ):
        assert all(
            [
                len(list_op_classes[i]) == len(list_op_classes[0])
                for i in range(len(list_op_classes))
            ]
        )
        for idx, list_ops in enumerate(list_op_classes):
            if (len(op_carac) - len(list_op_classes[idx])) > 0:
                list_ops += [None] * (len(op_carac) - len(list_op_classes[idx]))
            else:
                op_carac += [(OP_TYPE.NO_OP, ["SUPPRESSED"])] * (
                    len(list_op_classes[idx]) - len(op_carac)
                )
                break

    assert all(
        [len(list_op_classes[i]) == len(op_carac) for i in range(len(list_op_classes))]
    )

    ret: List[List[str]] = []
    for op_classes in list_op_classes:
        new_operands: List[str] = []
        for op_idx, (op_class, (op_type, list_carac)) in enumerate(
            zip(op_classes, op_carac)
        ):
            if op_type == OP_TYPE.REG or op_type == OP_TYPE.REGLIST:
                assert op_class is not None
                reg_op = convert_register_name(op_class, list_carac[0], details)
                modifier_str = (
                    f', modifier="{reg_op.reg_modifier}"'
                    if reg_op.reg_modifier is not None
                    else ""
                )
                n_regs_str = f", n_regs={reg_op.n_regs}" if reg_op.n_regs else ""
                extend_str = f", has_extend=True" if reg_op.has_extend else ""
                shift_str = f", has_shift=True" if reg_op.has_shift else ""
                tied_str = f", tied={tied}" if tied > 0 and op_idx == 0 else ""
                tied_suffix = f"_T{tied}" if tied > 0 and op_idx == 0 else ""
                new_operands.append(
                    f'reg_op(name="{reg_op.reg_name}{tied_suffix}", reg_class=registers.{reg_op.reg_class}, '
                    f"action={list_carac[1]}, visibility={list_carac[2]}"
                    f"{tied_str}{modifier_str}{n_regs_str}{extend_str}{shift_str})"
                )
            elif op_type == OP_TYPE.ADDR:
                assert op_class is not None
                role, action, visibility = list_carac
                addr_class, addr_name, mem_width = convert_addr_name(op_class, role)
                action_str = f", action={action}" if action != "" else ""
                mem_width_str = (
                    f", memory_width={mem_width}" if mem_width is not None else ""
                )
                new_operands.append(
                    f'addr_op(name="{addr_name}", '
                    f"addr_class=operands.{addr_class}, "
                    f"visibility={visibility}"
                    f"{action_str}{mem_width_str})"
                )
            elif op_type == OP_TYPE.IMM:
                imm_class = op_class
                # Binop using another encoding here
                if (
                    list_carac[0]
                    in {
                        "COND1",
                        "FPIMM0",
                        "ADDR_SIMM9",
                        "ADDR_SIMM13",
                        "ADDR_SIMM19",
                        "ADDR_UIMM12",
                        "ADDR_SIMM7",
                    }
                    or imm_class == "imm_tag"
                ):
                    imm_class = list_carac[0]
                if imm_class is not None and imm_class.startswith("imm_"):
                    # Under the form imm_<min>_<max>
                    imm_min_max = imm_class.split("_")
                    min_val, max_val = [int(m) for m in imm_min_max[1:]]
                    new_operands.append(
                        f'imm_op(name="{list_carac[0]}", '
                        f"imm_class=operands.{list_carac[0]}, "
                        f"visibility={list_carac[1]}, min_val={min_val}, "
                        f"max_val={max_val})"
                    )
                else:
                    if imm_class is None or imm_class == "NIL":
                        imm_class = f"{list_carac[0]}"
                    if imm_class in {"LSL", "LSR", "MSL"}:
                        imm_class = f"IMM_{imm_class}"
                    imm_class = f"operands.{imm_class}"
                    new_operands.append(
                        f'imm_op(name="{list_carac[0]}", imm_class={imm_class}, '
                        f"visibility={list_carac[1]})"
                    )
            elif op_type == OP_TYPE.IMM_VLS:
                # reg_class = convert_register_name(op_class, list_carac[0])
                new_operands.append(
                    f'imm_op(name="{list_carac[0]}", '
                    f"imm_class=operands.{list_carac[0]}, "  # reg_class=registers.{reg_class}, "
                    f"visibility={list_carac[1]})"
                )
            elif op_type == OP_TYPE.FLAG:
                new_operands.append(
                    f'flags_op(name="{list_carac[0]}", visibility={list_carac[1]})'
                )
            elif op_type == OP_TYPE.OTHER:
                raise Exception(f"Unsupported operand type: {list_carac[0]}")
            elif op_type == OP_TYPE.NO_OP:
                pass
            else:
                raise Exception("Wrong OP_TYPE")
        ret.append(new_operands)
    return ret


def clean_instruction_dict(instruction: Dict) -> Dict[str, Any]:
    instruction["extension"] = instruction["extension"][len("&aarch64_feature_") :]
    if instruction["insn_class"][0].isnumeric():
        instruction["insn_class"] = "_" + instruction["insn_class"]

    mnemonic = instruction["mnemonic"]
    operands = instruction.pop("operands")
    op_roles = instruction.pop("op_types")
    details = instruction["details"]
    tied = instruction["tied"]
    instruction["operands"] = parse_operands(
        operands, op_roles, tied, details, mnemonic
    )

    return instruction


def parse_line(line: str, features_sets: Dict) -> Optional[Tuple[List, Dict]]:
    # move is present in two variants, discarding one
    if line.startswith('  { "mov", 0x52800000, 0x7f800000'):
        return None

    line = line.strip()
    if not line:
        return None
    if not line.endswith("},"):
        raise Exception("Invalid end of line: <{}>".format(line))
    if not line.startswith("{"):
        raise Exception("Invalid start of line: <{}>".format(line))

    line_split = list(map(lambda x: x.strip(), brace_split(line[1:-2])))
    if line_split[3] in UNSUPPORTED_SET:
        return None
    assert len(line_split) == 12
    mnemonic = line_split[0][1:-1]  # strip quotes
    if mnemonic in UNSUPPORTED_INS:
        return None
    for pfx in UNSUPPORTED_INS_PFX:
        if mnemonic.startswith(pfx):
            return None
    details = set([x.strip() for x in line_split[8].split("|")]) - set(["0"])
    constraints = set([x.strip() for x in line_split[9].split("|")]) - set(["0"])
    tied = int(line_split[10])
    logger.debug(line_split)
    return line_split, clean_instruction_dict(
        {
            "mnemonic": mnemonic,
            "insn_class": line_split[3],
            "extension": line_split[5],
            "op_types": parse_operands_str(line_split[6], True),
            "operands": parse_operands_str(line_split[7], False),
            "tied": tied,
            "details": details,
            "constraints": constraints,
        }
    )


def generate_header(file, instruction_list: List[Dict], features_sets: Dict) -> None:
    file.write(
        "## This file is derived from the original ARM database of GNU binutils,\n"
        "## available at https://www.gnu.org/software/binutils/\n\n"
    )
    file.write(
        "from pipedream.asm.armv8a import registers, flags, operands\n"
        "from pipedream.asm.ir import Use_Def, Operand_Visibility\n\n"
        "import enum\n"
        "from typing import *\n\n"
    )
    file.write(
        "## read/write\n"
        "R   = Use_Def.USE\n"
        "W   = Use_Def.DEF\n"
        "RW  = Use_Def.USE_DEF\n\n"
        "## operand visibility\n"
        "EXPLICIT   = Operand_Visibility.EXPLICIT\n"
        "IMPLICIT   = Operand_Visibility.IMPLICIT\n"
        "SUPPRESSED = Operand_Visibility.SUPPRESSED\n\n"
    )
    file.write("class ISA(enum.Enum):\n")
    isa = sorted(set([inst["insn_class"] for inst in instruction_list]))
    for ext in isa:
        file.write(f"\t{ext} = '{ext if ext[0] != '_' else ext[1:]}'\n")

    file.write("\nclass ISA_Extension(enum.Enum):\n")
    isa_ext = sorted(set([inst["extension"] for inst in instruction_list]))
    for ext in isa_ext:
        file.write(f"\t{ext} = '{ext}'\n")

    file.write("\nclass ISA_Feature(enum.Enum):\n")
    features: Set = set().union(
        *[features_sets[inst["extension"]] for inst in instruction_list]
    )
    for feat in sorted(features):
        file.write(f"\t{feat} = '{feat}'\n")

    file.write("\nISA_Feature_Set = {\n")
    isa = sorted(set([inst["extension"] for inst in instruction_list]))
    for ext in isa:
        fset = "|".join(sorted(features_sets[ext]))
        file.write(f"\tISA_Extension.{ext}: '{fset}',\n")
    file.write("}\n")

    file.write("\n\n")
    file.write(
        "# type: ignore\n\n"
        "def make_instruction_database(*, make_instruction, reg_op, "
        "addr_op, imm_op, flags_op):\n"
    )


def generate_make_instr(file, mnemonic, insn_class, extension, operands):
    file.write(
        "\tmake_instruction(\n"
        f'\t\tmnemonic = "{mnemonic}",\n'
        f"\t\tisa_set = ISA.{insn_class},\n"
        f"\t\tisa_extension = ISA_Extension.{extension},\n"
        f"\t\tisa_features = ISA_Feature_Set[ISA_Extension.{extension}],\n"
        f"\t\toperands = [\n{operands}\t\t],\n\t)\n"
    )


def generate_instruction_file() -> None:
    parser = argparse.ArgumentParser(
        description="Process binutils aarch64-tbl.h and generate instructions database."
    )
    parser.add_argument(
        "--force-download",
        action="store_true",
        help="force download of binutils file even if present",
    )
    parser.add_argument("--debug", action="store_true", help="debug mode")
    args = parser.parse_args()

    logging.basicConfig()
    logger.setLevel(logging.INFO)
    if args.debug:
        logger.setLevel(logging.DEBUG)

    if args.force_download and Path(INFILE).is_file():
        shutil.move(INFILE, f"{INFILE}.bak")
    if not Path(INFILE).is_file():
        logger.debug(f"Downloading binutil's description: {URL}")
        resp = wget.download(URL)
        assert resp == INFILE, f"Unexpected response: {resp}"
    assert Path(INFILE).is_file

    with open(INFILE, "r") as f:
        lines = f.readlines()

    # Removing includes and defines of "OPx" as it will output a preprocessor
    with open(INFILE, "w") as f:
        for l in lines:
            if not l.startswith("#include") and not l.startswith("#error"):
                f.write(l)

    # Running the preprocessor to handle #defines
    infile_processed = INFILE + ".processed"
    subprocess.run(["cpp", INFILE, "-o", infile_processed])
    instruction_decl_list: List = []
    feature_set_decl_list: List = []
    with open(infile_processed, "r") as f:
        # 0 = no, 1 = first line ("{"), 2 = yes
        inside_aarch64_feature_set = 0
        inside_aarch64_decl = 0
        for line in f:
            if inside_aarch64_decl == 2:
                # Last line is `{0, 0, 0, 0, 0, 0, {}, {}, 0},\n`
                if line.startswith("  {0, 0, 0"):
                    inside_aarch64_decl = 0
                    continue
                instruction_decl_list.append(line)
            elif inside_aarch64_decl == 1 or line.startswith("struct aarch64_opcode"):
                inside_aarch64_decl += 1
            elif inside_aarch64_feature_set == 0 and line.startswith(
                "static const aarch64_feature_set"
            ):
                inside_aarch64_feature_set += 1
                feature_set = ""
            if inside_aarch64_feature_set:
                feature_set += line.strip()
                if feature_set[-1] == ";":
                    feature_set_decl_list.append(feature_set)
                    inside_aarch64_feature_set = 0

    feature_sets: Dict[str, Set] = {}
    rex = re.compile(
        r".*aarch64_feature_set aarch64_feature_(\w*)\s*=\s*AARCH64_FEATURE\s*\(([^,]*), 0\);"
    )
    for decl in feature_set_decl_list:
        m = rex.match(decl)
        assert m is not None, f"Unexpected feature set declaration: {decl}"
        name, sets = m.groups()
        fset = set([f.strip().replace("AARCH64_FEATURE_", "") for f in sets.split("|")])
        feature_sets[name] = fset
        logger.debug(f"Feature set: {name}: {fset}")

    instruction_list: List = []
    for line in instruction_decl_list:
        ret = parse_line(line, feature_sets)
        if ret is not None:
            logger.debug(f"I {ret}")
            _, instruction = ret
            instruction_list.append(instruction)

    unique_instrs: Set = set()
    with open(OUTFILE, "w") as f:
        generate_header(f, instruction_list, feature_sets)

        for arguments in instruction_list:
            name = arguments["mnemonic"]
            # in practice some instructions like dsb are duplicates
            # they differ because of ISA extension but have same name
            if len(arguments["operands"]) == 0 and (name not in unique_instrs):
                generate_make_instr(
                    f,
                    arguments["mnemonic"],
                    arguments["insn_class"],
                    arguments["extension"],
                    operands="",
                )
                unique_instrs.add(name)

            for ops in arguments["operands"]:
                operands = "".join([f"\t\t\t{op},\n" for op in ops])
                generate_make_instr(
                    f,
                    arguments["mnemonic"],
                    arguments["insn_class"],
                    arguments["extension"],
                    operands,
                )
                if (
                    arguments["mnemonic"] == "b"
                ):  # adding not-zero variation of `branch`
                    generate_make_instr(
                        f,
                        arguments["mnemonic"] + "ne",
                        arguments["insn_class"],
                        arguments["extension"],
                        operands,
                    )


def get_name(arguments: Dict, operands: str) -> str:
    return "_".join((arguments["mnemonic"], operands)).upper()


if __name__ == "main":
    generate_instruction_file()
