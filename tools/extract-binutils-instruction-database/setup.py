#!/usr/bin/env python3

from setuptools import setup, find_packages  # type:ignore
import sys


def parse_requirements():
    reqs = []
    with open("requirements.txt", "r") as handle:
        for line in handle:
            if line.startswith("-"):
                continue
            reqs.append(line)
    return reqs


setup(
    name="extract-binutils-db",
    version="0.0.2",
    description="",
    author="CORSE",
    license="LICENSE",
    url="https://gitlab.inria.fr/CORSE/pipedream",
    packages=find_packages(),
    include_package_data=True,
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    entry_points={
        "console_scripts": [
            (
                "extract-arm-instructions=extract_binutils_db.extract_binutils:generate_instruction_file"
            ),
        ]
    },
)
