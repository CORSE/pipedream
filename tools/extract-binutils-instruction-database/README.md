# Extract-binutils-instruction-database

Simple tool to create a Pipedream representation from the ARMv8a description written in `binutils`
source files.

## Authors
* Nicolas Derumigny
* Théophile Bastian

## Dependencies
* `cpp` the C Preprocessor
* `Python` >= 3.8 with all packages listed in `requirements.txt`

## Installing
It is recommended to install the python dependencies and modules inside a
virtualenv:

```bash
    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt
    pip install -e .
```

## Usage
Execute
```bash
    extract-arm-instructions
    cp instructions_binutils.py ../../src/pipedream/asm/armv8a/
```
This will create the `instructions-binutils.py` file required for ARMv8a compatibility of Pipedream.

## Development
Before commiting encure that mypy is working correctly:
```bash
    mypy extract_binutils_db
    Success: no issues found in 2 source files
```
