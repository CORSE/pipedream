///
/// Small helper classes
///

#pragma once

#include "config.hpp"

#include <utility>  /// for std::initializer_list
#include <tuple>    /// for std::tie
#include <vector>   /// for std::vector
#include <set>      /// for std::set
#include <map>      /// for std::map
#include <iostream> /// for std::ostream
#include <memory>   /// for std::unique_ptr
#include <variant>  /// for std::variant

/// Simple version of std::span
/// @todo Replace with std::span
template<typename T>
struct Span {
  using const_iterator = const T*;

  Span() : _size{0}, _data{0} {}

  constexpr Span(const std::initializer_list<T> v) : _size{v.size()}, _data{v.begin()} {}

  Span(const std::vector<T> &v) : _size{v.size()}, _data{v.data()} {}

  Span(size_t size, const T *data) : _size{size}, _data{data} {}

  Span(const T *bgn, const T *end) : _size{end - bgn}, _data{bgn} {}

  /// initialize from array
  /// warning for string literals this INCLUDES the zero terminator.
  template<size_t N>
  constexpr Span(const T(&array)[N]) : _size{N}, _data{array} {}

  const T &operator[](size_t idx) const {
    assert(idx < size());
    return _data[idx];
  }

  const T &front() const {
    assert(!empty());
    return *begin();
  }

  Span drop_front() const {
    return empty() ? *this : Span(_size - 1, _data + 1);
  }

  bool contains(const T &t) const {
    for (const T &elem : *this) {
      if (elem == t) { return true; }
    }
    return false;
  }

  const T *data() const { return _data; }

  const_iterator begin() const { return _data; }
  const_iterator end() const { return _data + _size; }

  size_t size() const { return _size; }

  bool empty() const { return size() == 0; }

  /// @name comparison
  /// @{

  bool operator==(Span<T> that) const {
    return std::equal(begin(), end(), that.begin(), that.end());
  }

  bool operator!=(Span<T> that) const {
    return !std::equal(begin(), end(), that.begin(), that.end());
  }

  bool operator<(Span<T> that) const {
    return std::lexicographical_compare(begin(), end(), that.begin(), that.end());
  }

  /// @}
private:
  size_t _size;
  const T *_data;
};

/// A pair of iterators that can be iterated over with a C++11 range-based for loop.
template<typename Iterator, typename Sentinel = Iterator>
struct Iterator_Range {
  using iterator   = Iterator;
  using sentinel_t = Sentinel;

  Iterator_Range(Iterator bgn, Sentinel end) : _bgn{bgn}, _end{end} {}

  Iterator begin() const { return _bgn; }
  Sentinel end() const { return _end; }

  void operator++() {
    assert(!empty());
    ++_bgn;
  }

  bool empty() const { return _bgn == _end; }

  bool operator==(const Iterator_Range &that) const { return std::tie(_bgn, _end) == std::tie(that._bgn, that._end); }
  bool operator!=(const Iterator_Range &that) const { return std::tie(_bgn, _end) != std::tie(that._bgn, that._end); }
private:
  Iterator _bgn;
  Sentinel _end;
};


/// Check if @p str starts with @p prefix
static inline bool startswith(const std::string &str, const std::string &prefix) {
  return str.compare(0, prefix.size(), prefix) == 0;
}


////////////////////////////////////////////////////////////////////////////////
/// @name I/O helpers
/// @{



template<typename T>
std::ostream &operator<<(std::ostream &O, const std::set<T> &s) {
  O << "{";

  auto it = s.begin();
  const auto end = s.end();

  if (it != end) {
    O << *it;
    ++it;

    for (; it != end; ++it) {
      O << ", " << *it;
    }
  }

  O << "}";
  return O;
}

template<typename T>
std::ostream &operator<<(std::ostream &O, const std::vector<T> &s) {
  O << "[";

  auto it = s.begin();
  const auto end = s.end();

  if (it != end) {
    O << *it;
    ++it;

    for (; it != end; ++it) {
      O << ", " << *it;
    }
  }

  O << "]";
  return O;
}

namespace details {
  template<std::size_t...> struct seq{};

  template<std::size_t N, std::size_t... Is>
  struct gen_seq : gen_seq<N-1, N-1, Is...>{};

  template<std::size_t... Is>
  struct gen_seq<0, Is...> : seq<Is...>{};

  template<typename Tuple, std::size_t... Is>
  void print_tuple(std::ostream& O, Tuple const& t, seq<Is...>){
    using swallow = int[];
    (void)swallow{0, (void(O << (Is == 0? "" : ", ") << std::get<Is>(t)), 0)...};
  }
} // end namespace details

template<typename... Args>
std::ostream &operator<<(std::ostream &O, std::tuple<Args...> const&t) {
  O << "(";
  details::print_tuple(O, t, details::gen_seq<sizeof...(Args)>());
  return O << ")";
}

/// @}