///
/// Helpers for working with Intel XED
///

#pragma once

#include "config.hpp"

#include <cassert>
#include <optional>
#include "utils.hpp"
extern "C" {
#include "xed/xed-interface.h"
}

using Register_Class = Span<xed_reg_enum_t>;

enum Effective_Operand_Width {
  EOSZ_16 = 16,
  EOSZ_32 = 32,
  EOSZ_64 = 64,
};

enum Effective_Address_Width {
  EASZ_16 = 16,
  EASZ_32 = 32,
  EASZ_64 = 64,
};

/// X86 CPU mode
struct XED_CPU_Mode {
  static inline constexpr XED_CPU_Mode make(xed_machine_mode_enum_t mmode,
                                            xed_address_width_enum_t stack_addr_width) {
    const xed_state_t cpu_mode = {
      .mmode            = mmode,
      .stack_addr_width = stack_addr_width,
    };
    return XED_CPU_Mode{cpu_mode};
  }

  explicit constexpr XED_CPU_Mode(xed_state_t state) : _state{state} {}

  /// @name observers
  /// @{

  xed_machine_mode_enum_t mode() const {
    return _state.mmode;
  }

  const char *mode_str() const {
    return xed_machine_mode_enum_t2str(mode());
  }

  xed_address_width_enum_t stack_addr_width() const {
    return _state.stack_addr_width;
  }

  const char *stack_addr_width_str() const {
    return xed_address_width_enum_t2str(stack_addr_width());
  }

  /// @}

  /// @name comparators
  /// @{

  bool operator<(XED_CPU_Mode that) const {
    return std::tie(_state.mmode, _state.stack_addr_width) < std::tie(that._state.mmode, that._state.stack_addr_width);
  }
  bool operator==(XED_CPU_Mode that) const {
    return std::tie(_state.mmode, _state.stack_addr_width) == std::tie(that._state.mmode, that._state.stack_addr_width);
  }
  bool operator!=(XED_CPU_Mode that) const {
    return std::tie(_state.mmode, _state.stack_addr_width) != std::tie(that._state.mmode, that._state.stack_addr_width);
  }

  /// @}

  /// @name direct XED API access
  /// @{

  xed_state_t raw() const { return _state; }

  /// @}
private:
  xed_state_t _state;
};

struct XED_Encoder_State {
  XED_CPU_Mode cpu_mode;
  Effective_Operand_Width effective_operand_width;
  Effective_Address_Width effective_address_width;
  bool allow_rex;

  unsigned cpu_mode_bits() const {
    switch (cpu_mode.mode()) {
      case XED_MACHINE_MODE_REAL_16:
      case XED_MACHINE_MODE_LONG_COMPAT_16:
      case XED_MACHINE_MODE_LEGACY_16:
        return 16;
      case XED_MACHINE_MODE_REAL_32:
      case XED_MACHINE_MODE_LONG_COMPAT_32:
      case XED_MACHINE_MODE_LEGACY_32:
        return 32;
      case XED_MACHINE_MODE_LONG_64:
        return 64;
      case XED_MACHINE_MODE_INVALID:
      case XED_MACHINE_MODE_LAST:
        abort();
    }
    abort();
  }
  unsigned stack_addr_bits() const {
    switch (cpu_mode.stack_addr_width()) {
      case XED_ADDRESS_WIDTH_16b: return 16;
      case XED_ADDRESS_WIDTH_32b: return 32;
      case XED_ADDRESS_WIDTH_64b: return 64;

      case XED_ADDRESS_WIDTH_INVALID:
      case XED_ADDRESS_WIDTH_LAST:
        abort();
    }
    abort();
  }
  unsigned xed_eosz() const {
    switch (effective_operand_width) {
      case EOSZ_16: return 1;
      case EOSZ_32: return 2;
      case EOSZ_64: return 3;
    }
    abort();
  }
  unsigned xed_easz() const {
    switch (effective_address_width) {
      case EASZ_16: return 1;
      case EASZ_32: return 2;
      case EASZ_64: return 3;
    }
    abort();
  }
};


/// One operand of an instruction template (xed_operand_t).
struct XED_Instruction_Operand {
  /// @name fields common to all operand types
  /// @{

  xed_operand_enum_t name() const {
    return xed_operand_name(raw());
  }

  const char *name_str() const {
    return xed_operand_enum_t2str(name());
  }

  xed_operand_type_enum_t type() const {
    return xed_operand_type(raw());
  }

  const char *type_str() const {
    return xed_operand_type_enum_t2str(type());
  }

  xed_operand_element_xtype_enum_t xtype() const {
    return xed_operand_xtype(raw());
  }

  const char *xtype_str() const {
    return xed_operand_element_xtype_enum_t2str(xtype());
  }

  xed_operand_visibility_enum_t visibility() const {
    return xed_operand_operand_visibility(raw());
  }

  const char *visibility_str() const {
    return xed_operand_visibility_enum_t2str(visibility());
  }

  xed_operand_action_enum_t action() const {
    return xed_operand_rw(raw());
  }

  const char *action_str() const {
    return xed_operand_action_enum_t2str(action());
  }

  /// @}

  /// @name register operands
  /// @{

  /// is this a normal register operand (not a base/index/segment register ...)
  bool is_reg_op() const;

  /// is the base register for another MEM0/1 operand of the same instruction
  bool is_base_op() const;

  /// try to look up register used by oprand
  /// Operand can be a normal register or a memory access base/index register, a mask register, ...
  std::optional<xed_reg_enum_t> get_reg(const XED_Encoder_State &state) const;

  std::optional<Register_Class > get_reg_class(const XED_Encoder_State &state) const;

  bool is_fixed_reg() const {
    return xed_operand_type(raw()) == XED_OPERAND_TYPE_REG;
  }

  xed_reg_enum_t get_fixed_reg() const {
    assert(is_fixed_reg());
    return xed_operand_reg(raw());
  }

  const char *fixed_reg_str() const {
    return xed_reg_enum_t2str(get_fixed_reg());
  }

  /// @}

  /// @name memory operands
  /// @{

  bool is_memory_addressing_register() const {
    return xed_operand_is_memory_addressing_register(name());
  }

  bool is_mem0() const {
    return name() == XED_OPERAND_MEM0;
  }

  bool is_base0() const {
    return name() == XED_OPERAND_BASE0;
  }

  bool is_memory_op() const {
    return is_imm_const() && ((name() == XED_OPERAND_MEM0) || (name() == XED_OPERAND_MEM1));
  }

  unsigned memory_operand_index() const {
    switch (name()) {
      case XED_OPERAND_MEM0: return 0;
      case XED_OPERAND_MEM1: return 1;
        /// TODO: XED_OPERAND_AGEN?
      default: abort();
    }
  }

  /// @}

  /// @name address operands (for LEA)
  /// @{

  /// address operand of LEA
  bool is_address_generate_op() const {
    return is_imm_const() && (name() == XED_OPERAND_AGEN);
  }

  /// @}

  /// @name branch target operands
  /// @{

  /// address operand of LEA
  bool is_relative_branch_target() const {
    return name() == XED_OPERAND_RELBR;
  }

  /// @}

  /// @name immediate operands
  /// @{

  bool is_imm_const() const {
    return xed_operand_type(raw()) == XED_OPERAND_TYPE_IMM_CONST;
  }

  bool is_imm0_op() const {
    return is_imm_const() && ((name() == XED_OPERAND_IMM0) || (name() == XED_OPERAND_IMM0SIGNED));
  }

  bool is_imm1_op() const {
    return is_imm_const() && (name() == XED_OPERAND_IMM1);
  }

  uint64_t imm_const() const {
    assert(type() == XED_OPERAND_TYPE_IMM_CONST);
    return xed_operand_imm(raw());
  }

  /// @}

  /// @name lookup fn operands
  /// @{

  bool is_lookup_fn() const {
    return xed_operand_type(raw()) == XED_OPERAND_TYPE_NT_LOOKUP_FN;
  }

  xed_nonterminal_enum_t get_lookup_fn() const {
    assert(is_lookup_fn());
    return xed_operand_nonterminal_name(raw());
  }

  const char *get_lookup_fn_name() const {
    return xed_nonterminal_enum_t2str(get_lookup_fn());
  }

  /// @}

  /// @name misc
  /// @{

  const xed_operand_t* raw() const { return _op; };

  XED_Instruction_Operand(const xed_operand_t *op) : _op{op} {}

  /// @}
private:
  const xed_operand_t *_op;
};

/// A template for a few variations of an x86 instruction (xed_inst_t).
struct XED_Instruction {
  XED_Instruction(const xed_inst_t *inst) : _inst{inst} {
    assert(inst >= xed_inst_table_base());
    assert(inst < (xed_inst_table_base() + XED_MAX_INST_TABLE_NODES));
  }

  XED_Instruction() : XED_Instruction(xed_inst_table_base()) {}

  XED_Instruction(const XED_Instruction&) = default;

  /// @name general instruction info
  /// @{

  xed_iclass_enum_t iclass() const {
    return xed_inst_iclass(_inst);
  }

  const char *iclass_str() const {
    return xed_iclass_enum_t2str(iclass());
  }

  xed_iform_enum_t iform() const {
    return xed_inst_iform_enum(_inst);
  }

  const char *iform_str() const {
    return xed_iform_enum_t2str(iform());
  }

  xed_category_enum_t category() const {
    return xed_inst_category(raw());
  }

  const char *category_str() const {
    return xed_category_enum_t2str(category());
  }

  xed_extension_enum_t extension() const {
    return xed_inst_extension(raw());
  }

  const char *extension_str() const {
    return xed_extension_enum_t2str(extension());
  }

  xed_isa_set_enum_t isa_set() const {
    return xed_inst_isa_set(raw());
  }

  const char *isa_set_str() const {
    return xed_isa_set_enum_t2str(isa_set());
  }

  bool has_lock_prefix() const {
    return has_attribute(XED_ATTRIBUTE_LOCKED);
  }

  bool has_rep_prefix() const {
    return has_attribute(XED_ATTRIBUTE_REP);
  }

  std::set<xed_attribute_enum_t> attributes() const {
    std::set<xed_attribute_enum_t> out;

    for(unsigned i = 0, end = xed_attribute_max(); i < end; i++) {
      const xed_attribute_enum_t attr = xed_attribute(i);

      if (xed_inst_get_attribute(raw(), attr)) {
        out.insert(attr);
      }
    }

    return out;
  }

  bool has_attribute(xed_attribute_enum_t attr) const {
    return xed_inst_get_attribute(raw(), attr);
  }

  std::vector<const char *> attribute_strs() const {
    std::vector<const char *> out;

    for(unsigned i = 0, end = xed_attribute_max(); i < end; i++) {
      xed_attribute_enum_t attr = xed_attribute(i);

      if (has_attribute(attr)) {
        out.push_back(xed_attribute_enum_t2str(attr));
      }
    }

    return out;
  }

  bool is_branch() const {
    switch (category()) {
      case XED_CATEGORY_UNCOND_BR:
      case XED_CATEGORY_COND_BR:
      case XED_CATEGORY_CALL:
      case XED_CATEGORY_RET:
      case XED_CATEGORY_SYSCALL:
      case XED_CATEGORY_SYSRET:
        /// TODO: I'm sure I missed some
        return true;
      default:
        return false;
    }
  }

  /// AT&T asm syntax instruction mnemonic (in uppercase, without the size suffix!)
  const char *att_instruction_name() const {
    return xed_iform_to_iclass_string_att(iform());
  }

  /// @}

  size_t num_operands() const {
    return xed_inst_noperands(_inst);
  }

  XED_Instruction_Operand operand(unsigned idx) const {
    assert(idx < xed_inst_noperands(_inst));
    return XED_Instruction_Operand{xed_inst_operand(_inst, idx)};
  }

  bool has_memory_operands() const {
    for (size_t i = 0, end = num_operands(); i < end; i++) {
      switch (operand(i).name()) {
        case XED_OPERAND_MEM0:
        case XED_OPERAND_MEM1:
          return true;
        case XED_OPERAND_REG0:
        case XED_OPERAND_REG1:
        case XED_OPERAND_REG2:
        case XED_OPERAND_REG3:
        case XED_OPERAND_REG4:
        case XED_OPERAND_REG5:
        case XED_OPERAND_REG6:
        case XED_OPERAND_REG7:
        case XED_OPERAND_REG8:
        case XED_OPERAND_IMM0:
        case XED_OPERAND_IMM1:
        case XED_OPERAND_IMM0SIGNED:
        case XED_OPERAND_AGEN:
        case XED_OPERAND_PTR:
        case XED_OPERAND_RELBR:
          continue;
        default:
          std::cerr << "error: invalid operand name: " << xed_operand_enum_t2str(operand(i).name());
          std::cerr << " (" << unsigned(operand(i).name()) << ")" << std::endl;
          abort();
      }
    }
    return false;
  }

  bool operator==(XED_Instruction that) const { return _inst == that._inst; }
  bool operator!=(XED_Instruction that) const { return _inst != that._inst; }
  bool operator<(XED_Instruction that) const { return _inst < that._inst; }

  /// @name raw XED API access
  /// @{

  const xed_inst_t *raw() const { return _inst; }

  /// @}
private:
  const xed_inst_t *_inst;
};

/// A conrete encodable x86 instruction operand.
struct XED_Decoded_Instruction_Operand {
  XED_Decoded_Instruction_Operand(const xed_decoded_inst_t *inst, unsigned idx) : _inst{inst}, _idx{idx} {}

  /// @name observers
  /// @{

  unsigned idx() const { return _idx; }

  const XED_Instruction_Operand static_op() const { return raw(); }

  xed_operand_enum_t name() const { return static_op().name(); }

  const char *name_str() const { return static_op().name_str(); }

  xed_operand_type_enum_t type() const { return static_op().type(); }

  const char *type_str() const { return static_op().type_str(); }

  xed_operand_element_xtype_enum_t xtype() const { return static_op().xtype(); }

  const char *xtype_str() const { return static_op().xtype_str(); }

  xed_operand_visibility_enum_t visibility() const { return static_op().visibility(); }

  const char *visibility_str() const { return static_op().visibility_str(); }

  xed_operand_action_enum_t action() const {
    return xed_decoded_inst_operand_action(_inst, idx());
  }

  const char *action_str() const {
    return xed_operand_action_enum_t2str(action());
  }

  xed_operand_element_xtype_enum_t element_xtype() const {
    return static_op().xtype();
  }

  xed_operand_element_type_enum_t element_type() const {
    return xed_decoded_inst_operand_element_type(_inst, idx());
  }

  size_t element_bits() const {
    return xed_decoded_inst_operand_element_size_bits(_inst, idx());
  }

  size_t num_elements() const {
    return xed_decoded_inst_operand_elements(_inst, idx());
  }

  size_t width_in_bits() const {
    return element_bits() * num_elements();
  }

  /// @}

  /// @name operand value
  /// @{

  bool is_mem0() const {
    return name() == XED_OPERAND_MEM0;
  }

  bool is_base0() const {
    return name() == XED_OPERAND_BASE0;
  }

  unsigned memory_operand_index() const {
    return static_op().memory_operand_index();
  }

  size_t memory_operand_bits() const {
    return xed_decoded_inst_get_memory_operand_length(_inst, memory_operand_index()) * 8;
  }

  size_t memory_address_bits() const {
    return xed_decoded_inst_get_memop_address_width(_inst, idx());
  }

  xed_nonterminal_enum_t nonterminal() const {
    assert(type() == XED_OPERAND_TYPE_NT_LOOKUP_FN);
    return xed_operand_nonterminal_name(raw());
  }

  const char *nonterminal_str() const {
    return xed_nonterminal_enum_t2str(nonterminal());
  }

  xed_reg_enum_t reg() const {
    switch (type()) {
      case XED_OPERAND_TYPE_NT_LOOKUP_FN:
        return xed_decoded_inst_get_reg(_inst, name());
      case XED_OPERAND_TYPE_REG:
        return xed_operand_reg(raw());
      default:
        abort();
    }
  }

  const char *reg_str() const {
    return xed_reg_enum_t2str(reg());
  }

  uint32_t imm_const() const {
    assert(type() == XED_OPERAND_TYPE_IMM_CONST);
    return xed_operand_imm(raw());
  }

  /// @}

  /// @name raw XED API access
  /// @{

  const xed_operand_t *raw() const { return xed_inst_operand(xed_decoded_inst_inst(_inst), _idx); }

  /// @}
private:
  const xed_decoded_inst_t *_inst;
  unsigned _idx;
};

/// A conrete encodable x86 instruction (xed_decoded_inst_t)
struct XED_Decoded_Instruction {
  XED_Decoded_Instruction() {
    xed_decoded_inst_zero(&_inst);
  }

  XED_Decoded_Instruction(xed_state_t cpu_mode) {
    xed_decoded_inst_zero_set_mode(&_inst, &cpu_mode);
  }

  XED_Decoded_Instruction(const XED_Decoded_Instruction&) = default;

  /// @name observers
  /// @{

  XED_Instruction inst() const { return XED_Instruction{xed_decoded_inst_inst(raw())}; }

  unsigned get_machine_mode_bits() const {
    return xed_decoded_inst_get_machine_mode_bits(raw());
  }

  unsigned get_stack_address_bits() const {
    return xed_decoded_inst_get_stack_address_mode_bits(raw());
  }

  Effective_Operand_Width effective_operand_width() const {
    const xed_operand_values_t *const vals = xed_decoded_inst_operands_const(&_inst);
    unsigned width = xed_operand_values_get_effective_operand_width(vals);
    assert((width == 16) || (width == 32) || (width == 64));
    return Effective_Operand_Width(width);
  }

  Effective_Address_Width effective_address_width() const {
    const xed_operand_values_t *const vals = xed_decoded_inst_operands_const(&_inst);
    unsigned width = xed_operand_values_get_effective_address_width(vals);
    assert((width == 16) || (width == 32) || (width == 64));
    return Effective_Address_Width(width);
  }

  bool is_nop() const {
    const xed_operand_values_t *const values = xed_decoded_inst_operands_const(&_inst);
    return xed_operand_values_is_nop(values);
  }

  std::string asm_str(xed_syntax_enum_t syntax, uint64_t runtime_instruction_address = 0x0) const {
    const size_t buffer_len = 1024;
    char buffer[buffer_len];

    xed_format_context(syntax,
                       raw(),
                       buffer,
                       buffer_len,
                       runtime_instruction_address,
                       nullptr, nullptr);

    return buffer;
  }


  /// length of instruction in bytes
  size_t get_length() const { return xed_decoded_inst_get_length(&_inst); }

  /// bytes of machine code of the encoded instruction
  std::string bytes() const {
    const unsigned nbytes = get_length();
    std::string out;
    out.resize(nbytes);

    for (unsigned i = 0; i > nbytes; i++) {
      out[i] = xed_decoded_inst_get_byte(raw(), i);
    }

    return out;
  }

  /// @}

  /// @name operands
  /// @{

  unsigned num_operands() const {
    return xed_decoded_inst_noperands(&_inst);
  }

  XED_Decoded_Instruction_Operand operand(unsigned idx) const {
    assert(idx < num_operands());
    return XED_Decoded_Instruction_Operand{&_inst, idx};
  }

  unsigned num_memory_operands() const {
    return xed_decoded_inst_number_of_memory_operands(&_inst);
  }

  std::optional<xed_reg_enum_t> get_base_reg(unsigned memory_operand_idx) const {
    assert(memory_operand_idx < num_memory_operands());

    const xed_reg_enum_t reg = xed_decoded_inst_get_base_reg(&_inst, memory_operand_idx);

    if (reg == XED_REG_INVALID) {
      return std::nullopt;
    } else {
      return reg;
    }
  }

  /// @}

  /// @name raw XED API access
  /// @{

  xed_decoded_inst_t *raw() { return &_inst; }

  const xed_decoded_inst_t *raw() const { return &_inst; }

  /// @}
private:
  xed_decoded_inst_t _inst;
};

/// Helper for iterating over all XED machine modes
struct XED_Machine_Mode_Iterator {
  static XED_Machine_Mode_Iterator begin() { return XED_MACHINE_MODE_LONG_64; }
  static XED_Machine_Mode_Iterator end() { return XED_MACHINE_MODE_LAST; }
  static Iterator_Range<XED_Machine_Mode_Iterator> all() { return Iterator_Range{begin(), end()}; }

  XED_Machine_Mode_Iterator(xed_machine_mode_enum_t mode) : mode{mode} {
    assert(mode > XED_MACHINE_MODE_INVALID);
    assert(mode <= XED_MACHINE_MODE_LAST);
  }

  bool operator==(XED_Machine_Mode_Iterator that) const { return mode == that.mode; }
  bool operator!=(XED_Machine_Mode_Iterator that) const { return mode != that.mode; }

  void operator++() {
    assert(mode < XED_MACHINE_MODE_LAST);
    mode = xed_machine_mode_enum_t(int(mode) + 1);
  }

  xed_machine_mode_enum_t operator*() const { return mode; }
private:
  xed_machine_mode_enum_t mode;
};

/// Helper for iterating over all XED address widths
struct XED_Address_Width_Iterator {
  static XED_Address_Width_Iterator begin() { return XED_ADDRESS_WIDTH_16b; }
  static XED_Address_Width_Iterator end() { return XED_ADDRESS_WIDTH_LAST; }
  static Iterator_Range<XED_Address_Width_Iterator> all() { return Iterator_Range{begin(), end()}; }

  XED_Address_Width_Iterator(xed_address_width_enum_t width) : width{width} {
    assert(width > XED_ADDRESS_WIDTH_INVALID);
    assert(width <= XED_ADDRESS_WIDTH_LAST);
  }

  bool operator==(XED_Address_Width_Iterator that) const { return width == that.width; }
  bool operator!=(XED_Address_Width_Iterator that) const { return width != that.width; }

  void operator++() {
    assert(width > XED_ADDRESS_WIDTH_INVALID);
    assert(width < XED_ADDRESS_WIDTH_LAST);

    switch (width) {
      case XED_ADDRESS_WIDTH_16b:
        width = XED_ADDRESS_WIDTH_32b;
        break;
      case XED_ADDRESS_WIDTH_32b:
        width = XED_ADDRESS_WIDTH_64b;
        break;
      case XED_ADDRESS_WIDTH_64b:
        width = XED_ADDRESS_WIDTH_LAST;
        break;
      default:
        abort();
    }
  }

  xed_address_width_enum_t operator*() const { return width ; }
private:
  xed_address_width_enum_t width;
};

/// Information about x86 registers.
struct XED_Register_Info {
  static std::optional<xed_reg_enum_t> reg(xed_nonterminal_enum_t nonterminal, const XED_Encoder_State &state);

  static std::optional<Register_Class> reg_class(xed_nonterminal_enum_t nonterminal, const XED_Encoder_State &state);

  /// Register useable as base in a memory reference for the given address size.
  static xed_reg_enum_t base_reg(const XED_Encoder_State &state);

  /// Is the register a 8-bit GPR register only encodable without a rex prefix?
  static bool is_GPR8norex(xed_reg_enum_t reg) {
    return (XED_REG_GPR8h_FIRST <= reg) && (reg <= XED_REG_GPR8h_LAST);
  }

  /// Is the register a 16-bit GPR register?
  static bool is_GPR16(xed_reg_enum_t reg) {
    return (XED_REG_GPR16_FIRST <= reg) && (reg <= XED_REG_GPR16_LAST);
  }

  /// Is the register a 32-bit GPR register?
  static bool is_GPR32(xed_reg_enum_t reg) {
    return (XED_REG_GPR32_FIRST <= reg) && (reg <= XED_REG_GPR32_LAST);
  }

  /// Is the register a 64-bit GPR register?
  static bool is_GPR64(xed_reg_enum_t reg) {
    return (XED_REG_GPR64_FIRST <= reg) && (reg <= XED_REG_GPR64_LAST);
  }
};
