cmake_minimum_required(VERSION 3.15.1)

project(extract-xed-instruction-database)

include(ExternalProject)

find_package(OpenMP)
find_package(Python3 REQUIRED)

set(WARNINGS_FLAGS -Wall -Wextra -Werror)
#set(CMAKE_C_COMPILER clang)
#set(CMAKE_CXX_COMPILER clang)

set(XED_SRC         "${CMAKE_SOURCE_DIR}/xed")
set(XED_INCLUDE_DIR "${XED_SRC}/obj/wkit/include")
set(XED_LIBRARY     "${XED_SRC}/obj/libxed.a")
ExternalProject_Add(xed-build
  SOURCE_DIR        "${XED_SRC}"
  BINARY_DIR        "${XED_SRC}"
  INSTALL_DIR       "${XED_SRC}"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND     "${Python3_EXECUTABLE}" mfile.py
  INSTALL_COMMAND   ""
)
add_library(xed STATIC IMPORTED)
# Avoid CMake complaining about non-existent include dir
file(MAKE_DIRECTORY "${XED_INCLUDE_DIR}")
set_target_properties(xed PROPERTIES IMPORTED_LOCATION "${XED_LIBRARY}")
target_include_directories(xed INTERFACE "${XED_INCLUDE_DIR}")

add_executable(extract-xed-instruction-database
  extract-xed-instruction-database.cpp
  XED_Info.cpp XED_Info.hpp
  Pipedream_Info.cpp Pipedream_Info.hpp
  utils.cpp utils.hpp
  XED_DB_Extraction_Utils.cpp
)
add_dependencies(extract-xed-instruction-database xed-build)
target_link_libraries(extract-xed-instruction-database PRIVATE xed)
target_compile_options(extract-xed-instruction-database PRIVATE ${WARNINGS_FLAGS})

## set compiler mode to standard C++17
target_compile_features(extract-xed-instruction-database PUBLIC cxx_std_17)
set_target_properties(extract-xed-instruction-database PROPERTIES CXX_EXTENSIONS OFF)

## speed up with OpenMP if possible
if(OpenMP_CXX_FOUND)
  target_link_libraries(extract-xed-instruction-database PUBLIC OpenMP::OpenMP_CXX)
endif()

install(TARGETS extract-xed-instruction-database RUNTIME DESTINATION bin)
