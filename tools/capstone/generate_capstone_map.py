#!/usr/bin/env python3

__doc__ = """
  Example showing how to map list of instructions to capstone disassembly
"""

import sys
from pathlib import Path
from collections import defaultdict
from statistics import mean
from types import SimpleNamespace as ns
import argparse
import random
import tempfile
import subprocess
import logging

# Force importing the local version (outside of venv): avoid bootstrapping problems
sys.path.append(str(Path(__file__).parent.parent.parent / "src"))

from pipedream.disasm.reader import ObjReader
from pipedream.disasm.capstone.disassembler import Disassembler
from pipedream.disasm.capstone.capstone_map_alias_armv8a import (
    map as capstone_map_alias_armv8a, )
from pipedream.disasm.capstone.utils import cs_insn_class
import pipedream.benchmark as pipebench
import pipedream.asm.ir as pipeasm
import pipedream.asm.allocator as pipealloc

logging.basicConfig()
logger = logging.getLogger(__name__)


def generate_asm(args, io):
    rand = random.Random(0)

    arch = pipeasm.Architecture.for_name(args.arch)
    inst_set = arch.instruction_set()
    insts = [inst for inst in inst_set if args.all or inst.can_benchmark]

    func_name = "test"
    if args.dialect is None:
        dialect = arch.asm_dialects()[0]
    else:
        dialect = [d for d in arch.asm_dialects() if d.name == args.dialect][0]
    asm_writer = arch.make_asm_writer(dialect, io)

    regs = arch.register_set()
    all_regs = iter(regs.all_registers())
    allocator = pipealloc.Punctual_Register_Allocator(all_regs, random=rand)
    irb = arch.make_ir_builder()
    allocated_insts = allocator.allocate(insts, irb)

    asm_writer.begin_file(io.name)
    asm_writer.begin_function(func_name)
    for inst, allocated in zip(insts, allocated_insts):
        if not args.no_comment:
            io.write(f"# {repr(inst)}\n")
            io.write(f"# {repr(allocated)}\n")
        asm_writer.insts([allocated])
    asm_writer.end_function(func_name)
    asm_writer.end_file(io.name)
    return allocated_insts


def assemble_file(args, s_name, o_name):
    arch = pipeasm.Architecture.for_name(args.arch)
    p = subprocess.run([arch.tools_config.as_cmd, "-o", o_name, s_name],
                       text=True)
    return p.returncode


def disassemble_file(args, o_name):
    arch = pipeasm.Architecture.for_name(args.arch)
    with open(o_name, "rb") as f:
        func = ObjReader(f).get_function("test")
    disassembled = Disassembler(arch).get_binary_dis(func.binary, skip=True)
    return disassembled


def print_map(args, disas_map, io):
    if args.format == "yaml":
        for cap, pipes in disas_map.items():
            io.write(f"{cap}: [ {', '.join([p.name for p in pipes])} ]\n")
    else:  # python
        io.write(f"map = dict(\n")
        for cap, pipes in disas_map.items():
            io.write(f"    {cap}={[p.name for p in pipes]},\n")
        io.write(f")\n")


def main():
    parser = argparse.ArgumentParser(
        description="map architecture instructions")
    parser.add_argument("--output",
                        default="-",
                        type=str,
                        help="Output file. Default to stdout")
    parser.add_argument(
        "--format",
        default="yaml",
        type=str,
        help="Format of output, one of: yaml, python. Default to yaml",
    )
    parser.add_argument("--arch",
                        required=True,
                        type=str,
                        help="Architecture name: x86, ARMv8a,...")
    parser.add_argument(
        "--dialect",
        type=str,
        help="Optional assembler dialect, for x86: att or intel")
    parser.add_argument("--all",
                        action="store_true",
                        help="Do not filter instruction lists")
    parser.add_argument(
        "--no-comment",
        action="store_true",
        help="Do not emit comments in generated file",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Debug mode, generate tempfile locally with --validate",
    )
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)

    if args.dialect is None:
        arch = pipeasm.Architecture.for_name(args.arch)
        args.dialect = arch.asm_dialects(
        )[0].name  # take first dialect as default

    if args.debug:
        options = dict(
            dir=".",
            prefix=f"list-instructions-{args.arch}-{args.dialect}-",
        )
    else:
        options = {}
    try:
        f_name, o_name = "", ""
        with tempfile.NamedTemporaryFile(mode="w",
                                         suffix=".s",
                                         delete=False,
                                         **options) as f:
            f_name = f.name
            o_name = str(Path(f.name).with_suffix(".o"))
            logger.debug(f"Generating asm file: {f.name}")
            allocated = generate_asm(args, f)

        logger.debug(f"Assembling asm file: {f_name} -> {o_name}")
        ret = assemble_file(args, f_name, o_name)
        if ret != 0:
            logger.error(f"can't assemble file {f_name}")
            sys.exit(1)
        logger.debug(f"Disassembling asm file: {o_name}")
        disassembled = disassemble_file(args, o_name)
        if len(disassembled) != len(allocated):
            logger.error(f"can't disassemble file {o_name}")
            sys.exit(1)
        logger.debug(
            f"Disassembled: {len(disassembled)} instructions, {len([x for x in disassembled if x.cs_insn is None])} skipped"
        )
    finally:
        if not args.debug:
            if f_name:
                Path(f_name).unlink(missing_ok=True)
            if o_name:
                Path(o_name).unlink(missing_ok=True)
    alias_map = capstone_map_alias_armv8a if args.arch == "ARMv8a" else {}
    reverse_alias_map = {v: k for k, v in alias_map.items()}
    disas_map = defaultdict(list)
    alias_set = set()
    duplicates = defaultdict(list)
    for pipe, dis in zip(allocated, disassembled):
        # Do not include duplicates which are treated by map alias
        if pipe.name in reverse_alias_map:
            disas_map[reverse_alias_map[pipe.name]].append(pipe)
            duplicates[reverse_alias_map[pipe.name]].append(dis)
        elif dis.insn_class:
            if dis.binary not in alias_set:
                disas_map[dis.insn_class].append(pipe)
                duplicates[dis.insn_class].append(dis)
                alias_set.add(dis.binary)
    duplicates = {k: v for k, v in duplicates.items() if len(v) > 1}
    map_len = len(disas_map)
    max_len = max([len(v) for v in disas_map.values()])
    n_matched = sum([len(v) == 1 for v in disas_map.values()])
    logger.debug(
        f"Disassembly map: len {map_len}, matched {n_matched} ({n_matched/map_len*100:.2f}%)"
    )
    if max_len > 1:
        mean_len = mean([len(v) for v in disas_map.values() if len(v) > 1])
        logger.warning(
            f"Found {map_len - n_matched} entries with duplicates, mean len: {mean_len}, max len: {max_len}"
        )
        for ident, dups in duplicates.items():
            bins = [cap.binary for cap in dups]
            disas = [cap.disasm for cap in dups]
            pipes = [pipe.name for pipe in disas_map[ident]]
            logger.warning(
                f"  {ident}: {', '.join([f'{pipe} ({dis} [{bin}])' for pipe, dis, bin in zip(pipes, disas, bins)])}"
            )
    # Verify that map was correctly constructed
    for dis, pipe in zip(disassembled, allocated):
        if dis.cs_insn:
            cs_id = cs_insn_class(dis.cs_insn)
            m_pipe = disas_map.get(cs_id, [ns(name="")])[0]
            assert (
                m_pipe.name
            ), f"failed  verification for {dis.disasm} {pipe.name}: ({cs_id} -> {m_pipe.name}): mapping not found"

    if args.output != "-":
        with open(args.output, "w") as f:
            print_map(args, disas_map, f)
    else:
        print_map(args, disas_map, sys.stdout)
    return 0


if __name__ == "__main__":
    sys.exit(main())
